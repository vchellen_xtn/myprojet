<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new GM\SiteBundle\GMSiteBundle(),
            new FOS\UserBundle\FOSUserBundle(),
            new Knp\Bundle\SnappyBundle\KnpSnappyBundle(),
            new EE\DataExporterBundle\EEDataExporterBundle(),
            //new FR3D\LdapBundle\FR3DLdapBundle(),
            // new IMAG\LdapBundle\IMAGLdapBundle(),
            // Allouet alpha
            new GM\UserBundle\GMUserBundle(),
            new GM\MediaBundle\GMMediaBundle(),
            new GM\MailBundle\GMMailBundle(),
            new GM\XeonBundle\GMXeonBundle(),
            new GM\WidgetBundle\GMWidgetBundle(),
            new GM\CacheBundle\GMCacheBundle(),
            new GM\StatBundle\GMStatBundle(),
            new GM\GeoBundle\GMGeoBundle(),
            // Totem
            new Totem\SiteBundle\TotemSiteBundle(),
            new Totem\RequestBundle\TotemRequestBundle(),
            new Totem\CronBundle\TotemCronBundle(),
            new Totem\XeonBundle\TotemXeonBundle(),
            new Totem\MediaBundle\TotemMediaBundle(),
            new Totem\ExportBundle\TotemExportBundle(),
            new Totem\WinIdenBundle\TotemWinIdenBundle(),
            new Totem\FrontBundle\TotemFrontBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
            $bundles[] = new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }
}
