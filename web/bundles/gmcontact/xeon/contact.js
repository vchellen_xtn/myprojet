
//
// Interface contact v1.0.0
// Auteur : Thierry Marianne
//

Xeon.module.contact =
{
	allowDrop: function(tree, node, target) {
		return true;
	},

	getAction: function(v)
	{
		if (v == 0) return lang.xeon.mod_contact.state_pending;
		if (v == 1) return lang.xeon.mod_contact.state_processed;
		if (v == 2) return lang.xeon.mod_contact.state_archived;
		else return false;
	},
	
	makeContactRoot: function(ct, item, data)
	{
		var contact_grid = new Ext.ux.grid.GridPanel({
			title: lang.xeon.mod_contact.list_ind,
			iconCls: 'i-vcard',
			itemId: 'contact_grid',
			itype: 'contact.cnt',
			reloadAfterEdit: true,
			rowId: 'id',
			paging: true,
			quickSearch: true,
			reloadAfteEdit: true,
			exportPath: true,
			search: [{
					fieldLabel: lang.xeon.firstname,
					name: 'first_name'
				},{
					fieldLabel: lang.xeon.lastname,
					name: 'last_name'
				},{
					fieldLabel: lang.xeon.email,
					name: 'email'
			}],
			columns: [{
					header: lang.xeon.date,
					dataIndex: 'date',
					width: 93
				},{
					header: lang.xeon.firstname,
					dataIndex: 'first_name',
					itemLink: ['contact.ind', 'id']
				},{
					header: lang.xeon.lastname,
					dataIndex: 'last_name',
					itemLink: ['contact.ind', 'id']
				},{
					header: lang.xeon.mod_contact.email,
					dataIndex: 'email',
					width: 150,
					renderer: Xeon.renderEmail
				},{
					header: lang.xeon.mod_contact.com,
					dataIndex: 'message',
					width: 200
				},{
	                header: lang.xeon.mod_contact.state,
	                dataIndex: 'status',
	                width: 80,
	                //allowBlank: false,
	                renderer: function(v){
	                    for(i=0; i<data.status_list.length; i++)
	                    {
	                        if(data.status_list[i][0] == v)
	                            return data.status_list[i][1];
	                    }
	                },
	                editor: new Ext.ux.form.ComboBox({
	                    data: data.status_list
	                })
            }]
        });
/*{
					header: lang.xeon.mod_contact.state,
					width: 80,
					dataIndex: 'status',
					renderer: this.getAction,
					editor: new Ext.ux.form.setAction(),
					align: 'left'
			}]
		}*/

		ct.add(contact_grid);
		
		
		var email_grid = new Ext.ux.grid.GridPanel({
			title: lang.xeon.mod_contact.list_email,
			iconCls: 'i-vcard',
			itemId: 'email_grid',
			itype: 'contact.cne',
			reloadAfterEdit: true,
			rowId: 'id',
			paging: true,
			quickSearch: false,
			reloadAfteEdit: true,
			exportPath: false,
			search: false,
			columns: [{
					header: lang.xeon.mod_contact.email,
					dataIndex: 'email',
					width: 150,					
					editor: new Ext.form.TextField({
                        allowBlank: false
                    })
				}]
		});

		ct.add(email_grid);
	},
		
	makeContactForm: function(ct, item, data)
	{
		if (!item.add)
		{
			var thisGrid = new Ext.ux.grid.GridPanel({
				title: lang.xeon.mod_contact.list_ind,
				exportPath: true,
				iconCls: 'i-vcard',
				itype: 'contact.cnt',
				height: 450,
				paging: true,
				quickSearch: true,
				reloadAfteEdit: true,
				rowId: 'id',
				search: [{
					fieldLabel: lang.xeon.firstname,
					name: 'first_name'
				},{
					fieldLabel: lang.xeon.lastname,
					name: 'last_name'
				},{
					fieldLabel: lang.xeon.email,
					name: 'email'
				}],
				columns: [{
						header: lang.xeon.firstname,
						dataIndex: 'first_name',
						itemLink: ['contact.ind', 'id']
					},{
						header: lang.xeon.lastname,
						dataIndex: 'last_name',
						itemLink: ['contact.ind', 'id']
					},{
						header: lang.xeon.mod_contact.email,
						dataIndex: 'email',
						width: 150,
						renderer: Xeon.renderEmail
					},{
						header: lang.xeon.mod_contact.com,
						dataIndex: 'message',
						width: 150
					},{
						header: 'Date',
						dataIndex: 'date',
						width: 115
					},{
	                header: lang.xeon.mod_contact.state,
	                dataIndex: 'status',
	                width: 80,
	                //allowBlank: false,
	                renderer: function(v){
	                    for(i=0; i<data.status_list.length; i++)
	                    {
	                        if(data.status_list[i][0] == v)
	                            return data.status_list[i][1];
	                    }
	                },
	                editor: new Ext.ux.form.ComboBox({
	                    data: data.status_list
	                })
            }]
        });
			
			ct.add(thisGrid);
		}
		else
		{
			var items = [];
			items.push({
				xtype: 'listbox',
                fieldLabel: lang.xeon.mod_contact.title,               
                itype: 'contact.title',
                name: 'title',
				link:  data.db.title,
                value: data.db.title,
                displayValue: data.db.title_title
			},{
				fieldLabel: lang.xeon.mod_contact.first_name,
				allowBlank: false,
				name: 'first_name'
			},{
				fieldLabel: lang.xeon.mod_contact.last_name,
				allowBlank: false,
				name: 'last_name'
			},{
				fieldLabel: lang.xeon.email,
				allowBlank: false,
				name: 'email'
			},{
				fieldLabel: lang.xeon.mod_contact.address,
				allowBlank: true,
				name: 'address'
			},{
				fieldLabel: lang.xeon.mod_contact.postal_code,
				allowBlank: true,
				name: 'zip_code'
			},{
				fieldLabel: lang.xeon.mod_contact.city,
				allowBlank: true,
				name: 'city'
			},{
				fieldLabel: lang.xeon.mod_contact.phone,
				allowBlank: true,
				name: 'phone'
			},{
				fieldLabel: lang.xeon.mod_contact.fax,
				allowBlank: true,
				name: 'fax'
			},{
				fieldLabel: lang.xeon.mod_contact.firm,
				name: 'company'
			},{
				xtype: 'textarea',
				fieldLabel: lang.xeon.mod_contact.com,
				name: 'message',
				height: 200
			});

			ct.add(new Ext.ux.FormPanel({
				iconCls: 'i-user',
				items: items
			}));
		}
	},

	makeIndividualForm: function(ct, item, data)
	{
		var items = [];
		items.push({
				xtype: 'listbox',
                fieldLabel: lang.xeon.mod_contact.title,               
                itype: 'contact.title',
                name: 'title',
				link:  data.db.title,
                value: data.db.title,
                displayValue: data.db.title_title
			},{
				fieldLabel: lang.xeon.mod_contact.first_name,
				allowBlank: false,
				name: 'first_name',
				value: data.db.first_name
			},{
				fieldLabel: lang.xeon.mod_contact.last_name,
				allowBlank: false,
				name: 'last_name',
				value: data.db.last_name
			},{
				fieldLabel: lang.xeon.email,
				allowBlank: false,
				name: 'email',
				value: data.db.email
			},{
				fieldLabel: lang.xeon.mod_contact.address,
				allowBlank: true,
				name: 'address',
				value: data.db.address
			},{
				fieldLabel: lang.xeon.mod_contact.address2,
				allowBlank: true,
				name: 'address2',
				value: data.db.address2
			},{
				fieldLabel: lang.xeon.mod_contact.address2,
				allowBlank: true,
				name: 'address3',
				value: data.db.address3
			},{
				fieldLabel: lang.xeon.mod_contact.postal_code,
				allowBlank: true,
				name: 'zip_code',
				value: data.db.zip_code
			},{
				fieldLabel: lang.xeon.mod_contact.city,
				allowBlank: true,
				name: 'city',
				value: data.db.city
			},{
				fieldLabel: lang.xeon.mod_contact.phone,
				allowBlank: true,
				name: 'phone',
				value: data.db.phone
			}/*,{
				fieldLabel: lang.xeon.mod_contact.fax,
				allowBlank: true,
				name: 'fax',
				value: data.db.fax
			}*/,{
				fieldLabel: lang.xeon.mod_contact.subject,
				allowBlank: true,
				name: 'object',
				value: data.db.object
			},{
				xtype: 'textarea',
				fieldLabel: lang.xeon.mod_contact.com,
				name: 'message',
				height: 200,
				value: data.db.message
			},{
				xtype: 'listbox',
                fieldLabel: lang.xeon.mod_contact.state,               
                itype: 'contact.status',
                name: 'status',
				link:  data.db.status,
                value: data.db.status,
                displayValue: data.db.status_title
			});
		
			ct.add(new Ext.ux.FormPanel({
				title: lang.xeon.mod_contact.block_ind,
				iconCls: 'i-user',
				items: items
		}));
	},

	makeEmailForm: function(ct, item, data){
		 var items = [{
            fieldLabel: lang.xeon.email,
            name: 'email',
            allowBlank: false,
            value: data.db.email
        }];

         ct.add(new Ext.ux.FormPanel({
                items: items
            }));
	}
}

Ext.ux.form.setAction = Ext.extend(Ext.ux.form.ComboBox, {
	data: [
		[0, lang.xeon.mod_contact.state_pending],
		[1, lang.xeon.mod_contact.state_processed],
		[2, lang.xeon.mod_contact.state_archived]
	]
});