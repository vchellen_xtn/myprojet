
//
// Interface media v2.1.10
// Auteur : Sylvain Durozard
//

Xeon.module.media =
{
	allowDrop: function(tree, node, target) {
		return true;
	},

	makeFolderForm: function(ct, item, data)
	{
		var items = [];
		
		if (item.add) {
			items.push({
				xtype: 'listbox',
				fieldLabel: lang.xeon.parent,
				allowBlank: false,
				itype: data.itype,
				name: 'parent',
				value: data.db.parent,
				displayValue: data.db.parent_title
			});
		}
		
		items.push({
			fieldLabel: lang.xeon.title,
			allowBlank: false,
			name: 'title',
			value: data.db.title
		});

		if (item.add)
		{
			ct.add(new Ext.ux.FormPanel({
				items: items
			}));
		}
		else
		{
			ct.add(new Ext.ux.FormPanel({
				title: lang.xeon.mod_media.folder,
				iconCls: ct.iconCls,
				items: items
			}));

			ct.add(new Ext.ux.InfoPanel({
				title: lang.xeon.info,
				iconCls: 'i-information',
				items: [{
					infoLabel: lang.xeon.make_date,
					value: data.info.make
				}]
			}));

			ct.add(new Ext.ux.MediaPanel({
				title: lang.xeon.mod_media.file_list,
				iconCls: 'i-page-white-stack',
				mode: 'folder',
				height: 508
			}));
		}
	},

	makeMediaForm: function(ct, item, data)
	{
		var items = [{
			xtype: 'listbox',
			fieldLabel: lang.xeon.mod_media.folder,
			allowBlank: false,
			itype: data.itype,
			name: 'folder',
			value: data.db.folder,
			displayValue: data.db.folder_title
		},{
			fieldLabel: lang.xeon.title,
			allowBlank: false,
			name: 'title',
			value: data.db.title
		},{
			fieldLabel: lang.xeon.mod_media.caption,
			name: 'caption',
			value: data.db.caption
		},{
			fieldLabel: lang.xeon.mod_media.link,
			// vtype: 'url',
			name: 'link',
			value: data.db.link
		},{
			xtype: 'combobox',
			fieldLabel: lang.xeon.mod_media.link_target,
			data: [[0, lang.xeon.mod_media.target_self], [1, lang.xeon.mod_media.target_blank]],
			name: 'link_target',
			value: data.db.link_target
		},{
			xtype: 'textarea',
			fieldLabel: lang.xeon.mod_media.description,
			name: 'description',
			value: data.db.description
		}];

		if (!item.add)
		{
			ct.add(new Ext.ux.FormPanel({
				title: lang.xeon.mod_media.file,
				iconCls: ct.iconCls,
				items: items
			}));
			
			var info = [{
				infoLabel: lang.xeon.make_date,
				value: data.info.make
			},{
				infoLabel: lang.xeon.mod_media.type,
				value: data.info.type
			}];
			
			
			
			if (data.info.size)
			{
				info.push({
					infoLabel: lang.xeon.mod_media.file_size,
					value: data.info.size
				});
			}
			
			if (data.info.dimension)
			{
				info.push({
					infoLabel: lang.xeon.mod_media.dimension,
					value: data.info.dimension
				});
			}
			
			
			if(data.info.cms.length>0){
				var t='';
				Ext.each(data.info.cms,function(e){
					t=t+'<a class="x-link" onclick="Xeon.loadItem(\'cms.pge.'+e.pge_id+'\');">'+e.pge_title+'</a><br />';
				});
			
			}else{
				var t=lang.xeon.mod_media.no_cms_linked;
			}
			info.push({
				infoLabel: lang.xeon.mod_media.cms_linked,
				value: t
			});
			
			if(data.info.prd.length>0){
				var t='';
				Ext.each(data.info.prd,function(e){
					t=t+'<a class="x-link" onclick="Xeon.loadItem(\'shop.prd.'+e.prd_id+'\');">'+e.prd_title+'</a><br />';
				});
			
			}else{
				var t=lang.xeon.mod_media.no_product_linked;
			}
			info.push({
					infoLabel: lang.xeon.mod_media.product_linked,
					value: t
				});
			
			ct.add(new Ext.ux.InfoPanel({
				title: lang.xeon.info,
				iconCls: 'i-information',
				items: info
			}));
			
			ct.add(new Ext.Panel({
				cls: 'x-layoutpanel',
				title: lang.xeon.mod_media.preview,
				iconCls: 'i-image',
				bodyStyle: {
					padding: '10px',
					background: '#ffffff'
				},
				html: data.html
			}));
		}
	},
	
	makeModuleForm: function(ct, item, data)
	{
		ct.add(new Ext.ux.FormPanel({
			title: lang.xeon.settings,
			iconCls: 'i-cog',
			items: [{
				fieldLabel: lang.xeon.base_url,
				name: 'base_url',
				value: data.cfg.base_url
			},{
				fieldLabel: lang.xeon.mod_media.file_dir,
				name: 'xeon.file_dir',
				value: data.cfg.xeon.file_dir
			},{
				fieldLabel: lang.media_dimension,
				name: 'image_size',
				value: data.cfg.image_size
			}]
		}));
		
		// ct.add(new Ext.ux.FormPanel({
		// 	title: lang.xeon.options,
		// 	iconCls: 'i-brick',
		// 	items: [{
		// 		xtype: 'togglebox',
		// 		fieldLabel: lang.xeon.mod_media.images,
		// 		name: 'plugin_image',
		// 		value: data.cfg.plugin_image
		// 	},{
		// 		xtype: 'togglebox',
		// 		fieldLabel: lang.xeon.mod_media.documents,
		// 		name: 'plugin_document',
		// 		value: data.cfg.plugin_document
		// 	},{
		// 		xtype: 'togglebox',
		// 		fieldLabel: lang.xeon.mod_media.sounds,
		// 		name: 'plugin_sound',
		// 		value: data.cfg.plugin_sound
		// 	},{
		// 		xtype: 'togglebox',
		// 		fieldLabel: lang.xeon.mod_media.videos,
		// 		name: 'plugin_video',
		// 		value: data.cfg.plugin_video
		// 	},{
		// 		xtype: 'togglebox',
		// 		fieldLabel: lang.xeon.mod_media.animations,
		// 		name: 'plugin_animation',
		// 		value: data.cfg.plugin_animation
		// 	}]
		// }));
	}
}

Ext.ux.MediaPanel = Ext.extend(Ext.Panel, {
	cls: 'x-mediapanel',
	iconCls: 'i-image',
	layout: 'fit',
	autoScroll: true,
	mode: 'browser',
	selectedItem: 0,
	selectedTitle: '',
	multiSelect: false,
	initComponent: function()
	{
        var p = this;

		p.items = p.view = new Ext.DataView({
        	autoWidth: true,
        	autoHeight: true,
        	singleSelect: true,
			multiSelect: p.multiSelect,
            itemSelector: 'div.item',
            overClass: 'x-view-over',
            emptyText: 'Aucun fichier', // TODO ?
            plugins: (p.mode != 'browser') ? new Ext.DataView.DragSelector() : false,
            tpl: new Ext.XTemplate(
				'<tpl for=".">',
					'<div class="item" title="{title}">',
						'<div style="background-image: url({icon});">',
							'<tpl if="nbLink == 0">',
								'<div class="no-link-media">&nbsp;</div>',
							'</tpl>',
						'</div>',
						'<p>{shortTitle}</p>',						
					'</div>',
				'</tpl>'
			),
			prepareData: function(data)
			{
                data.shortTitle = Ext.util.Format.ellipsis(data.title, 16);
                return data;
            }
        });

        p.view.on('render', function() {
        	p.view.store.load();
        });
        
        p.view.on('selectionchange', function(d, s)
        {
			//console.log(p.view.getSelectedRecords());
        	if (p.mode == 'browser')
        	{
        		var r = p.view.getSelectedRecords();
        
        		if (r.length != 0 && r[0].data.item)
        		{
	        		var item = Xeon.getId(r[0].data.item);
	        		p.selectedItem = item.id;
	        		p.selectedTitle = r[0].data.title;
	        	}
	        	else
	        	{
	        		p.selectedItem = 0;
	        		p.selectedTitle = '';
	        	}
	        
	        	if (p.hiddenField) {
	        		p.hiddenField.value = p.selectedItem;
	        	}
        	}
        	else {
        		p.topToolbar.items.get(2).setDisabled(s.length == 0);
        	}
        });
        
        p.view.on('click', function(d, i, n)
        {
			if (p.mode == 'browser')
			{
				var data = p.view.store.data.items[i].data;
				if (!data.isFile) p.loadItem(data.item);
			}
        });

		p.view.on('dblclick', function(d, i, n)
		{
			if (p.mode != 'browser')
			{
				var data = p.view.store.data.items[i].data;
				Xeon.loadItem(data.item);
			}
        });
        
        p.tbar = [{
			text: lang.xeon.mod_media.file_add,
			iconCls: 'i-image-add',
			handler: function()
			{
				var btn = new Ext.Panel({
					border: false,
					bodyStyle: {
						marginBottom: '10px',
						background: '#dfe8f6',
						fontSize: '12px'
					},
					html: '<span id="swfu_button"></span> <span id="swfu_status">'+lang.xeon.mod_media.upload_select+'</span>'
				});
				
				var bar = new Ext.ProgressBar();
				
				var win = new Ext.Window({
					title: lang.xeon.mod_media.file_add,
					iconCls: 'i-image-add',
					resizable: false,
					modal: true,
					border: false,
					width: 400,
					//height: 100,
					items: new Ext.Panel({
						frame: true,
						bodyStyle: {
							padding: '10px'
						},
						items: [btn, bar]
					})
				});
				
				win.on('show', function()
				{
					var total = 0;
					var current = 1;
                                      //  alert('lol');
					
					//SWFUpload.Console.writeLine = console.log;
					win.swfu = new SWFUpload({
						//debug: true,
						flash_url: '/bundles/gmmedia/xeon/swfupload.swf',
						upload_url: Ext.Ajax.url,
						
						file_types: p.upload.type,
						file_types_description: p.upload.desc,
						file_post_name: 'media',
						file_size_limit: p.size_limit+'B',
						
						button_width: '16',
						button_height: '16',
						button_placeholder_id: 'swfu_button',
						button_image_url: '/bundles/gmmedia/img/upload.png',
						button_cursor: SWFUpload.CURSOR.HAND,
						
						post_params: {
							cmd: 'media.saveMediaFile',
							folder: p.folder,
							PHPSESSID: Xeon.getCookie('PHPSESSID')
						},
						
						file_queued_handler: function(file)
						{
							total++;
							Ext.get('swfu_status').update(String.format(lang.xeon.mod_media.upload_status, current, total));
						},
						
						file_queue_error_handler: function(file, error, message)
						{
							Ext.Msg.alert(lang.xeon.error, file.name+' : '+message);
							// TODO : g�rer les erreurs
						},
						
						file_dialog_complete_handler: function()
						{
							var stats = win.swfu.getStats();
	
							if (stats.files_queued > 0 && stats.in_progress == 0) {
								win.swfu.startUpload();
							}
						},
						
						upload_progress_handler: function(file, loaded, total)
						{
							var progress = Math.ceil((loaded/total)*100);
							bar.updateProgress(progress/100, file.name+' ('+progress+'%)');
						},
						
						upload_success_handler: function(file, data)
						{
							if (win.swfu.getStats().files_queued > 0)
							{
								current++;
								Ext.get('swfu_status').update(String.format(lang.xeon.mod_media.upload_status, current, total));
								win.swfu.startUpload();
							}
							else
							{
								win.close();
								p.view.store.load();
							}
						}
					});
				});
				
				win.on('beforedestroy', function() {
					if (win.swfu) win.swfu.destroy();});
				
				win.show();
			}
		},{
			text: lang.xeon.mod_media.link_add,
			iconCls: 'i-link-add',
			handler: function()
			{
				new Ext.ux.Window({
					title: lang.xeon.mod_media.link_add,
					iconCls: 'i-link-add',
					items: new Ext.ux.FormPanel({
						params: {
							cmd: 'media.saveMediaLink',
							folder: p.folder
						},
						success: function() {
							p.view.store.load();
						},
						items: [{
							fieldLabel: lang.xeon.url,
							allowBlank: false,
							vtype: 'url',
							name: 'url'
						}]
					})
				}).show();
			}
		}];

		if (p.mode == 'browser')
		{
			p.tbar.push({
				text: lang.xeon.mod_media.parent_folder,
				iconCls: 'i-parent-folder',
				handler: function() {
					p.loadItem(p.parent);
				}
			});
		}
		else
		{
			p.tbar.push({
				text: lang.xeon.delete,
				iconCls: 'i-cross',
				disabled: true,
				handler: function()
				{
					var list = '';
					var count = p.view.getSelectionCount();
					
					if (count > 0)
					{
						var text = (count > 1) ? String.format(lang.xeon.delete_items, count) : lang.xeon.delete_item;
						
						Ext.Msg.confirm(lang.xeon.confirm, text, function(btn)
						{
							if (btn == 'yes')
							{
								Ext.each(p.view.getSelectedRecords(), function(r) {
									list += (list == '') ? r.data.item : '-'+Xeon.getId(r.data.item).id;
								});
								
								Ext.Ajax.request({
									params: {
										cmd: 'xeon.deleteItem',
										item: list,
										parent: p.ownerCt.itemId
									},
									success: function() {
										p.view.store.load();
									}
								});
							}
						});
					}
				}
			});
		}
		
		p.tbar.push({
			text: lang.xeon.reload,
			iconCls: 'i-refresh',
			handler: function() {
				p.view.store.load();
			}
		});
		
		if (p.alignName)
		{
			p.tbar.push('->',{
				xtype: 'combobox',
				width: 120,
				data: [[1, lang.xeon.mod_media.align_left], [2, lang.xeon.mod_media.align_right]],
				name: p.alignName,
				value: p.alignValue || 1
			});
		}

		Ext.ux.MediaPanel.superclass.initComponent.call(this);
	},

	onRender: function(ct, position)
	{
            
		var p = this;
		var Record = Ext.data.Record.create(['item', 'title', 'icon', 'isFile','fle_id','nbLink']);

		p.folder = (p.mode == 'browser') ? 'media.root.0' : p.ownerCt.itemId;
		p.file = (p.value) ? p.value : '0';

		p.view.store = new Ext.data.JsonStore({
			url: Ext.Ajax.url,
			root: 'data',
			fields: Record,
			baseParams: {
				cmd: 'media.makeMediaList',
				mode: p.mode,
				folder: p.folder,
				file: p.file
			}});

		p.view.store.on('beforeload', function() {
			p.bwrap.mask(lang.xeon.loading, 'x-mask-loading');
		});

		p.view.store.on('load', function(s)
		{
			p.bwrap.unmask();
			p.folder = s.reader.jsonData.folder;
			p.upload = s.reader.jsonData.upload;
			p.parent = s.reader.jsonData.parent;
			p.size_limit = s.reader.jsonData.size_limit;

			if (p.mode == 'browser')
			{
				p.topToolbar.items.get(0).setDisabled(!s.reader.jsonData.upload);
				p.topToolbar.items.get(2).setDisabled(!s.reader.jsonData.parent);
			}
			
			p.topToolbar.items.get(1).setDisabled(!s.reader.jsonData.link);

			if (p.file != '0')
			{
				s.each(function(r, i)
				{
					var item = Xeon.getId(r.data.item);

					if (r.data.isFile && item.id == p.file)
					{
						p.view.select(i);
						p.view.store.baseParams.file = p.file = '0';
					}
				});
			}
		});

		Ext.ux.MediaPanel.superclass.onRender.call(this, ct, position);

		if (p.mode == 'browser' && p.name)
		{
			p.hiddenField = p.el.insertSibling({
				tag: 'input',
				type:'hidden',
				name: p.name,
				value: p.value
			}, 'before', true);
		}
		
		if (p.mode != 'browser')
		{
			new Ext.Resizable(p.el, {
				minWidth: 700,
				handles: 's,e,se',
				transparent: true,
				resizeElement: function()
				{
					var box = this.proxy.getBox();
					p.updateBox(box);
					p.el.setLeftTop(0, 0);
					return box;
				}
		   });
		}
	},
	
	loadItem: function(id)
	{
		this.view.store.baseParams.folder = id;
		this.view.store.load();
	}
});

Ext.ux.form.MediaBox = Ext.extend(Ext.form.TriggerField, {
	triggerClass: 'x-form-media-trigger',
	editable: false,
	emptyText: lang.xeon.mod_media.not_selected,

	initComponent: function()
	{
		this.hiddenName = this.name;
		this.hiddenValue = this.value;
		this.value = this.displayValue;

		Ext.ux.form.MediaBox.superclass.initComponent.call(this);
	},

	onRender: function(ct, position)
	{
		Ext.ux.form.MediaBox.superclass.onRender.call(this, ct, position);

		this.hiddenField = this.el.insertSibling({
			tag: 'input',
			type: 'hidden',
			name: this.hiddenName,
			value: this.hiddenValue
		}, 'before', true);

		this.el.dom.removeAttribute('name');
		//this.el.on('mousedown', this.onTriggerClick, this);
	},

	onTriggerClick: function()
	{
		var f = this;

		var mp = new Ext.ux.MediaPanel({
			value: f.hiddenField.value
		});

		var win = new Ext.Window({
			title: lang.xeon.mod_media.select,
			iconCls: 'i-image',
			layout: 'fit',
			resizable: false,
			buttonAlign: 'center',
			modal: true,
			border: false,
			width: 700,
			height: 393,
			items: mp,
			buttons: [{
				text: lang.xeon.edit,
				handler: function()
				{
					f.setValue(mp.selectedTitle);
					f.hiddenField.value = mp.selectedItem;
					win.close();
				}
			},{
				text: lang.xeon.cancel,
				handler: function() {
					win.close();
				}
			}]
		});

		win.show();
	},

	reset: function()
	{
		this.setValue(this.originalValue);
		this.hiddenField.value = this.hiddenValue;
                this.clearInvalid();
	}
});

Ext.ux.MediaProductPanel = Ext.extend(Ext.Panel, {
       cls: 'x-mediapanel',
       title:'Galerie photo(s)',
	iconCls: 'i-image',
	layout: 'fit',
	autoScroll: true,
        bodyStyle : 'padding:5px 5px 5px 5px;',
	mode: 'browser',
	selectedItem: 0,
	selectedTitle: '',

	initComponent: function()
	{
         var prdid = this.prdid;
         var p = this;

		p.items = p.view = new Ext.DataView({
        	autoWidth: true,
        	autoHeight: true,
        	singleSelect: true,
                itemSelector: 'div.item',
                overClass: 'x-view-over',
                emptyText: 'Aucune image(s) lié à ce produit', // TODO ?
                plugins: (p.mode != 'browser') ? new Ext.DataView.DragSelector() : false,
                tpl: new Ext.XTemplate(
				'<tpl for=".">',
					'<div class="item" title="{title}">',
						'<div style="background-image: url({icon});"></div>',
						'<p>{shortTitle}</p>',
					'</div>',
				'</tpl>',
				'<div class="x-clear"></div>'
			),
		prepareData: function(data)
            {
                data.shortTitle = Ext.util.Format.ellipsis(data.title, 16);
                return data;
            }
        });

        p.view.on('render', function() {
        	p.view.store.load();
        });

	p.view.on('dblclick', function(d, i, n)
		{
			if (p.mode != 'browser')
			{
				var data = p.view.store.data.items[i].data;
				Xeon.loadItem(data.item);
			}
                });

        p.tbar = [{    
			text: lang.xeon.mod_media.file_add,
			iconCls: 'i-image-add',
                        handler: function() {
                            var f = this;

                    var mp = new Ext.ux.MediaPanel({multiSelect:true});


		var win = new Ext.Window({
			title: lang.xeon.mod_media.select,
			iconCls: 'i-image',
			layout: 'fit',
			resizable: false,
			buttonAlign: 'center',
			modal: true,
			border: false,
			width: 700,
			height: 393,
			items: mp,
			buttons: [{
				text: lang.xeon.edit,
				handler: function()
                                {
									
                                        r = mp.view.getSelectedRecords();
									for(var j=0;j<r.length;j++){
                                        i = r[j].data.fle_id;

                                        Ext.Ajax.request({
                                                params: {
                                                            cmd: 'media.saveMediaProduct',
                                                            item: i,
                                                            parent: p.prdid
										},
										success: function() {
																		p.view.store.load();}
										});
									}
					win.close();
				}
			},{
				text: lang.xeon.cancel,
				handler: function() {
					win.close();
				}
			}]
                    });

                    win.show();
                        }  
		}];

	
			p.tbar.push({
				text: lang.xeon.delete,
				iconCls: 'i-cross',
				//disabled: false,
				handler: function()
				{
					var list = '';
					var count = p.view.getSelectionCount();

					if (count > 0)
					{
						var text = (count > 1) ? String.format(lang.xeon.delete_items, count) : lang.xeon.delete_item;

						Ext.Msg.confirm(lang.xeon.confirm, text, function(btn)
						{
							if (btn == 'yes')
							{
								Ext.each(p.view.getSelectedRecords(), function(r) {
									list += (list == '') ? r.data.item : '-'+Xeon.getId(r.data.item).id;
								});

								Ext.Ajax.request({
									params: {
										cmd: 'media.deleteMediaProductItem',
										item: list
									},
									success: function() {
                                                                          p.view.store.load();
									}
								});
							}
						});
					}
				}
			});
		

		p.tbar.push({
			text: lang.xeon.reload,
			iconCls: 'i-refresh',
			handler: function() {
                                p.view.store.load();
			}
		});
		Ext.ux.MediaProductPanel.superclass.initComponent.call(this);
	},
	onRender: function(ct, position)
	{

		var p = this;
		var Record = Ext.data.Record.create(['item', 'title', 'icon', 'isFile','fle_id']);

		p.view.store = new Ext.data.JsonStore({
			url: Ext.Ajax.url,
			root: 'data',
			fields: Record,
			baseParams: {
				cmd: 'media.makeMediaListByProduct',
				id:this.prdid
			}});

		p.view.store.on('beforeload', function() {p.bwrap.mask(lang.xeon.loading, 'x-mask-loading');});

		p.view.store.on('load', function(s){p.bwrap.unmask();});

		Ext.ux.MediaProductPanel.superclass.onRender.call(this, ct, position);
	}
});
Ext.reg('mediaproductpanel', Ext.ux.MediaProductPanel);
Ext.reg('mediapanel', Ext.ux.MediaPanel);
Ext.reg('mediabox', Ext.ux.form.MediaBox);