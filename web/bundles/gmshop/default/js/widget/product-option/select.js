$(document).ready(function(){
	productOption = $('#fast-shop-widget-product-option');
	//productOption.children('select').change(function(){
	productOption.on('change', 'select', function(){
		if($(this).val() != ''){
			var select = {};
			productOption.children('select').each(function(){
				select[$(this).attr('name')] = $(this).val();

			});

			$.post( productOption.attr('data-url'), {'select': JSON.stringify(select)})
				.done(function( data ) {
					if(data.substr(0,1) == '{'){
						window.location.href(data.href);
					}else{
						productOption.html($(data).html());
					}
				});
		}
	})
})