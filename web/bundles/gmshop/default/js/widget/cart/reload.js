(function( $ ){
    $.fn.reloadCart = function(){
    	cart = $('#cart');
    	console.log('reloadCart : ' + cart.attr('data-url') );
    	$.get(cart.attr('data-url'))
			  .done(function( data ) {
					cart.html(data);
			  });
    } 
})(jQuery)