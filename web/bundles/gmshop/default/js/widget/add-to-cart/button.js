$(document).ready(function(){
	addToCart = $('#gm-shop-widget-add-to-cart');
	addToCart.find('button').click(function(){
		$.post( addToCart.attr('data-url'), { slug: $(location).attr('pathname'), quantity: addToCart.find('select[name="quantity"]').val() })
			  .done(function( data ) {
			  	if(data == 1){
					$.fn.reloadWidget('gm-shop-widget-cart');
			  	}
			  });
	})
})