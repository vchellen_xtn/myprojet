
//
// Interface cron v1.1.0

Xeon.module.cron =
{     
	allowDrop: function(tree, node, target) {
		return true;
	},
  
  makeCronRoot:function(ct,item,data){
    ct.add(new Ext.ux.grid.GridPanel({
        title: lang.xeon.mod_cron.grid_name,
        iconCls: 'i-page-white-stack',
        itype: 'cron.cron',
        rowId: 'id',
        paging:  true,
        quickSearch: true,
        columns: [{
            header: lang.xeon.title,
            dataIndex: 'title',
            width: 200,
            itemLink: ['cron.cron', 'id']
        }/*,{
            header: lang.xeon.status,
            dataIndex: 'status',
            width: 60,
            renderer: this.renderYesorNo,
            editor: new Ext.ux.form.ToggleBox()
        }*/]
    })
    );
  },
	makeCronForm: function(_ct, _item, _data)
	{
    var items = [];

    items.push({
        fieldLabel: lang.xeon.title,
        allowBlank: false,
        name: 'title',
        value: _data.db.cron.title
      });

    if(_item.add)
    {
      _ct.add(new Ext.ux.FormPanel({
        items: items
      }));
      return;
    }

    /*
    ZONE : information
    */

    items.push(
    {
      fieldLabel: lang.xeon.mod_cron.heure_lancement,
      name: 'heure_lancement',
      value: _data.db.values.heure_lancement
    },
    {
      xtype: 'listbox',
      fieldLabel: lang.xeon.mod_cron.request_title,
      itype: 'request.req',
      name: 'request',
      value: _data.db.request.id,
      displayValue: _data.db.request.title,
      listeners:{
        'select' : function(obj)
        {
          var tab_var = findById('cron_tableau_variable_' + _item.id);
          tab_var.parentId = 'request.req.' + obj.value;
          tab_var.reloadData();
        }
      }
    });

    _ct.add(new Ext.ux.FormPanel({
      buttons:[],
      items: items,
      title: lang.xeon.mod_cron.label_cron
    }));

    /*
    ZONE : choisir la pédiocité
    */
    var items = [];
    items.push(
    {
      id : 'id_periode_choix_'+_item.id,
      xtype: 'listbox',
      itype: 'cron.periode',
      fieldLabel: lang.xeon.mod_cron.periode_choix,
      name: 'periode_choix',
      value: _data.db.values.periode_choix,
      displayValue: _data.db.value_titles.periode_choix,
      listeners:{
        'select' : function(obj)
        {
          showOnePeriode(obj.value, _item.id);
        }
      }
    });

    _ct.add(new Ext.ux.FormPanel({
      buttons:[],
      items: items,
      title: lang.xeon.mod_cron.label_periodicite
    }));
    
    var is_show_periode = {'0': true, '1': true};
    if (_data.db.values.periode_choix != undefined)
      is_show_periode[_data.db.values.periode_choix] = false;

    /*
    ZONE : periode hebdo
    */
    var strHebdoDays = new String(_data.db.values.periode_hebdo);
    var items = [];
    var dayLibels = new Array();
    dayLibels[1] = lang.xeon.mod_cron_day.monday;
    dayLibels[2] = lang.xeon.mod_cron_day.tuesday;
    dayLibels[3] = lang.xeon.mod_cron_day.wednesday;
    dayLibels[4] = lang.xeon.mod_cron_day.thursday;
    dayLibels[5] = lang.xeon.mod_cron_day.friday;
    dayLibels[6] = lang.xeon.mod_cron_day.saturday;
    dayLibels[7] = lang.xeon.mod_cron_day.sunday;

    var itemDayss = [];
    Ext.each(dayLibels, function (v, i)
    {
      if (v != undefined)
      {
        itemDayss.push({boxLabel: v, name: 'periode_hebdo['+i+']', inputValue: i, checked : strHebdoDays.indexOf(i) != -1 ? true : false});
      }
    });
    items.push(
    {
      xtype: 'checkboxgroup',
      columns: 2,
      vertical: true,
      fieldLabel: lang.xeon.mod_cron.periode_hebdo_days,
      items: itemDayss
    },
    {
      fieldLabel: lang.xeon.mod_cron.periode_hebdo_semaine,
      name: 'periode_hebdo_semaine',
      value: _data.db.values.periode_hebdo_semaine
    }
    );
    _ct.add(new Ext.ux.FormPanel({
      id: 'zone_periode_hebdo_'+_item.id,
      hidden : is_show_periode['0'],
      buttons:[],
      items: items,
      title: lang.xeon.mod_cron.label_periodicite + ' - ' + lang.xeon.mod_cron.periode_hebdo
    }));

    /*
    ZONE : periode mensuelle - 2
    */
    var items = [];
    items.push(
    {
      xtype: 'combobox',
      fieldLabel: lang.xeon.mod_cron.periode_mensuelle_le,
      name: 'periode_mensuelle_le_2',
      data: [[0, 'Premier']],
      value: _data.db.values.periode_mensuelle_le_2
    },
    {
      xtype: 'combobox',
      name: 'periode_mensuelle_le_2_1',
      data: [[1, 'Lundi'], [2, 'Mardi'], [3, 'Mercredi'], [4, 'Jeudi'], [5, 'Vendredi'], [6, 'Samedi'] , [0, 'Dimanche']],
      value: _data.db.values.periode_mensuelle_le_2_1
    },
    {
      fieldLabel: lang.xeon.mod_cron.periode_mensuelle_tous,
      name: 'periode_mensuelle_tous_2',
      value: _data.db.values.periode_mensuelle_tous_2
    }
    );
    _ct.add(new Ext.ux.FormPanel({
      id: 'zone_periode_mensuelle_2_'+_item.id,
      hidden : is_show_periode['1'],
      buttons:[],
      items: items,
      title: lang.xeon.mod_cron.label_periodicite + ' - ' + lang.xeon.mod_cron.periode_mensuelle
    }));
    /*
    Media FILES
    */
    _ct.add(new Ext.ux.MediaPanelCron({
        title: "Liste des fichiers",
        iconCls: 'i-page-white-stack',
        mode: 'folder',
        itype :'media.cron.'+_item.id,
        height: 300
    }));
    /*
    PARTIE VARIABLE
    */
    var user_var = new Ext.ux.grid.GridPanel({
      id: 'cron_tableau_variable_' + _item.id,
      showIconRefresh : false,
      itype: 'request.def.fromCron_'+_item.id,
      parentId: 'request.req.' + _data.db.request.id,
      title: lang.xeon.mod_request_title.variable,
      paging:  true,
      quickSearch: false,
      search: false,
      reloadAfterEdit: false,
      exportPath: true,
      noRefreshToolbar: true,
      columns: [{
          header: lang.xeon.mod_request_title.label,
          dataIndex: 'wording',
          width: 150
        },
        {
          header: lang.xeon.mod_request_title.type,
          dataIndex: 'type',
          width: 150
        },
        {
          header: lang.xeon.mod_request_title.value,
          dataIndex: 'default_value',
          width: 150,
          editor: new Ext.form.TextField()
        },
        {
          header: lang.xeon.mod_request.list_values,
          dataIndex: 'list_value',
          hidden: true
        }],
        listeners :
        {
          // pour empecher le lancement de la sauvegarde de la valeur saisie
          // puisqu'ils ne sont que temporaire
          afteredit : function()
          {
            return false;
          },
          celldblclick : function (g, i, col, e)
          {
            if (g.getStore().getAt(i).data.list_value != undefined && g.getStore().getAt(i).data.list_value != '' && col == 3)
            {
              var tmps = g.getStore().getAt(i).data.list_value.split(';');
              var listval = [];
              Ext.each(tmps, function(val)
                {
                  listval.push([val, val]);
                });
                g.getColumnModel().setEditor(col, new Ext.ux.form.ComboBox({
                data : listval
              }));
            }
            else if(Xeon.user.isMaster || col == 3)
            {
              g.getColumnModel().setEditor(col, new Ext.form.TextField());
            }
          }
        }
    });
      
    _ct.add(user_var);
    /*
    PARTIE ENREGISTREMENT
    */
    var items = [];
    items.push(
    {
      xtype: 'combobox',
      fieldLabel: lang.xeon.mod_cron.actif,
      name: 'active',
      data:[[0, lang.xeon.mod_cron.actif_nok], [1, lang.xeon.mod_cron.actif_ok]],
      value: _data.db.cron.status
    });

    _ct.add(new Ext.ux.FormPanel({
      items: items,
      buttons:[
      {
        text: lang.xeon.mod_cron.save,
        handler: function() {
          submitAllForm(_ct);
        }
      },
      {
        text: lang.xeon.mod_cron.cancel,
        handler: function() {
            console.log('cancel');
        }
      }
      ],
      title: lang.xeon.mod_cron.validation
    }));

  }
}

function hideAllPeriode(_cron_id)
{
  findById('zone_periode_hebdo_'+_cron_id).hide();
  findById('zone_periode_mensuelle_2_'+_cron_id).hide();
}
function hidePlage(_cron_id)
{
  findById('fin_date_'+_cron_id).hide();
  findById('fin_occurrence_'+_cron_id).hide();
}


/*
en fonction de la periode choisi on affiche ou pas la zone correspondant à 0,1,2,3
*/
function showOnePeriode(id, _cron_id)
{
  var items = {0: 'zone_periode_hebdo', 1 : 'zone_periode_mensuelle_2'};
  hideAllPeriode(_cron_id);
  if(items[id] != undefined)
  {
    findById(items[id]+'_'+_cron_id).show();
  }
}

// le bouton valider en bas pour valider l'ensemble des formulaire sur cette cron
function submitAllForm(_ct)
{
  var forms = _ct.items;
  var params = {};
  params['cmd'] = 'xeon.saveData';
  params['item'] = _ct.itemId;
  

  forms.each(function()
  {
    if (this.getXType() == 'formpanel')
    {
      this.form.applyDataArray();
      var values = this.form.getValues('data');
      for(i in values)
      {
        if (i != '')
          params[i] = values[i];
      }
    }
    else if (this.getXType() == 'gridpanel')
    {
      var grid_var = this.getStore().data;
      if(grid_var.length > 0)
      {
        grid_var.each(function()
        {
          var val = this.data.default_value;
          if(val == null)
          {
            val = '';
          }
          params['data[grid_var]['+this.id+']'] = val;
        });
      }
    }
  });

  Ext.Ajax.request({
    params: params,
    before: function() {
        _ct.bwrap.mask(lang.xeon.saving, 'x-mask-loading');
    },
    success: _ct.success,
    callback: function(o, s, a)
    {
      _ct.bwrap.unmask();

      if (!a.error)
      {
        if (_ct.boxComponentId) {
            s = Ext.getCmp(_ct.boxComponentId);
            s.getEl().update(a.json.db.html);
        }

        Xeon.addItem(a.json.item, a);
        
        if (_ct.ownerCt && _ct.ownerCt.baseCls == 'x-window') {
            _ct.ownerCt.close();
            Xeon.activeGrid.store.reload();
        }
      }
    }
  });
}

function findById(_id)
{
  return Xeon.ui.center.getActiveTab().find('id', _id)[0];
}

Ext.ux.MediaPanelCron = Ext.extend(Ext.ux.MediaPanel, {

    initComponent: function()
    {
        var p = this;

        p.items = p.view = new Ext.DataView({
            autoWidth: true,
            autoHeight: true,
            singleSelect: true,
            itemSelector: 'div.item',
            overClass: 'x-view-over',
            emptyText: 'Aucun fichier', // TODO ?
            plugins: (p.mode != 'browser') ? new Ext.DataView.DragSelector() : false,
            tpl: new Ext.XTemplate(
                '<tpl for=".">',
                '<div class="item" title="{title}">{htmlAccept}',
                '<div  style="background-image: url({icon});"></div>',
                '<p>{shortTitle}</p>',
                '</div>',
                '</tpl>',
                '<div class="x-clear"></div>'
                ),
            prepareData: function(data)
            {

                data.htmlAccept = (data.accept==1)?'':'';
                data.shortTitle = Ext.util.Format.ellipsis(data.title, 16);
                return data;
            }
        });

        p.view.on('render', function() {
            p.view.store.load();
        });

        p.view.on('selectionchange', function(d, s)
        {
            if (p.mode == 'browser')
            {
                var r = p.view.getSelectedRecords();

                if (r.length != 0 && r[0].data.item)
                {
                    var item = Xeon.getId(r[0].data.item);
                    p.selectedItem = item.id;
                    p.selectedTitle = r[0].data.title;
                }
                else
                {
                    p.selectedItem = 0;
                    p.selectedTitle = '';
                }

                if (p.hiddenField) {
                    p.hiddenField.value = p.selectedItem;
                }
            }
            else {
                // p.topToolbar.items.get(2).setDisabled(s.length == 0);
                // p.topToolbar.items.get(3).setDisabled(s.length == 0);
                // p.topToolbar.items.get(4).setDisabled(s.length == 0);
            }
        });

        p.view.on('click', function(d, i, n)
        {
            if (p.mode == 'browser')
            {
                var data = p.view.store.data.items[i].data;
                if (!data.isFile) p.loadItem(data.item);
            }
        });

        p.view.on('dblclick', function(d, i, n)
        {
            if (p.mode != 'browser')
            {
                var data = p.view.store.data.items[i].data;
                Xeon.loadItem(data.item);
            }
        });

        p.tbar = [];

        if (p.mode == 'browser')
        {
            p.tbar.push({
                text: lang.MEDIA_PARENT_FOLDER,
                iconCls: 'i-parent-folder',
                handler: function() {
                    p.loadItem(p.parent);
                }
            });
        }
        else
        {
  
            p.tbar.push({
                text: lang.XEON_DELETE,
                iconCls: 'i-cross',
                disabled: false,
                handler: function()
                {
                    var list = '';
                    var count = p.view.getSelectionCount();

                    if (count > 0)
                    {
                        //var text = (count > 1) ? String.format(lang.XEON_DELETE_ITEMS, count) : lang.XEON_DELETE_ITEM;
                        var text = "Supprimer les éléments sélectionnés ?";
                        Ext.Msg.confirm("Confirmation", text, function(btn)
                        {
                            if (btn == 'yes')
                            {
                                Ext.each(p.view.getSelectedRecords(), function(r) {
                                    list += (list == '') ? r.data.item : '-'+Xeon.getId(r.data.item).id;
                                });

                                Ext.Ajax.request({
                                    params: {
                                        cmd: 'xeon.deleteItem',
                                        item: list,
                                        parent: p.ownerCt.itemId
                                    },
                                    success: function() {
                                        p.view.store.load();
                                    }
                                });
                            }
                        });
                    }
                }
            });
        }

        p.tbar.push({
            text: lang.XEON_RELOAD,
            iconCls: 'i-refresh',
            handler: function() {
                p.view.store.load();
            }
        });

        if (p.alignName)
        {
            p.tbar.push('->',{
                xtype: 'combobox',
                width: 120,
                data: [[1, lang.MEDIA_ALIGN_LEFT], [2, lang.MEDIA_ALIGN_RIGHT]],
                name: p.alignName,
                value: p.alignValue || 1
            });
        }

        Ext.ux.MediaPanel.superclass.initComponent.call(this);
    },
    onRender: function(ct, position)
    {

        var p = this;
        var Record = Ext.data.Record.create(['item', 'title', 'icon', 'isFile','fle_id','accept']);

        p.folder = (p.mode == 'browser') ? 'media.root.0' : p.ownerCt.itemId;
        p.file = (p.value) ? p.value : '0';

        p.view.store = new Ext.data.JsonStore({
            url: Ext.Ajax.url,
            root: 'data',
            fields: Record,
            baseParams: {
                cmd: 'cron.makeMediaList',
                mode: p.mode,
                folder: p.itype,
                file: p.file
            }
        });

        p.view.store.on('beforeload', function() {
            p.bwrap.mask(lang.XEON_LOADING, 'x-mask-loading');
        });

        p.view.store.on('load', function(s)
        {
            p.bwrap.unmask();
            p.folder = s.reader.jsonData.folder;
            p.upload = s.reader.jsonData.upload;
            p.parent = s.reader.jsonData.parent;
            p.size_limit = s.reader.jsonData.size_limit;

            if (p.mode == 'browser')
            {
                //p.topToolbar.items.get(0).setDisabled(!s.reader.jsonData.upload);
               // p.topToolbar.items.get(2).setDisabled(!s.reader.jsonData.parent);
            }

           // p.topToolbar.items.get(1).setDisabled(!s.reader.jsonData.link);

            if (p.file != '0')
            {
                s.each(function(r, i)
                {
                    var item = Xeon.getId(r.data.item);

                    if (r.data.isFile && item.id == p.file)
                    {
                        p.view.select(i);
                        p.view.store.baseParams.file = p.file = '0';
                    }
                });
            }
        });

        Ext.ux.MediaPanel.superclass.onRender.call(this, ct, position);

        if (p.mode == 'browser' && p.name)
        {
            p.hiddenField = p.el.insertSibling({
                tag: 'input',
                type:'hidden',
                name: p.name,
                value: p.value
            }, 'before', true);
        }

        if (p.mode != 'browser')
        {
            new Ext.Resizable(p.el, {
                minWidth: 700,
                handles: 's,e,se',
                transparent: true,
                resizeElement: function()
                {
                    var box = this.proxy.getBox();
                    p.updateBox(box);
                    p.el.setLeftTop(0, 0);
                    return box;
                }
            });
        }
    }
});

Ext.reg('MediaPanelCron', Ext.ux.MediaPanelCron);