$(document).ready(function(){

	//-------------------------------------------
	//	LOAD Left panel
	//-------------------------------------------
	//$(document).on("click", '.widget .reload', function (event) {
	$(document).on("click", 'a.reload-left', function (event) {
		event.preventDefault();
		//var el = $(this).parents(".widget:first");

		$(".left.side-menu .sidebar-inner").addClass('panel-loading');

		window.setTimeout(function () {
			$(".left.side-menu .sidebar-inner").removeClass('panel-loading');
			//open_column_left();
		}, 3000);
	});

    $(".ajax-link-menu").click(function(){
        $.ajax({
            cache:false,
            type: 'POST',
            dataType: "html",
            url: $(this).attr('data-href'),
            success: function( html )
            {
                $("#content-left-menu").html("");
                $("#content-left-menu").html(html);
                init();

            }
        });
    });


    $("#btn-search").click(function(e){
        e.preventDefault();
        data = {};
        data.keyword = $("#keyword").val();
        $.ajax({
            data: data,
            cache:false,
            type: 'POST',
            dataType: "html",
            url: $(this).attr('data-href'),
            success: function( html )
            {
                $("#content-left-menu").html("");
                $("#content-left-menu").html(html);
                init();
            }
        });
    });

    var tabs = $('#mc-tabs').tabs();
    tabs.tabs()
        .removeClass('ui-widget ui-widget-content ui-corner-all')
        .mcScrollTabs();

    $("ul#ticker-topbar").liScroll({travelocity: 0.05});



    init();
    function init()
	{


        // LEFT SIDE MAIN NAVIGATION
        $("#sidebar-menu a").on('click',function(e){
            if(!$("#wrapper").hasClass("enlarged")){

                if($(this).parent().hasClass("has_sub")) {
                    e.preventDefault();
                }

                if(!$(this).hasClass("subdrop")) {
                    // hide any open menus and remove all other classes
                    $("ul",$(this).parents("ul:first")).slideUp(350);
                    $("a",$(this).parents("ul:first")).removeClass("subdrop");
                    $("#sidebar-menu .pull-right i").removeClass("fa-angle-up").addClass("fa-angle-down");

                    // open our new menu and add the open class
                    $(this).next("ul").slideDown(350);
                    $(this).addClass("subdrop");
                    $(".pull-right i",$(this).parents(".has_sub:last")).removeClass("fa-angle-down").addClass("fa-angle-up");
                    $(".pull-right i",$(this).siblings("ul")).removeClass("fa-angle-up").addClass("fa-angle-down");
                }else if($(this).hasClass("subdrop")) {
                    $(this).removeClass("subdrop");
                    $(this).next("ul").slideUp(350);
                    $(".pull-right i",$(this).parent()).removeClass("fa-angle-up").addClass("fa-angle-down");
                    //$(".pull-right i",$(this).parents("ul:eq(1)")).removeClass("fa-chevron-down").addClass("fa-chevron-left");
                }
            }
        });

// NAVIGATION HIGHLIGHT & OPEN PARENT
        $("#sidebar-menu ul li.has_sub a.active").parents("li:last").children("a:first").addClass("active").trigger("click");

		// Close a tabs
		tabs.delegate( "span.ui-icon-close", "click", function() {
			var panelId = $( this ).closest( "li" ).remove().attr( "aria-controls" );
			$( "#" + panelId ).remove();
			tabs.tabs( "refresh" );
		});

        function getFormParameters(id){
            var parameters = {};

            $(".form-parameters-"+id).each(function( ) {

                parameters[$(this).data("id")]= $(this).val();
            });
            return parameters;
        }

        // vérifie si tous les parametres obligatoires sont remplis
        function verifFormParamers(id){
            var parameters = {};
            var error = false;
            $(".form-parameters-"+id).each(function( ) {
                if ($(this).prop('required') && $(this).val()==""){
                    var title = $(this).attr("id");
                    $("#form-error-"+id).fadeIn( "slow", function() {
                        $("#form-error-"+id).html(title + " doit être rempli.");
                    });
                    setTimeout(function() {
                        $("#form-error-"+id).fadeOut( "slow", function() {
                            $(this).html("");
                        });
                    }, 5000);
                    error = true;
                }
            });
            if (error){
                console.log(error);
                return false;
            }else{
                return true;
            }
        }

        $('#sidebar-menu a').click(function(e){
            e.preventDefault();
            favorite = $(this).attr('data-favorite');
                if( typeof favorite != "undefined" ||  $(this).data('request-id').indexOf('request.req') >= 0   ){

                if (typeof favorite !=  "undefined" ){
                    var	tab_id = favorite;
                }else{
                    tab_id = $(this).data('request-id').substr(12);
                }
                requestId = $(this).data('request-id').substr(12);

                if( $('#'+tab_id).length == 0 )
                {
                    var title  = $(this).data('title');
                    var icon   = $(this).data('icon');
                    data = {};
                    data.id = requestId;
                    data.title = $(this).data('title');
                    data.icon =  $(this).data('icon');
                    data.favorite = $(this).data('favorite');
                    $.ajax({
                        cache:false,
                        type: 'POST',
                        data: data,
                        dataType: "html",
                        url: $(this).attr('data-href'),
                        success: function( html )
                        {
                            addTab( tab_id, title, html );




                            $('.add-favorite-'+tab_id).click(function(e){
                                e.preventDefault();

                                if($('#favori-name-'+ tab_id) ==""){
                                    alert('Merci de choisi un nom pour ce favori.');
                                    return false;
                                }
                                if (verifFormParamers(tab_id)== false){
                                    return false;
                                };
                                var id = $(this).attr('data-id');
                                parameters = getFormParameters(tab_id);
                                data = {};
                                data.title = $("#favori-name-"+tab_id).val();
                                data.id = $(this).attr("data-id");
                                data.parameters = parameters;
                                $.ajax({
                                    cache:false,
                                    type: 'POST',
                                    data: data,
                                    dataType: "html",
                                    url: $(this).attr('data-href'),
                                    success: function( data  )
                                    {
                                        $("#form-info-"+id).fadeIn( "slow", function() {
                                            $("#form-info-"+id).html($.parseJSON(data));
                                        });
                                        setTimeout(function() {
                                            $("#form-info-"+id).fadeOut( "slow", function() {
                                                $(this).html("");
                                            });
                                        }, 5000);
                                    }
                                });
                            });


                            $('.remove-favorite-'+tab_id).click(function(e){
                                e.preventDefault();

                                var id = $(this).attr('data-favorite');
                                var reqId = $(this).attr('data-id');
                                data = {};
                                data.id = id;
                                $.ajax({
                                    cache:false,
                                    type: 'POST',
                                    data: data,
                                    dataType: "html",
                                    url: $(this).attr('data-href'),
                                    success: function( data  )
                                    {
                                        $("#form-info-"+tab_id).fadeIn( "slow", function() {
                                            $("#form-info-"+reqId).html($.parseJSON(data));
                                        });
                                        setTimeout(function() {
                                            $("#form-info-"+tab_id).fadeOut( "slow", function() {
                                                $(this).html("");
                                            });
                                        }, 5000);

                                        location.reload();
                                    }
                                });
                            });

                            $('.launch-'+tab_id).click(function(e){
                                e.preventDefault();
                                if (verifFormParamers($(this).attr('data-id'))== false){
                                    return false;
                                };
                                // lancement de la requête
                                var id = $(this).attr('data-id');
                                $("#results-"+id).html("");
                                parameters = getFormParameters(tab_id);
                                data = {};
                                data.id = $(this).attr("data-id");
                                data.parameters = parameters;
                                $.ajax({
                                    cache:false,
                                    type: 'POST',
                                    data: data,
                                    dataType: "html",
                                    url: $(this).attr('data-href'),
                                    success: function( data  )
                                    {

                                        if (data.indexOf(',"text":') > 0){
                                            var retour = $.parseJSON(data);
                                            $("#form-error-"+tab_id).fadeIn( "slow", function() {
                                                $("#form-error-"+tab_id).html(retour.text);
                                            });
                                            setTimeout(function() {
                                                $("#form-error-"+tab_id).fadeOut( "slow", function() {
                                                    $(this).html("");
                                                });
                                            }, 10000);
                                        }else{
                                            $("#results-"+tab_id).html(data);
                                        }

                                        // ,

                                        $('#datatables-1').dataTable( {
                                            dom: 'T<"clear">lfrtip',
                                            tableTools: {
                                                "sSwfPath": "/bundles/totemfront/assets/libs/jquery-datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
                                                "aButtons": [
                                                    {
                                                        "sExtends": "csv",
                                                        "sTitle": "My title",
                                                        "sToolTip": "Save as CSV"
                                                    },
                                                    {
                                                        "sExtends": "pdf",
                                                        "sPdfOrientation": "landscape",
                                                        "sPdfMessage": "Save as PDF"
                                                    }
                                                ]
                                            },
                                            language: {
                                                url: '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json'
                                            }
                                        } );
                                        //
                                    }
                                });
                            });

                        }
                    });
                }else {
                    setTabActiveById( tab_id );
                }
            }
        });



        /*
		$('#sidebar-menu > ul > li > ul > li > a').click(function(e)
		{
			e.preventDefault();

			var	tab_id = $(this).data('id');
			var title  = $(this).data('title');
			var icon   = $(this).data('icon');

				title = '<i class="'+icon+' pos-left"></i>'+title;

			if( $('#'+tab_id).length == 0 )
			{
				if( title )
				{
					$.ajax({
						type: 'POST',
						url: $(this).attr('href'),
						contentType : 'application/x-www-form-urlencoded; charset=utf-8',
						success: function( html )
						{
							addTab( tab_id, title, html );
						}
					});
				}
			}
			else {
				setTabActiveById( tab_id );
			}
		});
        */

		function addTab(tab_id, title, content)
		{
			var label = title,
				tabTemplate = "<li><a href='#{href}'>#{label}</a> <span class='ui-icon ui-icon-close' role='presentation'>Remove Tab</span></li>",
				li = $( tabTemplate.replace( /#\{href\}/g, "#" + tab_id ).replace( /#\{label\}/g, label ) );

				tabs.find( ".ui-tabs-nav" ).append( li );
				tabs.append( "<div id='" + tab_id + "'>" + content + "</div>" );
				tabs.tabs( "refresh" );

			setTabActiveById( tab_id );
		}


		function setTabActiveById ( tab_id )
		{
			//var index = $('#mc-tabs ul li').index( $('#'+tab_id) );
			var index = $('#mc-tabs .ui-tabs-panel').index( $('#'+tab_id) );
			//console.log( index );

			$('#mc-tabs').tabs({ active: index });

			// SCROLL TO tabs begin
			var target_offset = $('body').offset();
			var target_top = target_offset.top;

			//goto that anchor by setting the body scroll top to anchor top
			//$('html, body').animate({scrollTop:target_top}, 1500, 'easeInSine');
			//$('html, body').animate({scrollTop:target_top}, 1000, 'easeInSine');
			$('html, body').animate({scrollTop:target_top}, 1000, 'easeOutQuint');
		}
	};
});