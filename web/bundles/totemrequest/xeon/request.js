
  //
  // Interface request v1.1.0


  Xeon.module.request =
  {     
  	allowDrop: function(tree, node, target) {
  		return true;
  	},
    makeImportRoot: function(ct, item, data) {
      var p = this;
      ct.add(new Ext.ux.FormPanel({
        buttons:[{
          text: lang.xeon.mod_request.upload_file_XML,
          iconCls: 'i-image-add',
          handler: function()
          {
            var btn = new Ext.Panel({
              border: false,
              bodyStyle: {
                marginBottom: '10px',
                background: '#dfe8f6',
                fontSize: '12px'
              },
              html: '<span id="swfu_button"></span> <span id="swfu_status">'+lang.xeon.mod_media.upload_select+'</span>'
            });
            
            var bar = new Ext.ProgressBar();
            
            var win = new Ext.Window({
              title: lang.xeon.mod_media.file_add,
              iconCls: 'i-image-add',
              resizable: false,
              modal: true,
              border: false,
              width: 400,
              //height: 100,
              items: new Ext.Panel({
                frame: true,
                bodyStyle: {
                  padding: '10px'
                },
                items: [btn, bar]
              })
            });
            
            win.on('show', function()
            {
              var total = 0;
              var current = 1;
                                          //  alert('lol');
              
              //SWFUpload.Console.writeLine = console.log;
              win.swfu = new SWFUpload({
                //debug: true,
                flash_url: '/bundles/gmmedia/xeon/swfupload.swf',
                upload_url: Ext.Ajax.url,
                
                file_types: "",
                file_types_description: "Document",
                file_post_name: 'media',
                file_size_limit: '134217728B',
                
                button_width: '16',
                button_height: '16',
                button_placeholder_id: 'swfu_button',
                button_image_url: '/bundles/gmmedia/img/upload.png',
                button_cursor: SWFUpload.CURSOR.HAND,
                
                post_params: {
                  cmd: 'xeon.saveData',
                  item: 'request.import',
                  folder: "",
                  PHPSESSID: Xeon.getCookie('PHPSESSID')
                },
                file_queued_handler: function(file)
                {
                  total++;
                  Ext.get('swfu_status').update(String.format(lang.xeon.mod_media.upload_status, current, total));
                },
                
                file_queue_error_handler: function(file, error, message)
                {
                  Ext.Msg.alert(lang.xeon.error, file.name+' : '+message);
                  // TODO : g�rer les erreurs
                },
                
                file_dialog_complete_handler: function()
                {
                  var stats = win.swfu.getStats();
      
                  if (stats.files_queued > 0 && stats.in_progress == 0) {
                    win.swfu.startUpload();
                  }
                },
                
                upload_progress_handler: function(file, loaded, total)
                {
                  var progress = Math.ceil((loaded/total)*100);
                  bar.updateProgress(progress/100, file.name+' ('+progress+'%)');
                },
                
                upload_success_handler: function(file, data, responseReceived)
                {
                  var objJSON = Ext.util.JSON.decode(data);

                  if(objJSON && objJSON.error != undefined)
                  {
                    Xeon.showError(objJSON.error.title, objJSON.error.text);
                    Ext.get('swfu_status').update(lang.xeon.mod_media.upload_select);
                    bar.updateProgress(0, ' ');
                    return true;
                  }
                  if (win.swfu.getStats().files_queued > 0)
                  {
                    current++;
                    Ext.get('swfu_status').update(String.format(lang.xeon.mod_media.upload_status, current, total));
                    win.swfu.startUpload();
                  }
                  else
                  {
                    win.close();
                  }
                }
              });
            });
            
            win.on('beforedestroy', function() {
              if (win.swfu) win.swfu.destroy();});
            
            win.show();
          }
        }]
      }));
    },
    makeColonneForm: function(ct, item, data)
    {
      var items = [];
      
      var field_title = {
        fieldLabel: lang.xeon.mod_request_colonne.add_title,
        allowBlank: false,
        name: 'name_sql'
      };

      if (item.add)
      {
        items.push(field_title,
          {
            hidden : true,
            name: 'parent',
            value: data.parent
          });
      }
      ct.add(new Ext.ux.FormPanel({
        items: items
      }));
    },             
    makeCategorieForm: function(ct, item, data)
    {  
      var items = [];
      items.push({
          xtype: 'listbox',
          fieldLabel: lang.xeon.parent,
          itype: data.itype,
          name: 'parent',
          value: data.db.parent,
          displayValue: data.db.parent_title
      });
      items.push({
        fieldLabel: lang.xeon.title,
        allowBlank: false,
        name: 'title',
        value: data.db.title
      });
      ct.add(new Ext.ux.FormPanel({
        items: items
      }));

      var items = [];
      items.push({
              header: lang.xeon.mod_request_title.title,
              dataIndex: 'title',
              width: 150,
              itemLink: ['request.req', 'id']
          },{
              header: lang.xeon.mod_request_title.category,
              dataIndex: 'category_title',
              width: 150
          },{
              header: lang.xeon.mod_request_title.description,
              dataIndex: 'description',
              width: 200
          });
      if(Xeon.user.isMaster)
      {
        items.push({
              header: lang.xeon.mod_request_title.actif,
              dataIndex: 'actif',
              width: 60,
              renderer: Xeon.renderStatus,
              editor: new Ext.ux.form.ToggleBox()
          });
      }

      if(!item.add)
      {
        ct.add(new Ext.ux.grid.GridPanel({
            title: lang.xeon.mod_request_title.list_request,
            iconCls: 'i-page-white-stack',
            itype: 'request.req',
            rowId: 'id',
            paging:  true,
            quickSearch: true,
            search:engine.getSearchCmsItems(data),
            columns: items,
            quickSearch: false,
            search: false
        }));
      }
    },
    makeRequestCopy: function(ct, item, data){
      var items = [];
      
      var field_cate = {
        xtype: 'listbox',
        fieldLabel: lang.xeon.mod_request.cat,
        itype: 'request.cat',
        name: 'parent',
        value: data.db.parent,
        displayValue: data.db.parent_title
      };
      var field_title = {
        fieldLabel: lang.xeon.title,
        allowBlank: false,
        name: 'title',
        value: data.db.title
      };

      var field_description = {
        xtype: 'textarea',
        fieldLabel: lang.xeon.mod_site.description,
        name: 'description',
        value: data.db.description
      };
      items.push(field_cate, field_title, field_description);
      // les info lié à la requete
      ct.add(new Ext.ux.FormPanel({
        command: 'xeon.copyItem',
        items: items
      }));
    },
  	makeRequestForm: function(ct, item, data)
  	{
      var items = [];
      
      var field_cate = {
        xtype: 'listbox',
        fieldLabel: lang.xeon.mod_request.cat,
        itype: 'request.cat',
        name: 'parent',
        value: data.db.parent,
        displayValue: data.db.parent_title
      };
      var field_title = {
        fieldLabel: lang.xeon.title,
        allowBlank: false,
        name: 'title',
        value: data.db.title
      };

      var field_description = {
        xtype: 'textarea',
        fieldLabel: lang.xeon.mod_site.description,
        name: 'description',
        value: data.db.description
      };
      if (item.add)
      {
        items.push(field_cate, field_title, field_description);
        // les info lié à la requete
        ct.add(new Ext.ux.FormPanel({
          items: items
        }));
      }
      else
      {
        items.push(field_title,
          field_cate,
          {
            fieldLabel: lang.xeon.mod_request_title.label,
            name: 'full_title',
            value: data.db.full_title
          },
          field_description);
        if (Xeon.user.isMaster)
        {
          items.push({
            fieldLabel: lang.xeon.mod_request_title.keywords,
            name: 'keywords',
            value: data.db.keywords
          },
          {
            xtype: 'listbox',
            fieldLabel: lang.xeon.mod_request_title.environment,
            itype: 'request.env',
            name: 'environment',
            value: data.db.environment_id,
            displayValue: data.db.environment_title
          },
          {
            xtype: 'togglebox',
            fieldLabel: lang.xeon.mod_request_title.actif,
            name: 'actif',
            value: data.db.actif
          }          
          );
        }
      
      
        if(Xeon.user.isMaster)
        {
          
          // les info lié à la requete
          ct.add(new Ext.ux.FormPanel({
              items: items,
              title: lang.xeon.mod_request_title.request
            }));
          ct.add(new Ext.ux.ListPanel({
                    title: lang.xeon.mod_request_title.group_user,
                    iconCls: 'i-page-white',
                    name: 'group',
                    itype: 'user.grp',
                    data: data.db.multi_grp
                }));
        }
        else
        {
          // les info lié à la requete
          ct.add(new Ext.ux.InfoPanel({
              items: [{
                infoLabel: lang.xeon.mod_request_title.label,
                value: data.db.full_title
              },
              {
                infoLabel: lang.xeon.mod_site.description,
                value: data.db.description
              }],
              title: lang.xeon.mod_request_title.request
            }));
        }
        // la zone de la requete
        if (Xeon.user.isMaster)
        {
          ct.add(new Ext.ux.FormPanel({
            items: [{
              xtype: 'textarea',
              fieldLabel: lang.xeon.mod_request_title.sql,
              name: 'codeSql',
              value: data.db.value,
              width: 500,
              height: 300
            },
            {
              hidden : true,
              name: 'saveSQL',
              value: 'OK'
            }],
            title: lang.xeon.mod_request_title.request,
            buttons: [
            {
              text : lang.xeon.mod_request_title.analyse,
              handler: function()
              {
                this.ownerCt.ownerCt.submit();
              }
            }],
            success: function(){
              // on recharge les trois colonnes apres analyse
              if(Ext.getCmp('request_colonne_'+item.id) != undefined)
              {
                Ext.getCmp('request_colonne_'+item.id).reloadData();  
              }
              if(Ext.getCmp('request_definition_'+item.id) != undefined)
              {
                Ext.getCmp('request_definition_'+item.id).reloadData();  
              }
              if(Ext.getCmp('request_user_variable_' + item.id) != undefined)
              {
                Ext.getCmp('request_user_variable_' + item.id).reloadData();
              }
            }
          }));
          ct.add(new Ext.ux.grid.GridPanel({
            rowId: 'id',
            id: 'request_colonne_'+item.id,
            enableDragDrop: true,
            itype: 'request.col',
            title: lang.xeon.mod_request_title.param_show_col,
            paging:  true,
            quickSearch: false,
            reloadAfterEdit: true,
            search: false,
            exportPath:true,
            columns: [{
                header: lang.xeon.mod_request_title.order,
                dataIndex: 'rang',
                dataType: 'int',
                width: 60
            },{
                header: lang.xeon.mod_request_title.unique,
                dataIndex: 'unique_title',
                width: 80
            },{
                header: lang.xeon.mod_request_title.col,
                dataIndex: 'title',
                width: 150,
                editor: new Ext.form.TextField({
                  allowBlank: false
                })
            },{
                header: lang.xeon.mod_request_title.cadrage,
                dataIndex: 'cadrage',
                width: 60,
                editor: new Ext.ux.form.ComboBox({
                  data : [[0 , lang.xeon.mod_request_colonne.cadrage_left],
                  [1 , lang.xeon.mod_request_colonne.cadrage_center],
                  [2 , lang.xeon.mod_request_colonne.cadrage_right]]
                })
            },{
                header: lang.xeon.mod_request_title.nb_float,
                dataIndex: 'format_numeric',
                width: 60,
                editor: new Ext.form.TextField({
                  allowBlank: false
                })
            },{
                header: lang.xeon.mod_request_title.separator,
                dataIndex: 'format_separator',
                width: 60,
                editor: new Ext.form.TextField({
                  allowBlank: false
                })
            },{
                header: lang.xeon.mod_request.date_format,
                dataIndex: 'format_date',
                width: 60,
                editor: new Ext.form.TextField({
                  allowBlank: false
                })
            },{
                header: lang.xeon.mod_request.cols_width,
                dataIndex: 'width',
                width: 60,
                editor: new Ext.form.TextField({
                  allowBlank: false
                })
            }
            ]
          }));
          var defListType = [];
          for(var lt_v in data.listDefinitionType)
          {
            defListType.push([lt_v, data.listDefinitionType[lt_v]]);
          }
          var type_combo = new Ext.ux.form.ComboBox({
              data : defListType
          });
          var var_grid = new Ext.ux.grid.GridPanel({
            id: 'request_definition_'+item.id,
            itype: 'request.def',
            title: lang.xeon.mod_request_title.definition,
            paging:  true,
            quickSearch: false,
            search: false,
            reloadAfterEdit: true,
            reloadAfterErrorEdit: true,
            exportPath: true,
            columns: [{
                header: lang.xeon.mod_request_title.title,
                dataIndex: 'title',
                width: 100
              },
              {
                header: lang.xeon.mod_request_title.label,
                dataIndex: 'wording',
                width: 100,
                editor: new Ext.form.TextField()
              },
              {
                header: lang.xeon.mod_request.default_value,
                dataIndex: 'default_value',
                width: 100,
                editor: new Ext.form.TextField()
              },
              {
                header: lang.xeon.mod_request.controle,
                dataIndex: 'type',
                width: 100,
                editor: type_combo
              },
              {
                header: lang.xeon.mod_request.list_values,
                dataIndex: 'list_value',
                width: 100,
                editor: new Ext.form.TextField()
              },
              {
                header: lang.xeon.mod_request.requier_value,
                dataIndex: 'obligatory',
                renderer: this.renderYesorNo,
                editor: new Ext.ux.form.ToggleBox()
              }
              ],
              listeners : {
                beforeedit : function(e)
                {
                  if(e.record.data.type == 'aucun' && e.column == 5)
                  {
                    return false;
                  }
                }
              }
          });
          
          ct.add(var_grid);

        }
        var user_var = new Ext.ux.grid.GridPanel({
          showIconRefresh : false,
          id : 'request_user_variable_'+item.id,
          itype: 'request.def.affectation',
          title: lang.xeon.mod_request_title.variable,
          paging:  true,
          quickSearch: false,
          search: false,
          reloadAfterEdit: false,
          exportPath: true,
          noRefreshToolbar: true,
          columns: [{
              header: lang.xeon.mod_request_title.label,
              dataIndex: 'wording',
              width: 150
            },
            {
              header: lang.xeon.mod_request_title.type,
              dataIndex: 'type',
              width: 150
            },
            {
              header: lang.xeon.mod_request_title.value,
              dataIndex: 'default_value',
              width: 150,
              editor: new Ext.form.TextField()
            },
            {
              header: lang.xeon.mod_request.list_values,
              dataIndex: 'list_value',
              hidden: true
            }],
            listeners :
            {
              // pour empecher le lancement de la sauvegarde de la valeur saisie
              // puisqu'ils ne sont que temporaire
              afteredit : function()
              {
                return false;
              },
              celldblclick : function (g, i, col, e)
              {
                if (g.getStore().getAt(i).data.list_value != undefined && g.getStore().getAt(i).data.list_value != '' && col == 3)
                {
                  var tmps = g.getStore().getAt(i).data.list_value.split(';');
                  var listval = [];
                  Ext.each(tmps, function(val)
                    {
                      listval.push([val, val]);
                    });
                  g.getColumnModel().setEditor(col, new Ext.ux.form.ComboBox({
                    data : listval
                  }));
                }
                else if(Xeon.user.isMaster || col == 3)
                {
                  g.getColumnModel().setEditor(col, new Ext.form.TextField());
                }
              }
            }
        });
          
        ct.add(user_var);

        // les info lié à la requete

        var itemsButtons = [];
        if(Xeon.user.isMaster)
        {
          itemsButtons.push(
            {
              text: lang.xeon.export_xml,
              handler : function()
              {
                var p = this.ownerCt.ownerCt;
                Ext.Ajax.request({
                    params: {
                        cmd: 'xeon.exportXML',
                        item: ct.itemId
                    },
                    before: function() {
                        p.bwrap.mask(lang.xeon.loading, 'x-mask-loading');
                    },
                    success: function() {
                        Xeon.getFileXML();
                    },
                    callback: function() {
                        p.bwrap.unmask();
                    }
                });

              }
            },
            {
            text: lang.xeon.mod_request_title.show_sql,
            handler: function()
            {
              var params = {};
              params['cmd'] = 'xeon.saveData';
              params['item'] = ct.itemId;
              params['parent'] = 'request.showSQL';
              var dataVars = Ext.getCmp('request_user_variable_' + item.id).getStore().data;
              if(dataVars.length > 0)
              {
                dataVars.each(function()
                {
                  var val = this.data.default_value;
                  if(val == null)
                  {
                    val = '';
                  }
                  params['data[values]['+this.id+']'] = val;
                });
              }
              else
              {
                params['data'] = '';
              }
              Ext.Ajax.request({
                params: params,
                callback: function(o, s, a)
                {
                  Ext.Msg.alert('SQL', a.json.sql);
                }
              });
            }
          });
        }

        itemsButtons.push(
          {text: lang.xeon.mod_request_title.launch,
          handler: function()
          {
            var dataVars = Ext.getCmp('request_user_variable_' + item.id).getStore().data;
            var params = {};
            params['cmd'] = 'xeon.saveData';
            params['item'] = ct.itemId;
            params['parent'] = 'request.launchSQL';
            if(this.ownerCt.ownerCt.getForm().items.item(2) != undefined)
            {
              params['data[admin_environment]'] = this.ownerCt.ownerCt.getForm().items.item(2).getValue();
            }
            if(dataVars.length > 0)
            {
              dataVars.each(function()
              {
                var val = this.data.default_value;
                if(val == null)
                {
                  val = '';
                }
                params['data[values]['+this.id+']'] = val;
              });
            }
            else
            {
              params['data'] = '';
            }
            
            Ext.Ajax.request({
              params: params,
              before: function() {
                  Ext.getCmp('request_resultat_'+item.id).bwrap.mask(lang.xeon.saving, 'x-mask-loading');
              },
              success: ct.success,
              callback: function(o, s, a)
              {
                Ext.getCmp('request_resultat_'+item.id).bwrap.unmask();
                Ext.getCmp('request_resultat_'+item.id).itemId = a.json.log_id;
                Ext.getCmp('request_resultat_'+item.id).itype = 'request.sql.'+a.json.log_id;
                /*
                // VERSION 1
                Ext.getCmp('request_resultat_'+item.id).resetColumns();
                var col = [];
                col.push(new Ext.grid.CheckboxSelectionModel({
                    moveEditorOnEnter: false
                }));
                Ext.each(a.json.columns, function(v)
                {
                    col.push(v);
                });

                Ext.getCmp('request_resultat_'+item.id).addColumns(col);*/
                Ext.getCmp('request_resultat_'+item.id).reloadData();
              }
            });
          }},
          {text:lang.xeon.mod_request_title.save_favori,
            handler: function()
            {
              var itemFavTitle = this.ownerCt.ownerCt.getForm().items.item(1);
              var itemFavSelect = this.ownerCt.ownerCt.getForm().items.item(0);
              var buttons = this.ownerCt.ownerCt.buttons;
              if(itemFavTitle.getValue() == '')
              {
                Ext.Msg.alert(lang.xeon.mod_request_error.title, lang.xeon.mod_request_error.favori_need_title);
                return;
              }
              var dataVars = Ext.getCmp('request_user_variable_' + item.id).getStore().data;
              var params = {};
              params['cmd'] = 'xeon.saveData';
              params['item'] = 'request.fav';
              params['parent'] = 'request.req.'+item.id;
              params['data[title]'] = itemFavTitle.getValue();
              if(dataVars.length > 0)
              {
                dataVars.each(function()
                {
                  var val = this.data.default_value;
                  if(val == null)
                  {
                    val = '';
                  }
                  params['data[values]['+this.id+']'] = val;
                });
              }
              else
              {
                params['data'] = '';
              }
              
              Ext.Ajax.request({
                params: params,
                before: function() {
                    ct.bwrap.mask(lang.xeon.saving, 'x-mask-loading');
                },
                success: ct.success,
                callback: function(o, s, a)
                {
                  // on vient d'ajouter on supprime tout la liste de listbox et on le reload
                  itemFavTitle.setValue('');
                  itemFavSelect.clearValue();
                  itemFavSelect.getStore().reload();
                  // cache bouton supprimer
                  buttons[3].hide();
                  ct.bwrap.unmask();
                }
              });
            }
          },
          {text:lang.xeon.mod_request_title.del_favori,
            hidden: true,
            handler : function()
            {
              var val = this.ownerCt.ownerCt.getForm().items.item(0).value;
              var itemFavSelect = this.ownerCt.ownerCt.getForm().items.item(0);
              var buttons = this.ownerCt.ownerCt.buttons;
              if(val != '')
              {
                var params = {};
                params['cmd'] = 'xeon.deleteItem';
                params['item'] = 'request.fav.'+val;
                Ext.Ajax.request({
                  params: params,
                  before: function() {
                      ct.bwrap.mask(lang.xeon.saving, 'x-mask-loading');
                  },
                  success: ct.success,
                  callback: function(o, s, a)
                  {
                    // on vient de supprimer un favori alors on le reload
                    itemFavSelect.clearValue();
                    itemFavSelect.getStore().reload();
                    // cache bouton supprimer
                    buttons[3].hide();
                    ct.bwrap.unmask();
                  }
                });   
              }
            }
          },
          {text:lang.xeon.mod_request_title.reset,
            handler : function()
            {
              // on efface la liste et la valeur dans new + cache button supprimer et on recharge avec les donnes sans la notion favorie
              this.ownerCt.ownerCt.getForm().items.item(0).clearValue();
              this.ownerCt.ownerCt.getForm().items.item(1).setValue('');
              this.ownerCt.ownerCt.buttons[3].hide();
              Ext.getCmp('request_user_variable_' + item.id).itype = 'request.def.affectation';
              Ext.getCmp('request_user_variable_' + item.id).reloadData();
            }
          });

        var itemActions = [];
        itemActions.push({
            xtype: 'listbox',
            fieldLabel: lang.xeon.mod_request_title.favori_list,
            itype: 'request.fav',
            listeners : 
            {
              select : function ()
              {
                // on change le itype de la grid pour que le makeGrid prend en compte qu'il y a un favori
                Ext.getCmp('request_user_variable_' + item.id).itype = 'request.def.selectFav_' + this.value;
                Ext.getCmp('request_user_variable_' + item.id).reloadData();
                // on affiche le bouton supprimer
                this.ownerCt.buttons[3].show();
              },
              blur : function()
              {                
                if(this.value == '')
                {
                  // on cache le bouton supprimer
                  this.ownerCt.buttons[3].hide();
                }
              }
            }
          },
          {
            fieldLabel: lang.xeon.mod_request_title.favori_title,
            name: 'favori'
          });

          if(Xeon.user.isMaster)
          {
            itemActions.push({
              xtype: 'listbox',
              fieldLabel: lang.xeon.mod_request_title.environment,
              itype: 'request.env',
              name: 'environment',
              value: data.db.environment_id,
              displayValue: data.db.environment_title
            });
          }
        ct.add(new Ext.ux.FormPanel({
          items: itemActions,
          title: 'Actions',
          buttons: itemsButtons
        }));


        var request_resultat = new Ext.ux.grid.GridPanel({
          id : 'request_resultat_'+item.id,
          itype: 'request.sql',
          title: lang.xeon.mod_request_title.result,
          paging:  true,
          quickSearch: true,
          search: false,
          reloadAfterEdit: false,
          exportPath: true,
          showIconRefresh: false,
          noRefreshToolbar: true,
          columns: [],
          listeners: {
            columnmove : function()
            {
              p = this;
              var params = {};
              var cols = this.getColumnModel().columns;
              Ext.each(cols , function(it, ind){
                if(ind > 0)
                {
                  params['data[columns]['+(ind - 1)+']'] = it.header;
                }
              });
              params['data[id_request]'] = item.id;
              params['cmd'] = 'xeon.saveData';
              params['item'] = 'request.col.saveOrderColonne';
             
              Ext.Ajax.request({
                params: params,
                before: function() {
                    p.bwrap.mask(lang.xeon.saving, 'x-mask-loading');
                },
                success: ct.success,
                callback: function(o, s, a)
                {
                  p.bwrap.unmask();
                }
              });
            }
          }
        });
        
        ct.add(request_resultat);
      }  
  	},

   makeRequestRoot: function(ct, item, data)
    {
        var items = [];
        items.push({
                header: lang.xeon.mod_request_title.title,
                dataIndex: 'title',
                width: 150,
                itemLink: ['request.req', 'id']
            },{
                header: lang.xeon.mod_request_title.category,
                dataIndex: 'category_title',
                width: 150
            },{
                header: lang.xeon.mod_request_title.description,
                dataIndex: 'description',
                width: 200
            });
        if (Xeon.user.isMaster)
        {
          items.push({
                  header: lang.xeon.mod_request_title.actif,
                  dataIndex: 'actif',
                  width: 60,
                  renderer: Xeon.renderStatus,
                  editor: new Ext.ux.form.ToggleBox()
              });
        }
        ct.add(new Ext.ux.grid.GridPanel({
            title: lang.xeon.mod_request_title.list_request,
            iconCls: 'i-page-white-stack',
            itype: 'request.req',
            rowId: 'id',
            paging:  true,
            quickSearch: true,
            search:engine.getSearchCmsItems(data),
            columns: items,
            quickSearch: true,
            search: false
        }));
      },

    makeLogRoot:function(ct, item, data)
    {
      var items = [];
      items.push({
            header: lang.xeon.mod_request_title.launched_date,
            dataIndex: 'date',
            width: 120
        },{
            header: lang.xeon.mod_request.mod_name,
            dataIndex: 'request_title',
            width: 150,
            itemLink: ['request.req', 'id_request']
        });
      if(Xeon.user.isMaster)
      {
        items.push(
        {
            header: lang.xeon.mod_request.mod_env,
            dataIndex: 'environment',
            width: 120
        },{
            header: lang.xeon.mod_request.user,
            dataIndex: 'user',
            width: 120
        });
      }
      items.push({
            header: lang.xeon.mod_request.variable,
            dataIndex: 'variable',
            width: 200
        },{
            header: lang.xeon.mod_request_title.sql,
            dataIndex: 'sql_final',
            width: 200
        });
      ct.add(new Ext.ux.grid.GridPanel({
        title: lang.xeon.mod_request_title.log,
        iconCls: 'i-page-white-stack',
        itype: 'request.log',
        rowId: 'id',
        paging:  true,
        quickSearch: true,
        search:engine.getSearchCmsItems(data),
        columns: items
      }));
    },


    makeEnvironmentRoot: function(ct, item, data)
    {
        ct.add(new Ext.ux.grid.GridPanel({
            title: lang.xeon.mod_request.list_env,
            iconCls: 'i-server',
            itype: 'request.env',
            rowId: 'id',
            enableDragDrop: true,
            search: true,
            columns: [{
                header: lang.xeon.title,
                dataIndex: 'title',
                //width: 100,
                editor: new Ext.form.TextField(),
                itemLink: ['request.env', 'id']
            },{
                header: lang.xeon.mod_request.host,
                dataIndex: 'host',
                //width: 100,
                editor: new Ext.form.TextArea()
            },{
                header: lang.xeon.mod_request.db,
                dataIndex: 'db',
                //width: 100,
                editor: new Ext.form.TextArea()
            },{
                header: lang.xeon.mod_request.user,
                dataIndex: 'user',
                //width: 100,
                editor: new Ext.form.TextArea()
            }]
        }));
    },

    makeEnvironmentForm: function(ct, item, data)
    {
        var title = '';

        if( !item.add ) {
          title = lang.xeon.mod_request.edit_env;
        }

        var items = [];

        items.push({
            fieldLabel: lang.xeon.title,
            allowBlank: false,
            name: 'title',
            value: data.db.title,
            displayValue: data.db.title
        },{
            fieldLabel: lang.xeon.mod_request_title.driver,
            allowBlank: false,
            name: 'driver',
            value: data.db.driver
        },{
            fieldLabel: lang.xeon.mod_request.host,
            allowBlank: false,
            name: 'host',
            value: data.db.host,
            displayValue: data.db.host
        },{
            fieldLabel: lang.xeon.mod_request.db,
            allowBlank: false,
            name: 'db',
            value: data.db.db,
            displayValue: data.db.db
        },{
            fieldLabel: lang.xeon.mod_request.user,
            allowBlank: false,
            name: 'user',
            value: data.db.user,
            displayValue: data.db.user
        },{
            fieldLabel: lang.xeon.mod_request.password,
            inputType: 'password',
            allowBlank: false,
            name: 'password',
            value: data.db.password,
            displayValue: data.db.password
        });

        ct.add(new Ext.ux.FormPanel({
            title: title,
            iconCls: ct.iconCls,
            items: items,
            reloadAfterEdit: true
        }));
    },

    renderYesorNo: function(v, m, r)
    {
        m.css = (v == 1) ? 'x-button i-accept' : 'x-button i-delete';
        m.css = (v == -1) ? 'x-button i-loading' : m.css;
    }
}