/*
 * Google Charts Wrapper
 * Extjs 3.0.0
 * Author: Aimad Harit
 */

// convert extjs store to google store with correct datatype
Ext.ux.GDataTableAdapter = function(config) {
    // hashmap to convert from Ext data types
    // datatypes
    var convert = {
        'auto': 'string',
        'string': 'string',
        'int': 'number',
        'float': 'number',
        'boolean': 'boolean',
        'date':'date'    
    };
      
    return {
        adapt: function(config) {
           // console.log(config.store);
            var store = config.store;//Ext.StoreMgr.lookup('statorder');
            //console.log(store);
            var tbl = new google.visualization.DataTable();
            var cols = config.columns.items;
            for (var i = 0; i < cols.length; i++) {
                var c = cols[i];
                var id = c.name;
                var f = store.fields.get(id);
                //console.log('add colums' + f.type + ' name=' +c.name);
                tbl.addColumn(convert[f.type], c.name);
            }
        
         var rs =  store.getRange();
         //console.log(rs);
            var data = [];
            for (var i = 0; i < store.totalLength; i++) {
                rs[i].json[0] = new Date(rs[i].json[0]); // fix issue here convert the string date given to a date object
                   //store.reader.jsonData.data[i][0] = new Date(store.reader.jsonData.data[i][0]); // fix issue here convert the string date given to a date object

                 //console.log(store.reader.jsonData.data);
                 tbl.addRow(rs[i].json);
            }
            return tbl;
        }
    };
      
}();


// extjs panel including google chart draw container
Ext.ux.GVisualizationPanel = Ext.extend(Ext.Panel, {
    // These are required by Google API
    // To add more visualizations you can extend
    // visualizationPkgs
    customPanel:false,
    chartType:'',
    orderBy:'',
    ct:undefined,
    iconCls:'i-chart-line',
    align:'center',
    visualizationAPI: 'visualization',
    visualizationAPIVer: '1',
    visualizationPkgs: {
        'areachart' : 'AreaChart',
        'intensitymap': 'IntensityMap',
        'orgchart': 'OrgChart',
        'linechart': 'LineChart',
        'gauge': 'Gauge',
        'scatterchart': 'ScatterChart',
        'piechart': 'PieChart'
    },
    
    /**
     * visualizationPkg {String} 
     * (Required) Valid values are intesitymap, orgchart, gauge and scatterchart
     * The error "Module: 'visualization' with package: '' not found!" will be
     * thrown if you do not use this configuration.
     */
    visualizationPkg: '',
   // bodyStyle:{"background-color":"white","padding-left":"5px","margin-bottom":"5px"},
    
    /**
     * html {String}
     * Initial html to show before loading the visualization
     */
    html: "<div id='stat1'>Chargements de vos statistiques...</div>",//<div id='control1' style='float:left;'>dd</div><div id='control2' style='float:left;margin-left:10px;'>ddd</div><div style='clear:both;float:left;' id='chart1'>ddd</div><div style='float: left;margin-left:10px;' id='chart2'>ddd</div></div>",
        //'Loading...',
    
    /**
     * store {Ext.data.Store/String}
     * Any valid instance of a store and/or storeId
     */    
    store: null,
    data:null,
    // Overriden methods
    initComponent: function()
    {
        
        /*this.tbar= [{
			text: 'Export',
			iconCls: 'i-page-excel',
			handler: this.reloadData,
			scope: this
		}];*/

        if (this.ct) 
        {
            this.ct.defaults= {
		frame: true,
		width: 740,
		collapsible: true,
		titleCollapse: true,
		hideCollapseTool: true,
		animCollapse: false,
		style: {
			margin: '20px'
		}};
            this.ct.doLayout();
        }
        
       
        if (typeof this.visualizationPkg === 'object') {
            Ext.apply(this.visualizationPkgs, this.visualizationPkg);            
            for (var key in this.visualizationPkg) {
                this.visualizationPkg = key;
                break;
            }
        }

        google.load(
            this.visualizationAPI,
            this.visualizationAPIVer,
            {
                packages: ['controls','corechart'],
                callback: this.onLoadCallback.createDelegate(this)
            }
        );        
       /* this.store = Ext.StoreMgr.lookup(this.store);*/
        Ext.ux.GVisualizationPanel.superclass.initComponent.call(this);
    },
    
    // custom methods
    onLoadCallback: function() {
        /*var tableCfg = {
            store: this.store,
            columns: this.store.fields
        };*/
        //this.datatable= Ext.ux.GDataTableAdapter.adapt(tableCfg);

        var p = this;
        var cls = this.visualizationPkgs[this.visualizationPkg];

        //this.body.update("<div id='"+this.id+"' style='height: 200px; width: 350px;'>hello</div>");

        this.body.update("<div id='stat1' style='float:left;height: 200px; width: 350px;margin:5px;border:solid #99BBE8 1px;'></div><div id='stat2' style='float:left;margin:5px;border:solid #99BBE8 1px;height: 200px; width: 350px;'></div><div id='stat3' style='float:left;margin:5px;border:solid #99BBE8 1px;width:350px;height:200px;'></div><div id='stat4' style='float:left;margin:5px;border:solid #99BBE8 1px;width:350px;height:200px;'></div>");
       // this.visualization = new google.visualization.AreaChart(document.getElementById('stat1'));
        this.bwrap.mask('Chargement de vos statistiques', 'x-mask-loading');
       /* var relaySelect = function() {
            this.fireEvent('select', this, this.visualization);
        };*/
        
     //google.visualization.events.addListener(this.visualization, 'select', relaySelect.createDelegate(this));

     //this.visualization.draw(this.datatable, Ext.apply({width: 400, height: 200,'title':'Evolution des commandes par origine/an',bleInteractivity:true}, this.visualizationCfg));

     new Ext.data.JsonStore({
				root: 'data',
				autoLoad: true,
                                baseParams:{cmd: 'xeon.makeGraph',item: 'shop.statcart'},
                                fields: [{name:'date', type:'date'}, {name:'cart', type: 'float'}],
				proxy: new Ext.data.HttpProxy({
					url: Ext.Ajax.url
				}),
                                listeners: {
                                load: {
                                    fn: function(store, records, options){
                                        var data = new google.visualization.DataTable();
                                        data.addColumn('string', 'Année');
                                        data.addColumn('number', 'Panier moyen');

                                        Ext.each(store.reader.jsonData.data, function(value){
                                            data.addRow(value);
                                        });

                                     var wrapper = new google.visualization.ChartWrapper({
                                            chartType: 'ColumnChart',
                                            dataTable: data,
                                            options: {
                                                'title': 'Panier moyen en euros',
                                                 vAxis: {
                                                    title: 'Montant en €uros',
                                                    titleTextStyle: {color: 'blue'},
                                                    format : '#,### €'
                                                    }
                                            },
                                            containerId: 'stat2'
                                            });
                                        wrapper.draw();
                                    }
                                }
                            }
			});

    new Ext.data.JsonStore({
				root: 'data',
				autoLoad: true,
                                baseParams:{cmd: 'xeon.makeGraph',item: 'shop.statsource'},
                                fields: [{name:'date', type:'date'}, {name:'source', type: 'float'}],
				proxy: new Ext.data.HttpProxy({
					url: Ext.Ajax.url
				}),
                                listeners: {
                                load: {
                                    fn: function(store, records, options){
                                     var wrapper = new google.visualization.ChartWrapper({
                                            chartType: 'PieChart',
                                            dataTable: store.reader.jsonData.data,
                                            options: {'title': 'Pourcentage de commande/origine'},
                                            containerId: 'stat3'
                                            });
                                        wrapper.draw();
                                    }
                                }
                            }
			});
                        
      new Ext.data.JsonStore({
				root: 'data',
				autoLoad: true,
                                baseParams:{cmd: 'xeon.makeGraph',item: 'shop.statpayment'},
                                fields: [{name:'title', type:'string'}, {name:'total', type: 'float'}],
				proxy: new Ext.data.HttpProxy({
					url: Ext.Ajax.url
				}),
                                listeners: {
                                load: {
                                    fn: function(store, records, options){
                                     var wrapper = new google.visualization.ChartWrapper({
                                            chartType: 'PieChart',
                                            dataTable: store.reader.jsonData.data,
                                            options: {'title': 'Pourcentage de commande/réglement'},
                                            containerId: 'stat4'
                                            });
                                        wrapper.draw();
                                    }
                                }
                            }
			});
                          new Ext.data.JsonStore({
				root: 'data',
				autoLoad: true,
                                baseParams:{cmd: 'xeon.makeGraph',item: 'shop.statorderbystatus'},
                                fields: [{name:'title', type:'string'}, {name:'total', type: 'float'}],
				proxy: new Ext.data.HttpProxy({
					url: Ext.Ajax.url
				}),
                                listeners: {
                                load: {
                                    fn: function(store, records, options){
                                     var wrapper = new google.visualization.ChartWrapper({
                                            chartType: 'PieChart',
                                            dataTable: store.reader.jsonData.data,
                                            options: {'title': 'Pourcentage de commande par etat (en %)','legend':'hello'},
                                            containerId: 'stat1'
                                            });
                                        wrapper.draw();
                                    }
                                }
                            }
			});
                    

                    
                         this.bwrap.unmask();
    }}),
Ext.reg('gvisualization', Ext.ux.GVisualizationPanel);


// extjs panel including google chart draw container
Ext.ux.GVisualizationPanelOrder = Ext.extend(Ext.Panel, {
    // These are required by Google API
    // To add more visualizations you can extend
    // visualizationPkgs
    /*tbar : new Ext.Toolbar({
				cls: 'x-datapanel-toolbar',
				renderTo: this.body.dom,
				items: [{
                                        text: 'Changer de date',
                                        iconCls: 'i-cog-edit',
                                        handler: function(){alert('lol');},
                                        scope: this
                                        }]
			}),*/
    tools: [{
		id: 'help',
		qtip: 'Evolutions des commandes validée(s) et traité sur la durée selectionné dans vos préferences',
		handler: function(e, t, p)
		{
		}
	}],
    orderBy:'',
    ct:undefined,
    test:'',
    iconCls:'i-chart-line',
    align:'center',
    visualizationAPI: 'visualization',
    visualizationAPIVer: '1',
    visualizationPkgs: {
        'areachart' : 'AreaChart',
        'intensitymap': 'IntensityMap',
        'orgchart': 'OrgChart',
        'linechart': 'LineChart',
        'gauge': 'Gauge',
        'scatterchart': 'ScatterChart',
        'piechart': 'PieChart'
    },

    /**
     * visualizationPkg {String}
     * (Required) Valid values are intesitymap, orgchart, gauge and scatterchart
     * The error "Module: 'visualization' with package: '' not found!" will be
     * thrown if you do not use this configuration.
     */
    visualizationPkg: '',
   // bodyStyle:{"background-color":"white","padding-left":"5px","margin-bottom":"5px"},

    /**
     * html {String}
     * Initial html to show before loading the visualization
     */
    html: "<div id='stat1'>Chargements de vos statistiques...</div>",//<div id='control1' style='float:left;'>dd</div><div id='control2' style='float:left;margin-left:10px;'>ddd</div><div style='clear:both;float:left;' id='chart1'>ddd</div><div style='float: left;margin-left:10px;' id='chart2'>ddd</div></div>",
        //'Loading...',

    /**
     * store {Ext.data.Store/String}
     * Any valid instance of a store and/or storeId
     */
    store: null,
    chart:null,
    data:null,
    // Overriden methods
    initComponent: function() {
        var p = this;
        this.ct.defaults= {
		frame: true,
		width: 740,
		collapsible: true,
		titleCollapse: true,
		hideCollapseTool: true,
		animCollapse: false,
		style: {
			margin: '20px'
		}};
        this.ct.doLayout();

        /* this.tbar= [{
			text: 'Export',
			iconCls: 'i-page-excel',
			handler: this.reloadData,
			scope: this
		}];*/
        
        if (typeof this.visualizationPkg === 'object') {
            Ext.apply(this.visualizationPkgs, this.visualizationPkg);
            for (var key in this.visualizationPkg) {
                this.visualizationPkg = key;
                break;
            }
        }

        google.load(
            this.visualizationAPI,
            this.visualizationAPIVer,
            {
                packages: ['controls','corechart','annotatedtimeline'],
                callback: this.onLoadCallback.createDelegate(this)
            }
        );
           
     
        Ext.ux.GVisualizationPanel.superclass.initComponent.call(this);
    },
    reloadData: function() {
        this.chart.draw(this.datatable, {displayAnnotations: true});
       // console.log(this.store);
    },
    // custom methods
    onLoadCallback: function() {
        var p = this;
        p.body.update("<div id='order' style='float:left;height: 350px; width: 720px;margin:1px;border:solid #99BBE8 1px;'></div>");
            
        p.bwrap.mask('Chargement de vos statistiques', 'x-mask-loading');
        
        this.store = new Ext.data.JsonStore({
				root: 'data',
                                storeId:'statorder',
				autoLoad: true,
                                autoSave:false,
                                batch:false,
                                baseParams:{cmd: 'xeon.makeGraph',item: 'shop.stat'},
                                fields: [
                                    {name:'date', type:'date'},
                                    {name:'vente web', type: 'float'},
                                    {name:'vente courrier', type:'float'},
                                    {name:'vente tel', type:'float'},
                                    {name:'vente magasin', type:'float'}
                                ],
				proxy: new Ext.data.HttpProxy({
					url: Ext.Ajax.url
				})
			});

         this.store.on('load', function(s){
              //console.log(s.getRange());
           var tableCfg = {
                store: s,
                columns: s.fields
            };
            p.datatable= Ext.ux.GDataTableAdapter.adapt(tableCfg);

            var cls = p.visualizationPkgs[p.visualizationPkg];
            
           p.bwrap.unmask();
            p.chart = new google.visualization.AnnotatedTimeLine(document.getElementById('order'));

            p.chart.draw(p.datatable, {displayAnnotations: true});
       
         });
}});

Ext.reg('gvisualization', Ext.ux.GVisualizationPanelOrder);


// extjs panel including google chart draw container
Ext.ux.GoogleChartPanel = Ext.extend(Ext.Panel, {
    ct:undefined,
    iconCls:'i-chart-line',
    align:'center',
    visualizationAPI: 'visualization',
    visualizationAPIVer: '1',
    visualizationPkgs: {
        'areachart' : 'AreaChart',
        'intensitymap': 'IntensityMap',
        'orgchart': 'OrgChart',
        'linechart': 'LineChart',
        'gauge': 'Gauge',
        'scatterchart': 'ScatterChart',
        'piechart': 'PieChart'
    },

    /**
     * visualizationPkg {String}
     * (Required) Valid values are intesitymap, orgchart, gauge and scatterchart
     * The error "Module: 'visualization' with package: '' not found!" will be
     * thrown if you do not use this configuration.
     */
    visualizationPkg: '',
   // bodyStyle:{"background-color":"white","padding-left":"5px","margin-bottom":"5px"},

    /**
     * html {String}
     * Initial html to show before loading the visualization
     */
    html: "<div id='statproduct1' style='float:left;height: 200px; width: 700px;margin:1px;border:solid #99BBE8 1px;margin: 0 auto;'></div>",//<div id='control1' style='float:left;'>dd</div><div id='control2' style='float:left;margin-left:10px;'>ddd</div><div style='clear:both;float:left;' id='chart1'>ddd</div><div style='float: left;margin-left:10px;' id='chart2'>ddd</div></div>",
        //'Loading...',

    /**
     * store {Ext.data.Store/String}
     * Any valid instance of a store and/or storeId
     */
    store: null,
    data:null,
    // Overriden methods
    initComponent: function()
    {

        google.load(
            this.visualizationAPI,
            this.visualizationAPIVer,
            {
                packages: ['controls','corechart'],
                callback: this.onLoadCallback.createDelegate(this)
            }
        );

        Ext.ux.GVisualizationPanel.superclass.initComponent.call(this);
    },

    // custom methods
    onLoadCallback: function() {

        var p = this;

        if (this.xview) this.body.update("<div id='statproduct1' style='float:left;height: 200px; width: 350px;margin:1px;border:solid #99BBE8 1px;'></div><div id='statproduct2' style='float:left;height: 200px; width: 350px;margin:1px;border:solid #99BBE8 1px;'></div>");

        p.bwrap.mask('Chargement de vos statistiques', 'x-mask-loading');
  
      var chartType  = this.chartType;
     
       var data = new google.visualization.DataTable();
       Ext.each(this.columns, function(value){
               data.addColumn(value[0], value[1]);
       });
                         
                         new Ext.data.JsonStore({
				root: 'data',
				autoLoad: true,
                                baseParams:{cmd: 'xeon.makeGraph',item: this.itype},
                                fields: [{name:'title', type:'string'}, {name:'total', type: 'float'}],
				proxy: new Ext.data.HttpProxy({
					url: Ext.Ajax.url
				}),
                                listeners: {
                                load: {
                                    fn: function(store, records, options){
                                        
                                        Ext.each(store.reader.jsonData.data, function(value){
                                            data.addRow(value);
                                        });
                                        
                                        wrapper = new google.visualization.ChartWrapper({
                                            chartType: chartType,
                                            dataTable: data,
                                            options: {'title': p.title,'legend':'hello'},
                                            containerId: 'statproduct1'
                                            });
                                        wrapper.draw();
                                    }
                                }
                            }
			});
      if (this.xview)
          {
              var xview = this.xview;
              var data2 = new google.visualization.DataTable();
              Ext.each(this.columns, function(value){
                    data2.addColumn(value[0], value[1]);
                });

             
              new Ext.data.JsonStore({
				root: 'data',
				autoLoad: true,
                                baseParams:{cmd: 'xeon.makeGraph',item: xview.itype},
                                fields: [{name:'title', type:'string'}, {name:'total', type: 'float'}],
				proxy: new Ext.data.HttpProxy({
					url: Ext.Ajax.url
				}),
                                listeners: {
                                load: {
                                    fn: function(store, records, options){

                                        Ext.each(store.reader.jsonData.data, function(value){
                                            data2.addRow(value);
                                        });

                                        wrapper2 = new google.visualization.ChartWrapper({
                                            chartType: xview.chartType,
                                            dataTable: data2,
                                            options: {
                                                'title': xview.title,
                                                tooltip: {text:'Ventes'}
                                            },
                                            containerId: 'statproduct2'
                                            });
                                        wrapper2.draw();
                                    }
                                }
                            }
			});
          }
      this.bwrap.unmask();
    }}),
Ext.reg('xvisualization', Ext.ux.GoogleChartPanel);