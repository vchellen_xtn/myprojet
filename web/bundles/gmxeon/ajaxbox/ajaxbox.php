<?php

// Envoi de la r�ponse AJAX
if (isset($_POST["test"]))
{
	echo "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Cras sed lacus. Quisque suscipit lacus. Morbi condimentum. Vestibulum viverra felis non nisi sollicitudin sollicitudin. Integer ante. Nullam id enim. Vestibulum ultrices placerat diam. Phasellus scelerisque massa eget massa. Curabitur sapien metus, venenatis a, accumsan sed, aliquam sit amet, lorem. Quisque vulputate, libero et venenatis nonummy, nulla erat faucibus nisl, ut pretium arcu justo sit amet diam. Proin volutpat vehicula erat. Sed tempor purus pulvinar magna. Suspendisse lobortis, tortor sagittis ornare commodo, tortor felis condimentum tellus, convallis ornare nunc mi et nunc.<br /><br />";
	echo "Donec id dolor eget odio ullamcorper placerat. Donec varius luctus ligula. Morbi eros arcu, aliquam vel, ultricies non, pharetra non, nibh. Fusce ut neque. Mauris mauris. Etiam suscipit, nisl ut convallis interdum, pede sapien gravida nibh, ac venenatis lorem neque sit amet lorem. Quisque quis metus ut tellus aliquam auctor. Integer id pede a velit ultrices pharetra. Aenean tincidunt odio eu est. Nam quis eros et urna tempor pellentesque. <br /><br />";
	echo "<a href=\"javascript:hideBox();\">Fermer</a>";
	exit();
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Ajaxbox</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<style type="text/css">

body {
	font-family: Arial, sans-serif;
	font-size: 12px;
}

div#ajaxbox {
	background-color: #ffffff;
	width: 600px;
	padding: 10px;
}

</style>
<script type="text/javascript" src="/src/prototype/prototype.js"></script>
<script type="text/javascript" src="/src/ajaxbox/ajaxbox.js"></script>
<script type="text/javascript">

// Initialisation de la box
window.onload = function()
{
	box = new Ajaxbox();
}

// Affichage de la box
function showBox()
{
	box.show();
	
	// Requ�te AJAX
	new Ajax.Request('ajaxbox.php',
	{
		method: 'post',
		parameters: 'test=1',
		onSuccess: function(ajax)
		{
			// Mise � jour de la box
			box.update(ajax.responseText);
		}
	});
}

// Fermeture de la box
function hideBox()
{
	box.hide();
}

</script>
</head>
<body>
	<h1>Ajaxbox</h1>
	<div>
		Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent sem nisl, eleifend eu, sagittis tincidunt, porttitor non, quam. Vivamus nec odio sed diam faucibus mattis. Donec non neque vel massa volutpat tempus. Ut dictum consequat est. Fusce ante risus, commodo ut, bibendum et, iaculis sed, odio. Vestibulum in augue. Nulla fringilla ante pulvinar nulla. Pellentesque interdum imperdiet augue. Sed ullamcorper nonummy quam. Phasellus scelerisque quam id tellus.<br /><br />
		Morbi quis elit. Integer euismod sem ac odio. Nunc ac ante non leo semper tincidunt. In sed urna in est mollis lacinia. Sed commodo, lorem eu elementum pretium, nibh nunc ultricies nibh, non euismod sem nisi tempus turpis. Integer non est ullamcorper turpis ornare aliquet. Fusce lobortis, arcu a dignissim pharetra, justo erat convallis nisl, id porttitor urna nunc congue nisi. Quisque tincidunt, nunc eget imperdiet bibendum, enim elit facilisis nulla, condimentum imperdiet odio leo non ligula. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed adipiscing sollicitudin lectus. Phasellus ac ipsum eget urna imperdiet nonummy. Cras elit sem, porta eu, volutpat ac, ornare at, felis. Nam laoreet tellus vel mauris fringilla pharetra. Sed id ligula. Praesent vel risus sed nisl consectetuer pulvinar. Nam ullamcorper erat sit amet sapien. Mauris felis nibh, rhoncus nec, vestibulum suscipit, rutrum quis, risus. Donec in justo.<br /><br />
		Ut a urna sit amet tellus bibendum hendrerit. Nullam ullamcorper, erat sit amet condimentum tincidunt, urna enim mattis velit, non egestas nunc orci ut libero. Curabitur sodales, massa id ultricies viverra, mauris eros imperdiet enim, nec tincidunt felis dui posuere justo. Pellentesque sapien ante, congue sit amet, tempus quis, dignissim et, orci. Nunc eleifend nunc eget nunc. Vivamus congue. Curabitur malesuada mauris ut nulla. Ut magna felis, bibendum eu, laoreet non, fringilla et, lacus. Integer iaculis pretium enim. Fusce ut est. Etiam porttitor feugiat nibh. Pellentesque malesuada arcu vitae dui. Duis ornare sollicitudin elit. Etiam urna massa, faucibus sit amet, feugiat eget, aliquam et, ante. Fusce eu sem. Fusce vel nibh ac ipsum pretium congue. Maecenas id eros nec ante ullamcorper volutpat.<br /><br />
		<a href="javascript:showBox();">Afficher</a>
	</div>
</body>
</html>