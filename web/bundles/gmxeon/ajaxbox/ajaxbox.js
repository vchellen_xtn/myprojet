
//
// Syst�me de fen�tre Ajaxbox v1.2.1
// Auteur : Sylvain DUROZARD
//

Ajaxbox = function()
{
	this.init();
}

Ajaxbox.prototype =
{
	init: function()
	{
		this.load('/src/ajaxbox/ajaxbox.css');
		if (typeof document.body.style.maxHeight == 'undefined') this.load('/src/ajaxbox/ajaxbox-ie6.css');

		this.overlay = document.createElement('DIV');
		this.overlay.id = 'ajaxbox-overlay';
		this.box = document.createElement('DIV');
		this.box.id = 'ajaxbox';

		document.body.appendChild(this.box);
		document.body.appendChild(this.overlay);
	},

	load: function(file)
	{
		var css = document.createElement('link');
		css.type = 'text/css';
		css.rel = 'stylesheet';
		css.media = 'screen';
		css.href = file;
		document.getElementsByTagName("head")[0].appendChild(css);
	},

	show: function()
	{
		this.overlay.style.display = 'block';
	},

	update: function(data)
	{
		this.box.style.visibility = 'hidden';
		this.box.style.display = 'block';
		if (data) this.box.innerHTML = data;
		this.box.style.marginTop = '-'+Math.round(this.box.offsetHeight/2)+'px';
		this.box.style.marginLeft = '-'+Math.round(this.box.offsetWidth/2)+'px';
		this.box.style.visibility = 'visible';
	},

	hide: function()
	{
		this.box.style.display = 'none';
		this.overlay.style.display = 'none';
		this.box.innerHTML = '';
	}
}

