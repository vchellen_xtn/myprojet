<?php

$pack = array('flags', 'silk', 'zak');

// Cr�ation des fichiers CSS pour chaque pack
if (isset($_GET['make']))
{
	foreach ($pack as $id)
	{
		if ($dp = opendir('../icons/'.$id))
		{
			$fp = fopen('style/icons-'.$id.'.css', 'w');
			
			while (($item = readdir($dp)) !== false)
			{
				if (is_file('../icons/'.$id.'/'.$item))
				{
					$i = explode('.', $item);
					$class = 'i-'.str_replace('_', '-', $i[0]);
					fputs($fp, ".{$class}{background-image:url(../../icons/{$id}/{$item})!important;}\n");
				}
			}
			
			fclose($fp);
			closedir($dp);
		}
	}
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Liste des icones</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<?php

foreach ($pack as $id) {
	echo '<link rel="stylesheet" type="text/css" href="/src/xeon/style/icons-'.$id.'.css" />';
}

?>
<style type="text/css">

body {
	font-family: Verdana, sans-serif;
	font-size: 12px;
	margin: 30px;
}

h1 {
	font-size: 24px;
	font-weight: normal;
	border-bottom: 1px solid #cccccc;
}

p {
	background-repeat: no-repeat;
	background-position: top left;
	padding-left: 20px;
	height: 20px;
	margin: 0;
}

</style>
</head>
<body>
<?php

// Affichage des icones de chaque pack
foreach ($pack as $id)
{
	echo '<h1>'.$id.'</h1>';
	
	$data = file_get_contents('style/icons-'.$id.'.css');
	preg_match_all("#\.(i-[a-z0-9-]+)#", $data, $match);
	
	foreach ($match[1] as $i) {
		echo "<p class=\"{$i}\">{$i}</p>\n";
	}
}

?>
</body>
</html>