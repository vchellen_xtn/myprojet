
//
// Interface Xeon v1.3.0
// Auteur : Sylvain Durozard
Ext.BLANK_IMAGE_URL = '/bundles/gmxeon/extjs/resources/images/default/s.gif';
Ext.Ajax.method = 'POST';
Ext.Ajax.url = location.pathname;
Ext.Container.prototype.forceLayout = true;
Ext.MessageBox.minWidth = 300;
Ext.form.FormPanel.prototype.defaultType = 'textfield';
Ext.form.FieldSet.prototype.defaultType = 'textfield';
Ext.form.Field.prototype.width = 300;
Ext.form.Field.prototype.msgTarget = 'side';
Ext.form.Field.prototype.labelSeparator = ' :';
Ext.form.TextArea.prototype.height = 80;
Ext.Window.prototype.constrain = true;
Ext.QuickTips.init();

Ext.override(Ext.form.BasicForm, {
    applyDataArray: function()
    {
        var f = this.el.dom.elements;

        for (var i=0; i<f.length; i++)
        {
            if (f[i].name && (f[i].name.indexOf('data[') != 0))
            {
                var a = f[i].name.indexOf('[');
                f[i].name = (a != -1) ? 'data['+f[i].name.substr(0, a)+']'+f[i].name.substr(a) : 'data['+f[i].name+']';
            }
        }
    }
});

Ext.Ajax.on('beforerequest', function(c, o)
{
    if (o.before) {
        return o.before.call(this, o);
    }
});

Ext.Ajax.on('requestcomplete', function(c, a, o)
{
    var status = a.getResponseHeader('X-Status');
    a.json = Ext.decode(a.responseText) || false;
    a.error = (status != 100);
    // pour afficher les messages flash
    if(Ext.getCmp('xeonMessageFlash'))
    {
        if(a.json.messageFlash != undefined)
        {
            Ext.getCmp('xeonMessageFlash').body.update('<h2>' + a.json.messageFlash + '</h2>');
        }
    }
    if (a.error)
    {
        Xeon.user.request = Ext.apply({}, o);
        o.success = false;
    }

    if (status == 101) Xeon.signIn();
    if (status == 102) Xeon.setLink();
    if (status == 103) Xeon.showError(a.json.title, a.json.text);

    if (o.after) {
          
        o.after.call(this, o, true, a);
    }
});

Xeon = {
    ui: {},
    user: {},
    module: {},
    plugin: {},
    activeGrid: false, // FIXME
    version: '2.0',
        
    init: function()
    {
        if (Xeon.ui.view)
        {
            Xeon.ui.view.destroy();
            Xeon.deleteCookie('tree');
            Xeon.deleteCookie('item');
        }

        Ext.Ajax.request({
            params: {
                cmd: 'xeon.init'
            },
            success: function(a)
            {
                Xeon.user = a.json.user;
                var tree = Xeon.getCookie('tree');
                var item = Xeon.getCookie('item');

                Xeon.ui.tree = new Ext.Panel({
                    region: 'west',
                    layout: 'accordion',
                    collapsible: true,
                    collapseMode: 'mini',
                    animCollapse: false,
                    split: true,
                    width: 240,
                    minSize: 240,
                    maxSize: 480,
                    margins: '5 0 5 5'
                });
                var itemsCenter = [];
                itemsCenter.push({
                    frame: false,
                    border: false,
                    html: '<h2>'+lang.xeon.home+'</h2>'
                },{
                    xtype: 'infopanel',
                    title: lang.xeon.welcome,
                    iconCls: 'i-information',
                    html: a.json.welcome
                });
                if(a.json.showInformationPanel == true)
                {
                    if(Xeon.user.isMaster == true)
                    {
                        itemsCenter.push(
                        {
                            xtype: 'formpanel',
                            items:
                            {
                                xtype: 'editorpanel',
                                name: 'htmlContent',
                                title: lang.xeon.info,
                                value : a.json.htmlHomeInformation
                            },
                            buttons : [{
                                text: lang.xeon.edit,
                                handler: function()
                                {
                                    var p = this.ownerCt.ownerCt;
                                    var editor = p.getForm().findField('htmlContent');
                                    var html = editor.getValue();
                                    var params = {};
                                    params['cmd'] = 'xeon.saveHomeInformation';
                                    params['html'] = html;
                                    Ext.Ajax.request({
                                      params: params,
                                      before: function() {
                                          p.bwrap.mask(lang.xeon.saving, 'x-mask-loading');
                                      },
                                      callback: function(o, s, a)
                                      {
                                        p.bwrap.unmask();
                                      }
                                    });
                                }
                            }]
                        });
                    }
                    else
                    {
                        itemsCenter.push({
                            frame: false,
                            border: false,
                            html: '<h2>'+lang.xeon.info+'</h2><br/><div>'+a.json.htmlHomeInformation+'</div>'
                        });
                    }
                }
                Xeon.ui.center = new Ext.TabPanel({
                    region: 'center',
                    plain: true,
                    enableTabScroll: true,
                    deferredRender: false,
                    activeTab: 0,
                    tabWidth: 180,
                    minTabWidth: 180,
                    resizeTabs: true,
                    margins: '5 5 5 0',
                    items: new Ext.ux.xeon.CenterPanel({
                        itemId: 'xeon.home',
                        title: lang.xeon.home,
                        iconCls: 'i-house',
                        closable: false,
                        items: itemsCenter
                    })
                });

                //console.log(Xeon.ui.center);
                                
                Xeon.ui.center.on('tabchange', function(p, t)
                {
                    Xeon.setCookie('item', t.itemId);
                    var tree = Xeon.ui.tree.layout.activeItem;
                    
                    if (tree)
                    {
                        //console.log(tree);
                        tree.selModel.clearSelections();
                        var n = tree.getNodeById(t.itemId);
                        
                        if (n)
                        {
                            tree.selModel.select(n);
                            n.ensureVisible();
                        }
                    }
                });

                Xeon.ui.center.on('contextmenu', function(p, t, e)
                {
                    if (!p.contextMenu)
                    {
                        p.contextMenu = new Ext.menu.Menu([{
                            text: lang.xeon.tab_reload,
                            iconCls: 'i-refresh',
                            handler: function() {
                                p.getActiveTab().reloadItem();
                            }
                        },{
                            text: lang.xeon.tab_close_others,
                            iconCls: 'i-tab-delete',
                            handler: function()
                            {
                                p.items.each(function(tab)
                                {
                                    if (tab.closable && tab != p.getActiveTab()) {
                                        p.remove(tab);
                                    }
                                });
                            }
                        },{
                            text: lang.xeon.tab_close_all,
                            iconCls: 'i-cross',
                            handler: function()
                            {
                                p.items.each(function(tab)
                                {
                                    if (tab.closable) {
                                        p.remove(tab);
                                    }
                                });
                            }
                        }]);
                    }

                    var m = p.contextMenu;
                    var closeOthers = false;
                    var closeAll = false;

                    p.items.each(function()
                    {
                        if (this.closable && this != t)
                        {
                            closeOthers = true;
                            return false;
                        }
                    });

                    p.items.each(function()
                    {
                        if (this.closable)
                        {
                            closeAll = true;
                            return false;
                        }
                    });

                    m.items.get(0).setDisabled(!t.closable);
                    m.items.get(1).setDisabled(!closeOthers);
                    m.items.get(2).setDisabled(!closeAll);

                    p.setActiveTab(t);
                    p.contextMenu.showAt(e.getPoint());
                });

                Xeon.ui.header = new Ext.Panel({
                    region: 'north',
                    html: '<h1>'+a.json.title+'</h1>'
                //html: '<h1>'+a.json.title+' <img src="'+Ext.BLANK_IMAGE_URL+'" title="'+a.json.lang[0]+'" class="'+a.json.lang[1]+'" /></h1>'
                });

                var menu = [];

                Ext.each(a.json.mod_menu, function(item)
                {
                    item.handler = function() {
                        Xeon.loadModule(this.itemId, 0);
                    };

                    if (menu.length != 0) menu.push('-');
                    menu.push(item);
                });

                menu.push('->');

                var options = [{
                    text: lang.xeon.domain,
                    iconCls: 'i-house',
                    menu: new Ext.menu.Menu({
                        items: a.json.domain_menu,
                        listeners: {
                            'itemclick': Xeon.setLang
                        }
                    })
                },{
                    text: lang.xeon.site,
                    iconCls: 'i-world',
                    menu: new Ext.menu.Menu({
                        items: a.json.site_menu,
                        listeners: {
                            'itemclick': Xeon.setSite
                        }
                    })
                }];
                
                if (a.json.multi_btq_menu){
                    options.push({
                    text: lang.xeon.multi_btq,
                    iconCls: 'i-map',
                    menu: new Ext.menu.Menu({
                        items: a.json.multi_btq_menu,
                        listeners: {
                            'itemclick': Xeon.setBtq
                        }
                    })});
                }
                
                if (Xeon.user.isMaster)
                {
                    options.push({
                        text: lang.xeon.config,
                        iconCls: 'i-wrench',
                        handler: function() {
                        //console.log('call loadModule line 278');
                            Xeon.loadModule('xeon', 0);
                        }
                    });
                }

                options.push('-',{
                    text: lang.xeon.help,
                    iconCls: 'i-help',
                    handler: function() {
                        window.open('http://aide.goldenmarket.fr/');
                    }
                },{
                    text: lang.xeon.about,
                    iconCls: 'i-information',
                    handler: Xeon.showAbout
                },'-',{
                    text: lang.xeon.sign_out,
                    iconCls: 'i-exit',
                    handler: Xeon.signOut
                });

                menu.push({
                    xtype:'panel',
                    id: 'xeonMessageFlash',
                    border: false,
                    bodyStyle: 'background: transparent; color: red',
                    padding: '0px 10px'
                },
                {
                    xtype: 'listbox',
                    emptyText: lang.xeon.quick_search,
                    hideTrigger: true,
                    width: 120,
                    listWidth: 'auto',
                    listClass: 'x-finder',
                    itype: 'xeon.fnd',
                    listeners: {
                        select: function(c, r) {
                            c.clear();
                            Xeon.loadItem(r.data.v);
                        }
                    }
                },{
                    iconCls: 'i-cog',
                    tooltip: lang.xeon.options,
                    menu: new Ext.menu.Menu(options)
                });

                Xeon.ui.menu = new Ext.Panel({
                    region: 'center',
                    layout: 'border',
                    tbar: menu,
                    border: false,
                    items: [Xeon.ui.tree, Xeon.ui.center]
                });

                Xeon.ui.view = new Ext.Viewport({
                    layout: 'border',
                    items: [Xeon.ui.header, Xeon.ui.menu]
                });

                if (tree != null) {
                        //console.log('call loadModule line 334');
                        //console.log(tree);
                    Xeon.loadModule(Xeon.getId(tree).module, tree);
                } else if (a.json.mod_menu.length != 0) {
                        //console.log('call loadModule line 337');
                    Xeon.loadModule(a.json.mod_menu[0].itemId, 0);
                } else if (Xeon.user.isMaster) {
                        //console.log('call loadModule line 340');
                    Xeon.loadModule('xeon', 0);
                }

                if (item != null && item != 'xeon.home') {
                    //console.log('!xeon.home: '+ item);
                    Xeon.loadItem(item);
                }
            }
        });
    },

    signIn: function()
    {
        //console.log('test');
        var fp = new Ext.FormPanel({
            cls: 'x-formpanel',
            frame: true,
            border: false,
            //autoHeight: true,
            labelWidth: 110,
            buttonAlign: 'center',
            items: [{
                fieldLabel: lang.xeon.login,
                width: 130,
                name: 'login',
                value: Xeon.user.login
            },{
                fieldLabel: lang.xeon.password,
                width: 130,
                inputType: 'password',
                name: 'password'
            }],
            buttons: [{
                text: lang.xeon.ok,
                handler: function() {
                    fp.submit();
                }
            }],
            keys: [{
                key: [10, 13],
                handler: function() {
                    fp.submit();
                }
            }],

            submit: function()
            {
                Ext.Ajax.request({
                    form: fp.form.el.dom,
                    params: {
                        cmd: 'xeon.signIn'
                    },
                    before: function() {
                        fp.bwrap.mask(lang.xeon.please_wait, 'x-mask-loading');
                    },
                    success: function(a)
                    {
                        if (a.json.id !== false)
                        {
                            fp.ownerCt.close();

                            if (Xeon.user.id != undefined && Xeon.user.id != a.json.id) {
                                Xeon.init();
                            } else {
                                Ext.Ajax.request(Xeon.user.request);
                            }
                        }
                        else
                        {
                            fp.focusField();
                        }
                    },
                    callback: function() {
                        fp.bwrap.unmask();
                    }
                });
            },

            focusField: function()
            {
                var login = fp.items.get(0);
                var pass = fp.items.get(1);

                pass.setValue('');

                /*login.on('blur', function() {
                    //console.log('blur!!');
                });*/

                if (login.getValue() == '') login.focus(); // bug focus
                else pass.focus();
            }
        });

        var win = new Ext.Window({
            layout: 'fit',
            title: lang.xeon.sign_in,
            iconCls: 'i-key',
            resizable: false,
            closable: false,
            border: false,
            modal: true,
            width: 290,
            /*tools: [{
                id: 'help',
                qtip: 'Mot de passe perdu',
                handler: function() {
                    alert('mot de passe perdu ?');
                }
            }],*/
            items: fp
        });

        win.show(false, fp.focusField);
    },

    signOut: function()
    {
        location.href = location.pathname + 'logout';
        /*Ext.Ajax.request({
            url : location.pathname + 'logout',
            params: {
                cmd: 'xeon.signOut'
            },
            before: function() {
                Xeon.ui.view.el.mask(lang.xeon.loading, 'x-mask-loading');
            },
            success: function()
            {
                Xeon.ui.view.el.unmask();
                location.href = location.pathname;
            }
        });*/
    },

    setLang: function(b, e)
    {
        Ext.Ajax.request({
            params: {
                cmd: 'xeon.setLang',
                lang: b.itemId
            },
            before: function() {
                Xeon.ui.view.el.mask(lang.xeon.loading, 'x-mask-loading');
            },
            success: function(r)
            {
                Xeon.ui.view.el.unmask();
                Xeon.init();
            }
        });
    },

    setSite: function(b, e)
    {
        Ext.Ajax.request({
            params: {
                cmd: 'xeon.setSite',
                site: b.itemId
            },
            before: function() {
                Xeon.ui.view.el.mask(lang.xeon.loading, 'x-mask-loading');
            },
            success: function(r)
            {
                Xeon.ui.view.el.unmask();
                Xeon.init();
            }
        });
    },
    
    setBtq: function(b, e)
    {
        Ext.Ajax.request({
            params: {
                cmd: 'xeon.setBtq',
                btq: b.itemId
            },
            before: function() {
                Xeon.ui.view.el.mask(lang.xeon.loading, 'x-mask-loading');
            },
            success: function(r)
            {
                Xeon.ui.view.el.unmask();
                Xeon.init();
            }
        });
    },
    
    setLink: function()
    {
        new Ext.ux.Window({
            title: lang.xeon.database,
            iconCls: 'i-database',
            closable: false,
            items: new Ext.ux.FormPanel({
                params: {
                    cmd: 'xeon.saveDatabaseInfo'
                },
                success: function(r) {
                    Xeon.init();
                },
                items: [{
                    fieldLabel: lang.xeon.server,
                    allowBlank: false,
                    name: 'server'
                },{
                    fieldLabel: lang.xeon.login,
                    allowBlank: false,
                    name: 'username'
                },{
                    fieldLabel: lang.xeon.password,
                    allowBlank: false,
                    name: 'password'
                },{
                    fieldLabel: lang.xeon.database,
                    allowBlank: false,
                    name: 'database'
                }]
            })
        }).show();
    },

    showError: function(title, text)
    {
        Ext.Msg.show({
            title: title,
            msg: text,
            icon: Ext.MessageBox.WARNING,
            buttons: Ext.MessageBox.OK
        });
    },

    showAbout: function()
    {
        var link = function(text, domain) {
            return '<a href="http://'+domain+'/" class="x-link" target="_blank">'+text+'</a>';
        }

        new Ext.Window({
            title: lang.xeon.about,
            iconCls: 'i-information',
            resizable: false,
            border: false,
            modal: true,
            width: 300,
            items: new Ext.Panel({
                frame: true,
                height: 123,
                bodyStyle: {
                    padding: '10px'
                },
                html:   '<b>Xeon Beta</b> '+Xeon.version+
                '<br>Portage sur Symfony2 par <a href="http://www.linkedin.com/in/xavierherriot">Xavier HERRIOT</a><br />' +
                '&copy; '+link('GoldenMarket', 'www.goldenmarket.fr')+'<br /><br />'+
                link('Ext JS', 'www.extjs.com')+' '+Ext.version+'<br />'+
                link('TinyMCE', 'tinymce.moxiecode.com')+' '+tinymce.majorVersion+'.'+tinymce.minorVersion+'<br />'+
                link('SWFUpload', 'www.swfupload.org')+' '+SWFUpload.version
            })
        }).show();
    },

    loadModule: function(id, tree)
    {
        //console.log('loadModule: '+ id + ' - ' + tree);
        if (Xeon.ui.tree.bwrap.isMasked()) return;
        if (Xeon.ui.tree.collapsed) Xeon.ui.tree.expand();

        Ext.Ajax.request({
            params: {
                cmd: 'xeon.makeModule',
                module: id
            },
            before: function() {
                Xeon.ui.tree.bwrap.mask(lang.xeon.loading, 'x-mask-loading');
            },
            success: function(a)
            {
                //console.log('load module success: '+ id);
                Xeon.ui.tree.items.each(function(item) {
                    Xeon.ui.tree.remove(item);
                });

                Ext.each(a.json, function(t)
                {
                    Xeon.ui.menu.topToolbar.items.each(function(b)
                    {
                        if (b.itemId) {
                            b.toggle(b.itemId == id);
                        }
                    });
                    
                    t.root.id = t.root.item;

                    Xeon.ui.tree.add(new Ext.ux.xeon.TreePanel({
                        iconCls:t.iconCls,
                        itemId: t.item,
                        title: t.title,
                        tbar: t.toolbar,
                        root: t.root,
                        enableDD: t.dragDrop,
                        sort: t.sort
                    }));
                });

                Xeon.ui.tree.doLayout();

                var tab = Xeon.ui.tree.getComponent(tree);
                if (tab) tab.expand();
            },
            callback: function() {
                Xeon.ui.tree.bwrap.unmask();
            }
        });
    },
    loadItem: function(id, parent, grid)
    {
        var item = Xeon.getId(id);

        if (item.add)
        {
            if (grid) Xeon.activeGrid = grid;
            else Xeon.activeGrid = false;

            Ext.Ajax.request({
                params: {
                    cmd: 'xeon.makeForm',
                    item: id,
                    parent: parent
                },
                before: function() {
                    Xeon.ui.view.el.mask(lang.xeon.loading, 'x-mask-loading');
                },
                success: function(a)
                {
                    var win = new Ext.ux.Window({
                        itemId: id,
                        title: a.json.title,
                        iconCls: a.json.icon
                    });

                    Xeon.callback(item.module, a.json.callback, win, item, a.json);
                    win.show();
                },
                callback: function() {
                    Xeon.ui.view.el.unmask();
                }
            });
        }
        else
        {
            var tab = Xeon.ui.center.getComponent(id);

            if (tab)
            {
                Xeon.ui.center.setActiveTab(tab);
                return;
            }

            if (Xeon.ui.center.items.length > 10)
            {
                Ext.Msg.alert(lang.xeon.error, lang.xeon.tab_max);
                return;
            }

            tab = Xeon.ui.center.add(new Ext.ux.xeon.CenterPanel({
                itemId: id
            }));
                       
                      
            tab.makeForm();
        }
    },

    copyItem: function(id)
    {
        var item = Xeon.getId(id);

        /*if (grid) Xeon.activeGrid = grid;
        else Xeon.activeGrid = false;*/

        Ext.Ajax.request({
            params: {
                cmd: 'xeon.makeCopy',
                item: id
            },
            before: function() {
                Xeon.ui.view.el.mask(lang.xeon.loading, 'x-mask-loading');
            },
            success: function(a)
            {
                var win = new Ext.ux.Window({
                    itemId: id,
                    title: a.json.title,
                    iconCls: 'i-page-white-copy'
                });

                Xeon.callback(item.module, a.json.callback, win, item, a.json);
                win.show();
            },
            callback: function() {
                Xeon.ui.view.el.unmask();
            }
        });
    },
    callpersonalisedCommand: function(act,id,module){
        
        
        Xeon.callback(module,act,id);
    },
    addItem: function(item, a)
    {
        if (a.json.tree)
        {
            var tree = Xeon.ui.tree.getComponent(a.json.tree);
            if (tree) tree.root.reload();
        }

        if (a.json.item)
        {
            //console.log(a.json);
            if (Xeon.activeGrid ) {
                Xeon.activeGrid.store.load();
                Xeon.loadItem(a.json.item);
            }
            else {
                      
                Xeon.loadItem(a.json.item);
            }
        }

        if (a.json.title)
        {
            var tab = Xeon.ui.center.getComponent(item);

            if (tab)
            {
                tab.setTitle(a.json.title);
                tab.getComponent(0).body.update('<h2>'+a.json.title+'</h2>');
            }
        }
    },

    callback: function(m, f, ct, item, data)
    {
        if (Xeon.module[m] && typeof Xeon.module[m][f] == 'function') {
            Xeon.module[m][f](ct, item, data);
        }

        if (Xeon.plugin[m] && typeof Xeon.plugin[m][f] == 'function') {
            Xeon.plugin[m][f](ct, item, data);
        }
    },

    getFile: function() {
        location.href = location.pathname+'?file';
    },
    getFilePDF: function()
    {
        location.href = location.pathname+'?filePDF';
    },
    getFileXML: function()
    {
        location.href = location.pathname+'?fileXML';
    },
    renderTitle: function(v)
    {
        if (v == '1') return lang.xeon.title_mr;
        if (v == '2') return lang.xeon.title_mrs;
        if (v == '3') return lang.xeon.title_ms;
    },

    renderDate: function(v)
    {
        var d = Date.parseDate(v, 'Y-m-d') || Date.parseDate(v, 'Y-m-d H:i:s');
        if (!d || v.substr(0, 4) == '0000') return '';
        return d.format('d-m-Y');
    },

    renderDatetime: function(v)
    {
        var d = Date.parseDate(v, 'Y-m-d') || Date.parseDate(v, 'Y-m-d H:i:s');
        if (!d || v.substr(0, 4) == '0000') return '';
        return d.format('d-m-Y H:i:s');
    },

    renderStatus: function(v) {
        return (v == 1) ? lang.xeon.yes : lang.xeon.no;
    },

    renderLink: function(v) {
        return '<a href="'+v+'" class="x-link" onclick="window.open(this.href); return false;">'+v+'</a>';
    },

    renderEmail: function(v) {
        return '<a href="mailto:'+v+'" class="x-link">'+v+'</a>';
    },
    renderHtmlText: function(v) {
        return Ext.util.Format.nl2br(v.replace(/(<([^>]+)>)/ig,""));
    },
    renderText: function(v) {
        return Ext.util.Format.nl2br(v);
    },

    getDate: function(dt)
    {
        if (dt == '' || dt == undefined) return dt;
        else
        {
            var date = dt.split(' ')[0].split('-');
            return date[2]+'-'+date[1]+'-'+date[0];
        }
    },

    getTime: function(dt)
    {
        if (dt == '') return dt;
        else
        {
            var time = dt.split(' ')[1];
            return time;
        }
    },

    getEuro: function(v)
    {
        if (v == 0 || v == '' || v == undefined) return '';
        else return v +' &euro;';
    },

    getId: function(i)
    {
        if (!i) return false;

        i = i.split('.');
        var item = {};

        if (i[0]) {
            item.module = i[0];
        }

        if (i[1]) {
            item.type = i[1];
        }

        if (i[2])
        {
            item.id = i[2];
            item.add = (i[2] === '0');
        }

        return item;
    },

    disableItems: function()
    {
        if (this.edit == false)
        {
            Ext.each(this.buttons, function(b) {
                b.setDisabled(true);
            });
        }
    },

    upFirstLetter: function(v)
    {
        var l = v.length-1;
        var v = v.substr(0,1).toUpperCase()+v.substr(1,1);
        return v;
    },

    setCookie: function(name, value, expires, path, domain, secure)
    {
        //console.log('setCookie: '+ name);
        //console.log('value :'+ value);
        expires = expires * 60*60*24*1000;
        var today = new Date();
        var expires_date = new Date(today.getTime() + expires);
        document.cookie = name + '=' + escape(value) +
        (expires ? ';expires=' + expires_date.toGMTString() : '') +
        (path ? ';path=' + path : '') +
        (domain ? ';domain=' + domain : '') +
        (secure ? ';secure' : '');
    },

    getCookie: function(name)
    {
        //console.log('getcookie: '+ name);
        var start = document.cookie.indexOf(name+'=');
        var len = start+name.length+1;
        if ((!start) && (name != document.cookie.substring(0, name.length))) return null;
        if (start == -1) return null;
        var end = document.cookie.indexOf(';', len);
        if (end == -1) end = document.cookie.length;
        //console.log('value: '+ unescape(document.cookie.substring(len, end)));
        return unescape(document.cookie.substring(len, end));
    },

    deleteCookie: function(name, path, domain)
    {
        if (Xeon.getCookie(name)) document.cookie = name + '=' +
            (path ? ';path=' + path : '') +
            (domain ? ';domain=' + domain : '') +
            ';expires=Thu, 01-Jan-1970 00:00:01 GMT';
    }
}

Xeon.module.xeon =
{
    allowDrop: function(tree, node, target) {
        return (node.parentNode.getDepth() == target.getDepth());
    },

    makeAdminRoot: function(ct, item, data)
    {
        /*ct.add(new Ext.ux.FormPanel({
            title: lang.xeon.ui,
            iconCls: 'i-layout',
            items: [{
                fieldLabel: lang.xeon.title,
                allowBlank: false,
                name: 'xeon.title',
                value: data.xeon.title
            },{
                xtype: 'combobox',
                fieldLabel: lang.xeon.lang,
                allowBlank: false,
                data: [['fr', lang.xeon.lang_fr, 'i-flag-fr']], // temp
                name: 'xeon.lang',
                value: data.xeon.lang
            }]
        }));*/

              
        /*ct.add(new Ext.ux.FormPanel(
        {
            title:lang.SHOP_LEGAL_MENTIONS,
            iconCls:'i-house',
            items : [
            {
                xtype:'listbox',
                itype:'shop.cmstobrand',
                name:'shop.pge_mentions',
                fieldLabel:lang.SHOP_PAGE_MENTIONS,
                displayValue: data.shop.pge_mentions
            },
            {
                xtype:'listbox',
                itype:'shop.cmstobrand',
                name:'shop.pge_credits',
                fieldLabel:lang.SHOP_PAGE_CREDITS,
                displayValue:data.shop.pge_credits
            }]
        }));*/
                
        /*ct.add(new Ext.ux.FormPanel({
            title: lang.XEON_DATABASE,
            iconCls: 'i-database',
            items: [{
                fieldLabel: lang.XEON_SERVER,
                allowBlank: false,
                name: 'sql.server',
                value: data.sql.server
            },{
                fieldLabel: lang.XEON_LOGIN,
                allowBlank: false,
                name: 'sql.username',
                value: data.sql.username
            },{
                fieldLabel: lang.XEON_PASSWORD,
                allowBlank: false,
                name: 'sql.password',
                value: data.sql.password
            },{
                fieldLabel: lang.XEON_DATABASE,
                allowBlank: false,
                name: 'sql.database',
                value: data.sql.database
            }]
        }));*/

        /*ct.add(new Ext.ux.FormPanel({
            title: lang.xeon.seo,
            iconCls: 'i-magnifier',
            items: [{
                xtype: 'combobox',
                data: [ [ 0, lang.xeon.no ], [ 1, lang.xeon.yes ] ],
                fieldLabel: lang.xeon.rewrite_url,
                name: 'site.rewrite_url',
                value: data.site.rewrite_url
            },{
                fieldLabel: lang.xeon.meta_title,
                name: 'site.title',
                value: data.site.title
            },{
                fieldLabel: lang.xeon.meta_description,
                name: 'site.description',
                value: data.site.description
            },{
                fieldLabel: lang.xeon.meta_keywords,
                name: 'site.keywords',
                value: data.site.keywords
            }]
        }));*/

        ct.add(new Ext.ux.grid.GridPanel({
            height: 150,
            title: lang.xeon.site_list,
            iconCls: 'i-bullet-error',
            itype: 'xeon.site',
            reloadAfterEdit: true,
            rowId: 'id',
            columns: [{
                header: lang.xeon.site_name,
                dataIndex: 'title',
                width: 450,
                editor: new Ext.form.TextArea()
            //    itemLink: ['xeon.lng', 'lng_id']
            },{
                header: lang.xeon.site_lang,
                dataIndex: 'lang',
                width: 150,
                //allowBlank: false,
                renderer: function(v){
                    for(i=0; i<data.lang_list.length; i++)
                    {
                        if(data.lang_list[i][0] == v)
                            return data.lang_list[i][1];
                    }
                },
                editor: new Ext.ux.form.ComboBox({
                    data: data.lang_list
                    //data: [[1, 'Fr'], [2, 'En']]
                })
            },{
                header: lang.xeon.active,
                dataIndex: 'active',
                width: 100,
                renderer: this.renderYesorNo,
                editor: new Ext.ux.form.ToggleBox()
            }]
        }));

        ct.add(new Ext.ux.grid.GridPanel({
            height: 200,
            title: lang.xeon.domain_list,
            iconCls: 'i-house',
            itype: 'xeon.domain',
            rowId: 'id',
            columns: [{
                header: lang.xeon.domain_name,
                dataIndex: 'domain',
                width: 125,
                editor: new Ext.form.TextArea()
            //    itemLink: ['xeon.lng', 'lng_id']
            },{
                header: lang.xeon.domain_title,
                dataIndex: 'title',
                width: 100,
                editor: new Ext.form.TextArea()
            //    itemLink: ['xeon.lng', 'lng_id']
            },{
                header: lang.xeon.domain_tagline,
                dataIndex: 'tagline',
                width: 175,
                editor: new Ext.form.TextArea()
            //    itemLink: ['xeon.lng', 'lng_id']
            },{
                header: lang.xeon.domain_secure,
                dataIndex: 'secure',
                width: 50,
                renderer: this.renderYesorNo,
                editor: new Ext.ux.form.ToggleBox()
            },{
                header: lang.xeon.active,
                dataIndex: 'active',
                width: 50,
                renderer: this.renderYesorNo,
                editor: new Ext.ux.form.ToggleBox()
            },{
                header: lang.xeon.domain_site,
                dataIndex: 'site',
                width: 75,
                //allowBlank: false,
                renderer: function(v){
                    for(i=0; i<data.site_list.length; i++)
                    {
                        if(data.site_list[i][0] == v)
                            return data.site_list[i][1];
                    }
                },
                editor: new Ext.ux.form.ComboBox({
                    data: data.site_list
                    //data: [[1, 'Fr'], [2, 'En']]
                })
            },{
                header: lang.xeon.domain_country,
                dataIndex: 'country',
                width: 200,
                //allowBlank: false,
                renderer: function(v){
                    for(i=0; i<data.country_list.length; i++)
                    {
                        if(data.country_list[i][0] == v)
                            return data.country_list[i][1];
                    }
                },
                editor: new Ext.ux.form.ComboBox({
                    data: data.country_list
                    //data: [[1, 'Fr'], [2, 'En']]
                })
            }]
        }));

        ct.add(new Ext.ux.grid.GridPanel({
            height: 150,
            title: lang.xeon.alias_list,
            iconCls: 'i-house-go',
            itype: 'xeon.alias',
            rowId: 'id',
            columns: [{
                header: lang.xeon.alias,
                dataIndex: 'alias',
                width: 350,
                editor: new Ext.form.TextArea()
            //    itemLink: ['xeon.lng', 'lng_id']
            },{
                header: lang.xeon.domain_name,
                dataIndex: 'domain',
                width: 350,
                //allowBlank: false,
                renderer: function(v){
                    for(i=0; i<data.domain_list.length; i++)
                    {
                        if(data.domain_list[i][0] == v)
                            return data.domain_list[i][1];
                    }
                },
                editor: new Ext.ux.form.ComboBox({
                    data: data.domain_list
                })
            }]
        }));
        
        /*ct.add(new Ext.ux.grid.GridPanel({
            title: lang.xeon.lang_list,
            iconCls: 'i-comments',
            itype: 'xeon.lng',
            rowId: 'id',
            columns: [{
                header: lang.xeon.lang,
                dataIndex: 'iso',
            //    itemLink: ['xeon.lng', 'lng_id']
            },{
                header: lang.xeon.meta_title,
                dataIndex: 'title',
                width: 100,
                editor: new Ext.form.TextField()
            },{
                header: lang.xeon.meta_description,
                dataIndex: 'description',
                width: 340,
                editor: new Ext.form.TextArea()
            },{
                header: lang.xeon.meta_keywords,
                dataIndex: 'keywords',
                width: 340,
                editor: new Ext.form.TextArea()
            }]
        }));*/
    },
    
    makeSiteForm: function(ct, item, data)
    {
        var items = [];
        
        items.push({
            fieldLabel: lang.xeon.site_name,
            name: 'title',
            value: data.db.title,
            allowBlank: false,
        });

        if (item.add)
        {
            items.push({
                xtype: 'listbox',
                fieldLabel: lang.xeon.site_lang,
                allowBlank: false,
                itype: 'xeon.lng',
                name: 'lang',
                value: data.db.lang,
                displayValue: data.db.iso
            });
        }
        
        if (item.add)
        {
            ct.add(new Ext.ux.FormPanel({
                items: items
            }));
        }
        else
        {
            ct.add(new Ext.ux.FormPanel({
                title: lang.xeon.lang,
                iconCls: 'i-comment',
                items: items
            }));
        }
    },
    
    makeDomainForm: function(ct, item, data)
    {
        var items = [];
        
        items.push({
            fieldLabel: lang.xeon.domain_name,
            name: 'domain',
            value: data.db.domain,
            allowBlank: false,
        });

        if (item.add)
        {
            items.push({
                xtype: 'listbox',
                fieldLabel: lang.xeon.site,
                allowBlank: false,
                itype: 'xeon.site',
                name: 'site',
                value: data.db.lang,
                displayValue: data.db.title
            });

            items.push({
                xtype: 'listbox',
                fieldLabel: lang.xeon.domain_country,
                allowBlank: false,
                itype: 'xeon.country',
                name: 'country',
                value: data.db.country,
                displayValue: data.db.alpha2
            });
        }

        items.push({
            fieldLabel: lang.xeon.domain_title,
            name: 'title',
            value: data.db.title,
            allowBlank: false,
        });

        items.push({
            fieldLabel: lang.xeon.domain_tagline,
            name: 'tagline',
            value: data.db.tagline
        });

        if (item.add)
        {
            ct.add(new Ext.ux.FormPanel({
                items: items
            }));
        }
        else
        {
            ct.add(new Ext.ux.FormPanel({
                title: lang.xeon.lang,
                iconCls: 'i-comment',
                items: items
            }));
        }
    },
    
    makeAliasForm: function(ct, item, data)
    {
        var items = [];
        
        items.push({
            fieldLabel: lang.xeon.alias,
            name: 'alias',
            value: data.db.alias,
            allowBlank: false,
        });

        items.push({
            xtype: 'listbox',
            fieldLabel: lang.xeon.domain,
            allowBlank: false,
            itype: 'xeon.domain',
            name: 'domain',
            value: data.db.domain,
            displayValue: data.db.domain
        });
        
        if (item.add)
        {
            ct.add(new Ext.ux.FormPanel({
                items: items
            }));
        }
        else
        {
            ct.add(new Ext.ux.FormPanel({
                title: lang.xeon.lang,
                iconCls: 'i-comment',
                items: items
            }));
        }
    },
    
    makeLanguageForm: function(ct, item, data)
    {
        var items = [];
        
        if (item.add)
        {
            items.push({
                xtype: 'listbox',
                fieldLabel: lang.xeon.lang,
                allowBlank: false,
                itype: 'xeon.lng',
                name: 'lng_id',
                value: data.db.lng_id,
                displayValue: data.db.lng_name
            });
        }
        
        items.push({
            fieldLabel: lang.xeon.title,
            name: 'lng_title',
            value: data.db.title
        },{
            xtype: 'textarea',
            fieldLabel: lang.xeon.description,
            name: 'lng_description',
            value: data.db.lng_description
        });
        
        if (item.add)
        {
            ct.add(new Ext.ux.FormPanel({
                items: items
            }));
        }
        else
        {
            ct.add(new Ext.ux.FormPanel({
                title: lang.xeon.lang,
                iconCls: 'i-comment',
                items: items
            }));
        }
    },

    makeModuleForm: function(ct, item, data)
    {
        if (item.add)
        {
            ct.add(new Ext.ux.FormPanel({
                items: [{
                    xtype: 'combobox',
                    fieldLabel: lang.xeon.module,
                    allowBlank: false,
                    name: 'module',
                    data: data.mod
                }]
            }));
        }
        else
        {
            ct.add(new Ext.ux.InfoPanel({
                title: lang.xeon.info,
                iconCls: 'i-information',
                items: [{
                    infoLabel: lang.xeon.version,
                    value: data.version
                },{
                    infoLabel: lang.xeon.author,
                    value: data.author
                },{
                    infoLabel: lang.xeon.plugin,
                    value: (data.plugin || Xeon.plugin[data.module]) ? lang.xeon.yes : lang.xeon.no
                }]
            }));
                       
            Xeon.callback(data.module, 'makeModuleForm', ct, item, data);
        }
    },

    renderYesorNo: function(v, m, r)
    {
        m.css = (v == 1) ? 'x-button i-accept' : 'x-button i-delete';
        m.css = (v == -1) ? 'x-button i-loading' : m.css;
    },
    
    makeSiteRoot: function(ct, item, data)
    {
        var lipsum = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed metus nibh, sodales a, porta at, vulputate eget, dui. Pellentesque ut nisl. Maecenas tortor turpis, interdum non, sodales non, iaculis ac, lacus. Vestibulum auctor, tortor quis iaculis malesuada, libero lectus bibendum purus, sit amet tincidunt quam turpis vel lacus. In pellentesque nisl non sem. Suspendisse nunc sem, pretium eget, cursus a, fringilla vel, urna.';
                
        /*ct.add(new Ext.ux.Portal({
            title: lang.XEON_LAYOUT,
            iconCls: 'i-layout',
            items: [{
                columnWidth:.20,
                items: [{
                    html: lipsum
                },{
                    html: lipsum
                },{
                    html: lipsum
                }]
            },{
                columnWidth:.60,
                items: [{
                    html: lipsum
                },{
                    html: lipsum
                },{
                    html: lipsum
                }]
            },{
                columnWidth:.20,
                items: [{
                    html: lipsum
                },{
                    html: lipsum
                },{
                    html: lipsum
                }]
            }]
        }));*/
        
        var test = new Ext.Panel({
            title: 'DragDrop',
            iconCls: 'i-tux',
            draggable: true,
            defaults: {
                style: {
                    background: 'white',
                    padding: '5px'
                },
                draggable: {
                    insertProxy: true
                },
                frame: false
            },
            items: [{
                html: lipsum
            },{
                html: lipsum
            },{
                html: lipsum
            }]
        });
        
        ct.add(test);
    }
}
Ext.onReady(Xeon.init);