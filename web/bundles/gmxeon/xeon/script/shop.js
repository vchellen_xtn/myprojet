
// Interface shop v2.1.1
// Auteur : Sylvain Durozard
//
Xeon.module.shop =
{
        
    allowDrop: function(tree, node, target) {
        return (node.parentNode.getDepth() == target.getDepth());
    },
    exportOrder: function( cmd, order )
    {
        Ext.Ajax.request({
            params: {
                cmd: cmd,
                order_id: order
            },

            success: function( result ){
                Xeon.getFile();
            }
        });
    },
    myRenderDate: function(v){
        var d = Date.parseDate(v, 'Y-m-d');
        if (!d || v.substr(0, 4) == '0000') return lang.SHOP_NONE;
        return d.format('d-m-Y H:i:s');
    },
    renderWeight: function(v) {
        return (v < 1) ? (v*1000)+' g' : v+' kg';
    },

    renderWeightG: function(v) {
        return v+' g';
    },
    renderQuantity: function(v) {
        return (v > 1) ? v+' '+lang.SHOP_PIECES : v+' '+lang.SHOP_PIECE;
    },

    renderPrice1: function(v) {
        return (v != 0) ? v+' &euro;' : lang.SHOP_FREE;
    },

    renderPrice2: function(v) {
        return Ext.util.Format.number(v, '0,0.00') + ' &euro;';
    // return (v != 0) ? v+' &euro;' : lang.XEON_NO;
    },

    renderDiscountType: function(v) {
        return (v == 1) ? lang.SHOP_AMOUNT : lang.SHOP_PERCENTAGE;
    },

    renderDiscountValue: function(v, m, r) {
        return (r.data.dsc_type == 1) ? v+' &euro;' : v+' %';
    },
	
    renderOrderStep: function(v)
    {
        if (v == 1) return lang.SHOP_ESTIMATE;
        if (v == 2) return lang.SHOP_PAYMENT_TODO;
        if (v == 3) return lang.SHOP_PROCESS_TODO;
        if (v == 4) return lang.SHOP_COMPLETED;
        if (v == 5) return lang.SHOP_CANCELLED;
    },
	
    renderOrderSource: function(v)
    {
        if (v == 1) return lang.SHOP_WEB;
        if (v == 2) return lang.SHOP_PHONE;
        if (v == 3) return lang.SHOP_MAIL;
        if (v == 4) return lang.SHOP_STORE;
    },

    makeProductRoot: function(ct, item, data)
    {
        var columns = [];
        
        columns.push({
            header: lang.SHOP_PRODUCT_ID,
            dataIndex: 'prd_id',
            width: 50,
            itemLink: ['shop.prd', 'prd_id']
        });
            
        if (data.brd)
        {
            columns.push({
                header: lang.SHOP_BRAND,
                dataIndex: 'brd_title',
                width: 100,
                itemLink: ['shop.brd', 'brd_id']
            });
        }
        
        
            
        // add parent cat field
        columns.push({
            header:  'Categorie',
            dataIndex: 'fam_parent_text',
            width: 100,
            itemLink: ['shop.fam', 'fam_parent']
        });

        //if (data.sub_cat)
        //{
        columns.push({
            header:'Sous-Cat',
            dataIndex: 'fam_title',
            width: 100,
            itemLink: ['shop.fam', 'fam_id']
        });
        //}
      
        columns.push({
            header: lang.XEON_TITLE,
            dataIndex: 'prd_title',
            width: 150,
            itemLink: ['shop.prd', 'prd_id']
        });


        if (data.opt)
            columns.push({
                header: lang.XEON_OPT_TITLE,
                dataIndex: 'opt_title',
                width: 150,
                itemLink: ['shop.opt', 'opt_id']
            });

        columns.push(
        {
            header: 'Nouveautes home',
            dataIndex: (data.opt) ? 'opt_new_home' : 'prd_new_home',
            width: 100,
            renderer: this.renderYesorNo,
            editor: new Ext.ux.form.ToggleBox()
        },{
            header: 'Promo home',
            dataIndex: (data.opt) ? 'opt_promo_home' : 'prd_promo_home',
            width: 100,
            renderer: this.renderYesorNo,
            editor: new Ext.ux.form.ToggleBox()
        },{
            header: 'Coup de coeur',
            dataIndex: (data.opt) ? 'opt_flash':'prd_flash',
            width: 100,
            renderer: this.renderYesorNo,
            editor: new Ext.ux.form.ToggleBox()
        },{
            header: 'Top vente Home',
            dataIndex: (data.opt) ? 'opt_topsale_home' : 'prd_topsale_home',
            width: 100,
            renderer: this.renderYesorNo,
            editor: new Ext.ux.form.ToggleBox()
        },{
            header: 'Top budget home',
            dataIndex: (data.opt) ? 'opt_topbudget_home' : 'prd_topbudget_home',
            width: 100,
            renderer: this.renderYesorNo,
            editor: new Ext.ux.form.ToggleBox()
        },{
            header: 'Publier',
            dataIndex: (data.opt) ? 'opt_publish' : 'prd_publish',
            width: 60,
            renderer: this.renderYesorNo,
            editor: new Ext.ux.form.ToggleBox()
        });
                    
        columns.push({
            header: lang.XEON_STATUS,
            dataIndex: 'prd_status',
            width: 60,
            renderer: this.renderYesorNo,
            editor: new Ext.ux.form.ToggleBox()
        });
        
        // console.log(data.engine);
        ct.add(new Ext.ux.grid.GridPanel({
            title: lang.SHOP_PRODUCT_LIST,
            search : engine.getSearchOptionsItems(data),
            optionCell:data.opt, // if set to true, change the itype to discard changes
            iconCls: 'i-database',
            itype: 'shop.prd',
            rowId: (data.opt) ? 'opt_id':'prd_id',
            paging: true,
            quickSearch: true,
            columns: columns,
            redirectAfterDelete: data.redirect,
            configRedirectionAfterDelete: {
                grantFreeInput: true,
                textFreeInput: lang.XEON_FREE_INPUT_ID,
                choices: [{
                    text: lang.SHOP_HOME,
                    value: '0'
                },{
                    text: lang.SHOP_FAMILLE_MERE,
                    value: 'famille'
                }]
            }
        }));
		
        if (data.tag)
        {
            ct.add(new Ext.ux.grid.GridPanel({
                title: lang.XEON_TAG_LIST,
                iconCls: 'i-tag-blue',
                itype: 'xeon.tag',
                rowId: 'tag_id',
                reloadAfterEdit: true,
                quickSearch: true,
                search:false,
                columns: [{
                    header: lang.XEON_TAG,
                    dataIndex: 'tag_text',
                    width: 200,
                    editor: new Ext.form.TextField({
                        allowBlank: false
                    })
                },{
                    header: lang.XEON_USAGE,
                    dataIndex: 'tag_count',
                    dataType: 'int'
                }]
            }));
        }
		
        if (data.com)
        {
            ct.add(new Ext.ux.CommentPanel({
                title: lang.XEON_COMMENT_WAITING,
                iconCls: 'i-comments',
                data: data.com,
                root: true
            }));
        }
    },

    makeInformationForm: function(ct, item, data)
    {
        var media = new Ext.BoxComponent({
            autoEl: {
                tag: 'img',
                src: data.db.url,
                style:"border:1px solid black;"
            },
            style:"position: absolute; top: 10px; left: 510px;"
        });
                        
        ct.add(new Ext.ux.FormPanel({
            boxComponentId:media.id,
            title:lang.SHOP_ADRESSPRO,
            iconCls:'i-note',
            items:[
            {
                name:'name',
                fieldLabel:lang.SHOP_NAME,
                value:data.db.name
            },media,
            {
                xtype:'mediabox',
                name:'logo',
                fieldLabel:lang.SHOP_LOGO,
                value:data.db.logo,
                displayValue: data.db.logotitle
            },
            {
                xtype:'textarea',
                name:'adress',
                fieldLabel:lang.SHOP_ADRESS,
                value:data.db.adress
            },
            {
                name:'zipcode',
                fieldLabel:lang.SHOP_ZIPCODE,
                value:data.db.zipcode
            },
            {
                name:'town',
                fieldLabel:lang.SHOP_TOWN,
                value:data.db.town
            },
            {
                name:'tel1',
                fieldLabel:lang.SHOP_TEL1,
                value:data.db.tel1
            },
            {
                name:'tel2',
                fieldLabel:lang.SHOP_TEL2,
                value:data.db.tel2
            },{
                name:'ntva',
                fieldLabel:lang.SHOP_TVAINTRA,
                value:data.db.tva
            },
            {
                name:'siret',
                fieldLabel:lang.SHOP_SIRET,
                value:data.db.siret
            }
            ]
        }))
          
    },

    makeGiftmessageForm: function(ct, item, data)
    {
        var media = new Ext.BoxComponent({
            id:'gift',
            autoEl: {
                tag: 'div',
                html:data.db.html
            }
        });

        ct.add(new Ext.ux.FormPanel({
            boxComponentId: media.id,
            title:'Personnaliser votre signature',
            iconCls:'i-note',
            items:[
            {
                xtype:'mediabox',
                name:'prd_media',
                fieldLabel:lang.SHOP_LOGO,
                value:data.db.id,
                displayValue: data.db.title
            },
            {
                xtype:'textarea',
                name:'text',
                fieldLabel:lang.SHOP_GIFT_TEXT,
                value:data.db.text
            },media
            ]
        }))
    },
        
    makeFamilyForm: function(ct, item, data)
    {
          
        var items = [];
        var columns = [];

        columns.push({
            header: lang.SHOP_PRODUCT_ID,
            dataIndex: 'prd_id',
            width: 50,
            itemLink: ['shop.prd', 'prd_id']
        });
            
        //add brand field
        if (data.brd)
        {
            columns.push({
                header: lang.SHOP_BRAND,
                dataIndex: 'brd_title',
                width: 50,
                itemLink: ['shop.brd', 'brd_id']
            });
        }
                
        // add catgeorie field
        columns.push({
            header: 'Categorie',
            dataIndex: 'fam_parent_text',
            width: 100,
            itemLink: ['shop.fam', 'fam_parent']
        });
		
        // add parent cat field
        
            columns.push({
                header: 'Sous-Cat',
                dataIndex: 'fam_title',
                width: 100,
				 itemLink: ['shop.fam', 'fam_id']
            });
        
                
        columns.push({
            header: lang.XEON_TITLE,
            dataIndex: 'prd_title',
            width: 150,
            itemLink: ['shop.prd', 'prd_id']
        });

        if (data.opt)
            columns.push({
                header: lang.XEON_OPT_TITLE,
                dataIndex: 'opt_title',
                width: 150,
                itemLink: ['shop.opt', 'opt_id']
            });
                
        columns.push({
            header: 'Nouveautes home',
            dataIndex: (data.opt) ? 'opt_new_home' : 'prd_new_home',
            width: 100,
            renderer: this.renderYesorNo,
            editor: new Ext.ux.form.ToggleBox()
        },{
            header: 'Promo home',
            dataIndex: (data.opt) ? 'opt_promo_home' : 'prd_promo_home',
            width: 100,
            renderer: this.renderYesorNo,
            editor: new Ext.ux.form.ToggleBox()
        },{
            header: 'Coup de coeur',
            dataIndex: (data.opt) ? 'opt_flash':'prd_flash',
            width: 100,
            renderer: this.renderYesorNo,
            editor: new Ext.ux.form.ToggleBox()
        },{
            header: 'Top vente Home',
            dataIndex: (data.opt) ? 'opt_topsale_home' : 'prd_topsale_home',
            width: 100,
            renderer: this.renderYesorNo,
            editor: new Ext.ux.form.ToggleBox()
        },{
            header: 'Top budget home',
            dataIndex: (data.opt) ? 'opt_topbudget_home' : 'prd_topbudget_home',
            width: 100,
            renderer: this.renderYesorNo,
            editor: new Ext.ux.form.ToggleBox()
        },{
            header: 'Publier',
            dataIndex: (data.opt) ? 'opt_publish' : 'prd_publish',
            width: 60,
            renderer: this.renderYesorNo,
            editor: new Ext.ux.form.ToggleBox()
        });
        

        if (item.add)
        {
            items.push({
                fieldLabel: lang.XEON_TITLE,
                allowBlank: false,
                name: 'fam_title',
                value: data.db.fam_title
            });

            items.push({
                xtype: 'listbox',
                fieldLabel: lang.XEON_PARENT,
                itype: 'shop.fam',
                name: 'fam_parent',
                itemLink: ['shop.fam', 'fam_id'],
                value: data.db.fam_parent,
                displayValue: data.db.fam_parent_title
            });
                        
            items.push({
                fieldLabel: lang.SHOP_FULL_TITLE,
                allowBlank: false,
                name: 'fam_full_title',
                value: data.db.fam_full_title
            });
                        
            items.push({
                xtype:'textarea',
                fieldLabel: lang.XEON_CATCHER,
                name: 'fam_catcher',
                value: data.db.fam_catcher
            });
                        
            items.push({
                xtype:'textarea',
                fieldLabel: lang.XEON_DESCRIPTION,
                name: 'fam_description',
                value: data.db.fam_description
            });
                        
            items.push({
                fieldLabel: 'Tags',
                name: 'tag_text',
                value: data.db.tag_text
            });
                        
            items.push({
                xtype: 'mediabox',
                fieldLabel: lang.XEON_IMAGE,
                name: 'fam_media',
                value: data.db.fam_media,
                displayValue: data.db.fam_media_title
            },{
                xtype: 'textfield',
                fieldLabel: 'Taxe',
                allowBlank: false,
                name: 'fam_tax',
                value: (data.db.fam_tax) ? data.db.fam_tax : 19.6
            },{
                xtype: 'togglebox',
                fieldLabel: lang.XEON_STATUS,
                allowBlank: false,
                name: 'fam_status',
                value: 0
            });
        }
        else
        {
                         
            p = new Ext.ux.grid.GridPanel({
                title: lang.SHOP_PRODUCT_LIST,
                rowId:(data.opt) ? 'opt_id': 'prd_id',
                iconCls: 'i-database',
                itype: 'shop.prd',
                optionCell:data.opt,
                paging:true,
                quickSearch: true,
                columns: columns,
                redirectAfterDelete: data.redirect,
                configRedirectionAfterDelete: {
                    grantFreeInput: true,
                    textFreeInput: lang.XEON_FREE_INPUT_ID,
                    choices: [{
                        text: lang.SHOP_HOME,
                        value: '0'
                    },{
                        text: lang.SHOP_FAMILLE_MERE,
                        value: 'famille'
                    }]
                },
                search:engine.getSearchItems(data)
            });
        }
		
		
		
        if (item.add)
        {
            ct.add(new Ext.ux.FormPanel({
                items: items
            }));
        }
        else
        {
                    
            items.push({
                xtype: 'listbox',
                fieldLabel: lang.XEON_PARENT,
                allowBlank: false,
                itype: 'shop.fam',
                name: 'fam_parent',
                value: data.db.fam_parent,
                displayValue: data.db.fam_parent_title
            });
                        
            items.push({
                fieldLabel: lang.XEON_TITLE,
                allowBlank: false,
                name: 'fam_title',
                value: data.db.fam_title
            });
                        
            items.push({
                fieldLabel: lang.SHOP_FULL_TITLE,
                allowBlank: false,
                name: 'fam_full_title',
                value: data.db.fam_full_title
            });
			
            if (data.tag)
            {
                items.push({
                    xtype: 'tagbox',
                    fieldLabel: lang.XEON_TAGS,
                    data: data.tag,
                    name: 'fam_tags',
                    value: data.db.fam_tags
                });
            }
			
            items.push({
                xtype: 'mediabox',
                fieldLabel: lang.XEON_IMAGE,
                name: 'fam_media',
                value: data.db.fam_media,
                displayValue: data.db.fam_media_title
            },{
                xtype: 'textarea',
                fieldLabel: lang.XEON_CATCHER,
                name: 'fam_catcher',
                value: data.db.fam_catcher
            },{
                xtype: 'textarea',
                fieldLabel: lang.XEON_DESCRIPTION,
                name: 'fam_description',
                value: data.db.fam_description
            },{
                xtype: 'textfield',
                fieldLabel: 'Taxe',
                allowBlank: false,
                name: 'fam_tax',
                value: data.db.fam_tax
            },{
                xtype: 'togglebox',
                fieldLabel: lang.XEON_STATUS,
                allowBlank: false,
                name: 'fam_status',
                value: data.db.fam_status
            });
			
            ct.add(new Ext.ux.FormPanel({
                title: lang.SHOP_FAMILY,
                iconCls: ct.iconCls,
                items: items
            }));
                        
            if (data.ref) {

                // partie ref acex tooltip (voir ux.js)
                title =  new Ext.form.TextField({
                    fieldLabel: lang.XEON_TITLE,
                    name: 'fam_meta_title',
                    value: data.db.fam_meta_title,
                    enableKeyEvents:true,
                    maxLength:70
                });
                description = new Ext.form.TextArea({
                    xtype:'textarea',
                    fieldLabel: lang.XEON_DESCRIPTION,
                    name: 'fam_meta_description',
                    value: data.db.fam_meta_description,
                    enableKeyEvents:true,
                //maxLength:70
                });

                keywords = new Ext.form.TextArea({
                    xtype: 'textarea',
                    fieldLabel: lang.XEON_KEYWORDS,
                    name: 'fam_meta_keywords',
                    value: data.db.fam_meta_keywords,
                    enableKeyEvents:true
                });

                tp = new Ext.ux.RefTip({
                    maxC:70,
                    cp:title
                });
                tp2 = new Ext.ux.RefTip({
                    maxC:160,
                    cp:description
                });
                tp3 = new Ext.ux.RefTip({
                    maxC:160,
                    cp:keywords
                });

                ct.add(new Ext.ux.FormPanel({
                    title: lang.XEON_SEO,
                    iconCls: 'i-magnifier',
                    collapsed: false,//(!data.db.fam_meta_title && !data.db.fam_meta_description && !data.db.fam_meta_keywords),
                    items: [title,description,keywords,{
                        fieldLabel: lang.SHOP_URL,
                        name: 'fam_url',
                        value: data.db.fam_url,
                        disabled : (data.ismaster) ? false:data.url
                    }]
                }));
            }
                        
            ct.add(p);
             
			if (data.lpg)
            {
                ct.add(new Ext.ux.ListPanel({
                    title: lang.SHOP_LINK_PAGE,
                    iconCls: 'i-page-white',
                    name: 'fam_link_page',
                    itype: 'cms.pge',
                    data: data.lpg
                }));
            }
			
            if (data.prd)
            {
                var columns = [];
				
                if (data.brd)
                {
                    columns.push({
                        header: lang.SHOP_BRAND,
                        dataIndex: 'brd_title',
                        width: 150,
                        itemLink: ['shop.brd', 'brd_id']
                    });
                }
				
                columns.push({
                    header: lang.XEON_TITLE,
                    dataIndex: 'prd_title',
                    width: 200,
                    itemLink: ['shop.prd', 'prd_id']
                },{
                    header: lang.XEON_HITS,
                    dataIndex: 'prd_hits',
                    dataType: 'int'
                },{
                    header: lang.SHOP_SALES,
                    dataIndex: 'prd_sales',
                    dataType: 'int'
                },{
                    header: lang.XEON_STATUS,
                    dataIndex: 'prd_status',
                    width: 60,
                    renderer: Xeon.renderStatus,
                    editor: new Ext.ux.form.ToggleBox()
                });
				
            /*ct.add(new Ext.ux.grid.GridPanel({
					title: lang.SHOP_PRODUCT_LIST,
					iconCls: 'i-database',
					itype: 'shop.prd',
					rowId: 'prd_id',
					columns: columns
				}));*/
            }

            ct.add(new Ext.ux.grid.GridPanel({
                title: lang.SHOP_FEATURE_LIST,
                iconCls: 'i-table',
                itype: 'shop.ftr',
                rowId: 'ftr_id',
                enableDragDrop: true,
                search:false,
                columns: [{
                    header: lang.XEON_TITLE,
                    dataIndex: 'ftr_title',
                    width: 200,
                    editor: new Ext.form.TextField()
                },{
                    header: lang.SHOP_FEATURE_VALUE,
                    dataIndex: 'ftr_list',
                    width: 400,
                    editor: new Ext.form.TextArea()
                }]
            }));
        }
    },

    makeFeatureForm: function(ct, item, data)
    {
        if (item.add)
        {
            ct.add(new Ext.ux.FormPanel({
                items: [{
                    xtype: 'hidden',
                    name: 'fam_id',
                    value: data.db.fam_id
                },{
                    fieldLabel: lang.XEON_TITLE,
                    allowBlank: false,
                    name: 'ftr_title',
                    value: data.db.ftr_title
                },{
                    xtype: 'textarea',
                    fieldLabel: lang.SHOP_FEATURE_VALUE,
                    name: 'ftr_list',
                    value: data.db.ftr_list
                }]
            }));
        }
    },

    makeStatForm: function(ct, item, data)
    {
        this.makeStatRoot(ct, item, data);
    },

    makeStatproductForm : function(ct, item, data)
    {
        // stat produit
        ct.add(new Ext.ux.grid.GridPanel({
            title: lang.SHOP_STAT_SALE_PRD,
            iconCls: 'i-chart-curve',
            itype: 'shop.itemsale',
            paging:true,
            rowId: 'prd_id',
            quickSearch: true,
            search :[
            new Ext.ux.form.ListBox({
                fieldLabel:lang.SHOP_STAT_BY_BRAND,
                itype: 'shop.brd',
                name: 'brd_id',
                displayValue:lang.SHOP_STAT_ALL_BRAND,
                value:''
            }),
            {
                xtype: 'compositefield',
                defaults:{
                    flex:1
                },
                items: [{
                    xtype: 'datefield',
                    format: 'Y-m-d',
                    fieldLabel: lang.SHOP_STAT_PERIOD,
                    name: 'start_date'
                },
                {
                    xtype: 'datefield',
                    format: 'Y-m-d',
                    fieldLabel: lang.SHOP_STAT_PERIOD_END,
                    name: 'end_date'
                }]
            }],
            columns: [{
                header: lang.SHOP_BRAND,
                dataIndex: 'itm_brand',
                itemLink: ['shop.brd', 'brd_id']
            },{
                header: lang.SHOP_DESIGNATION,
                dataIndex: 'opt_title',
                width: 200,
                itemLink: ['shop.opt', 'opt_id']
            },{
                header: lang.SHOP_SALES,
                dataIndex: 'sale_count',
                renderer: function(v){
                    return Ext.util.Format.number(v, '0,0');
                }
            },{
                header: lang.SHOP_TOTAL_PRICE_HT,
                dataIndex: 'total_ht',
                renderer:this.renderPrice2
            },{
                header: lang.SHOP_TAX,
                dataIndex: 'tva',
                renderer:this.renderPrice2
            },{
                header: lang.SHOP_TOTAL_PRICE_TTC,
                dataIndex: 'total',
                renderer:this.renderPrice2
            }]
        }));

        // stat produit j -1
        ct.add(new Ext.ux.grid.GridPanel({
            title: lang.SHOP_STAT_SALE_DAY,
            iconCls: 'i-chart-curve',
            itype: 'shop.daysale',
            rowId: 'opt_id',
            collapsed:true,
            quickSearch: true,
            search:false,
            columns: [{
                header: lang.SHOP_BRAND,
                dataIndex: 'itm_brand',
                itemLink: ['shop.brd', 'brd_id']
            },{
                header: lang.SHOP_DESIGNATION,
                dataIndex: 'opt_title',
                width: 200,
                itemLink: ['shop.opt', 'opt_id']
            },{
                header: lang.SHOP_SALES,
                dataIndex: 'sale_count',
                renderer: function(v){
                    return Ext.util.Format.number(v, '0,0');
                }
            },{
                header: lang.SHOP_TOTAL_PRICE_HT,
                dataIndex: 'total_ht',
                renderer:this.renderPrice2
            },{
                header: lang.SHOP_TAX,
                dataIndex: 'tva',
                renderer:this.renderPrice2
            },{
                header: lang.SHOP_TOTAL_PRICE_TTC,
                dataIndex: 'total',
                renderer:this.renderPrice2
            }]
        }));
        
        ct.add(new Ext.ux.GoogleChartPanel({
            title:'Vente par marque',
            columns : [['string','Marque'], ['number','Ventes']],
            chartType:'ColumnChart',
            itype:'shop.statprd',
            xview: {
                itype:'shop.statbrd',
                chartType:'PieChart',
                title: 'Top 5 marque'
            }
        }));
                
    },
            
    makeProductForm: function(ct, item, data)
    {
           
        var items = [{
            xtype: 'listbox',
            fieldLabel: lang.SHOP_FAMILY,
            allowBlank: false,
            itype: 'shop.fam',
			parentId: 'shop.prd.'+data.db.prd_id,
            name: 'fam_id',
            itemLink: ['shop.fam', 'fam_id'],
            value: data.db.fam_id,
            displayValue: data.db.fam_title
        }];

        if (data.brd)
        {
            items.push({
                xtype: 'listbox',
                fieldLabel: lang.SHOP_BRAND,
                allowBlank: true,
                itype: 'shop.brd',
                name: 'brd_id',
                value: data.db.brd_id,
                itemLink: ['shop.brd', 'brd_id'],
                displayValue: data.db.brd_title
            });
        }
		
        items.push({
            fieldLabel: lang.XEON_TITLE,
            allowBlank: false,
            name: 'prd_title',
            value: data.db.prd_title
        });
		
		if (data.activetag)
            {
				
				items.push({
					xtype: 'tagbox',
					fieldLabel: lang.XEON_TAGS,
					name: 'prd_tags',
					data:data.tag,
					value: data.db.prd_tags
				});
			}

        items.push({
            xtype: 'mediabox',
            fieldLabel: lang.XEON_IMAGE,
            name: 'prd_media',
            value: data.db.prd_media,
            displayValue: data.db.fle_title
        });
                        
        items.push({
            xtype: 'floatfield',
            fieldLabel: lang.SHOP_TAX,
            allowBlank: false,
            name: 'prd_tax',
            value: data.db.prd_tax
        },{
            xtype: 'togglebox',
            fieldLabel: lang.SHOP_PRD_NEW_HOME,
            name: 'prd_new_home',
            value: (data.db.prd_new_home) ? data.db.prd_new_home : 0
        },{
            xtype: 'togglebox',
            fieldLabel: lang.SHOP_PRD_PROMO_HOME,
            name: 'prd_promo_home',
            value: (data.db.prd_promo_home) ? data.db.prd_promo_home : 0
        });

        if (item.add)
        {
                    
            items.push({
                xtype: 'togglebox',
                fieldLabel: lang.XEON_STATUS,
                allowBlank: false,
                name: 'prd_status',
                value: 0
            });

            ct.add(new Ext.ux.FormPanel({
                items: items
            }));
                       
        }
        else
        {
            items.push({
                xtype: 'togglebox',
                fieldLabel: lang.SHOP_NEW,
                allowBlank: false,
                name: 'prd_new',
                value: data.db.prd_new
            },{
                xtype: 'togglebox',
                fieldLabel: lang.SHOP_FLASH,
                allowBlank: false,
                name: 'prd_flash',
                value: data.db.prd_flash
            },{
                xtype: 'textarea',
                fieldLabel: lang.XEON_CATCHER,
                name: 'prd_catcher',
                value: data.db.prd_catcher
            },{
                xtype: 'togglebox',
                fieldLabel: lang.XEON_STATUS,
                allowBlank: false,
                name: 'prd_status',
                value: data.db.prd_status
            });
		
            if (data.pls)
            {
                items.push({
                    xtype: 'date',
                    fieldLabel: lang.XEON_START_DATE,
                    emptyText: lang.SHOP_NONE,
                    name: 'prd_start_date',
                    value: data.db.prd_start_date
                },{
                    xtype: 'date',
                    fieldLabel: lang.XEON_END_DATE,
                    emptyText: lang.SHOP_NEVER,
                    name: 'prd_end_date',
                    value: data.db.prd_end_date
                });
            }
						
            ct.add(new Ext.ux.FormPanel({
                title: lang.SHOP_PRODUCT,
                iconCls: ct.iconCls,
                items: items
            }));

            ct.add(new Ext.ux.MediaProductPanel({
                id:Math.random().toString(),
                prdid:item.id,
                iconCls: 'i-page-white-stack',
                mode: 'folder'
            }));
                      
            // ct.add(media);
            if (data.ref)
            {
             
                // partie ref acex tooltip (voir ux.js)
                title =  new Ext.form.TextField({
                    id:Math.random().toString(),
                    fieldLabel: lang.XEON_TITLE,
                    name: 'prd_meta_title',
                    value: data.db.prd_meta_title,
                    enableKeyEvents:true,
                //maxLength:70
                });
                description = new Ext.form.TextArea({
                    id:Math.random().toString(),
                    fieldLabel: lang.XEON_DESCRIPTION,
                    name: 'prd_meta_description',
                    value: data.db.prd_meta_description,
                    enableKeyEvents:true,
                // maxLength:70
                });

                keywords = new Ext.form.TextArea({
                    id:Math.random().toString(),
                    xtype: 'textarea',
                    fieldLabel: lang.XEON_KEYWORDS,
                    name: 'prd_meta_keywords',
                    value: data.db.prd_meta_keywords,
                    enableKeyEvents:true
                });

                tp = new Ext.ux.RefTip({
                    maxC:70,
                    cp:title
                });
                tp2 = new Ext.ux.RefTip({
                    maxC:170,
                    cp:description
                });
                tp3 = new Ext.ux.RefTip({
                    maxC:170,
                    cp:keywords
                });

               
                ct.add(new Ext.ux.FormPanel({
                    title: lang.XEON_SEO,
                    iconCls: 'i-magnifier',
                    collapsed: (!data.db.prd_meta_title && !data.db.prd_meta_description && !data.db.prd_meta_keywords),
                    items: [title,description,keywords,{
                        fieldLabel: lang.SHOP_URL,
                        name: 'prd_url',
                        value: data.db.prd_url,
                        disabled : (data.ismaster) ? false:data.url
                    }]
                }));
            }
			
            if (data.lfm)
            {
                ct.add(new Ext.ux.ListPanel({
                    title: lang.SHOP_LINK_FAMILY,
                    iconCls: 'i-folder',
                    name: 'prd_link_family',
                    itype: 'shop.fam',
                    data: data.lfm
                }));
            }
			
            if (data.ftr)
            {
                var items = [];

                Ext.each(data.ftr, function(db)
                {
                    if (db.ftr_list == '')
                    {
                        items.push({
                            fieldLabel: db.ftr_title,
                            name: db.ftr_id,
                            value: db.ftr_value
                        });
                    }
                    else
                    {
                        var data = [];
						data.push(["", ""]);
                        Ext.each(db.ftr_list.split('\n'), function(e) {
                            data.push([e, e]);
                        });
						
                        items.push({
                            xtype: 'combobox',
                            fieldLabel: db.ftr_title,
                            data: data,
                            name: db.ftr_id,
                            value: db.ftr_value
                        });
                    }
                });

                ct.add(new Ext.ux.FormPanel({
                    title: lang.SHOP_PRODUCT_FEATURES,
                    iconCls: 'i-table',
                    command: 'shop.saveProductFeature',
                    items: items
                }));
            }
			
            var columns = [{
                header: lang.XEON_TITLE,
                dataIndex: 'opt_title',
                itemLink: ['shop.opt', 'opt_id']
            },{
                header: lang.SHOP_PRICE,
                dataIndex: 'opt_price',
                dataType: 'float',
                editor: new Ext.form.TextField(),
                renderer: this.renderPrice1
            },{
                header: lang.SHOP_PROMOTION,
                dataIndex: 'opt_promotion',
                dataType: 'float',
                editor: new Ext.form.TextField(),
                renderer: this.renderPrice2
            },{
                header: lang.SHOP_PROMOTION_DATE_BEGIN,
                dataIndex: 'opt_promotion_begin_date',
                dataType: 'date',
                dateFormat:'m/d/y',
                editor: new Ext.ux.form.DateField,
                renderer: Xeon.renderDate
				
            },{
                header: lang.SHOP_PROMOTION_DATE_END,
                dataIndex: 'opt_promotion_end_date',
                dataType: 'date',
                dateFormat:'m/d/y',
                editor: new Ext.form.DateField({
                    format: 'd-m-Y'
                }),
                renderer: Xeon.module.shop.myRenderDate
            }];
			
            if (data.eco)
            {
                columns.push({
                    header: lang.SHOP_ECOTAX,
                    dataIndex: 'opt_ecotax',
                    dataType: 'float',
                    editor: new Ext.form.TextField(),
                    renderer: this.renderPrice2
                });
            }
			
            if (data.ext)
            {
                columns.push({
                    header: lang.SHOP_EXTRA,
                    dataIndex: 'opt_extra',
                    dataType: 'float',
                    editor: new Ext.form.TextField(),
                    renderer: this.renderPrice2
                });
            }
			
            if (data.stk)
            {
                columns.push({
                    header: lang.SHOP_STOCK,
                    dataIndex: 'opt_stock',
                    dataType: 'int',
                    editor: new Ext.form.TextField(),
                    renderer: this.renderQuantity
                });
            }
			
            columns.push({
                header: lang.SHOP_WEIGHT,
                dataIndex: 'opt_weight',
                dataType: 'float',
                editor: new Ext.ux.form.FloatField(),
                renderer: this.renderWeight
            },{
                header: lang.SHOP_REFERENCE,
                dataIndex: 'opt_reference',
                editor: new Ext.form.TextField()
            },{
                header: lang.XEON_STATUS,
                dataIndex: 'opt_status',
                width: 60,
                renderer: Xeon.renderStatus,
                editor: new Ext.ux.form.ToggleBox()
            });
			
            if (data.spl)
            {
                columns.push({
                    header: lang.SHOP_SUPPLIER,
                    dataIndex: 'spl_name',
                    width: 200,
                    itemLink: ['shop.spl', 'spl_id']
                });
            }
            //console.log(data);
            if (data.opt)
            {
                ct.add(new Ext.ux.grid.GridPanel({
                    title: lang.SHOP_OPTION_LIST,
                    iconCls: 'i-brick',
                    itype: 'shop.opt',
                    rowId: 'opt_id',
                    id:'x-option-grid',
                    search: engine.getSearchOptionsItems(data),
                    enableDragDrop: true,
                    columns: columns,
                    paging: true
                                
                }));
            }

            ct.add(new Ext.ux.LayoutPanel({
                previewURL : data.db.prd_url,
                title: lang.XEON_LAYOUT,
                iconCls: 'i-layout',
                itype: 'shop.cnt',
                titleName: 'prd_full_title',
                templates: data.tpl,
                data: data.cnt,
                previewURL: (data.url.charAt(0)=='/') ? data.url : '/'+data.url,
                mobile:data.mobile
            }));
                        
            if (data.lpd)
            {
                ct.add(new Ext.ux.ListPanel({
                    title: lang.SHOP_LINK_PRODUCT,
                    iconCls: 'i-database',
                    name: 'prd_link_product',
                    itype: 'shop.prd',
                    data: data.lpd
                }));
            }
			
            if (data.lpg)
            {
                ct.add(new Ext.ux.ListPanel({
                    title: lang.SHOP_LINK_PAGE,
                    iconCls: 'i-page-white',
                    name: 'prd_link_page',
                    itype: 'cms.pge',
                    data: data.lpg
                }));
            }
			
			
			
            if (data.com)
            {
                ct.add(new Ext.ux.CommentPanel({
                    title: lang.XEON_COMMENTS,
                    iconCls: 'i-comments',
                    data: data.com
                }));
            }
        }
    },
	
    makeProductCopy: function(ct, item, data)
    {
        ct.add(new Ext.ux.FormPanel({
            command: 'xeon.copyItem',
            items: [{
                xtype: 'listbox',
                fieldLabel: lang.XEON_TARGET,
                allowBlank: false,
                itype: 'shop.fam',
                name: 'fam_id',
                value: data.db.fam_id,
                displayValue: data.db.fam_title
            },{
                fieldLabel: lang.XEON_TITLE,
                allowBlank: false,
                name: 'prd_title',
                value: data.db.prd_title
            }]
        }));
    },

    makeOptionForm: function(ct, item, data)
    {
        var items = [{
            xtype: 'hidden',
            name: 'prd_id',
            value: data.db.prd_id
        }];
		
        if (data.spl)
        {
            items.push({
                xtype: 'listbox',
                fieldLabel: lang.SHOP_SUPPLIER,
                allowBlank: false,
                itype: 'shop.spl',
                name: 'spl_id',
                value: data.db.spl_id,
                displayValue: data.db.spl_name
            });
        }
		
        items.push({
            fieldLabel: lang.XEON_TITLE,
            allowBlank: false,
            name: 'opt_title',
            value: data.db.opt_title
        },{
            fieldLabel: lang.SHOP_PRICE,
            allowBlank: false,
            name: 'opt_price',
            value: data.db.opt_price
        },{
            fieldLabel: lang.SHOP_PROMOTION,
            name: 'opt_promotion',
            value: data.db.opt_promotion
        },{
            xtype: "date",
            fieldLabel: lang.SHOP_PROMOTION_DATE_BEGIN,
            name: 'opt_promotion_begin_date',
            emptyText: lang.SHOP_NONE,
            renderer: Xeon.renderDate,
            editor: new Ext.form.DateField({
                format: 'd-m-Y'
            }),
            value: data.db.opt_promotion_date_begin
        },{
            xtype:  "date",
            fieldLabel: lang.SHOP_PROMOTION_DATE_END,
            name: 'opt_promotion_end_date',
            emptyText: lang.SHOP_NONE,
            renderer: this.myRenderDate,
            editor: new Ext.form.DateField({
                format: 'd-m-Y'
            }),
            value: data.db.opt_promotion_date_end
        });
		
        if (data.eco)
        {
            items.push({
                fieldLabel: lang.SHOP_ECOTAX,
                name: 'opt_ecotax',
                value: data.db.opt_ecotax
            });
        }
		
        if (data.ext)
        {
            items.push({
                fieldLabel: lang.SHOP_EXTRA,
                name: 'opt_extra',
                value: data.db.opt_extra
            });
        }
		
        if (data.stk)
        {
            items.push({
                fieldLabel: lang.SHOP_STOCK,
                allowBlank: false,
                name: 'opt_stock',
                value: data.db.opt_stock
            });
        }
		
        items.push({
            fieldLabel: lang.SHOP_WEIGHT,
            allowBlank: false,
            name: 'opt_weight',
            value: data.db.opt_weight
        },{
            fieldLabel: lang.SHOP_REFERENCE,
            allowBlank: false,
            name: 'opt_reference',
            value: data.db.opt_reference
        });
		
        if (item.add)
        {
            ct.add(new Ext.ux.FormPanel({
                items: items
            }));
        }
        else
        {
            items.push({
                xtype: 'mediabox',
                fieldLabel: lang.XEON_IMAGE,
                name: 'opt_media',
                value: data.db.opt_media,
                displayValue: data.db.opt_media_title
            },{
                xtype: 'togglebox',
                fieldLabel: lang.XEON_STATUS,
                allowBlank: false,
                name: 'opt_status',
                value: data.db.opt_status
            });
			
            ct.add(new Ext.ux.FormPanel({
                title: lang.SHOP_OPTION,
                iconCls: ct.iconCls,
                items: items
            }));
        }
    },

    makeBrandRoot: function(ct, item, data)
    {
        ct.add(new Ext.ux.grid.GridPanel({
            title: lang.SHOP_BRAND_LIST,
            iconCls: 'i-tag-blue',
            itype: 'shop.brd',
            rowId: 'brd_id',
            quickSearch: true,
            columns: [{
                header: lang.XEON_TITLE,
                dataIndex: 'brd_title',
                width: 200,
                itemLink: ['shop.brd', 'brd_id'],
                editor: new Ext.form.TextField({
                    allowBlank: false
                })
            },{
                header: lang.SHOP_DESCRIPTION,
                dataIndex: 'brd_description',
                editor: new Ext.form.TextArea()
            }]
        }));
    },

    makeBrandForm: function(ct, item, data)
    {
                
        cms = new Ext.ux.form.ListBox({
            fieldLabel:'Lier à une page cms',
            itype:'shop.cmstobrand',
            name:'pge_id',
            displayValue:data.db.pge_title,
            value:data.db.pge_id
        });
                
        var items = [{
            fieldLabel: lang.XEON_TITLE,
            name: 'brd_title',
            allowBlank: false,
            value: data.db.brd_title
        },{
            xtype: 'textarea',
            fieldLabel: lang.SHOP_DESCRIPTION,
            name: 'brd_description',
            value: data.db.brd_description
        },cms, new Ext.ux.form.ToggleBox({
            fieldLabel:'Actif',
            name:'brd_status'
        })]
                
        var items = [{
            fieldLabel: lang.XEON_TITLE,
            name: 'brd_title',
            allowBlank: false,
            value: data.db.brd_title
        },{
            xtype: 'textarea',
            fieldLabel: lang.SHOP_DESCRIPTION,
            name: 'brd_description',
            value: data.db.brd_description
        },cms, new Ext.ux.form.ToggleBox({
            fieldLabel:'Actif',
            name:'brd_status',
            displayValue:data.db.brd_status,
            value:data.db.brd_status
        })];
		
        if (item.add)
        {
            ct.add(new Ext.ux.FormPanel({
                items: items
            }));
        }
        else
        {
            items.push({
                xtype: 'mediabox',
                fieldLabel: lang.XEON_IMAGE,
                name: 'brd_media',
                value: data.db.brd_media,
                displayValue: data.db.brd_media_title
            });
			
            ct.add(new Ext.ux.FormPanel({
                title: lang.SHOP_BRAND,
                iconCls: ct.iconCls,
                items: items
            }));
			
			
            //Pavé référencement si activé dans la configuration
            if(data.brand_ref){
                title =  new Ext.form.TextField({
                    fieldLabel: lang.XEON_TITLE,
                    name: 'brd_meta_title',
                    value: data.db.brd_meta_title,
                    enableKeyEvents:true,
                    maxLength:70
                });
                description = new Ext.form.TextArea({
                    xtype:'textarea',
                    fieldLabel: lang.XEON_DESCRIPTION,
                    name: 'brd_meta_description',
                    value: data.db.brd_meta_description,
                    enableKeyEvents:true,
                //maxLength:70
                });
                keywords = new Ext.form.TextArea({
                    xtype: 'textarea',
                    fieldLabel: lang.XEON_KEYWORDS,
                    name: 'brd_meta_keywords',
                    value: data.db.brd_meta_keywords,
                    enableKeyEvents:true
                });

                tp = new Ext.ux.RefTip({
                    maxC:70,
                    cp:title
                });
                tp2 = new Ext.ux.RefTip({
                    maxC:170,
                    cp:description
                });
                tp3 = new Ext.ux.RefTip({
                    maxC:170,
                    cp:keywords
                });

                ct.add(new Ext.ux.FormPanel({
                    title: lang.XEON_SEO,
                    iconCls: 'i-magnifier',
                    collapsed: false,//(!data.db.fam_meta_title && !data.db.fam_meta_description && !data.db.fam_meta_keywords),
                    items: [title,description,keywords]
                }));
            }
        }
    },

    makeSupplierRoot: function(ct, item, data)
    {
        ct.add(new Ext.ux.grid.GridPanel({
            title: lang.SHOP_SUPPLIER_LIST,
            iconCls: 'i-lorry',
            itype: 'shop.spl',
            rowId: 'spl_id',
            columns: [{
                header: lang.SHOP_NAME,
                dataIndex: 'spl_name',
                width: 150,
                itemLink: ['shop.spl', 'spl_id']
            },{
                header: lang.SHOP_REFERENCE,
                dataIndex: 'spl_reference',
                editor: new Ext.form.TextField({
                    allowBlank: false
                })
            },{
                header: lang.SHOP_ADDRESS,
                dataIndex: 'spl_address',
                width: 150,
                editor: new Ext.form.TextField()
            },{
                header: lang.SHOP_ZIP_CODE,
                dataIndex: 'spl_zip_code',
                editor: new Ext.form.TextField()
            },{
                header: lang.SHOP_CITY,
                dataIndex: 'spl_city',
                editor: new Ext.form.TextField()
            },{
                header: lang.SHOP_COUNTRY,
                dataIndex: 'spl_country'
            },{
                header: lang.SHOP_PHONE,
                dataIndex: 'spl_phone',
                editor: new Ext.form.TextField()
            }]
        }));
    },

    makeSupplierForm: function(ct, item, data)
    {
        var items = [{
            fieldLabel: lang.SHOP_NAME,
            allowBlank: false,
            name: 'spl_name',
            value: data.db.spl_name
        },{
            fieldLabel: lang.SHOP_REFERENCE,
            allowBlank: false,
            name: 'spl_reference',
            value: data.db.spl_reference
        },{
            fieldLabel: lang.SHOP_ADDRESS,
            name: 'spl_address',
            value: data.db.spl_address
        },{
            fieldLabel: lang.SHOP_ZIP_CODE,
            name: 'spl_zip_code',
            value: data.db.spl_zip_code
        },{
            fieldLabel: lang.SHOP_CITY,
            name: 'spl_city',
            value: data.db.spl_city
        },{
            xtype: 'countrybox',
            fieldLabel: lang.SHOP_COUNTRY,
            name: 'spl_country',
            value: data.db.spl_country,
            displayValue: data.db.spl_country_name
        },{
            fieldLabel: lang.SHOP_PHONE,
            name: 'spl_phone',
            value: data.db.spl_phone
        }];

        if (item.add)
        {
            ct.add(new Ext.ux.FormPanel({
                items: items
            }));
        }
        else
        {
            ct.add(new Ext.ux.FormPanel({
                title: lang.SHOP_SUPPLIER,
                iconCls: ct.iconCls,
                items: items
            }));
        }
    },
	
    makeDiscountRoot: function(ct, item, data)
    {
        ct.add(new Ext.ux.grid.GridPanel({
            title: lang.SHOP_DISCOUNT_LIST,
            iconCls: 'i-note',
            itype: 'shop.dsc',
            rowId: 'dsc_id',
            search: false,
            columns: [{
                header: lang.SHOP_CODE,
                dataIndex: 'dsc_code',
                width: 150,
                itemLink: ['shop.dsc', 'dsc_id']
            },{
                header: lang.SHOP_TYPE,
                dataIndex: 'dsc_type',
                renderer: this.renderDiscountType,
                editor: new Ext.ux.form.ComboBox({
                    data: data.lang_list,
                    allowBlank: false
                })
            },{
                header: lang.SHOP_DISCOUNT_ASSIGNED_TO,
                dataIndex: 'name',
                dataType: 'string'
            },{
                header: lang.SHOP_VALUE,
                dataIndex: 'dsc_value',
                dataType: 'float',
                renderer: this.renderDiscountValue,
                editor: new Ext.ux.form.FloatField({
                    allowBlank: false
                })
            },{
                header: lang.SHOP_UNUSED_VALUE,
                dataIndex: 'dsc_unused_value',
                dataType: 'float',
                renderer: this.renderDiscountValue
            },{
                header: lang.SHOP_MIN_CART,
                dataIndex: 'dsc_min_cart',
                dataType: 'float',
                renderer: this.renderPrice2,
                editor: new Ext.ux.form.FloatField({
                    allowBlank: false
                })
            },{
                dataType:'date',
                header: lang.SHOP_START_DATE,
                dataIndex: 'dsc_start_date',
                renderFormat:'d-m-Y',
                editor: new Ext.form.DateField
            },{
                dataType:'date',
                header: lang.SHOP_END_DATE,
                dataIndex: 'dsc_end_date',
                renderFormat:'d-m-Y',
                editor: new Ext.form.DateField
            },{
                header: lang.XEON_STATUS,
                dataIndex: 'dsc_status',
                width: 60,
                renderer: Xeon.renderStatus,
                editor: new Ext.ux.form.ToggleBox
            }]
        }));
    },

    makeDiscountForm: function(ct, item, data)
    {
        var items = [{
            fieldLabel: lang.SHOP_CODE,
            allowBlank: false,
            name: 'dsc_code',
            value: data.db.dsc_code
        },{
            xtype:'listbox',
            itype:'shop.discount',
            fieldLabel:'Attitré à',
            name:'dsc_assigned_to',
            displayValue:data.db.name,
            value : data.db.dsc_assigned_to
        },{
            xtype: 'combobox',
            fieldLabel: lang.SHOP_TYPE,
            data: [[1, lang.SHOP_AMOUNT], [2, lang.SHOP_PERCENTAGE]],
            allowBlank: false,
            name: 'dsc_type',
            value: data.db.dsc_type
        },{
            xtype: 'floatfield',
            fieldLabel: lang.SHOP_VALUE,
            allowBlank: false,
            name: 'dsc_value',
            value: data.db.dsc_value
        },{
            xtype: 'floatfield',
            fieldLabel: lang.SHOP_UNUSED_VALUE,
            allowBlank: true,
            name: 'dsc_unused_value',
            value: data.db.dsc_unused_value
        },{
            xtype: 'floatfield',
            fieldLabel: lang.SHOP_MIN_CART,
            allowBlank: false,
            name: 'dsc_min_cart',
            value: data.db.dsc_min_cart
        },{
            xtype: 'date',
            fieldLabel: lang.SHOP_START_DATE,
            emptyText: lang.SHOP_NONE,
            name: 'dsc_start_date',
            value: data.db.dsc_start_date
        },{
            xtype: 'date',
            fieldLabel: lang.SHOP_END_DATE,
            emptyText: lang.SHOP_NEVER,
            name: 'dsc_end_date',
            value: data.db.dsc_end_date,
                        
        },
        ];

        if (item.add)
        {
            ct.add(new Ext.ux.FormPanel({
                items: items
            }));
        }
        else
        {
            items.push({
                xtype: 'togglebox',
                fieldLabel: lang.XEON_STATUS,
                allowBlank: false,
                name: 'dsc_status',
                value: data.db.dsc_status
            });
			
            ct.add(new Ext.ux.FormPanel({
                title: lang.SHOP_DISCOUNT,
                iconCls: ct.iconCls,
                items: items
            }));
			
        /*ct.add(new Ext.ux.ListPanel({
				title: lang.SHOP_AFFECTED_FAMILY,
				iconCls: 'i-folder',
				name: 'dsc_family_list',
				itype: 'shop.fam',
				data: data.fam
			}));*/
        }
    },
	
    makePostageRoot: function(ct, item, data)
    {
        ct.add(new Ext.ux.grid.GridPanel({
            title: lang.SHOP_POSTAGE_LIST,
            iconCls: 'i-email',
            itype: 'shop.pst',
            rowId: 'pst_id',
            columns: [{
                header: lang.SHOP_NAME,
                dataIndex: 'pst_title',
                width: 150,
                itemLink: ['shop.pst', 'pst_id']
            },{
                header: lang.SHOP_DESCRIPTION,
                dataIndex: 'pst_description',
                width: 200,
                editor: new Ext.form.TextArea()
            },{
                header: lang.SHOP_PRICE,
                dataIndex: 'pst_price',
                dataType: 'float',
                editor: new Ext.ux.form.FloatField(),
                renderer: this.renderPrice1
            },{
                header: lang.XEON_STATUS,
                dataIndex: 'pst_status',
                width: 60,
                renderer: Xeon.renderStatus,
                editor: new Ext.ux.form.ToggleBox()
            }]
        }));
    },

    makePostageForm: function(ct, item, data)
    {
        var items = [{
            fieldLabel: lang.XEON_TITLE,
            allowBlank: false,
            name: 'pst_title',
            value: data.db.pst_title
        }];
		
        if (!data.wgt)
        {
            items.push({
                xtype: 'floatfield',
                fieldLabel: lang.SHOP_PRICE,
                allowBlank: false,
                name: 'pst_price',
                value: data.db.pst_price
            });
        }
		
        items.push({
            xtype: 'floatfield',
            fieldLabel: lang.SHOP_TAX,
            allowBlank: false,
            name: 'pst_tax',
            value: data.db.pst_tax
        });

        if (item.add)
        {
            ct.add(new Ext.ux.FormPanel({
                items: items
            }));
        }
        else
        {
            items.push({
                xtype: 'textarea',
                fieldLabel: lang.SHOP_DESCRIPTION,
                name: 'pst_description',
                value: data.db.pst_description
            },{
                xtype: 'togglebox',
                fieldLabel: lang.XEON_STATUS,
                allowBlank: false,
                name: 'pst_status',
                value: data.db.pst_status
            });
			
            ct.add(new Ext.ux.FormPanel({
                title: lang.SHOP_POSTAGE_METHOD,
                iconCls: ct.iconCls,
                items: items
            }));
			
            ct.add(new Ext.ux.ListPanel({
                title: lang.SHOP_AFFECTED_COUNTRY,
                iconCls: 'i-flag-blue',
                name: 'pst_country_list',
                itype: 'xeon.ctr',
                data: data.ctr
            }));

            if (data.wgt)
            {
                ct.add(new Ext.ux.grid.GridPanel({
                    title: lang.SHOP_WEIGHT_LIST,
                    iconCls: 'i-database',
                    itype: 'shop.wgt',
                    rowId: 'wgt_id',
                    columns: [{
                        header: lang.SHOP_MAX_WEIGHT,
                        dataIndex: 'wgt_value',
                        dataType: 'float',
                        editor: new Ext.form.TextField(),
                        renderer: this.renderWeight
                    },{
                        header: lang.SHOP_PRICE,
                        dataIndex: 'wgt_price',
                        dataType: 'float',
                        editor: new Ext.form.TextField(),
                        renderer: this.renderPrice1
                    }]
                }));
            }
        }
    },
	
    makeWeightForm: function(ct, item, data)
    {
        if (item.add)
        {
            ct.add(new Ext.ux.FormPanel({
                items: [{
                    xtype: 'hidden',
                    name: 'pst_id',
                    value: data.db.pst_id
                },{
                    xtype: 'floatfield',
                    fieldLabel: lang.SHOP_MAX_WEIGHT,
                    allowBlank: false,
                    name: 'wgt_value',
                    value: data.db.wgt_value
                },{
                    xtype: 'floatfield',
                    fieldLabel: lang.SHOP_PRICE,
                    allowBlank: false,
                    name: 'wgt_price',
                    value: data.db.wgt_price
                }]
            }));
        }
    },
    
    makeFshippingForm: function(ct, item, data)
    {
        item.add = true;
        this.makeShippingForm(ct,item,data);
    },

    makeBillingForm : function(ct, item, data)
    {
        item.add = true;
        data.bill = true;
        this.makeShippingForm(ct, item, data);
    },
    
    makeShippingForm: function(ct, item, data)
    {
        var items = [{
            xtype: 'hidden',
            name: 'usr_id',
            value: data.db.usr_id
        },{
            xtype: 'titlebox',
            fieldLabel: lang.XEON_TITLE,
            allowBlank: false,
            name: (data.bill) ? 'ord_bill_title' : 'usr_title',
            value:(data.bill) ? data.db.ord_bill_title : data.db.usr_title
        },{
            fieldLabel: lang.XEON_FIRSTNAME,
            allowBlank: false,
            name: (data.bill) ? 'ord_bill_firstname' :'usr_firstname',
            value: (data.bill) ? data.db.ord_bill_firstname :data.db.usr_firstname
        },{
            fieldLabel: lang.XEON_LASTNAME,
            allowBlank: false,
            name: (data.bill) ? 'ord_bill_lastname' : 'usr_lastname',
            value: (data.bill) ? data.db.ord_bill_lastname :data.db.usr_lastname
        },{
            fieldLabel: lang.XEON_EMAIL,
            vtype: 'email',
            name: (data.bill) ? 'ord_bill_email' : 'usr_email',
            value: (data.bill) ? data.db.ord_bill_email :data.db.usr_email
        },{
            fieldLabel: lang.XEON_ADDRESS_1,
            allowBlank: false,
            maxLength: 44,
            name: (data.bill) ? 'ord_bill_address_1' : 'usr_address_1',
            value: (data.bill) ? data.db.ord_bill_address_1 :data.db.usr_address_1
        },{
            fieldLabel: lang.XEON_ADDRESS_2,
            maxLength: 44,
            name: (data.bill) ? 'ord_bill_address_2' :'usr_address_2',
            value: (data.bill) ? data.db.ord_bill_address_2 :data.db.usr_address_2
        },{
            fieldLabel: lang.XEON_ADDRESS_3,
            maxLength: 44,
            name: (data.bill) ? 'ord_bill_address_3' : 'usr_address_3',
            value:(data.bill) ? data.db.ord_bill_address_3 : data.db.usr_address_3
        },{
            fieldLabel: lang.XEON_ZIP_CODE,
            allowBlank: false,
            name: (data.bill) ? 'ord_bill_zip_code' : 'usr_zip_code',
            value: (data.bill) ? data.db.ord_bill_zip_code :data.db.usr_zip_code
        },{
            fieldLabel: lang.XEON_CITY,
            allowBlank: false,
            name: (data.bill) ? 'ord_bill_city' :'usr_city',
            value: (data.bill) ? data.db.ord_bill_city :data.db.usr_city
        },{
            xtype: 'countrybox',
            fieldLabel: lang.XEON_COUNTRY,
            allowBlank: false,
            name: (data.bill) ? 'ord_bill_country' :'usr_country',
            value: (data.bill) ? data.db.ord_bill_country :data.db.usr_country,
            displayValue: (data.bill) ? data.db.ord_bill_country :data.db.usr_country
        },{
            fieldLabel: lang.XEON_PHONE,
            allowBlank: false,
            name: (data.bill) ? 'ord_bill_phone' :'usr_phone',
            value: (data.bill) ? data.db.ord_bill_phone :data.db.usr_phone
        },{
            fieldLabel: lang.XEON_MOBILE,
            name: (data.bill) ? 'ord_bill_mobile' :'usr_mobile',
            value: (data.bill) ? data.db.ord_bill_mobile :data.db.usr_mobile
        },{
            fieldLabel: lang.XEON_FAX,
            name: (data.bill) ? 'ord_bill_fax' :'usr_fax',
            value: (data.bill) ? data.db.ord_bill_fax :data.db.usr_fax
        },{
            fieldLabel: lang.XEON_COMPANY,
            name: (data.bill) ? 'ord_bill_company' :'usr_company',
            value: (data.bill) ? data.db.ord_bill_company :data.db.usr_company
        }];
		
        if (item.add)
        {
            ct.add(new Ext.ux.FormPanel({
                items: items
            }));
        }
        else
        {
            ct.add(new Ext.ux.FormPanel({
                title: lang.SHOP_SHIPPING,
                iconCls: ct.iconCls,
                items: items
            }));
        }
    },
    
    makeMultibtqForm: function(ct, item, data)
    {
        ct.add(new Ext.ux.FormPanel(
        {
            title : (data.db.str_title) ? data.title : '',
            iconCls:'i-map',
            items : [
            {
                fieldLabel: lang.SHOP_MULTI_BTQ_TITLE,
                name: 'str_title',
                allowBlank: false,
                value : (!item.add) ? data.db.str_title : ''
            },{
                fieldLabel: lang.SHOP_MULTI_BTQ_ADRESS,
                name: 'str_adress',
                value : (!item.add) ? data.db.str_adress : ''
            },{
                fieldLabel: lang.SHOP_MULTI_BTQ_ZIPCODE,
                name: 'str_zipcode',
                allowBlank: false,
                value : (!item.add) ? data.db.str_zipcode : ''
            },{
                fieldLabel: lang.SHOP_MULTI_BTQ_TOWN,
                name: 'str_town',
                allowBlank: false,
                value : (!item.add) ? data.db.str_town : ''
            },{
                xtype: 'combobox',
                fieldLabel: lang.SHOP_MULTI_BTQ_TYPE,
                data: [[1, lang.SHOP_MULTI_BTQ_MUTUAL], [2, lang.SHOP_MULTI_BTQ_STANDALONE]],
                name: 'str_type',
                allowBlank: false,
                value: (!item.add) ? data.db.str_type: '',
                renderer: function(t) {
                    if (t == '') return lang.SHOP_MULTI_BTQ_MUTUAL;
                        return lang.SHOP_MULTI_BTQ_STANDALONE;
                    }
            },{
                xtype: 'listbox',
                fieldLabel: lang.XEON_LANG,
                allowBlank: false,
                itype: 'xeon.lng',
                name: 'str_lang',
                value: (!item.add) ? data.db.str_lang : data.db.lang,
                displayValue: (!item.add) ? data.db.str_lang_title : data.db.lang_title,
                disabled: (!item.add)
            },{
                xtype: 'listbox',
                fieldLabel: lang.SHOP_MULTI_BTQ_GRP,
                allowBlank: false,
                itype: 'user.grp',
                name: 'str_grp',
                value: (!item.add) ? data.db.str_grp : '',
                displayValue: (!item.add) ? data.db.grp_name :'',
                disabled: Xeon.user.master
            }]
        }
        ));
        
    },
    
    makePaymentForm: function(ct, item, data)
    {
        var items = [{
            fieldLabel: lang.XEON_TITLE,
            name: 'pmt_title',
            allowBlank: false,
            value: data.db.pmt_title
        },{
            xtype: 'textarea',
            fieldLabel: lang.SHOP_DESCRIPTION,
            name: 'pmt_description',
            value: data.db.pmt_description
        }];
		
        if (!item.add)
        {
            items.push({
                xtype: 'mediabox',
                fieldLabel: lang.XEON_IMAGE,
                name: 'pmt_media',
                value: data.db.pmt_media,
                displayValue: data.db.pmt_media_title
            });
        }
		
        if (item.add)
        {
            ct.add(new Ext.ux.FormPanel({
                items: items
            }));
        }
        else
        {
            ct.add(new Ext.ux.FormPanel({
                title: lang.SHOP_PAYMENT_METHOD,
                iconCls: ct.iconCls,
                items: items
            }));
        }
    },

    makeStatRoot: function(ct, item, data)
    {
        ct.add(new Ext.ux.FormPanel({
            title:lang.SHOP_STAT_PERIOD ,
            iconCls: 'i-date',
            buttons: [{
                text: lang.XEON_SUBMIT,
                handler: function() {
                    this.ownerCt.ownerCt.submit();
                    ct.reloadItem();
                }
            }],
            items:[{
                xtype: 'compositefield',
                defaults:{
                    flex:1
                },
                items: [{
                    xtype: 'datefield',
                    format: 'Y-m-d',
                    fieldLabel: 'Periode du',
                    name: 'start_date',
                    value : (data.db.start_date) ? data.db.start_date : ''
                },
                {
                    xtype: 'datefield',
                    format: 'Y-m-d',
                    fieldLabel: 'Au',
                    name: 'end_date',
                    value : (data.db.end_date) ? data.db.end_date : ''
                }]
            }]
        }));
            
        
        ct.add(new Ext.ux.GVisualizationPanelOrder({
            ct : ct,
            title:lang.SHOP_STAT_GRAPH_ORDERS,
            orderby:data.orderBY
        }));

        ct.add(new Ext.ux.GVisualizationPanel({
            ct : ct,
            title:lang.SHOP_STAT_GRAPH,
            orderby:data.orderBY
        }));
            
        var reader = new Ext.data.JsonReader({
            root: 'data',
            totalProperty: 'total',
            id: 'ord_id',
            storeId: 'ord_id'
        }, [{
            name:'ord_id',
            type:'int'
        },

        {
            name: 'date',
            type: 'string'
        },

        {
            name: 'ord_make_date',
            type: 'date'
        },

        {
            name: 'total_ht',
            type: 'string'
        },

        {
            name: 'tva_5_5', 
            type: 'string'
        },

        {
            name: 'tva_19_6',
            type: 'string'
        },

        {
            name: 'total_postage',
            type: 'string'
        },

        {
            name: 'nb',
            type: 'int'
        },

        {
            name: 'avg_cart',
            type: 'float'
        }
        ]);

        var store = new Ext.data.GroupingStore({
            url: Ext.Ajax.url,
            autoLoad: true,
            reader: reader,
            groupField:'date',
            sortInfo:{
                field: 'date',
                direction: "ASC"
            }
        });

        // console.log(store);
        var view = new Ext.grid.GroupingView({
            enableGrouping: true,
            enableGroupingMenu : true,
            groupId: 'ord_id',
            groupTextTpl: '<img src="/src/icons/silk/calendar.png" /> ' + 'Resumé : {[values.rs[0].data.date]}',
            gvalue: 'date'
        });

        ct.add(new Ext.ux.grid.GridPanel({
            height: 300,
            title: lang.SHOP_STAT_GENERAL,
            iconCls: 'i-page',
            itype: 'shop.stat',
            store:store,
            view : view,
            search:false,
            columns: [{
                header: lang.SHOP_YEAR,
                dataIndex: 'date',
                width: 100,
                hidden: true
            },{
                header: lang.SHOP_TOTAL_PRICE_HT,
                dataIndex: 'total_ht',
                renderer:this.renderPrice2,
                width: 100
            },{
                header: lang.SHOP_TVA_5_5,
                dataIndex: 'tva_5_5',
                renderer:this.renderPrice2,
                width: 100
            },{
                header: lang.SHOP_TVA_19_6,
                dataIndex: 'tva_19_6',
                renderer:this.renderPrice2,
                width: 100
            },{
                header: lang.SHOP_TOTAL_PST,
                dataIndex: 'total_postage',
                renderer:this.renderPrice2,
                width: 100
            },{
                header: lang.SHOP_NB_ORDER,
                dataIndex: 'nb',
                width: 150
            },{
                header: lang.SHOP_AVG_CART,
                dataIndex: 'avg_cart',
                renderer:this.renderPrice2,
                width: 100
            }]
        }));
    },
        
    makeOrderRoot: function(ct, item, data)
    {
         var columns=[
            {
                header: 'Imp BDC',
                dataIndex: 'ord_printed_status',
                align: 'center',
                width:50,
                renderer : function (v,m,r)
                {
                    //console.log(r.json.ord_printed);
                    if (r.json.ord_printed == '1')
                        m.css = 'x-button i-tick';
                    else
                        m.css = 'x-button i-printer';
                    r.isButton = true;
                },
                handler: function(p,r,i)
                {
                    Ext.Ajax.request({
                        params: {
                            cmd: 'xeon.exportGrid',
                            item: 'shop.ordertopdf.'+r.id,
                            parent: ''
                        },
                        success: function() {
                            Xeon.getFile();
                        }
                    })
                }
            },{
                header: 'Imp cadeau',
                dataIndex: 'ord_gift_message',
                align: 'center',
                width:70,
                renderer : function (v,m,r){
                                
                    if (r.json.ord_gift_text) {
                        m.css = 'x-button i-rosette';
                        r.isButton = true;
                    }
                },
                handler: function(p,r,i)
                {
                    Ext.Ajax.request({
                        params: {
                            cmd: 'xeon.exportGrid',
                            item: 'shop.giftpdf.'+r.id,
                            parent: ''
                        },
                        success: function() {
                            Xeon.getFile();
                        }
                    })
                }
            },{
                header: lang.SHOP_NUMBER,
                dataIndex: 'ord_id',
                dataType: 'int',
                width: 40,
                itemLink: ['shop.ord', 'ord_id']
            },{
                header: lang.XEON_MAKE_DATE,
                dataIndex: 'ord_make_date',
                renderer: Xeon.renderDate
            },{
                header: lang.XEON_MEMBER,
                dataIndex: 'ord_bill_name',
                width: 150,
                itemLink: ['user.mbr', 'usr_id']
            },{
                header: lang.XEON_EMAIL,
                dataIndex: 'ord_bill_email',
                width: 150,
                renderer: Xeon.renderEmail
            },{
                header: lang.SHOP_TOTAL,
                dataIndex: 'ord_total'
            },{
                header: lang.SHOP_WEIGHT,
                dataIndex: 'ord_weight',
                renderer: this.renderWeightG
            },{
                header: lang.SHOP_PAYMENT_METHOD,
                dataIndex: 'pmt_title',
                itemLink: ['shop.pmt', 'pmt_id']
            },{
                header: lang.SHOP_PAYMENT_DATE,
                dataIndex: 'ord_payment_date',
                align: 'center',
                renderer: function(v, m, r)
                {
                    if (v = Xeon.renderDate(v)) return v;
                    if (r.json.ord_status != 2) return '';
					
                    m.css = 'x-button i-money';
                    r.isButton = true;
                },
                handler: function(p, r, i)
                {
                    if (!r.isButton) return;
                    p.store.reload();
                           
                    Ext.Ajax.request({
                        params: {
                            cmd: 'shop.setOrderPayment',
                            id: r.id
                        }
                    });
                }
            },{
                header: lang.SHOP_POSTAGE_METHOD,
                dataIndex: 'pst_title',
                itemLink: ['shop.pst', 'pst_id']
            },{
                header: lang.SHOP_PROCESS_DATE,
                dataIndex: 'ord_process_date',
                align: 'center',
                renderer: function(v, m, r)
                {
                    if (v = Xeon.renderDate(v)) return v;
                    if (r.json.ord_status != 3) return '';
					
                    m.css = 'x-button i-package';
                    r.isButton = true;
                },
                handler: function(p, r, i)
                {
                    if (!r.isButton) return;
                    p.store.remove(r);

                    Ext.Ajax.request({
                        params: {
                            cmd: 'shop.setOrderProcess',
                            id: r.id
                        }
                    });
                }
            },{

                header: lang.SHOP_MEMO,
                dataIndex: 'ord_memo',
                width: 150,
                renderer: Xeon.renderText,
                editor: new Ext.form.TextArea()
            }];

			if(data.colissimo){
				 columns.push({
						header: lang.SHOP_COLISSIMO,
						dataIndex: 'ord_package_id',
						align: 'left',
						renderer: function(v, m, r)
						{
							if (v != "") return v;
							if (r.json.ord_col_status == 1) return 'En cours';
							if (r.json.ord_status != 3) return '';
							// return v;
											
							m.css = 'x-button i-package';
							r.isButton = true;
						},
						handler: function(p, r, i)
						{
							
							if (r.json.ord_col_status != 1){
								Ext.Msg.confirm(lang.XEON_CONFIRM, lang.SHOP_CONFIRM_PROCESS_COLISSIMO, function(btn)
								{
									if(btn!='no'){
										if (r.json.ord_status == 3){
											//p.store.remove(r);
											

											Ext.Ajax.request({
												params: {
													cmd: 'shop.setCollissimoProcess',
													id: r.id
												},
												success:function(){p.store.reload();}
											});
										}
									}
								});
							}
						}
					});   
			}
			
        ct.add(new Ext.ux.grid.GridPanel({
            title: lang.SHOP_ORDER_LIST,
            iconCls: 'i-script',
            itype: 'shop.ord',
            rowId: 'ord_id',
            paging: true,
            quickSearch: new Ext.ux.form.ComboBox({
                emptyText: lang.SHOP_ORDER_STEP,
                data: [
                [1, lang.SHOP_ESTIMATE, 'i-page-white-edit'],
                [2, lang.SHOP_PAYMENT_TODO, 'i-money'],
                [3, lang.SHOP_PROCESS_TODO, 'i-package'],
                [4, lang.SHOP_COMPLETED, 'i-tick'],
                [5, lang.SHOP_CANCELLED, 'i-cross']
                ],
                value: (data.torder) ? data.torder : 3
            }),
            search: engine.getSearchOrderItems(data),
            columns: columns
        }));
    },
        
    // data contains the filter value for the order grid by devis/a traiter/ a payer (data.torder)
    makeOrder1Form : function(ct, item, data)
    {
        this.makeOrderRoot(ct,item,data);
    },
        
    makeOrder2Form : function(ct, item, data)
    {
        this.makeOrderRoot(ct,item,data);
    },
        
    makeOrder3Form : function(ct, item, data)
    {
        this.makeOrderRoot(ct,item,data);
    },

    makeOrderForm: function(ct, item, data)
    {
        if (item.add)
        {
            //console.log(data);
            ct.add(new Ext.ux.FormPanel({
                items: [{
                    xtype: 'listbox',
                    fieldLabel: lang.USER_MEMBER,
                    allowBlank: false,
                    itype: 'user.mbr',
                    name: 'usr_id',
                    displayValue: (data.user.usr_firstname) ? (data.user.usr_firstname + ' ' + data.user.usr_lastname):'',
                    value : data.user.usr_id
                },{
                    xtype: 'combobox',
                    fieldLabel: lang.SHOP_SOURCE,
                    allowBlank: false,
                    data: [[1, lang.SHOP_WEB], [2, lang.SHOP_PHONE], [3, lang.SHOP_MAIL], [4, lang.SHOP_STORE]],
                    name: 'ord_type'
                }]
            }));
        }
        else
        {
            //ORDER SUM-UP
            ct.add(new Ext.ux.DataPanel({
                title: lang.SHOP_PRODUCT_LIST,
                iconCls: 'i-package',
                data: data.itm,
                cls: 'x-datapanel x-orderpanel',
				
                editData: function(r, add, onUpdate)
                {
                    var p = this;
                    var db = r.data.info;
					
                    new Ext.ux.Window({
                        //width: 700,
                        itemId: r.data.item,
                        title: add ? lang.SHOP_PRODUCT_ADD : lang.SHOP_PRODUCT_EDIT,
                        iconCls: 'i-database',
                        items: new Ext.ux.FormPanel({
                            // width: 700,
                            addItem: false,
                            params: {
                                cmd: 'xeon.saveData',
                                item: r.data.item,
                                parent: p.ownerCt.itemId
                            },
                            success: function(a) {
                                onUpdate.call(this, a.json);
                            },
                            items:[{
                                xtype:'fieldset',
                                title: 'Ajout produit',
                                items: [{
                                    xtype: 'listbox',
                                    fieldLabel: lang.SHOP_PRODUCT,
                                    //allowBlank: false,
                                    itype: 'shop.prd',
                                    name: 'prd_id',
                                    value: db.prd_id,
                                    displayValue: db.itm_product
                                },{
                                    xtype: 'listbox',
                                    fieldLabel: lang.SHOP_OPTION,
                                    //allowBlank: false,
                                    itype: 'shop.opt',
                                    link: 'prd_id',
                                    name: 'opt_id',
                                    value: db.opt_id,
                                    displayValue: db.itm_option
                                },{
                                    fieldLabel: lang.SHOP_QUANTITY,
                                    //allowBlank: false,
                                    name: 'itm_quantity',
                                    value: db.itm_quantity
                                },{
                                    fieldLabel: lang.SHOP_ORDER_ITEM_INFO,
                                    name: 'itm_info',
                                    value: db.itm_info
                                }]
                            },

                            {
                                xtype:'fieldset',
                                title: 'Ajout flash',
                                items :[{
                                    xtype: 'compositefield',
                                    labelWidth: 120,
                                    items: [{
                                        xtype     : 'textfield',
                                        fieldLabel: 'Référence',
                                        name: 'opt_title',
                                        enableKeyEvents:true,
                                        listeners: {
                                            'blur': function(p,e)
                                            {
                                                if (v = p.getValue()){
                                                    p.setValue(lang.XEON_LOADING);
                                                    Ext.Ajax.request({
                                                        params: {
                                                            cmd: 'xeon.makeList',
                                                            item: 'shop.ref',
                                                            parent:0,
                                                            link:0,
                                                            query:v
                                                        },
                                                        success: function(r,o) {
                                                            Ext.getCmp('opt_id').setValue(r.json.data.opt_id);
                                                            Ext.getCmp('prd_id').setValue(r.json.data.prd_id);
                                                            p.setValue(r.json.data.prd_title+' '+r.json.data.opt_title);
                                                        }
                                                    })
                                                }
                                            }
                                        },
                                        width     : 180
                                    },{
                                        xtype     : 'textfield',
                                        hidden:true,
                                        name: 'flash_opt',
                                        id:'opt_id'
                                    },{
                                        xtype     : 'textfield',
                                        hidden:true,
                                        name: 'flash_prd',
                                        id:'prd_id'
                                    },{
                                        xtype     : 'textfield',
                                        fieldLabel: 'Quantité',
                                        name: 'flash_quantity',
                                        width:30
                                    },{
                                        xtype     : 'textfield',
                                        fieldLabel: 'Message',
                                        name: 'flash_info',
                                        value: db.itm_info,
                                        flex      : 1
                                    }]
                                }]
                            }]
                        })
                    }).show();
                },
				
                updateTotal: function(html)
                {
                    var s = this.view.store;
                    var r = s.getAt(s.getCount()-1);
					
                    r.set('html', html);
                },
				
                getToolbar: function(p)
                {
                    var p = this;
					
                    return [{
                        iconCls: 'i-add',
                        tooltip: lang.SHOP_PRODUCT_ADD,
                        handler: function()
                        {
                            var i = p.view.store.getCount()-1;
                            var r = new p.Record({
                                item: 'shop.itm.0',
                                html: '',
                                info: {}
                            });
							
                            p.editData(r, true, function(json)
                            {
                                r.set('item', json.id);
                                r.set('html', json.html);
                                r.set('info', '');
                                r.set('info', json.db);
								
                                p.view.store.insert(i, r);
                                p.highlight(i);
                                p.updateTotal(json.total);
                            });
                        }
                    },{
                        iconCls: 'i-page-white-edit',
                        tooltip: lang.XEON_EDIT,
                        handler: function()
                        {
                            var i = p.getSelectedIndex();
                            var r = p.getSelectedRecord();
							
                            p.editData(r, false, function(json)
                            {
                                r.set('html', json.html);
                                r.set('info', '');
                                r.set('info', json.db);
								
                                p.highlight(i);
                                p.updateTotal(json.total);
                            });
                        }
                    },{
                        iconCls: 'i-cross',
                        tooltip: lang.XEON_DELETE,
                        handler: function()
                        {
                            var i = p.getSelectedIndex();
                            var r = p.getSelectedRecord();
				
                            Ext.Msg.confirm(lang.XEON_CONFIRM, lang.XEON_DELETE, function(btn)
                            {
                                if (btn == 'yes')
                                {
                                    Ext.Ajax.request({
                                        params: {
                                            cmd: 'xeon.deleteItem',
                                            item: r.data.item,
                                            parent: p.ownerCt.itemId
                                        },
                                        success: function(a) {
                                            p.updateTotal(a.json.total);
                                        }
                                    });
									
                                    p.view.store.remove(r);
                                    p.hideToolbar();
                                }
                            });
                        }
                    }];
                },
				
                checkToolbar: function(s, i, t)
                {
                    if (data.db.ord_status != 1) {
                        return false;
                    }
					
                    if (i == s.getCount()-1) {
                        return false;
                    }
					
                    t.get(1).setDisabled(i == 0);
                    t.get(2).setDisabled(i == 0);
                }
            }));
			
            //OTHERS INFORMATIONS
            if (data.db.ord_status == 1)
            {
                info = new Ext.ux.FormPanel({
                    title: lang.SHOP_ORDER,
                    iconCls: 'i-script',
                    success: function() {
                        ct.reloadItem();
                    },
                    items: [{
                        xtype: 'listbox',
                        fieldLabel: lang.SHOP_DISCOUNT,
                        itype: 'shop.dsc',
                        name: 'n_dsc_id',
                        displayValue : data.db.n_dsc_code,
                        value : data.db.n_dsc_id
                    },{
                        xtype: 'listbox',
                        fieldLabel: lang.SHOP_USER_DISCOUNT,
                        itype: 'shop.user_dsc',
                        name: 'dsc_id',
                        displayValue:data.db.dsc_code,
                        value : data.db.dsc_id
                    },{
                        fieldLabel: lang.SHOP_ADMIN_DISCOUNT,
                        name: 'ord_extra'
                    },{
                        xtype: 'listbox',
                        id:'ship-'+data.db.usr_id,
                        fieldLabel: lang.SHOP_SHIPPING,
                        allowBlank: false,
                        itype: 'shop.shp',
                        name: 'shp_id',
                        displayValue : data.db.ship,
                        value : data.db.shp_id,
                        listeners: {
                            'select': function(c, r, i)
                            {
                                if (r.data.v == -1)
                                {
                                    c.clear();
                                    Xeon.loadItem('shop.shp.0', data.member);
                                }
                            }
                        }
                    },{
                        xtype:'button',
                        iconCls: 'i-wrench',
                        cls : 'x-edit-field',
                        tooltip:lang.SHOP_SHIP_TOOLTIP,
                        listeners: {
                            'click': function(c, r)
                            {
                                //console.log('ship-'+data.db.usr_id);
                                shipbox = info.findById('ship-'+data.db.usr_id);
                                 
                                Ext.Ajax.request({
                                    params: {
                                        cmd: 'xeon.makeForm',
                                        item: 'shop.shpf.'+ shipbox.getValue(),
                                        parent: data.member
                                    },
                                    before: function() {
                                        Xeon.ui.view.el.mask(lang.XEON_LOADING, 'x-mask-loading');
                                    },
                                    success: function(a)
                                    {
                                        var win = new Ext.ux.Window({
                                            itemId: 'shop.shpf.'+ shipbox.getValue(),
                                            iconCls: a.json.icon,
                                            title: a.json.title
                                        });

                                        Xeon.callback(item.module, a.json.callback, win, item, a.json);
                                        
                                        win.on('close', function (){
                                            shipbox.store.on('load', function(){
                                                shipbox.setValue(shipbox.getValue());
                                            });
                                            shipbox.store.load();
                                        })
                                        
                                        win.show();
                                    },
                                    callback: function() {
                                        Xeon.ui.view.el.unmask();
                                    }
                                });
                            }
                        }
                    },{
                        xtype: 'listbox',
                        id:'bill-'+data.db.usr_id,
                        fieldLabel: lang.SHOP_BILLING,
                        itype: 'shop.bill',
                        allowBlank: false,
                        name: 'ship_bill',
                        displayValue: data.db.ship_bill_title,
                        value : 0
                    },{
                        xtype: 'listbox',
                        fieldLabel: lang.SHOP_POSTAGE_METHOD,
                        itype: 'shop.pst',
                        allowBlank:false,
                        link: 'shp_id',
                        name: 'pst_id',
                        displayValue: data.db.pst_title,
                        value : data.db.pst_id
                    },{
                        xtype:'button',
                        iconCls: 'i-wrench',
                        cls : 'x-edit-field-bill',
                        tooltip:lang.SHOP_BILL_TOOLTIP,
                        listeners: {
                            'click': function(c, r)
                            {
                                billbox = info.findById('bill-'+data.db.usr_id);
                                Ext.Ajax.request({
                                    params: {
                                        cmd: 'xeon.makeForm',
                                        item: 'shop.bill.'+data.db.ord_id,
                                        parent: data.member
                                    },
                                    before: function() {
                                        Xeon.ui.view.el.mask(lang.XEON_LOADING, 'x-mask-loading');
                                    },
                                    success: function(a)
                                    {
                                        var win = new Ext.ux.Window({
                                            itemId: 'shop.bill.'+data.db.ord_id,
                                            iconCls: a.json.icon,
                                            title: a.json.title
                                        });

                                        Xeon.callback(item.module, a.json.callback, win, item, a.json);

                                        win.on('close', function (){
                                            billbox.store.on('load', function(){
                                                billbox.setValue(billbox.getValue());
                                            });
                                            billbox.store.load();
                                        })

                                        win.show();
                                    },
                                    callback: function() {
                                        Xeon.ui.view.el.unmask();
                                    }
                                });
                            }
                        }
                    }]
                });
                
                ct.add(info);
                
                ct.add(new Ext.ux.FormPanel({
                    title : lang.SHOP_ORDER_PAYMENT,
                    iconCls: 'i-money',
                    success: function() {
                        ct.reloadItem();
                    },
                    items:[{
                        xtype: 'listbox',
                        fieldLabel: lang.SHOP_PAYMENT_METHOD,
                        allowBlank: false,
                        itype: 'shop.pmt',
                        name: 'pmt_id'
                    },{
                        xtype: 'textarea',
                        fieldLabel: lang.SHOP_MEMO,
                        name: 'ord_memo',
                        value: data.db.ord_memo
                    }]
                }));
            }
            else
            {
                var columns= [{
                    infoLabel: lang.XEON_MAKE_DATE,
                    value: data.info.make
                },{
                    infoLabel: lang.SHOP_ORDER_STEP,
                    value: data.db.ord_status,
                    renderer: this.renderOrderStep
                },{
                    infoLabel: lang.SHOP_SOURCE,
                    value: data.db.ord_type,
                    renderer: this.renderOrderSource
                },{
                    infoLabel: lang.SHOP_POSTAGE_METHOD,
                    value: data.db.pst_title
                },{
                    infoLabel: lang.SHOP_PAYMENT_METHOD,
                    value: data.db.pmt_title
                },{
                    infoLabel: lang.SHOP_BILLING,
                    value: data.info.billing
                },{
                    infoLabel: lang.SHOP_SHIPPING,
                    value: data.info.shipping
                },{
                    infoLabel: lang.SHOP_COMMENT_CUSTOMER,
                    value: data.db.ord_comment
                },{
                    infoLabel: lang.SHOP_MESSAGE_CUSTOMER,
                    value: data.db.ord_message
                }];
					
                if(data.db.ord_isapresent==1){
                    columns.push({
                        infoLabel: lang.SHOP_PRESENT,
                        value: lang.SHOP_THIS_IS_A_PRESENT,
                    });
                }
				
                ct.add(new Ext.ux.InfoPanel({
                    title: lang.XEON_INFO,
                    iconCls: 'i-information',
                    items: columns
                }));
				
                ct.add(new Ext.ux.FormPanel({
                    title: lang.SHOP_ORDER,
                    iconCls: ct.iconCls,
                    success: function(){
                        ct.reloadItem();
                    },
                    items: [{
                        xtype: 'textarea',
                        fieldLabel: lang.SHOP_MEMO,
                        name: 'ord_memo',
                        value: data.db.ord_memo
                    },{
                        fieldLabel: lang.SHOP_PACKAGE_ID,
                        name: 'ord_tracker',
                        value: data.db.ord_package_id
                    },{
                        xtype: 'combobox',
                        data: [ [1, lang.SHOP_ESTIMATE], [2, lang.SHOP_PAYMENT_TODO], [3, lang.SHOP_PROCESS_TODO], [4, lang.SHOP_COMPLETED], [5, lang.SHOP_CANCELLED]],
                        value: data.db.ord_status,
                        fieldLabel: lang.SHOP_LABEL_ORDER_STATUS,
                        name: 'ord_status'
                    }]
                }));
            }
            //EXPORT BUTTONS
            var buttons = [];
            buttons.push({
                text: lang.SHOP_LABEL_EXPORT_CSV,
                iconCls:'i-page-white-excel',
                handler: function() {
                    Xeon.module.shop.exportOrder( 'shop.exportCsvOrder', data.db.ord_id );
                }
            },
            {
                text: lang.SHOP_LABEL_EXPORT_PDF,
                iconCls:'i-page-white-acrobat',
                handler: function() {
                    Xeon.module.shop.exportOrder( 'shop.exportPdfOrder', data.db.ord_id );
                }
            });
            
            ct.add( new Ext.Panel({
                title: 'Export',
                iconCls: 'i-disk',
                buttonAlign: 'center',
                buttons: buttons
            }));
        }
    },
	
    makeModuleForm: function(ct, item, data)
    {
        ct.add(new Ext.ux.FormPanel({
            title: lang.XEON_SETTINGS,
            iconCls: 'i-cog',
            items: [{
                fieldLabel: lang.SHOP_FAMILY_URL,
                name: 'family_url',
                value: data.cfg.family_url
            },{
                fieldLabel: lang.SHOP_PRODUCT_URL,
                name: 'product_url',
                value: data.cfg.product_url
            },{
                xtype: 'togglebox',
                fieldLabel: lang.XEON_URL_FULL_TITLE,
                name: 'url_full_title',
                value: data.cfg.url_full_title
            },{
                xtype: 'combobox',
                fieldLabel: lang.SHOP_IMAGE,
                data: [[1, '1'], [2, '2'], [3, '3'], [4, '4']],
                name: 'nb_photo',
                value: (data.cfg.nb_image) ? data.cfg.nb_image : 3
            },{
                xtype: 'combobox',
                fieldLabel: lang.SHOP_LEVEL,
                data: [[1, '1'], [2, '2'], [3, '3'], [4, '4']],
                name: 'level',
                value: data.cfg.level
            },{
                fieldLabel: lang.SHOP_DEFAULT_TAX,
                name: 'default_tax',
                value: data.cfg.default_tax
            },{
                xtype: 'combobox',
                fieldLabel: lang.SHOP_CURRENCY,
                data: [[1, lang.SHOP_EURO, 'i-money-euro'], [2, lang.SHOP_DOLLAR, 'i-money-dollar'], [3, lang.SHOP_POUND, 'i-money-pound'], [4, lang.SHOP_YEN, 'i-money-yen']],
                name: 'currency',
                value: data.cfg.currency
            },{
                fieldLabel: lang.XEON_TEMPLATE_LIST,
                name: 'layout_template',
                value: data.cfg.layout_template
            },{
                xtype: 'togglebox',
                fieldLabel: lang.XEON_COMMENT_CHECK,
                name: 'comment_check',
                value: data.cfg.comment_check
            },{
                fieldLabel: lang.XEON_RATING_MAX,
                name: 'rating_max',
                value: data.cfg.rating_max
            }]
        }));
		
        ct.add(new Ext.ux.FormPanel({
            title: lang.XEON_OPTIONS,
            iconCls: 'i-brick',
            items: [{
                xtype: 'togglebox',
                fieldLabel: lang.SHOP_BRANDS,
                name: 'plugin_brand',
                value: data.cfg.plugin_brand
            },{
                xtype: 'togglebox',
                fieldLabel: lang.SHOP_BRANDS_REF,
                name: 'plugin_brand_ref',
                value: data.cfg.plugin_brand_ref
            },{
                xtype: 'togglebox',
                fieldLabel: lang.SHOP_SUPPLIERS,
                name: 'plugin_supplier',
                value: data.cfg.plugin_supplier
            },{
                xtype: 'togglebox',
                fieldLabel: lang.SHOP_FAMILY_TAG,
                name: 'plugin_family_tag',
                value: data.cfg.plugin_family_tag
            },{
                xtype: 'togglebox',
                fieldLabel: lang.SHOP_PRODUCT_TAG,
                name: 'plugin_product_tag',
                value: data.cfg.plugin_product_tag
            },{
                xtype: 'togglebox',
                fieldLabel: lang.SHOP_PRODUCT_LIFESPAN,
                name: 'plugin_product_lifespan',
                value: data.cfg.plugin_product_lifespan
            },{
                xtype: 'togglebox',
                fieldLabel: lang.SHOP_LINK_FAMILY,
                name: 'plugin_link_family',
                value: data.cfg.plugin_link_family
            },{
                xtype: 'togglebox',
                fieldLabel: lang.SHOP_LINK_PRODUCT,
                name: 'plugin_link_product',
                value: data.cfg.plugin_link_product
            },{
                xtype: 'togglebox',
                fieldLabel: lang.SHOP_LINK_PAGE,
                name: 'plugin_link_page',
                value: data.cfg.plugin_link_page
            },{
                xtype: 'togglebox',
                fieldLabel: lang.SHOP_OPTIONS,
                name: 'plugin_option',
                value: data.cfg.plugin_option
            },{
                xtype: 'togglebox',
                fieldLabel: lang.SHOP_ECOTAX,
                name: 'plugin_ecotax',
                value: data.cfg.plugin_ecotax
            },{
                xtype: 'togglebox',
                fieldLabel: lang.SHOP_EXTRA,
                name: 'plugin_extra',
                value: data.cfg.plugin_extra
            },{
                xtype: 'togglebox',
                fieldLabel: lang.SHOP_STOCK,
                name: 'plugin_stock',
                value: data.cfg.plugin_stock
            },{
                xtype: 'togglebox',
                fieldLabel: lang.SHOP_DELAY,
                name: 'plugin_delay',
                value: data.cfg.plugin_delay
            },{
                xtype: 'togglebox',
                fieldLabel: lang.XEON_COMMENTS,
                name: 'plugin_comment',
                value: data.cfg.plugin_comment
            },{
                xtype: 'togglebox',
                fieldLabel: lang.SHOP_RATING,
                name: 'plugin_rating',
                value: data.cfg.plugin_rating
            },{
                xtype: 'togglebox',
                fieldLabel: lang.SHOP_WEIGHTS,
                name: 'plugin_weight',
                value: data.cfg.plugin_weight
            },{
                xtype: 'togglebox',
                fieldLabel: lang.XEON_FIDELITY,
                name: 'plugin_fidelity',
                value: data.cfg.plugin_fidelity
            },{
                xtype: 'togglebox',
                fieldLabel: lang.SHOP_OPTION,
                name: 'plugin_option',
                value: data.cfg.plugin_option
            },{
                xtype: 'togglebox',
                fieldLabel: lang.SHOP_STAT,
                name: 'plugin_stat',
                value: data.cfg.plugin_stat
            },{
                xtype: 'togglebox',
                fieldLabel: lang.SHOP_GIFT_MESSAGE,
                name: 'plugin_gift',
                value: data.cfg.plugin_gift
            },{
                xtype: 'togglebox',
                fieldLabel: lang.SHOP_GOOGLE_SHOPPING_XML,
                name: 'plugin_google_shopping',
                value: data.cfg.plugin_redirect_delete_product
            },{
                xtype: 'togglebox',
                fieldLabel: lang.SHOP_REDIRECT_AFTER_DELETE,
                name: 'plugin_redirect_delete_product',
                value: data.cfg.plugin_redirect_delete_product
            },{
                xtype: 'togglebox',
                fieldLabel: lang.SHOP_ALERT_STOCK_PROMO,
                name: 'plugin_alert_stock_promo',
                value: data.cfg.plugin_alert_stock_promo
            },{
                fieldLabel: lang.SHOP_INTERVAL_ALERT_PROMO,
                name: 'plugin_interval_alert_promo',
                value: data.cfg.plugin_interval_alert_promo
            },{
                fieldLabel: 'Email',
                name: 'adress_email',
                value: data.cfg.adress_email
            },{
                xtype: 'togglebox',
                fieldLabel: lang.SHOP_EDIT_DELAY_STOCK_PRICE,
                name: 'plugin_edit_delay_stock_price',
                value: data.cfg.plugin_edit_delay_stock_price
            },{
                xtype: 'togglebox',
                fieldLabel: lang.SHOP_MULTI_BTQ,
                name: 'plugin_multi_btq',
                value: data.cfg.plugin_multi_btq
            },{
                fieldLabel: lang.SHOP_EDIT_FEEDBACK_EMAIL,
                name: 'plugin_feedbackmail',
                value: data.cfg.plugin_feedbackmail
            },{                
                fieldLabel: lang.SHOP_EDIT_DELAY_FEEDBACK_EMAIL,
                name: 'plugin_feedbackmail_delay',
                value: data.cfg.plugin_feedbackmail_delay
            }]
        }));
                
        ct.add(new Ext.ux.FormPanel({
            title: lang.XEON_SEARCHENGINE,
            iconCls: 'i-brick',
            itemId:'shopengine',
            items: [{
                xtype: 'togglebox',
                fieldLabel: lang.SHOP_SLIDER,
                name: 'slider_field',
                value: data.cfg.shopengine.slider_field
            },{
                xtype: 'togglebox',
                fieldLabel: 'Par stock',
                name: 'stock_field',
                value: data.cfg.shopengine.stock_field
            },{
                xtype: 'togglebox',
                fieldLabel: 'Par reference',
                name: 'reference_field',
                value: data.cfg.shopengine.reference_field
            },{
                xtype: 'togglebox',
                fieldLabel: 'Par famille',
                name: 'family_field',
                value: data.cfg.shopengine.family_field
            },{
                xtype: 'togglebox',
                fieldLabel: 'Par status',
                name: 'status_field',
                value: data.cfg.shopengine.status_field
            },{
                xtype: 'togglebox',
                fieldLabel: 'Par marque',
                name: 'brand_field',
                value: data.cfg.shopengine.brand_field
            },{
                xtype: 'togglebox',
                fieldLabel: 'Par promotion',
                name: 'promo_field',
                value: data.cfg.shopengine.promo_field
            },{
                xtype: 'togglebox',
                fieldLabel: 'Par promotion home',
                name: 'promohome_field',
                value: data.cfg.shopengine.promohome_field
            }]
        }));

        ct.add(new Ext.ux.FormPanel({
            title: lang.SHOP_ORDERENGINE,
            iconCls: 'i-brick',
            itemId:'orderengine',
            items: [{
                xtype: 'togglebox',
                fieldLabel: lang.SHOP_ENGINE_ORDER_NAME,
                name: 'name_field',
                value: data.cfg.orderengine.name_field
            },{
                xtype: 'togglebox',
                fieldLabel: lang.SHOP_ENGINE_ORDER_NUMBER,
                name: 'id_order_field',
                value: data.cfg.orderengine.id_order_field
            },{
                xtype: 'togglebox',
                fieldLabel: lang.SHOP_ENGINE_ORDER_DATE,
                name: 'date_field',
                value: data.cfg.orderengine.date_field
            },{
                xtype: 'togglebox',
                fieldLabel: lang.SHOP_ENGINE_CUSTOMER_NUMBER,
                name: 'id_customer_field',
                value: data.cfg.orderengine.id_customer_field
            },{
                xtype: 'togglebox',
                fieldLabel: lang.SHOP_ENGINE_ORDER_SOURCE_TYPE,
                name: 'source_field',
                value: data.cfg.orderengine.source_field
            },{
                xtype: 'togglebox',
                fieldLabel: lang.SHOP_ENGINE_ORDER_PAYMENT_TYPE,
                name: 'payment_field',
                value: data.cfg.orderengine.payment_field
            },{
                xtype: 'togglebox',
                fieldLabel: 'Par mail',
                name: 'email_field',
                value: data.cfg.orderengine.email_field
            }]
        }));
		
		ct.add(new Ext.ux.FormPanel({
            title: lang.SHOP_PLUGIN_EXPEDIER_ACTIF,
            iconCls: 'i-cog',
            items: [
				{
					xtype: 'togglebox',
					fieldLabel: lang.SHOP_PLUGIN_EXPEDIER_ACTIF,
					name: 'plugin_expedier',
					value: data.cfg.plugin_expedier
				},
				{
					xtype: 'togglebox',
					fieldLabel: lang.SHOP_PLUGIN_EXPEDIER_FTP_ACTIF,
					name: 'expedier_ftp',
					value: data.cfg.expedier_ftp
				},
				{
					
					fieldLabel: lang.SHOP_PLUGIN_EXPEDIER_FTP_SERVER,
					name: 'expedier_ftp_server',
					value: data.cfg.expedier_ftp_server
				},
				{
					
					fieldLabel: lang.SHOP_PLUGIN_EXPEDIER_FTP_USERNAME,
					name: 'expedier_ftp_user',
					value: data.cfg.expedier_ftp_user
				},
				{
					
					fieldLabel: lang.SHOP_PLUGIN_EXPEDIER_FTP_PASSWORD,
					name: 'expedier_ftp_password',
					value: data.cfg.expedier_ftp_password
				},
			]
		}));
    },
      
    //GRILLE DES MODIFICATIONS DE PRIS, DELAY ET STOCKS
    makeEditdspRoot: function(ct, item, data){
        //MODIFICATION DU PRIX
        var columns = [{
            header: lang.SHOP_PRODUCT,
            dataIndex: 'prd_title',
            width: 200,
            itemLink: ['shop.prd', 'prd_id']
        },{
            header: lang.SHOP_OPTION,
            dataIndex: 'opt_title',
            width: 120,
            itemLink: ['shop.opt', 'opt_id']
        },{
            header: lang.SHOP_REFERENCE,
            width: 100,
            dataIndex: 'opt_reference'
        },{
            header: lang.SHOP_PRICE,
            dataIndex: 'opt_price',
            dataType: 'float',
            width: 60,
            editor: new Ext.form.TextField(),
            renderer: this.renderPrice1
        },{
            header: lang.SHOP_PROMOTION,
            dataIndex: 'opt_promotion',
            dataType: 'float',
            width: 80,
            editor: new Ext.form.TextField(),
            renderer: this.renderPrice2
        },{
            dataType:'date',
            header: lang.SHOP_PROMOTION_DATE_BEGIN,
            dataIndex: 'opt_promotion_begin_date',
            renderFormat:'d-m-Y',
            editor: new Ext.form.DateField
        },{
            dataType:'date',
            header: lang.SHOP_PROMOTION_DATE_END,
            dataIndex: 'opt_promotion_end_date',
            renderFormat:'d-m-Y',
            editor: new Ext.form.DateField
        }];
        ct.add(new Ext.ux.grid.GridPanel({
            title: lang.SHOP_EDIT_PRICE,
            iconCls: 'i-brick',
            itype: 'shop.opt',
            rowId: 'opt_id',
            search: engine.getSearchOptionsItems(data),
            enableDragDrop: true,
            columns: columns,
            paging: true
        }));
					
        //MODIFICATION DU STOCK
        var columns = [{
            header: lang.SHOP_PRODUCT,
            dataIndex: 'prd_title',
            width: 200,
            itemLink: ['shop.prd', 'prd_id']
        },{
            header: lang.SHOP_OPTION,
            dataIndex: 'opt_title',
            width: 120,
            itemLink: ['shop.opt', 'opt_id']
        },{
            header: lang.SHOP_REFERENCE,
            width: 100,
            dataIndex: 'opt_reference'
        },{
            header: lang.SHOP_PRICE,
            dataIndex: 'opt_price',
            dataType: 'float',
            width: 60,
            editor: new Ext.form.TextField(),
            renderer: this.renderPrice1
        },{
            header: lang.SHOP_PROMOTION,
            dataIndex: 'opt_promotion',
            dataType: 'float',
            width: 80,
            editor: new Ext.form.TextField(),
            renderer: this.renderPrice2
        },{
            header: lang.SHOP_STOCK,
            dataIndex: 'opt_stock',
            width: 80,
            editor: new Ext.form.TextField()
        },{
            header: lang.SHOP_SEUIL_STOCK,
            dataIndex: 'opt_seuil_stock',
            width: 80,
            editor: new Ext.form.TextField()
        }];
        ct.add(new Ext.ux.grid.GridPanel({
            title: lang.SHOP_EDIT_STOCK,
            iconCls: 'i-brick',
            itype: 'shop.opt',
            rowId: 'opt_id',
            search: engine.getSearchOptionsItems(data),
            enableDragDrop: true,
            columns: columns,
            paging: true
        }));
		
			
        //MODIFICATION DES DELAIS
        if(data.delay){
            var columns = [{
                header: lang.SHOP_PRODUCT,
                dataIndex: 'prd_title',
                width: 200,
                itemLink: ['shop.prd', 'prd_id']
            },{
                header: lang.SHOP_OPTION,
                dataIndex: 'opt_title',
                width: 120,
                itemLink: ['shop.opt', 'opt_id']
            },{
                header: lang.SHOP_REFERENCE,
                width: 100,
                dataIndex: 'opt_reference'
            },{
                header: lang.SHOP_PRICE,
                dataIndex: 'opt_price',
                dataType: 'float',
                width: 60,
                editor: new Ext.form.TextField(),
                renderer: this.renderPrice1
            },{
                header: lang.SHOP_PROMOTION,
                dataIndex: 'opt_promotion',
                dataType: 'float',
                width: 80,
                editor: new Ext.form.TextField(),
                renderer: this.renderPrice2
            },{
                header: lang.SHOP_DELAY,
                dataIndex: 'del_id',
                width: 80 ,
                editor: new Ext.ux.form.DelayBox() ,
                renderer: function(e){
                    var bin=new Ext.ux.form.DelayBox();
                    return bin.getValueToDisplay(e);
                }
					
            }];
				
            ct.add(new Ext.ux.grid.GridPanel({
                title: lang.SHOP_EDIT_DELAY,
                iconCls: 'i-brick',
                itype: 'shop.opt',
                rowId: 'opt_id',
                search: engine.getSearchOptionsItems(data),
                enableDragDrop: true,
                columns: columns,
                paging: true
            }));
        }
		
    },
	
    //GRILLE DES ALERTES PROMOTIONS ET STOCKS
    makeAlertRoot: function(ct, item, data)
    {
        var Promo_Grid = new Ext.ux.grid.GridPanel({
            title: lang.SHOP_ALERT_PROMOS,
            iconCls: 'i-note',
            itype: 'shop.alertp',
            rowId: 'opt_id',
            search: false,
            columns: [{
                header: lang.SHOP_PRODUCT,
                dataIndex: 'prd_title',
                width: 140,
                itemLink: ['shop.prd', 'prd_id']
            },{
                header: lang.SHOP_OPTION,
                dataIndex: 'opt_title',
                itemLink: ['shop.opt', 'opt_id']
            },{
                header: lang.SHOP_REFERENCE,
                dataIndex: 'opt_reference'
				
            },{
                header: lang.SHOP_PRICE,
                dataIndex: 'opt_price',
                renderer: this.renderPrice1
            },{
                header: lang.SHOP_PROMOTION,
                dataIndex: 'opt_promotion',
                renderer: this.renderPrice1
            },{
                dataType:'date',
                header: lang.SHOP_PROMOTION_DATE_END,
                dataIndex: 'opt_promotion_end_date',
                renderFormat:'d-m-Y'
            }
            ]
        });

        ct.add(Promo_Grid);
		
		
        var Stock_Grid = new Ext.ux.grid.GridPanel({
            title: lang.SHOP_ALERT_STOCK,
            iconCls: 'i-note',
            itype: 'shop.alerts',
            rowId: 'opt_id',
            search: false,
            columns: [{
                header: lang.SHOP_PRODUCT,
                dataIndex: 'prd_title',
                width: 240,
                itemLink: ['shop.prd', 'prd_id']
            },{
                header: lang.SHOP_OPTION,
                dataIndex: 'opt_title',
                width: 124,
                itemLink: ['shop.opt', 'opt_id']
            },{
                header: lang.SHOP_REFERENCE,
                dataIndex: 'opt_reference'
				
            },{
                header: lang.SHOP_PRICE,
                dataIndex: 'opt_price',
                renderer: this.renderPrice1
            },{
                header: lang.SHOP_STOCK,
                dataIndex: 'opt_stock'
            }
            ]
        });

        ct.add(Stock_Grid);
    },
        
    makeFidelityRoot: function(ct, item, data)
    {

        fidelity_grid = new Ext.ux.grid.GridPanel({
            title: lang.SHOP_FIDELITY_LIST,
            iconCls: 'i-note',
            itype: 'shop.fid',
            rowId: 'fid_id',
            columns: [{
                header: lang.SHOP_FIDELITY_TITLE,
                dataIndex: 'fid_title',
                itemLink: ['shop.fid', 'fid_id']
            },{
                header: lang.SHOP_FIDELITY_RATIOAP,
                dataIndex: 'fid_ratioAP'
            },{
                header: lang.SHOP_FIDELITY_RATIOPR,
                dataIndex: 'fid_ratioPR'
            },{
                header: lang.SHOP_FIDELITY_PROMO,
                dataIndex: 'fid_promo_get',
                renderer: Xeon.renderStatus,
                editor: new Ext.ux.form.ToggleBox()
            },{
                header: lang.SHOP_MIN_CART,
                dataIndex: 'fid_cart_mini',
                dataType: 'float',
                editor: new Ext.ux.form.FloatField({
                    allowBlank: false
                })
            },{
                header: lang.SHOP_FIDELITY_USE_OF_POINTS,
                dataIndex: 'fid_points_use',
                dataType: 'float',
                editor: new Ext.ux.form.FloatField({
                    allowBlank: false
                })
            },{
                header: lang.SHOP_FIDELITY_POINTS_VALIDITY,
                dataIndex: 'fid_valid_duration',
                dataType: 'float',
                editor: new Ext.ux.form.FloatField({
                    allowBlank: false
                })
            },{
                header: lang.SHOP_START_DATE,
                dataIndex: 'fid_start_date'
            },{
                header: lang.SHOP_END_DATE,
                dataIndex: 'fid_end_date'
            },{
                header: lang.XEON_STATUS,
                dataIndex: 'fid_status',
                width: 60,
                renderer: Xeon.renderStatus,
                editor: new Ext.ux.form.ToggleBox()
            }]
        });

        ct.add(fidelity_grid);
    },
        
    renderYesorNo: function(v, m, r)
    {
        m.css = (v == 1) ? 'x-button i-accept' : 'x-button i-delete';
        m.css = (v == -1) ? 'x-button i-loading' : m.css;
    },
    makeFidelityForm: function(ct, item, data)
    {
        var items = [{
            fieldLabel: lang.SHOP_FIDELITY_TITLE,
            allowBlank: false,
            name: 'fid_title',
            value: data.db.fid_title
        },{
            fieldLabel: lang.SHOP_FIDELITY_RATIOAP,
            allowBlank: false,
            name: 'fid_ratioAP',
            value: data.db.fid_ratioAP,
            width: 80
        },{
            xtype:'tbtext',
            text: lang.SHOP_FIDELITY_RATIOAP_TEXT,
            allowBlank: false,
            width: 300,
            style:"float:left;margin-left:280px;margin-top :-20px;"
        },{
            fieldLabel: lang.SHOP_FIDELITY_RATIOPR,
            allowBlank: false,
            name: 'fid_ratioPR',
            value: data.db.fid_ratioPR,
            width: 80
        },{
            xtype:'tbtext',
            text: lang.SHOP_FIDELITY_RATIOPR_TEXT,
            allowBlank: false,
            width: 300,
            style:"float:left;margin-left:280px;margin-top :-20px;"
        },{
            xtype: 'togglebox',
            fieldLabel: lang.SHOP_FIDELITY_GAIN_ON_PROMO,
            allowBlank: false,
            name: 'fid_promo_get',
            value: data.db.fid_promo_get
        },{
            xtype: 'floatfield',
            fieldLabel: lang.SHOP_MIN_CART,
            allowBlank: false,
            name: 'fid_cart_mini',
            value: data.db.fid_cart_mini
        },{
            xtype: 'togglebox',
            fieldLabel: lang.SHOP_FIDELITY_GAIN_OVER_PROMO,
            allowBlank: false,
            name: 'fid_promo_use',
            value: data.db.fid_promo_use
        },{
            xtype: 'listbox',
            fieldLabel: lang.SHOP_FIDELITY_USE_OF_POINTS,
            allowBlank: false,
            itype: 'shop.fid',
            name: 'fid_points_use',
            value: data.db.fid_points_use,
            displayValue: data.db.fid_points_use
        },{
            fieldLabel: lang.SHOP_FIDELITY_POINTS_VALIDITY,
            allowBlank: true,
            name: 'fid_valid_duration',
            value: data.db.fid_valid_duration
        },{
            xtype: 'date',
            fieldLabel: lang.SHOP_FIDELITY_START_DATE,
            emptyText: lang.SHOP_NONE,
            name: 'fid_start_date',
            value: data.db.fid_start_date
        },{
            xtype: 'date',
            fieldLabel: lang.SHOP_FIDELITY_END_DATE,
            emptyText: lang.SHOP_NEVER,
            name: 'fid_end_date',
            value: data.db.fid_end_date
        }];

        if (item.add)
        {
            ct.add(new Ext.ux.FormPanel({
                items: items
            }));
        }
        else
        {
            items.push({
                xtype: 'togglebox',
                fieldLabel: lang.XEON_STATUS,
                allowBlank: false,
                name: 'fid_status',
                value: data.db.fid_status
            });

            ct.add(new Ext.ux.FormPanel({
                title: lang.SHOP_FIDELITY,
                iconCls: ct.iconCls,
                items: items
            }));
                        
                       
        /*ct.add(new Ext.ux.ListPanel({
				title: lang.SHOP_AFFECTED_FAMILY,
				iconCls: 'i-folder',
				name: 'dsc_family_list',
				itype: 'shop.fam',
				data: data.fam
			}));*/
        }
    },
	makeColissimoRoot: function(ct, item,data)
	{
	
    var buttons = [];
	if(data.ftp){
		buttons.push({
			text: lang.SHOP_IMPORT_ETIQUETTE_FTP,
			handler: function() {
				Xeon.module.shop.exportOrder( 'shop.importEtiquetteFTP' );
			}
		});
	}
    buttons.push({
        text: lang.SHOP_IMPORT_MANUEL,
        handler: function() {
					
            var p = this;
            // p.file = (p.value) ? p.value : '0';
            // var Record = Ext.data.Record.create(['item', 'title', 'icon', 'isFile','fle_id']);
            // p.view.store = new Ext.data.JsonStore({
            // url: Ext.Ajax.url,
            // root: 'data',
            // fields: Record,
            // baseParams: {
            // cmd: 'media.makeMediaList',
            // mode: p.mode,
            // folder: p.folder,
            // file: p.file
            // }});
            var btn = new Ext.Panel({
                border: false,
                bodyStyle: {
                    marginBottom: '10px',
                    background: '#dfe8f6',
                    fontSize: '12px'
                },
                html: '<span id="swfu_button"></span> <span id="swfu_status">'+lang.MEDIA_UPLOAD_SELECT+'</span>'
            }); //fin bnt panel
					
            var bar = new Ext.ProgressBar();
					
            var win = new Ext.Window({
                title: lang.MEDIA_FILE_ADD,
                resizable: false,
                modal: true,
                border: false,
                width: 400,
                items: new Ext.Panel({
                    frame: true,
                    bodyStyle: {
                        padding: '10px'
                    },
                    items: [btn, bar]
                })
            });
            // consol.log(folder);
            win.on('show', function()
            {
					
                var total = 0;
                var current = 1;
                win.swfu = new SWFUpload({
                    //debug: true,
                    flash_url: '/src/swfupload/swfupload.swf',
                    upload_url: Ext.Ajax.url,
                    file_types: "*.txt",
                    file_types_description:  "Documents",
                    file_post_name: 'media',
                    file_size_limit: 2097152+'B',
							
                    button_width: '16',
                    button_height: '16',
                    button_placeholder_id: 'swfu_button',
                    button_image_url: '/bundles/gmmedia/img/upload.png',
                    button_cursor: SWFUpload.CURSOR.HAND,
							
                    post_params: {
                        cmd: 'shop.ordertoimport2manuel',
                        PHPSESSID: Xeon.getCookie('PHPSESSID')
                    },
								
                    file_queued_handler: function(file)
                    {
                        total++;
                        Ext.get('swfu_status').update(String.format(lang.MEDIA_UPLOAD_STATUS, current, total));
                    },
							
                    file_queue_error_handler: function(file, error, message)
                    {
                        Ext.Msg.alert(lang.XEON_ERROR, file.name+' : '+message);
                    },
							
                    file_dialog_complete_handler: function()
                    {
                        var stats = win.swfu.getStats();
		
                        if (stats.files_queued > 0 && stats.in_progress == 0) {
                            win.swfu.startUpload();
                        }
                    },
							
                    upload_progress_handler: function(file, loaded, total)
                    {
                        var progress = Math.ceil((loaded/total)*100);
                        bar.updateProgress(progress/100, file.name+' ('+progress+'%)');
                    },
							
                    upload_success_handler: function(file, data)
                    {
                        if (win.swfu.getStats().files_queued > 0)
                        {
                            current++;
                            Ext.get('swfu_status').update(String.format(lang.MEDIA_UPLOAD_STATUS, current, total));
                            win.swfu.startUpload();
                            alert('ok');
                        }
                        else
                        {
                            // console.log(data);
                            // console.log(file);
                            // Xeon.module.shop.exportOrder( 'shop.ordertoimport2manuel');
                            Ext.get('swfu_status').update(String.format(lang.MEDIA_UPLOAD_STATUS, current, total));
                            win.swfu.startUpload();
                            win.close();
                        // alert('pas ok');
                        //p.view.store.load();
                        }
                    }
                });
						 
            });
					
            win.on('beforedestroy', function() {
                if (win.swfu) win.swfu.destroy();
            });
					
            win.show();
        // Xeon.module.shop.exportOrder( 'shop.ordertoimport2manuel');
        }
					
    })
	var etiquette_grid = new Ext.ux.grid.GridPanel({
		title: lang.SHOP_LISTE_COMMANDE,
		iconCls: 'i-vcard',
		itemId: 'etiquette_grid',
		itype: 'shop.etiq',
		reloadAfterEdit: false,
		rowId: 'ord_id',
		buttons: buttons,
		paging: false,
		search: false,
		deleteRight: true,
		quickSearch: false,
		exportData: function()
		{
			var p = this;
			var rows = [];
					
			Ext.each(p.selModel.getSelections(), function(r) {
				rows.push(r.id);
			});
					
			Ext.Ajax.request({
				params: Ext.apply({
					cmd: 'xeon.exportGrid',
					item: p.itype,
					parent: p.ownerCt.itemId,
					rows: rows.join(',')
				}, p.searchParams),
				before: function() {
					p.bwrap.mask(lang.XEON_LOADING, 'x-mask-loading');
				},
				success: function() {
					
					if(data.ftp)
						alert('export terminé');
					else
						Xeon.getFile();
					p.store.reload();
				},
				callback: function() {
					p.bwrap.unmask();
				}
			});
		},
		// exportPath: false,
		columns: [{
			header: lang.SHOP_BILL_NUMBER,
			dataIndex: 'ord_id',
			itemLink: ['shop.ord', 'ord_id'],
			width: 80
		},{
			header: lang.XEON_DATE,
			dataIndex: 'ord_make_date',
			width: 80
		},{
			header: lang.XEON_FIRSTNAME,
			dataIndex: 'ord_bill_firstname',
			itemLink: ['user.mbr', 'usr_id']
		},{
			header: lang.XEON_LASTNAME,
			dataIndex: 'ord_bill_lastname',
			itemLink: ['user.mbr', 'usr_id']
		},{
			header: lang.CONTACT_EMAIL,
			dataIndex: 'ord_bill_email',
			width: 150,
			renderer: Xeon.renderEmail
		},{
			header: lang.SHOP_MONTANT,
			dataIndex: 'ord_total',
			width: 50
		},{
			header: lang.SHOP_PAIEMENT,
			width: 100,
			dataIndex: 'pmt_title',
			itemLink: ['shop.pmt', 'pmt_id']
		},{
			header: lang.SHOP_PAYER_LE,
			width: 80,
			dataIndex: 'ord_payment_date'
		},{
			header: lang.SHOP_POIDS,
			width: 60,
			dataIndex: 'ord_weight',
			renderer: Xeon.renderText,
			editor: new Ext.form.TextArea()
		},{
			header: lang.SHOP_VALEUR_AJOUTEE,
			width: 60,
			dataIndex: 'ord_val_aj',
			renderer: Xeon.renderText,
			editor: new Ext.form.TextArea()
		},{
			// CollissimoBox à mettre en place
			//Autre
			header: lang.SHOP_TYPE,
			dataIndex: 'ord_val_collissimo',
			width: 160,
			editor: new Ext.ux.form.setCollissimo()
		}
				
		// {header: lang.SHOP_TYPE,
		// dataIndex: 'ord_val_collissimo',
		// width: 80 ,
		// editor: new Ext.ux.form.DelayBox(),
		// renderer: function(e){var bin=new Ext.ux.form.DelayBox();return bin.getValueToDisplay(e);}
		]
		});

		ct.add(etiquette_grid);
			
	}
	
}
Ext.ux.form.setCollissimo = Ext.extend(Ext.ux.form.ComboBox, {
    data: [
    ['COLD', lang.SHOP_COLD],
    ['COL', lang.SHOP_COL],
    ['CDS', lang.SHOP_CDS],
    ['COM', lang.SHOP_COM],
    ['COLI', lang.SHOP_COLI],
    ['BPR', 'Bureau de poste'],
    ['CIT', 'Cytissimo'],
    ['RDV', 'Rendez vous domicile'],
    ['A2P', 'Commerçants'],
    ['DOM', 'Domicile sans signature'],
    ['DOS', 'Domicile avec signature']
    ]
});

Ext.override(Ext.form.Field, {
	
    showContainer: function() {
        this.setVisible(true);
		
        this.enable();
		
        this.getEl().up('.x-form-item').setDisplayed(true);
    },

    hideContainer: function() {
        this.disable();
		
        this.setVisible(false);
		
        this.getEl().up('.x-form-item').setDisplayed(false);
    },
	
    setContainerVisible: function(visible) {
        if (visible)
            this.showContainer();
        else
            this.hideContainer();
		
        return this;
    }
});