/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//
// Interface user v0.1
// Auteur : Mustapha CHELH
//
function dump(obj) {
    var out = '';
    for (var i in obj) {
        out += i + ": " + obj[i] + "\n";
    }
    alert(out);
}

Xeon.module.statistics =
{
    allowDrop: function(tree, node, target) {
        return (node.parentNode.getDepth() == target.getDepth());
    },

    makeClickRoot: function(ct, item, data)
    {

        var form1 = new Ext.ux.FormPanel({ 
            frame: true,
            title: lang.STATISTICS_DATE,
            iconCls: 'i-date',
            width: 250,
            items   : [
            {
                xtype: 'compositefield',
                msgTarget : 'side',
                anchor    : '-10',
                defaults: {
                    flex: 1
                },
                items: [
                {
                    xtype: 'datefield',
                    format: 'Y-m-d H:m:s',
                    fieldLabel: lang.STATISTICS_DATE_BEGIN,
                    name: 'date1'
                },
                {
                    xtype: 'datefield',
                    format: 'Y-m-d H:m:s',
                    fieldLabel: lang.STATISTICS_DATE_END,
                    name: 'date2'
                }
                ]
                }],
            buttons: [{
                text: "Submit",
                handler: function(response) {
                    var values = this.ownerCt.ownerCt.getForm().getValues();
                    //dump(values);
                    grid.store.load({
                        params: {
                            date1: values.date1, 
                            date2: values.date2
                        }
                    });
                    Ext.Ajax.request({
                        params: {
                            cmd: 'statistics.recupDataStat',
                            date1: values.date1, 
                            date2: values.date2,
                            type: "root",
                            isClick: 1
                        },
                        success: function(response,p) {
                                
                            var datastat = [];
                            Ext.each(response.json.stat, function(f)
                            {   

                                datastat.push([f[0],parseFloat(f[1])]);

                            });
                            store.loadData(datastat);
                                
                        }
                    });
                }
            }]
            
        });
        
        ct.add(form1);

    
        var xg = Ext.ux.grid;

        // shared reader
        var reader = new Ext.data.JsonReader({
            root: 'data'
        },[ 
        {
            name: 'company'
        },

        {
            name: 'price'
        },

        {
            name: 'numberClick', 
            type: 'float'
        },

        {
            name: 'pricePerClick', 
            type: 'float'
        },
        
        {
            name: 'totalimp', 
            type: 'float'
        },
        
        {
            name: 'spl', 
            type: 'float'
        },
        
        {
            name: 'fam', 
            type: 'float'
        },

        {
            name: 'supplier'
        },

        {
            name: 'desc'
        }
        ]);
        var myStore =  new Ext.data.GroupingStore({
            autoLoad: true,
            url: Ext.Ajax.url,
            reader: reader,
            iconCls: 'i-date',
            sortInfo:{
                field: 'company', 
                direction: "ASC"
            },
            groupField:'supplier'
        });
            
        var grid = new xg.GridPanel({
            columns: [
            {
                id:'pdf',
                header: "pdf", 
                width: 10, 
                sortable: true, 
                align: 'center',
                width:10,
                renderer : function (v,m,r)
                {

                    m.css = 'x-button i-zoom';
      
                    r.isButton = true;
                },
                handler: function(p,r,i)
                {
                                    
                            
                    var values = form1.form.getValues();
                                   
                    Ext.Ajax.request(
                    {
                        params: 
                        {
                            cmd: 'xeon.exportGrid',
                            item: 'statistics.ordertopdfClickFam',
                            fam: r.data.fam,
                            spl: r.data.spl,
                            date1: values.date1,
                            date2: values.date2,
                            parent: ''
                        },
                        success: function() {
                            Xeon.getFile();
                        }
                    })
                }
                        
            },
            {
                id:'company',
                header: lang.STATISTICS_NAME_SUPPLIER_AND_FAMILY, 
                width: 40, 
                sortable: true, 
                dataIndex: 'company'
            },

            {
                header: lang.STATISTICS_NUMBER_CLICK, 
                width: 20, 
                sortable: true, 
                dataIndex: 'numberClick'
            },

            {
                header: lang.STATISTICS_NUMBER_IMP, 
                width: 30, 
                sortable: true, 
                dataIndex: 'totalimp'
            },

            {
                header: lang.STATISTICS_PRICE_PER_CLICK, 
                width: 20, 
                sortable: true, 
                dataIndex: 'pricePerClick', 
                renderer: Xeon.getEuro
            },
            
            {
                header: lang.STATISTICS_TOTAL_TTC, 
                width: 20, 
                sortable: true, 
                dataIndex: 'price',
                renderer: Xeon.getEuro
            },
            
            {
                header: lang.STATISTICS_LABEL_SUPPLIER, 
                width: 20, 
                sortable: true, 
                dataIndex: 'supplier',
                hidden: true
            }
            ],

            view: new Ext.grid.GroupingView({
                forceFit:true,
                groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
            }),
            
            title: lang.STATISTICS_CLICK,
            enableHdMenu: true,
            height: 550,
            // proxy: new Ext.data.HttpProxy,
            iconCls: 'i-chart-bar',
            store: myStore,
            itype: 'statistics.click',
            rowId: 'spl_id',
            paging: true,
            search: false
            
        });


        ct.add(grid);
        
        var datastat = [];
        Ext.each(data.stat, function(f)
        {   

            datastat.push([f[0],parseFloat(f[1])]);
        
        });

        var store = new Ext.data.ArrayStore({
            fields: ['supplier', 'click'],
            data: datastat
        });

        var panel = new Ext.Panel({
            width: 700,
            iconCls: 'i-chart-bar',
            height: 600,
            title: lang.STATISTICS_CLICK,
            items: {
                xtype: 'columnchart',
                store: store,
                yField: 'click',
                xField: 'supplier',
                xAxis: new Ext.chart.CategoryAxis({
                    title: lang.STATISTICS_LABEL_SUPPLIER
                }),
                yAxis: new Ext.chart.NumericAxis({
                    title: 'Nombre de clic'
                }),
                extraStyle: {
                    xAxis: {
                        labelRotation: -90
                    }
                }
            }
        });
        ct.add(panel);
    },
    makeFamForm: function(ct, item, data)
    {  
        var form1 = new Ext.ux.FormPanel({ 
            frame: true,
            title: lang.STATISTICS_DATE,
            iconCls: 'i-date',
            width: 250,
            items   : [
            {
                xtype: 'compositefield',
                msgTarget : 'side',
                anchor    : '-10',
                defaults: {
                    flex: 1
                },
                items: [
                {
                    xtype: 'datefield',
                    format: 'Y-m-d H:m:s',
                    fieldLabel: lang.STATISTICS_DATE_BEGIN,
                    name: 'date1'
                },
                {
                    xtype: 'datefield',
                    format: 'Y-m-d H:m:s',
                    fieldLabel: lang.STATISTICS_DATE_END,
                    name: 'date2'
                }
                ]
                }],
            buttons: [{
                text: "Submit",
                handler: function(response) {
                    var values = this.ownerCt.ownerCt.getForm().getValues();
                    //dump(values);
                    grid.store.load({
                        params: {
                            date1: values.date1, 
                            date2: values.date2
                        }
                    });
                    Ext.Ajax.request({
                        params: {
                            cmd: 'statistics.recupDataStat',
                            date1: values.date1, 
                            date2: values.date2,
                            type: "fam",
                            isClick: 1
                        },
                        success: function(response,p) {
                                
                            var datastat = [];
                            Ext.each(response.json.stat, function(f)
                            {   

                                datastat.push([f[0],parseFloat(f[1])]);

                            });
                            store.loadData(datastat);
                                
                        }
                    });
                }
            }]
            
        });
        
        ct.add(form1);

    
        var xg = Ext.ux.grid;

        // shared reader
        var reader = new Ext.data.JsonReader({
            root: 'data'
        },[ 
        {
            name: 'company'
        },

        {
            name: 'price'
        },

        {
            name: 'numberClick', 
            type: 'float'
        },
        {
            name: "totalimp",
            type: 'float'
        },
        {
            name: 'spl', 
            type: 'float'
        },
        
        {
            name: 'fam', 
            type: 'float'
        },

        {
            name: 'supplier'
        },

        {
            name: 'desc'
        }
        ]);
        var myStore =  new Ext.data.GroupingStore({
            autoLoad: true,
            url: Ext.Ajax.url,
            params: {
                date1: 0, 
                date2: 0
            },
            reader: reader,
            iconCls: 'i-date',
            sortInfo:{
                field: 'company', 
                direction: "ASC"
            },
            groupField:'supplier'
        });
            
        var grid = new xg.GridPanel({
            columns: [
            {
                id:'company',
                header: lang.STATISTICS_NAME_SUPPLIER_AND_FAMILY, 
                width: 40, 
                sortable: true, 
                dataIndex: 'company'
            },

            {
                header: lang.STATISTICS_NUMBER_CLICK, 
                width: 20, 
                sortable: true, 
                dataIndex: 'numberClick'
            },

            {
                header: lang.STATISTICS_NUMBER_IMP, 
                width: 30, 
                sortable: true, 
                dataIndex: 'totalimp'
            },
            
            {
                header: lang.STATISTICS_LABEL_SUPPLIER, 
                width: 20, 
                sortable: true, 
                dataIndex: 'supplier',
                hidden: true
            }
            ],

            view: new Ext.grid.GroupingView({
                forceFit:true,
                groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
            }),
            
            title: lang.STATISTICS_CLICK,
            enableHdMenu: true,
            height: 550,
            // proxy: new Ext.data.HttpProxy,
            iconCls: 'i-chart-bar',
            store: myStore,
            itype: 'statistics.click',
            rowId: 'spl_id',
            paging: true,
            search: false
            
        });


        ct.add(grid);
        
        var datastat = [];
        Ext.each(data.stat, function(f)
        {   

            datastat.push([f[0],parseFloat(f[1])]);
        
        });

        var store = new Ext.data.ArrayStore({
            fields: ['fam', 'click'],
            data: datastat
        });

        var panel = new Ext.Panel({
            width: 700,
            iconCls: 'i-chart-bar',
            height: 600,
            title: lang.STATISTICS_CLICK,
            items: {
                xtype: 'columnchart',
                store: store,
                yField: 'click',
                xField: 'fam',
                xAxis: new Ext.chart.CategoryAxis({
                    title: lang.STATISTICS_LABEL_FAMILY
                }),
                yAxis: new Ext.chart.NumericAxis({
                    title: 'Nombre de clic'
                }),
                extraStyle: {
                    xAxis: {
                        labelRotation: -90
                    }
                }
            }
        });
        ct.add(panel);
    },
    
    makeSplForm: function(ct, item, data)
    {  
        var form1 = new Ext.ux.FormPanel({ 
            frame: true,
            title: lang.STATISTICS_DATE,
            iconCls: 'i-date',
            width: 250,
            items   : [
            {
                xtype: 'compositefield',
                msgTarget : 'side',
                anchor    : '-10',
                defaults: {
                    flex: 1
                },
                items: [
                {
                    xtype: 'datefield',
                    format: 'Y-m-d H:m:s',
                    fieldLabel: lang.STATISTICS_DATE_BEGIN,
                    name: 'date1'
                },
                {
                    xtype: 'datefield',
                    format: 'Y-m-d H:m:s',
                    fieldLabel: lang.STATISTICS_DATE_END,
                    name: 'date2'
                }
                ]
                }],
            buttons: [{
                text: "Submit",
                handler: function(response) {
                    var values = this.ownerCt.ownerCt.getForm().getValues();
                    //dump(values);
                    grid.store.load({
                        params: {
                            date1: values.date1, 
                            date2: values.date2
                        }
                    });
                    Ext.Ajax.request({
                        params: {
                            cmd: 'statistics.recupDataStat',
                            date1: values.date1, 
                            date2: values.date2,
                            type: "spl",
                            isClick: 1
                        },
                        success: function(response,p) {
                                
                            var datastat = [];
                            Ext.each(response.json.stat, function(f)
                            {   

                                datastat.push([f[0],parseFloat(f[1])]);

                            });
                            store.loadData(datastat);
                                
                        }
                    });
                }
            }]
            
        });
        
        ct.add(form1);

    
        var xg = Ext.ux.grid;

        // shared reader
        var reader = new Ext.data.JsonReader({
            root: 'data'
        },[ 
        {
            name: 'company'
        },

        {
            name: 'price'
        },
        
        {
            name: 'pricePerClick',
            type: 'float'
        },

        {
            name: 'numberClick', 
            type: 'float'
        },
        {
            name: "totalimp",
            type: 'float'
        },
        {
            name: 'spl', 
            type: 'float'
        },
        
        {
            name: 'fam', 
            type: 'float'
        },

        {
            name: 'supplier'
        },

        {
            name: 'desc'
        }
        ]);
        var myStore =  new Ext.data.GroupingStore({
            autoLoad: true,
            url: Ext.Ajax.url,
            params: {
                date1: 0, 
                date2: 0
            },
            reader: reader,
            iconCls: 'i-date',
            sortInfo:{
                field: 'company', 
                direction: "ASC"
            },
            groupField:'supplier'
        });
            
        var grid = new xg.GridPanel({
            columns: [
            {
                id:'pdf',
                header: "pdf", 
                width: 10, 
                sortable: true, 
                align: 'center',
                width:10,
                renderer : function (v,m,r)
                {

                    m.css = 'x-button i-zoom';
      
                    r.isButton = true;
                },
                handler: function(p,r,i)
                {
                                    
                            
                    var values = form1.form.getValues();
                                   
                    Ext.Ajax.request(
                    {
                        params: 
                        {
                            cmd: 'xeon.exportGrid',
                            item: 'statistics.ordertopdfClickSpl',
                            spl: r.data.spl,
                            date1: values.date1,
                            date2: values.date2,
                            parent: ''
                        },
                        success: function() {
                            Xeon.getFile();
                        }
                    })
                }
                        
            },
            {
                id:'company',
                header: lang.STATISTICS_NAME_SUPPLIER_AND_FAMILY, 
                width: 40, 
                sortable: true, 
                dataIndex: 'company'
            },

            {
                header: lang.STATISTICS_NUMBER_CLICK, 
                width: 20, 
                sortable: true, 
                dataIndex: 'numberClick'
            },

            {
                header: lang.STATISTICS_NUMBER_IMP, 
                width: 30, 
                sortable: true, 
                dataIndex: 'totalimp'
            },
            {
                header: lang.STATISTICS_PRICE_PER_CLICK, 
                width: 20, 
                sortable: true, 
                dataIndex: 'pricePerClick', 
                renderer: Xeon.getEuro
            },
            
            {
                header: lang.STATISTICS_TOTAL_TTC, 
                width: 20, 
                sortable: true, 
                dataIndex: 'price',
                renderer: Xeon.getEuro
            },
            {
                header: lang.STATISTICS_LABEL_SUPPLIER, 
                width: 20, 
                sortable: true, 
                dataIndex: 'supplier',
                hidden: true
            }
            ],

            view: new Ext.grid.GroupingView({
                forceFit:true,
                groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
            }),
            
            title: lang.STATISTICS_CLICK,
            enableHdMenu: true,
            height: 550,
            // proxy: new Ext.data.HttpProxy,
            iconCls: 'i-chart-bar',
            store: myStore,
            itype: 'statistics.click',
            rowId: 'spl_id',
            paging: true,
            search: false
            
        });


        ct.add(grid);
        
        var datastat = [];
        Ext.each(data.stat, function(f)
        {   

            datastat.push([f[0],parseFloat(f[1])]);
        
        });

        var store = new Ext.data.ArrayStore({
            fields: ['fam', 'click'],
            data: datastat
        });

        var panel = new Ext.Panel({
            width: 700,
            iconCls: 'i-chart-bar',
            height: 600,
            title: lang.STATISTICS_CLICK,
            items: {
                xtype: 'columnchart',
                store: store,
                yField: 'click',
                xField: 'fam',
                xAxis: new Ext.chart.CategoryAxis({
                    title: lang.STATISTICS_LABEL_SUPPLIER
                }),
                yAxis: new Ext.chart.NumericAxis({
                    title: 'Nombre de clic'
                }),
                extraStyle: {
                    xAxis: {
                        labelRotation: -90
                    }
                }
            }
        });
        ct.add(panel);
    },
    makePercentRoot: function(ct, item, data)
    {

        var form1 = new Ext.ux.FormPanel({ 
            frame: true,
            title: lang.STATISTICS_DATE,
            iconCls: 'i-date',
            width: 250,
            items   : [
            {
                xtype: 'compositefield',
                msgTarget : 'side',
                anchor    : '-10',
                defaults: {
                    flex: 1
                },
                items: [
                {
                    xtype: 'datefield',
                    format: 'Y-m-d H:m:s',
                    fieldLabel: lang.STATISTICS_DATE_BEGIN,
                    name: 'date1'
                },
                {
                    xtype: 'datefield',
                    format: 'Y-m-d H:m:s',
                    fieldLabel: lang.STATISTICS_DATE_END,
                    name: 'date2'
                }
                ]
                }],
            buttons: [{
                text: "Submit",
                handler: function(response) {
                    var values = this.ownerCt.ownerCt.getForm().getValues();
                    //dump(values);
                    grid.store.load({
                        params: {
                            date1: values.date1, 
                            date2: values.date2
                        }
                    });
                    Ext.Ajax.request({
                        params: {
                            cmd: 'statistics.recupDataStat',
                            date1: values.date1, 
                            date2: values.date2,
                            type: "root",
                            isClick: 0
                        },
                        success: function(response,p) {
                                
                            var datastat = [];
                            Ext.each(response.json.stat, function(f)
                            {   

                                datastat.push([f[0],parseFloat(f[1])]);

                            });
                            store.loadData(datastat);
                                
                        }
                    });
                }
            }]
            
        });
        
        ct.add(form1);

    
        var xg = Ext.ux.grid;

        // shared reader
        var reader = new Ext.data.JsonReader({
            root: 'data'
        },[ 
        {
            name: 'company'
        },

        {
            name: 'price'
        },

        {
            name: 'numberSales', 
            type: 'float'
        },
        
        {
            name: 'totalPriceSales'
        },

        {
            name: 'percentSales', 
            type: 'float'
        },
        
        {
            name: 'totalimp', 
            type: 'float'
        },
        
        {
            name: 'spl', 
            type: 'float'
        },
        
        {
            name: 'fam', 
            type: 'float'
        },

        {
            name: 'supplier'
        },

        {
            name: 'desc'
        }
        ]);
        var myStore =  new Ext.data.GroupingStore({
            autoLoad: true,
            url: Ext.Ajax.url,
            reader: reader,
            iconCls: 'i-date',
            sortInfo:{
                field: 'company', 
                direction: "ASC"
            },
            groupField:'supplier'
        });
            
        var grid = new xg.GridPanel({
            columns: [
            {
                id:'pdf',
                header: "pdf", 
                width: 10, 
                sortable: true, 
                align: 'center',
                width:10,
                renderer : function (v,m,r)
                {

                    m.css = 'x-button i-zoom';
      
                    r.isButton = true;
                },
                handler: function(p,r,i)
                {
                                    
                            
                    var values = form1.form.getValues();
                                   
                    Ext.Ajax.request(
                    {
                        params: 
                        {
                            cmd: 'xeon.exportGrid',
                            item: 'statistics.ordertopdfPercentFam',
                            fam: r.data.fam,
                            spl: r.data.spl,
                            date1: values.date1,
                            date2: values.date2,
                            parent: ''
                        },
                        success: function() {
                            Xeon.getFile();
                        }
                    })
                }
                        
            },
            {
                id:'company',
                header: lang.STATISTICS_NAME_SUPPLIER_AND_FAMILY, 
                width: 40, 
                sortable: true, 
                dataIndex: 'company'
            },

            {
                header: lang.STATISTICS_NUMBER_SALES, 
                width: 20, 
                sortable: true, 
                dataIndex: 'numberSales'
            },
            
            {
                header: lang.STATISTICS_TOTAL_SALES, 
                width: 30, 
                sortable: true, 
                dataIndex: 'totalPriceSales',
                renderer: Xeon.getEuro
            },
            {
                header: lang.STATISTICS_NUMBER_IMP, 
                width: 30, 
                sortable: true, 
                dataIndex: 'totalimp'
            },

            {
                header: lang.STATISTICS_PERCENT_PER_SALES, 
                width: 20, 
                sortable: true, 
                dataIndex: 'percentSales'
            },
            
            {
                header: lang.STATISTICS_TOTAL_TTC, 
                width: 20, 
                sortable: true, 
                dataIndex: 'price',
                renderer: Xeon.getEuro
            },
            
            {
                header: lang.STATISTICS_LABEL_SUPPLIER, 
                width: 20, 
                sortable: true, 
                dataIndex: 'supplier',
                hidden: true
            }
            ],

            view: new Ext.grid.GroupingView({
                forceFit:true,
                groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
            }),
            
            title: lang.STATISTICS_CLICK,
            enableHdMenu: true,
            height: 550,
            // proxy: new Ext.data.HttpProxy,
            iconCls: 'i-chart-bar',
            store: myStore,
            itype: 'statistics.percent',
            rowId: 'spl_id',
            paging: true,
            search: false
            
        });


        ct.add(grid);
        
        var datastat = [];
        Ext.each(data.stat, function(f)
        {   

            datastat.push([f[0],parseFloat(f[1])]);
        
        });

        var store = new Ext.data.ArrayStore({
            fields: ['supplier', 'click'],
            data: datastat
        });

        var panel = new Ext.Panel({
            width: 700,
            iconCls: 'i-chart-bar',
            height: 600,
            title: lang.STATISTICS_CLICK,
            items: {
                xtype: 'columnchart',
                store: store,
                yField: 'click',
                xField: 'supplier',
                xAxis: new Ext.chart.CategoryAxis({
                    title: lang.STATISTICS_LABEL_SUPPLIER
                }),
                yAxis: new Ext.chart.NumericAxis({
                    title: lang.STATISTICS_NUMBER_SALES
                }),
                extraStyle: {
                    xAxis: {
                        labelRotation: -90
                    }
                }
            }
        });
        ct.add(panel);
    },
    makeFampForm: function(ct, item, data)
    {  
        var form1 = new Ext.ux.FormPanel({ 
            frame: true,
            title: lang.STATISTICS_DATE,
            iconCls: 'i-date',
            width: 250,
            items   : [
            {
                xtype: 'compositefield',
                msgTarget : 'side',
                anchor    : '-10',
                defaults: {
                    flex: 1
                },
                items: [
                {
                    xtype: 'datefield',
                    format: 'Y-m-d H:m:s',
                    fieldLabel: lang.STATISTICS_DATE_BEGIN,
                    name: 'date1'
                },
                {
                    xtype: 'datefield',
                    format: 'Y-m-d H:m:s',
                    fieldLabel: lang.STATISTICS_DATE_END,
                    name: 'date2'
                }
                ]
                }],
            buttons: [{
                text: "Submit",
                handler: function(response) {
                    var values = this.ownerCt.ownerCt.getForm().getValues();
                    //dump(values);
                    grid.store.load({
                        params: {
                            date1: values.date1, 
                            date2: values.date2
                        }
                    });
                    Ext.Ajax.request({
                        params: {
                            cmd: 'statistics.recupDataStat',
                            date1: values.date1, 
                            date2: values.date2,
                            type: "fam",
                            isClick: 0
                        },
                        success: function(response,p) {
                                
                            var datastat = [];
                            Ext.each(response.json.stat, function(f)
                            {   

                                datastat.push([f[0],parseFloat(f[1])]);

                            });
                            store.loadData(datastat);
                                
                        }
                    });
                }
            }]
            
        });
        
        ct.add(form1);

    
        var xg = Ext.ux.grid;

        // shared reader
        var reader = new Ext.data.JsonReader({
            root: 'data'
        },[ 
        {
            name: 'company'
        },

        {
            name: 'price'
        },
        
        {
            name: 'totalPriceSales'
        },

        {
            name: 'numberSales', 
            type: 'float'
        },
        {
            name: "totalimp",
            type: 'float'
        },
        {
            name: 'spl', 
            type: 'float'
        },
        
        {
            name: 'fam', 
            type: 'float'
        },

        {
            name: 'supplier'
        },

        {
            name: 'desc'
        }
        ]);
        var myStore =  new Ext.data.GroupingStore({
            autoLoad: true,
            url: Ext.Ajax.url,
            params: {
                date1: 0, 
                date2: 0
            },
            reader: reader,
            iconCls: 'i-date',
            sortInfo:{
                field: 'company', 
                direction: "ASC"
            },
            groupField:'supplier'
        });
            
        var grid = new xg.GridPanel({
            columns: [
            {
                id:'company',
                header: lang.STATISTICS_NAME_SUPPLIER_AND_FAMILY, 
                width: 40, 
                sortable: true, 
                dataIndex: 'company'
            },

            {
                header: lang.STATISTICS_NUMBER_SALES, 
                width: 20, 
                sortable: true, 
                dataIndex: 'numberSales'
            },

            {
                header: lang.STATISTICS_NUMBER_IMP, 
                width: 30, 
                sortable: true, 
                dataIndex: 'totalimp'
            },
            
            {
                header: lang.STATISTICS_TOTAL_SALES, 
                width: 30, 
                sortable: true, 
                dataIndex: 'totalPriceSales',
                renderer: Xeon.getEuro
            },
            
            {
                header: lang.STATISTICS_LABEL_SUPPLIER, 
                width: 20, 
                sortable: true, 
                dataIndex: 'supplier',
                hidden: true
            }
            ],

            view: new Ext.grid.GroupingView({
                forceFit:true,
                groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
            }),
            
            title: lang.STATISTICS_CLICK,
            enableHdMenu: true,
            height: 550,
            // proxy: new Ext.data.HttpProxy,
            iconCls: 'i-chart-bar',
            store: myStore,
            itype: 'statistics.percent',
            rowId: 'spl_id',
            paging: true,
            search: false
            
        });


        ct.add(grid);
        
        var datastat = [];
        Ext.each(data.stat, function(f)
        {   

            datastat.push([f[0],parseFloat(f[1])]);
        
        });

        var store = new Ext.data.ArrayStore({
            fields: ['fam', 'click'],
            data: datastat
        });

        var panel = new Ext.Panel({
            width: 700,
            iconCls: 'i-chart-bar',
            height: 600,
            title: lang.STATISTICS_CLICK,
            items: {
                xtype: 'columnchart',
                store: store,
                yField: 'click',
                xField: 'fam',
                xAxis: new Ext.chart.CategoryAxis({
                    title: lang.STATISTICS_LABEL_FAMILY
                }),
                yAxis: new Ext.chart.NumericAxis({
                    title: lang.STATISTICS_NUMBER_SALES
                }),
                extraStyle: {
                    xAxis: {
                        labelRotation: -90
                    }
                }
            }
        });
        ct.add(panel);
    },
    makeSplpForm: function(ct, item, data)
    {  
        var form1 = new Ext.ux.FormPanel({ 
            frame: true,
            title: lang.STATISTICS_DATE,
            iconCls: 'i-date',
            width: 250,
            items   : [
            {
                xtype: 'compositefield',
                msgTarget : 'side',
                anchor    : '-10',
                defaults: {
                    flex: 1
                },
                items: [
                {
                    xtype: 'datefield',
                    format: 'Y-m-d H:m:s',
                    fieldLabel: lang.STATISTICS_DATE_BEGIN,
                    name: 'date1'
                },
                {
                    xtype: 'datefield',
                    format: 'Y-m-d H:m:s',
                    fieldLabel: lang.STATISTICS_DATE_END,
                    name: 'date2'
                }
                ]
                }],
            buttons: [{
                text: "Submit",
                handler: function(response) {
                    var values = this.ownerCt.ownerCt.getForm().getValues();
                    //dump(values);
                    grid.store.load({
                        params: {
                            date1: values.date1, 
                            date2: values.date2
                        }
                    });
                    Ext.Ajax.request({
                        params: {
                            cmd: 'statistics.recupDataStat',
                            date1: values.date1, 
                            date2: values.date2,
                            type: "spl",
                            isClick: 0
                        },
                        success: function(response,p) {
                                
                            var datastat = [];
                            Ext.each(response.json.stat, function(f)
                            {   

                                datastat.push([f[0],parseFloat(f[1])]);

                            });
                            store.loadData(datastat);
                                
                        }
                    });
                }
            }]
            
        });
        
        ct.add(form1);

    
        var xg = Ext.ux.grid;

        // shared reader
        var reader = new Ext.data.JsonReader({
            root: 'data'
        },[ 
        {
            name: 'company'
        },

        {
            name: 'price'
        },
        
        {
            name: 'percentSales',
            type: 'float'
        },
        
        {
            name: 'totalPriceSales'
        },

        {
            name: 'numberSales', 
            type: 'float'
        },
        {
            name: "totalimp",
            type: 'float'
        },
        {
            name: 'spl', 
            type: 'float'
        },
        
        {
            name: 'fam', 
            type: 'float'
        },

        {
            name: 'supplier'
        },

        {
            name: 'desc'
        }
        ]);
        var myStore =  new Ext.data.GroupingStore({
            autoLoad: true,
            url: Ext.Ajax.url,
            params: {
                date1: 0, 
                date2: 0
            },
            reader: reader,
            iconCls: 'i-date',
            sortInfo:{
                field: 'company', 
                direction: "ASC"
            },
            groupField:'supplier'
        });
            
        var grid = new xg.GridPanel({
            columns: [
            {
                id:'pdf',
                header: "pdf", 
                width: 10, 
                sortable: true, 
                align: 'center',
                width:10,
                renderer : function (v,m,r)
                {

                    m.css = 'x-button i-zoom';
      
                    r.isButton = true;
                },
                handler: function(p,r,i)
                {
                                    
                            
                    var values = form1.form.getValues();
                                   
                    Ext.Ajax.request(
                    {
                        params: 
                        {
                            cmd: 'xeon.exportGrid',
                            item: 'statistics.ordertopdfPercentSpl',
                            spl: r.data.spl,
                            date1: values.date1,
                            date2: values.date2,
                            parent: ''
                        },
                        success: function() {
                            Xeon.getFile();
                        }
                    })
                }
                        
            },
            {
                id:'company',
                header: lang.STATISTICS_NAME_SUPPLIER_AND_FAMILY, 
                width: 40, 
                sortable: true, 
                dataIndex: 'company'
            },

            {
                header: lang.STATISTICS_NUMBER_SALES, 
                width: 20, 
                sortable: true, 
                dataIndex: 'numberSales'
            },
            
            {
                header: lang.STATISTICS_TOTAL_SALES, 
                width: 30, 
                sortable: true, 
                dataIndex: 'totalPriceSales',
                renderer: Xeon.getEuro
            },

            {
                header: lang.STATISTICS_NUMBER_IMP, 
                width: 30, 
                sortable: true, 
                dataIndex: 'totalimp'
            },
            {
                header: lang.STATISTICS_PERCENT_PER_SALES, 
                width: 20, 
                sortable: true, 
                dataIndex: 'percentSales'
            },
            
            {
                header: lang.STATISTICS_TOTAL_TTC, 
                width: 20, 
                sortable: true, 
                dataIndex: 'price',
                renderer: Xeon.getEuro
            },
            {
                header: lang.STATISTICS_LABEL_SUPPLIER, 
                width: 20, 
                sortable: true, 
                dataIndex: 'supplier',
                hidden: true
            }
            ],

            view: new Ext.grid.GroupingView({
                forceFit:true,
                groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
            }),
            
            title: lang.STATISTICS_CLICK,
            enableHdMenu: true,
            height: 550,
            // proxy: new Ext.data.HttpProxy,
            iconCls: 'i-chart-bar',
            store: myStore,
            itype: 'statistics.percent',
            rowId: 'spl_id',
            paging: true,
            search: false
            
        });


        ct.add(grid);
        
        var datastat = [];
        Ext.each(data.stat, function(f)
        {   

            datastat.push([f[0],parseFloat(f[1])]);
        
        });

        var store = new Ext.data.ArrayStore({
            fields: ['fam', 'click'],
            data: datastat
        });

        var panel = new Ext.Panel({
            width: 700,
            iconCls: 'i-chart-bar',
            height: 600,
            title: lang.STATISTICS_CLICK,
            items: {
                xtype: 'columnchart',
                store: store,
                yField: 'click',
                xField: 'fam',
                xAxis: new Ext.chart.CategoryAxis({
                    title: lang.STATISTICS_LABEL_SUPPLIER
                }),
                yAxis: new Ext.chart.NumericAxis({
                    title: lang.STATISTICS_NUMBER_SALES
                }),
                extraStyle: {
                    xAxis: {
                        labelRotation: -90
                    }
                }
            }
        });
        ct.add(panel);
    },
    makeOrderclickForm: function(ct, item, data)
    {  
        

        var annees = new Ext.ux.form.ListBox({
            itype: 'statistics.annee',
            allowBlank: false,
            fieldLabel: lang.CRM_LABEL_ANNEE,
            name: 'annee',
            value: data.annee
        });
        
        var form1 = new Ext.ux.FormPanel({ 
            frame: true,
            title: lang.STATISTICS_GENERATE_ORDER,
            iconCls: 'i-money',
            width: 250,
            items: [annees],
            buttons: [{
                text: lang.STATISTICS_GENERATE,
                handler: function(response) {
                    var values = this.ownerCt.ownerCt.getForm().getValues();
                    
                    
                    Ext.Ajax.request({
                        params: {
                            cmd: 'statistics.createOrder',
                            annee: values.annee,
                            isClick: 1
                        },
                        success: function(response,p) {
                            var text = lang.STATISTICS_GENERATE_ORDER_SUCCESS;
                            if(response.json!="")
                                text = response.json;
                            
                        
                            Ext.MessageBox.show({
                                title: "Message",
                                iconCls: 'i-page-white-edit',
                                width: 300,
                                msg: text
                            });
                                
                        }
                    });
                    
                    
                    grid.store.load({
                        params: {
                            annees: values.annees
                        }
                        
                    });
                    
                    
                }
            },{
                text: lang.STATISTICS_EXPORT_ORDER,
                handler: function(response) {
                    var values = this.ownerCt.ownerCt.getForm().getValues();
                    
                    
                    Ext.Ajax.request(
                    {
                        params: 
                        {
                            cmd: 'xeon.exportGrid',
                            item: 'statistics.exportTopdf',
                            annee: values.annee,
                            isClick: 1,
                            parent: ''
                        },
                        success: function() {
                            Xeon.getFile();
                        }
                    })
                    
                    
                    grid.store.load({
                        params: {
                            annees: values.annees
                        }
                        
                    });
                    
                    
                }
            },{
                text: lang.STATISTICS_SEND_EMAIL,
                handler: function(response) {
                    var values = this.ownerCt.ownerCt.getForm().getValues();
                    
                    
                    Ext.Ajax.request(
                    {
                        params: 
                        {
                            cmd: 'xeon.exportGrid',
                            item: 'statistics.exportTopdf',
                            annee: values.annee,
                            isClick: 1,
                            parent: ''
                        },
                        success: function() {
                            Xeon.getFile();
                        }
                    })
                    
                    
                    grid.store.load({
                        params: {
                            annees: values.annees
                        }
                        
                    });
                    
                    
                }
            }]
            
        });
        
        ct.add(form1);

    
        var xg = Ext.ux.grid;

        // shared reader
        var reader = new Ext.data.JsonReader({
            root: 'data',
            type: 'float'
        },[ 
        {
            name: 'order'
        },
        {
            name: 'year'
        },
        {
            name: 'supplier'
        },
        {
            name: 'spl',
            type: 'float'
        },
        {
            name: 'ym'
        },
        {
            name: 'price'
        },
        {
            name: 'date_add'
        },
        {
            name: 'desc'
        }
        ]);
        var myStore =  new Ext.data.GroupingStore({
            autoLoad: true,
            url: Ext.Ajax.url,
            params: {
                date1: 0, 
                date2: 0
            },
            reader: reader,
            iconCls: 'i-date',
            sortInfo:{
                field: 'year', 
                direction: "ASC"
            },
            groupField:'supplier'
        });
            
        var grid = new xg.GridPanel({
            columns: [
            {
                id:'pdf',
                header: "pdf", 
                width: 10, 
                sortable: true, 
                align: 'center',
                width:10,
                renderer : function (v,m,r)
                {

                    m.css = 'x-button i-zoom';
      
                    r.isButton = true;
                },
                handler: function(p,r,i)
                {             
                            
                    var values = form1.form.getValues();
                                   
                    Ext.Ajax.request(
                    {
                        params: 
                        {
                            cmd: 'xeon.exportGrid',
                            item: 'statistics.ordertopdf',
                            year: r.data.ym,
                            spl: r.data.spl,
                            is_click: 1,
                            parent: ''
                        },
                        success: function() {
                            Xeon.getFile();
                        }
                    })
                }
                        
            },
            {
                header: lang.STATISTICS_ORDER_NUMBER, 
                width: 20, 
                sortable: true, 
                dataIndex: 'order'
            },
            {
                id:'year',
                header: lang.STATISTICS_YEAR_ORDER, 
                width: 40, 
                sortable: true, 
                dataIndex: 'year'
            },

            {
                header: lang.STATISTICS_PRICE, 
                width: 20, 
                sortable: true, 
                dataIndex: 'price', 
                renderer: Xeon.getEuro
            },

            {
                header: lang.STATISTICS_DATE_ADD, 
                width: 30, 
                sortable: true, 
                dataIndex: 'date_add'
            }
            ,
            {
                header: lang.STATISTICS_LABEL_SUPPLIER, 
                width: 20, 
                sortable: true, 
                dataIndex: 'supplier',
                hidden: true
            },
            {
                header: "id_spl", 
                width: 5, 
                sortable: true, 
                dataIndex: 'spl',
                hidden: true
            },
            {
                header: "ym", 
                width: 5, 
                sortable: true, 
                dataIndex: 'ym',
                hidden: true
            }
            ,{
                header: 'Send',
                dataIndex: (data.opt) ? 'ord_send' : 'ord_send',
                width: 10,
                renderer: this.renderYesorNo,
                editor: new Ext.ux.form.ToggleBox()
        
            }],

            view: new Ext.grid.GroupingView({
                forceFit:true,
                groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
            }),
            
            title: lang.STATISTICS_CLICK,
            enableHdMenu: true,
            height: 550,
            // proxy: new Ext.data.HttpProxy,
            iconCls: 'i-money',
            store: myStore,
            itype: 'statistics.orderClick',
            rowId: 'order',
            paging: true,
            search: false
            
        });


        ct.add(grid);
        
 
    },
    renderYesorNo: function(v, m, r)
    {
        m.css = (v != 1) ? 'x-button i-accept' : 'x-button i-delete';
    },
    makeOrderpercentForm: function(ct, item, data)
    {  
        

        var annees = new Ext.ux.form.ListBox({
            itype: 'statistics.annee',
            allowBlank: false,
            fieldLabel: lang.CRM_LABEL_ANNEE,
            name: 'annee',
            value: data.annee
        });
        
        var form1 = new Ext.ux.FormPanel({ 
            frame: true,
            title: lang.STATISTICS_GENERATE_ORDER,
            iconCls: 'i-money',
            width: 250,
            items: [annees],
            buttons: [{
                text: lang.STATISTICS_GENERATE,
                handler: function(response) {
                    var values = this.ownerCt.ownerCt.getForm().getValues();
                    
                    
                    Ext.Ajax.request({
                        params: {
                            cmd: 'statistics.createOrder',
                            annee: values.annee,
                            isClick: 0
                        },
                        success: function(response,p) {
                            var text = lang.STATISTICS_GENERATE_ORDER_SUCCESS;
                            if(response.json!="")
                                text = response.json;
                            
                        
                            Ext.MessageBox.show({
                                title: "Message",
                                iconCls: 'i-page-white-edit',
                                width: 300,
                                msg: text
                            });
                                
                        }
                    });
                    
                    
                    grid.store.load({
                        params: {
                            annees: values.annees
                        }
                        
                    });
                    
                    
                }
            },{
                text: lang.STATISTICS_EXPORT_ORDER,
                handler: function(response) {
                    var values = this.ownerCt.ownerCt.getForm().getValues();
                    
                    
                    Ext.Ajax.request(
                    {
                        params: 
                        {
                            cmd: 'xeon.exportGrid',
                            item: 'statistics.exportTopdf',
                            annee: values.annee,
                            isClick: 0,
                            parent: ''
                        },
                        success: function(response) {
                          
                            Xeon.getFile();
                        }
                    })
                    
                    
                    grid.store.load({
                        params: {
                            annees: values.annees
                        }
                        
                    });
                    
                    
                }
            },{
                text: lang.STATISTICS_SEND_EMAIL,
                handler: function(response) {
                    var values = this.ownerCt.ownerCt.getForm().getValues();
                    
                    
                    Ext.Ajax.request(
                    {
                        params: 
                        {
                            cmd: 'xeon.exportGrid',
                            item: 'statistics.exportTopdf',
                            annee: values.annee,
                            isClick: 1,
                            parent: ''
                        },
                        success: function() {
                            Xeon.getFile();
                        }
                    })
                    
                    
                    grid.store.load({
                        params: {
                            annees: values.annees
                        }
                        
                    });
                    
                    
                }
            }]
            
        });
        
        ct.add(form1);

    
        var xg = Ext.ux.grid;

        // shared reader
        var reader = new Ext.data.JsonReader({
            root: 'data',
            type: 'float'
        },[ 
        {
            name: 'order'
        },
        {
            name: 'year'
        },
        {
            name: 'supplier'
        },
        {
            name: 'spl',
            type: 'float'
        },
        {
            name: 'ym'
        },
        {
            name: 'price'
        },
        {
            name: 'date_add'
        },
        {
            name: 'desc'
        }
        ]);
        var myStore =  new Ext.data.GroupingStore({
            autoLoad: true,
            url: Ext.Ajax.url,
            params: {
                date1: 0, 
                date2: 0
            },
            reader: reader,
            iconCls: 'i-date',
            sortInfo:{
                field: 'year', 
                direction: "ASC"
            },
            groupField:'supplier'
        });
            
        var grid = new xg.GridPanel({
            columns: [
            {
                id:'pdf',
                header: "pdf", 
                width: 10, 
                sortable: true, 
                align: 'center',
                width:10,
                renderer : function (v,m,r)
                {

                    m.css = 'x-button i-zoom';
      
                    r.isButton = true;
                },
                handler: function(p,r,i)
                {             
                            
                    var values = form1.form.getValues();
                                   
                    Ext.Ajax.request(
                    {
                        params: 
                        {
                            cmd: 'xeon.exportGrid',
                            item: 'statistics.ordertopdf',
                            year: r.data.ym,
                            spl: r.data.spl,
                            is_click: 0,
                            parent: ''
                        },
                        success: function() {
                            Xeon.getFile();
                        }
                    })
                }
                        
            },
            {
                header: lang.STATISTICS_ORDER_NUMBER, 
                width: 20, 
                sortable: true, 
                dataIndex: 'order'
            },
            {
                id:'year',
                header: lang.STATISTICS_YEAR_ORDER, 
                width: 40, 
                sortable: true, 
                dataIndex: 'year'
            },

            {
                header: lang.STATISTICS_PRICE, 
                width: 20, 
                sortable: true, 
                dataIndex: 'price', 
                renderer: Xeon.getEuro
            },

            {
                header: lang.STATISTICS_DATE_ADD, 
                width: 30, 
                sortable: true, 
                dataIndex: 'date_add'
            }
            ,
            {
                header: lang.STATISTICS_LABEL_SUPPLIER, 
                width: 20, 
                sortable: true, 
                dataIndex: 'supplier',
                hidden: true
            },
            {
                header: "id_spl", 
                width: 5, 
                sortable: true, 
                dataIndex: 'spl',
                hidden: true
            },
            {
                header: "ym", 
                width: 5, 
                sortable: true, 
                dataIndex: 'ym',
                hidden: true
            }
            ,{
                header: 'Send',
                dataIndex: (data.opt) ? 'ord_send' : 'ord_send',
                width: 10,
                renderer: this.renderYesorNo,
                editor: new Ext.ux.form.ToggleBox()
        
            }],

            view: new Ext.grid.GroupingView({
                forceFit:true,
                groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
            }),
            
            title: lang.STATISTICS_CLICK,
            enableHdMenu: true,
            height: 550,
            // proxy: new Ext.data.HttpProxy,
            iconCls: 'i-money',
            store: myStore,
            itype: 'statistics.orderPercent',
            rowId: 'order',
            paging: true,
            search: false
            
        });


        ct.add(grid);
        
 
    },
    makeBrdForm: function(ct, item, data)
    {  
        var form1 = new Ext.ux.FormPanel({ 
            frame: true,
            title: lang.STATISTICS_DATE,
            iconCls: 'i-date',
            width: 250,
            items   : [
            {
                xtype: 'compositefield',
                msgTarget : 'side',
                anchor    : '-10',
                defaults: {
                    flex: 1
                },
                items: [
                {
                    xtype: 'datefield',
                    format: 'Y-m-d H:m:s',
                    fieldLabel: lang.STATISTICS_DATE_BEGIN,
                    name: 'date1'
                },
                {
                    xtype: 'datefield',
                    format: 'Y-m-d H:m:s',
                    fieldLabel: lang.STATISTICS_DATE_END,
                    name: 'date2'
                }
                ]
                }],
            buttons: [{
                text: "Submit",
                handler: function(response) {
                    var values = this.ownerCt.ownerCt.getForm().getValues();
                    //dump(values);
                    grid.store.load({
                        params: {
                            date1: values.date1, 
                            date2: values.date2
                        }
                    });
                    Ext.Ajax.request({
                        params: {
                            cmd: 'statistics.recupDataStat',
                            date1: values.date1, 
                            date2: values.date2,
                            type: "fam",
                            isClick: 1
                        },
                        success: function(response,p) {
                                
                            var datastat = [];
                            Ext.each(response.json.stat, function(f)
                            {   

                                datastat.push([f[0],parseFloat(f[1])]);

                            });
                            store.loadData(datastat);
                                
                        }
                    });
                }
            }]
            
        });
        
        ct.add(form1);

    
        var xg = Ext.ux.grid;

        // shared reader
        var reader = new Ext.data.JsonReader({
            root: 'data'
        },[ 
        {
            name: 'company'
        },

        {
            name: 'price'
        },

        {
            name: 'numberClick', 
            type: 'float'
        },
        {
            name: "totalimp",
            type: 'float'
        },
        {
            name: 'spl', 
            type: 'float'
        },
        
        {
            name: 'brd', 
            type: 'float'
        },

        {
            name: 'supplier'
        },

        {
            name: 'desc'
        }
        ]);
        var myStore =  new Ext.data.GroupingStore({
            autoLoad: true,
            url: Ext.Ajax.url,
            params: {
                date1: 0, 
                date2: 0
            },
            reader: reader,
            iconCls: 'i-date',
            sortInfo:{
                field: 'company', 
                direction: "ASC"
            },
            groupField:'supplier'
        });
            
        var grid = new xg.GridPanel({
            columns: [
            {
                id:'company',
                header: lang.STATISTICS_NAME_SUPPLIER_AND_BRAND, 
                width: 40, 
                sortable: true, 
                dataIndex: 'company'
            },

            {
                header: lang.STATISTICS_NUMBER_CLICK, 
                width: 20, 
                sortable: true, 
                dataIndex: 'numberClick'
            },

            {
                header: lang.STATISTICS_NUMBER_IMP, 
                width: 30, 
                sortable: true, 
                dataIndex: 'totalimp'
            },
            
            {
                header: lang.STATISTICS_LABEL_SUPPLIER, 
                width: 20, 
                sortable: true, 
                dataIndex: 'supplier',
                hidden: true
            }
            ],

            view: new Ext.grid.GroupingView({
                forceFit:true,
                groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
            }),
            
            title: lang.STATISTICS_CLICK,
            enableHdMenu: true,
            height: 550,
            // proxy: new Ext.data.HttpProxy,
            iconCls: 'i-chart-bar',
            store: myStore,
            itype: 'statistics.click',
            rowId: 'spl_id',
            paging: true,
            search: false
            
        });


        ct.add(grid);
        
        var datastat = [];
        Ext.each(data.stat, function(f)
        {   

            datastat.push([f[0],parseFloat(f[1])]);
        
        });

        var store = new Ext.data.ArrayStore({
            fields: ['brd', 'click'],
            data: datastat
        });

        var panel = new Ext.Panel({
            width: 700,
            iconCls: 'i-chart-bar',
            height: 600,
            title: lang.STATISTICS_CLICK,
            items: {
                xtype: 'columnchart',
                store: store,
                yField: 'click',
                xField: 'brd',
                xAxis: new Ext.chart.CategoryAxis({
                    title: lang.STATISTICS_LABEL_BRAND
                }),
                yAxis: new Ext.chart.NumericAxis({
                    title: 'Nombre de clic'
                }),
                extraStyle: {
                    xAxis: {
                        labelRotation: -90
                    }
                }
            }
        });
        ct.add(panel);
    },
    makeBrdpForm: function(ct, item, data)
    {  
        var form1 = new Ext.ux.FormPanel({ 
            frame: true,
            title: lang.STATISTICS_DATE,
            iconCls: 'i-date',
            width: 250,
            items   : [
            {
                xtype: 'compositefield',
                msgTarget : 'side',
                anchor    : '-10',
                defaults: {
                    flex: 1
                },
                items: [
                {
                    xtype: 'datefield',
                    format: 'Y-m-d H:m:s',
                    fieldLabel: lang.STATISTICS_DATE_BEGIN,
                    name: 'date1'
                },
                {
                    xtype: 'datefield',
                    format: 'Y-m-d H:m:s',
                    fieldLabel: lang.STATISTICS_DATE_END,
                    name: 'date2'
                }
                ]
                }],
            buttons: [{
                text: "Submit",
                handler: function(response) {
                    var values = this.ownerCt.ownerCt.getForm().getValues();
                    //dump(values);
                    grid.store.load({
                        params: {
                            date1: values.date1, 
                            date2: values.date2
                        }
                    });
                    Ext.Ajax.request({
                        params: {
                            cmd: 'statistics.recupDataStat',
                            date1: values.date1, 
                            date2: values.date2,
                            type: "fam",
                            isClick: 0
                        },
                        success: function(response,p) {
                                
                            var datastat = [];
                            Ext.each(response.json.stat, function(f)
                            {   

                                datastat.push([f[0],parseFloat(f[1])]);

                            });
                            store.loadData(datastat);
                                
                        }
                    });
                }
            }]
            
        });
        
        ct.add(form1);

    
        var xg = Ext.ux.grid;

        // shared reader
        var reader = new Ext.data.JsonReader({
            root: 'data'
        },[ 
        {
            name: 'company'
        },

        {
            name: 'price'
        },
        
        {
            name: 'totalPriceSales'
        },

        {
            name: 'numberSales', 
            type: 'float'
        },
        {
            name: "totalimp",
            type: 'float'
        },
        {
            name: 'spl', 
            type: 'float'
        },
        
        {
            name: 'brd', 
            type: 'float'
        },

        {
            name: 'supplier'
        },

        {
            name: 'desc'
        }
        ]);
        var myStore =  new Ext.data.GroupingStore({
            autoLoad: true,
            url: Ext.Ajax.url,
            params: {
                date1: 0, 
                date2: 0
            },
            reader: reader,
            iconCls: 'i-date',
            sortInfo:{
                field: 'company', 
                direction: "ASC"
            },
            groupField:'supplier'
        });
            
        var grid = new xg.GridPanel({
            columns: [
            {
                id:'company',
                header: lang.STATISTICS_NAME_SUPPLIER_AND_BRAND, 
                width: 40, 
                sortable: true, 
                dataIndex: 'company'
            },

            {
                header: lang.STATISTICS_NUMBER_SALES, 
                width: 20, 
                sortable: true, 
                dataIndex: 'numberSales'
            },

            {
                header: lang.STATISTICS_NUMBER_IMP, 
                width: 30, 
                sortable: true, 
                dataIndex: 'totalimp'
            },
            
            {
                header: lang.STATISTICS_TOTAL_SALES, 
                width: 30, 
                sortable: true, 
                dataIndex: 'totalPriceSales',
                renderer: Xeon.getEuro
            },
            
            {
                header: lang.STATISTICS_LABEL_SUPPLIER, 
                width: 20, 
                sortable: true, 
                dataIndex: 'supplier',
                hidden: true
            }
            ],

            view: new Ext.grid.GroupingView({
                forceFit:true,
                groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
            }),
            
            title: lang.STATISTICS_CLICK,
            enableHdMenu: true,
            height: 550,
            // proxy: new Ext.data.HttpProxy,
            iconCls: 'i-chart-bar',
            store: myStore,
            itype: 'statistics.percent',
            rowId: 'spl_id',
            paging: true,
            search: false
            
        });


        ct.add(grid);
        
        var datastat = [];
        Ext.each(data.stat, function(f)
        {   

            datastat.push([f[0],parseFloat(f[1])]);
        
        });

        var store = new Ext.data.ArrayStore({
            fields: ['brd', 'click'],
            data: datastat
        });

        var panel = new Ext.Panel({
            width: 700,
            iconCls: 'i-chart-bar',
            height: 600,
            title: lang.STATISTICS_CLICK,
            items: {
                xtype: 'columnchart',
                store: store,
                yField: 'click',
                xField: 'brd',
                xAxis: new Ext.chart.CategoryAxis({
                    title: lang.STATISTICS_LABEL_BRAND
                }),
                yAxis: new Ext.chart.NumericAxis({
                    title: lang.STATISTICS_NUMBER_SALES
                }),
                extraStyle: {
                    xAxis: {
                        labelRotation: -90
                    }
                }
            }
        });
        ct.add(panel);
    }
}