(function( $ ){
    $.fn.reloadWidget = function(id){
    	el = $('#' + id);
    	$.get(el.attr('data-url'))
			  .done(function( data ) {
					el.html($(data).html());
			  });
    } 
})(jQuery)