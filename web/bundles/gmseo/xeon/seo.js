
//
// Interface SEO v1.0
// Auteur : Stephane
//

Xeon.module.seo =
{
	allowDrop: function(tree, node, target) {
		return true;
	},
	
	// 1 - Grid Panel lorsqu'on click dans le tree sur un des metiers
	// 2 - Lightbox ajout d'un bureau
	
	makeIndividualForm: function(ct, item, data)
	{
		if(data.db.seo_panel_function_name !=''){
			eval('this.'+"CustomizedDisplayPanel"+data.db.seo_panel_function_name+'(ct, item, data);');
		}else{
			if (!item.add)
			{
				// FormPanel
				if( data.nbchilds > 0 )
				{
					
					// Boucle Childs => items.push
					for( i=0; i < data.nbchilds; i++  )
					{
						var items = [];
						var fieldtype = data.child[i].seo_cnt_type;
						
						// Textarea
						if( fieldtype == 'textarea' )
						{
							items.push({
								fieldLabel: data.child[i].seo_cnt_title,
								allowBlank: true,
								width: 450,
								height: 250,
								name: 'seo_cnt_name',
								xtype: data.child[i].seo_cnt_type,
								value: data.child[i].seo_cnt_name
							});
						}
						else if( fieldtype == 'mediabox' )
						{
							items.push({
								xtype: data.child[i].seo_cnt_type,
								fieldLabel: data.child[i].seo_cnt_title,
								name: 'seo_cnt_name',
								value: data.child[i].seo_cnt_name,
								displayValue: data.child[i].seo_cnt_name
							});
						}
						// Other ('textfield','combobox','togglebox')
						else
						{
						
							items.push({
								fieldLabel: data.child[i].seo_cnt_title,
								allowBlank: true,
								name: 'seo_cnt_name',
								xtype: data.child[i].seo_cnt_type,
								value: data.child[i].seo_cnt_name
							});
						}
						items.push({
								fieldLabel: '',
								allowBlank: true,
								name: 'seo_cnt_index',
								xtype: 'hidden',
								value: data.child[i].seo_cnt_index
							});
						ct.add(new Ext.ux.FormPanel({
							title: data.child[i].seo_cnt_title,
							iconCls: data.icon,
							items: items
						}));
					}
					
				}
				// GridPanel
				else
				{
					
					ct.add(new Ext.ux.grid.GridPanel({
						title: data.title,
						iconCls: data.icon,
						itype: 'seo.seo',
						rowId: 'seo_cnt_id',
						reloadAfterEdit: true,
						paging:  true,
						quickSearch: true,
						columns: [{
							header: lang.xeon.id,
							dataIndex: 'seo_cnt_id',
							dataType: 'int',
							width: 60
						},{
							header: lang.xeon.title,
							dataIndex: 'seo_cnt_title',
							width: 200,
							itemLink: ['seo.ind', 'seo_panel_id']
						},{
							header: 'Contenu',
							dataIndex: 'seo_cnt_name',
							width: 300,
							renderer: Xeon.renderHtmlText/*,
							editor: new Ext.form.TextField({
								allowBlank: false
							})*/
						},{
							header: lang.xeon.status,
							dataIndex: 'seo_cnt_status',
							width: 60,
							renderer: Xeon.renderStatus,
							editor: new Ext.ux.form.ToggleBox()
						}]
					}));
					
				}
			}
		}
	},
	makeError404Form: function(ct, item, data){
		columns=[{
                    infoLabel: lang.xeon.mod_seo.err_url,
                    value: data.db.err_unexisting_page
                },{
                    infoLabel: lang.xeon.mod_seo.err_nb,
                    value: data.db.nb
                },{
                    infoLabel: lang.xeon.mod_seo.err_mindate,
                    value: data.db.mindate,
					renderer: Xeon.renderDatetime
                },{
                    infoLabel: lang.xeon.mod_seo.err_maxdate,
                    value: data.db.maxdate,
					renderer: Xeon.renderDatetime
                }];
		 ct.add(new Ext.ux.InfoPanel({
                    title: lang.xeon.info,
                    iconCls: 'i-information',
                    items: columns
                }));
		
		ct.add(new Ext.ux.grid.GridPanel({
						title: 'Details des accès',
						iconCls: data.icon,
						itype: 'seo.errord404',
						rowId: 'err_id',
						parent: 'seo.error404.'+item.id,
						reloadAfterEdit: true,
						paging:  true,
						quickSearch: false,
						search: false,
						columns: [{
							header: 'Origine',
							dataIndex: 'err_referer',
							width: 300							
						},{
							header: "Date",
							dataIndex: 'err_occur_date',
							width: 100,
							renderer: Xeon.renderDatetime						
						},{
							header: 'Adresse IP',
							dataIndex: 'err_remote_host',
							width: 200
						}]
					}));
	},
	CustomizedDisplayPaneldisplay404Panel:function(ct, item, data){
		
	
		data.db.seo_panel_function_name ='';
		this.makeIndividualForm(ct, item, data);
		
		ct.add(new Ext.ux.grid.GridPanel({
						title: 'redirection 404 en 301 ',
						iconCls: data.icon,
						itype: 'seo.sar',
						rowId: 'sar_id',
						reloadAfterEdit: true,
						paging:  true,
						quickSearch: false,
						search: false,
						columns: [{
							header: "PAGE 404",
							dataIndex: 'sar_error_page',
							width: 200,
							editor: new Ext.form.TextField()							
						},{
							header: 'Cible 301',
							dataIndex: 'sar_target_page',
							width: 300,
							renderer: Xeon.renderText,
							editor: new Ext.form.TextField()
						}]
					}));
	ct.add(new Ext.ux.grid.GridPanel({
						title: 'Erreur 404 ',
						iconCls: data.icon,
						itype: 'seo.error404',
						rowId: 'err_maxid',
						reloadAfterEdit: true,
						paging:  true,
						quickSearch: false,
						search: false,
						columns: [{
							header: "PAGE 404",
							dataIndex: 'err_unexisting_page',
							width: 400,
							itemLink: ['seo.error404', 'err_maxid']							
						},{
							header: 'Nombre de hit',
							dataIndex: 'err_nb',
							width: 100
						},{
							header: 'Derniere Visite',
							dataIndex: 'err_lastdate',
							width: 100,
							renderer: Xeon.renderDatetime
						}]
					}));
	},
	CustomizedDisplayPanelGooglePlus:function(ct, item, data){
		//JAIME
		items=[];
		items.push({
			fieldLabel: lang.xeon.mod_seo.googleplus_cms,
			name: 'seo_googleplus_cms',
			xtype: 'togglebox',
			value: data.child[0].seo_cnt_name
		});
		items.push({
			fieldLabel: lang.xeon.mod_seo.googleplus_produits,
			name: 'seo_googleplus_pdt',
			xtype: 'togglebox',
			value: data.child[1].seo_cnt_name
		});
		
		ct.add(new Ext.ux.FormPanel({
			title: lang.xeon.mod_seo.gplus_googleplus,
			iconCls: data.icon,
			items: items,
			command: 'seo.saveGooglePlus'			
		}));
		
		
		
		//SCRIPT LIBRES
		ct.add(new Ext.ux.grid.GridPanel({
						title: 'Scripts libres '+data.title,
						iconCls: data.icon,
						itype: 'seo.ggp',
						rowId: 'seo_scrggp_id',
						reloadAfterEdit: true,
						paging:  true,
						quickSearch: false,
						columns: [{
							header: lang.xeon.id,
							dataIndex: 'seo_scrggp_id',
							dataType: 'int',
							width: 60
						},{
							header: lang.xeon.title,
							dataIndex: 'seo_scrggp_title',
							width: 200							
						},{
							header: 'Contenu',
							dataIndex: 'seo_scrggp_content',
							width: 300,
							renderer: Xeon.renderText,
							editor: new Ext.form.TextField()
						}]
					}));
	},makeAutoredirectForm: function(ct, item, data)
    { 
        var items = [{
            fieldLabel: "Page erreur",
            name: 'sar_error_page',
            allowBlank: false,
            value: data.db.sar_error_page
        },{
            fieldLabel: "Page cible",
            name: 'sar_target_page',
            allowBlank: false,
            value: data.db.sar_target_page
        }];

         ct.add(new Ext.ux.FormPanel({
                items: items
            }));
	}
	,makePingautomaticForm: function(ct, item, data)
    { 
        var items = [{
            fieldLabel: "Page de ping",
            name: 'pau_url',
            allowBlank: false,
            value: data.db.pau_url
        }];

         ct.add(new Ext.ux.FormPanel({
                items: items
            }));
	},makeGoogleplusscriptForm: function(ct, item, data)
    { 
        var items = [{
            fieldLabel: lang.xeon.title,
            name: 'seo_scrggp_title',
            allowBlank: false,
            value: data.db.seo_scrggp_title
        },{
            xtype: 'textarea',
            fieldLabel: 'Contenu',
            name: 'seo_scrggp_content',
            value: data.db.seo_scrggp_content
        }];

         ct.add(new Ext.ux.FormPanel({
                items: items
            }));
	},
	CustomizedDisplayPanelPingAutomatic:function(ct, item, data){
		//SCRIPT LIBRES
		ct.add(new Ext.ux.grid.GridPanel({
						title: 'Liste des URL &agrave; pinger tous les jours ',
						iconCls: data.icon,
						itype: 'seo.pau',
						rowId: 'pau_id',
						reloadAfterEdit: true,
						paging:  true,
						quickSearch: false,
						search: false,
						columns: [{
							header: lang.xeon.id,
							dataIndex: 'pau_id',
							dataType: 'int',
							width: 60
						},{
							header: lang.xeon.url,
							dataIndex: 'pau_url',
							width: 200,
							editor: new Ext.form.TextField()							
						}]
					}));
	},
	CustomizedDisplayPanelTwitter:function(ct, item, data){
		//JAIME
		items=[];
		items.push({
			fieldLabel: lang.xeon.mod_seo.twitter_cms,
			name: 'seo_twitter_cms',
			xtype: 'togglebox',
			value: data.child[0].seo_cnt_name
		});
		items.push({
			fieldLabel: lang.xeon.mod_seo.twitter_produits,
			name: 'seo_twitter_pdt',
			xtype: 'togglebox',
			value: data.child[1].seo_cnt_name
		});
		items.push({
			fieldLabel: lang.xeon.mod_seo.twitter_texte,
			name: 'seo_twitter_text',
			value: data.child[2].seo_cnt_name
		});
		ct.add(new Ext.ux.FormPanel({
			title: lang.xeon.mod_seo.twitter,
			iconCls: data.icon,
			items: items,
			command: 'seo.saveTwitter'			
		}));
		
		
		
		//SCRIPT LIBRES
		ct.add(new Ext.ux.grid.GridPanel({
						title: 'Scripts libres '+data.title,
						iconCls: data.icon,
						itype: 'seo.twts',
						rowId: 'seo_scrtwt_id',
						reloadAfterEdit: true,
						paging:  true,
						quickSearch: false,
						columns: [{
							header: lang.xeon.id,
							dataIndex: 'seo_scrtwt_id',
							dataType: 'int',
							width: 60
						},{
							header: lang.xeon.title,
							dataIndex: 'seo_scrtwt_title',
							width: 200							
						},{
							header: 'Contenu',
							dataIndex: 'seo_scrtwt_content',
							width: 300,
							renderer: Xeon.renderText,
							editor: new Ext.form.TextField()
						}]
					}));
	},makeTwitterscriptForm: function(ct, item, data)
    { 
        var items = [{
            fieldLabel: lang.xeon.title,
            name: 'seo_scrtwt_title',
            allowBlank: false,
            value: data.db.seo_scrtwt_title
        },{
            xtype: 'textarea',
            fieldLabel: 'Contenu',
            name: 'seo_scrtwt_content',
            value: data.db.seo_scrtwt_content
        }];

         ct.add(new Ext.ux.FormPanel({
                items: items
            }));
	},
	makeCmsstatiquesForm: function(ct, item, data){
		// new Ext.ux.ListPanel({
                    // title: lang.SHOP_LINK_PAGE,
                    // iconCls: 'i-page-white',
                    // name: 'prd_link_page',
                    // itype: 'cms.pge',
                    // data: data.lpg
                // })
				
				 cms = new Ext.ux.form.ListBox({
            fieldLabel:"Page cible",
            itype:'seo.cmspages',
            name:'pge_id',
            displayValue:data.db.pge_title,
            value:data.db.pge_id
        });
		var items = [{
            fieldLabel: lang.xeon.mod_seo.page_name,
            name: 'cst_name',
            allowBlank: false,
            value: data.db.cst_name
        },cms,{
            fieldLabel: lang.xeon.url,
            name: 'cst_url',
            allowBlank: false,
            value: data.db.cst_url
        }];

         ct.add(new Ext.ux.FormPanel({
                items: items
            }));
	},
	CustomizedDisplayPanelCMSstatiques:function(ct, item, data){
		//SCRIPT LIBRES
		ct.add(new Ext.ux.grid.GridPanel({
						title: 'Liste des pages statiques',
						iconCls: data.icon,
						itype: 'seo.cst',
						rowId: 'cst_id',
						reloadAfterEdit: true,
						enableDelete: false,
						paging:  true,
						quickSearch: false,
						search: false,
						columns: [{
							header: lang.xeon.id,
							dataIndex: 'cst_id',
							dataType: 'int',
							width: 60,
							itemLink: ['seo.cst', 'cst_id']
						},{
							header: lang.xeon.mod_seo.page_name,
							dataIndex: 'cst_name',
							width: 200
						},{
							header: lang.xeon.mod_seo.linked_page,
							dataIndex: 'pge_name',
							width: 200
						},{
							header: lang.xeon.url,
							dataIndex: 'cst_url',
							width: 200,
							editor: new Ext.form.TextField()							
						}]
					}));
	},
	CustomizedDisplayPanelFacebook:function(ct, item, data){
		//JAIME
		items=[];
		items.push({
			fieldLabel: lang.xeon.mod_seo.jaime_facebook_cms,
			name: 'seo_facebook_jaime_cms',
			xtype: 'togglebox',
			value: data.child[0].seo_cnt_name
		});
		items.push({
			fieldLabel: lang.xeon.mod_seo.jaime_facebook_produits,
			name: 'seo_facebook_jaime_pdt',
			xtype: 'togglebox',
			value: data.child[1].seo_cnt_name
		});
		ct.add(new Ext.ux.FormPanel({
			title: lang.xeon.mod_seo.jaime_facebook,
			iconCls: data.icon,
			items: items,
			command: 'seo.saveFacebook'			
		}));
		
		//PARTAGES
		items=[];
		items.push({
			fieldLabel: lang.xeon.mod_seo.partage_facebook_cms,
			name: 'seo_facebook_partage_cms',
			xtype: 'togglebox',
			value: data.child[2].seo_cnt_name
		});
		items.push({
			fieldLabel: lang.xeon.mod_seo.partage_facebook_produits,
			name: 'seo_facebook_partage_pdt',
			xtype: 'togglebox',
			value: data.child[3].seo_cnt_name
		});
		ct.add(new Ext.ux.FormPanel({
			title: lang.xeon.mod_seo.partage_facebook,
			iconCls: data.icon,
			items: items,
			command: 'seo.saveFacebook'		
		}));
		
		//SCRIPT LIBRES
		ct.add(new Ext.ux.grid.GridPanel({
						title: 'Scripts libres '+data.title,
						iconCls: data.icon,
						itype: 'seo.fbs',
						rowId: 'seo_scrfb_id',
						reloadAfterEdit: true,
						paging:  true,
						quickSearch: false,
						columns: [{
							header: lang.xeon.id,
							dataIndex: 'seo_scrfb_id',
							dataType: 'int',
							width: 60
						},{
							header: lang.xeon.title,
							dataIndex: 'seo_scrfb_title',
							width: 200							
						},{
							header: 'Contenu',
							dataIndex: 'seo_scrfb_content',
							width: 300,
							renderer: Xeon.renderText,
							editor: new Ext.form.TextField()
						}]
					}));
	},makeFacebookscriptForm: function(ct, item, data)
    { 
        var items = [{
            fieldLabel: lang.xeon.title,
            name: 'seo_scrfb_title',
            allowBlank: false,
            value: data.db.seo_scrfb_title
        },{
            xtype: 'textarea',
            fieldLabel: 'Contenu',
            name: 'seo_scrfb_content',
            value: data.db.seo_scrfb_content
        }];

         ct.add(new Ext.ux.FormPanel({
                items: items
            }));
	},makeScriptanaliticsadwordsForm: function(ct, item, data)
    { 
        var items = [{
            fieldLabel: lang.xeon.url,
            name: 'seo_scanad_url',
            allowBlank: false,
            value: data.db.seo_scanad_url
        },{
            fieldLabel: "Site ID",
            name: 'seo_scanad_siteid',
            allowBlank: false,
            value: data.db.seo_scanad_siteid
        },{
            fieldLabel: "Site label",
            name: 'seo_scanad_sitelabel',
            allowBlank: false,
            value: data.db.seo_scanad_sitelabel
        }];

         ct.add(new Ext.ux.FormPanel({
                items: items
            }));
	},CustomizedDisplayPanelAdwordsAnalitics: function(ct, item, data)
    {
		data.db.seo_panel_function_name='';
		this.makeIndividualForm(ct, item, data);
		//SCRIPT LIBRES
		ct.add(new Ext.ux.grid.GridPanel({
						title: 'Scripts libres '+data.title,
						iconCls: data.icon,
						itype: 'seo.scanad',
						rowId: 'seo_scanad_id',
						reloadAfterEdit: true,
						paging:  true,
						quickSearch: false,
						columns: [{
							header: lang.xeon.id,
							dataIndex: 'seo_scanad_id',
							dataType: 'int',
							width: 60
						},{
							header: lang.xeon.url,
							dataIndex: 'seo_scanad_url',
							width: 200,
							renderer: Xeon.renderText,
							editor: new Ext.form.TextField()							
						},{
							header: 'Site Id',
							dataIndex: 'seo_scanad_siteid',
							width: 300,
							renderer: Xeon.renderText,
							editor: new Ext.form.TextField()
						},{
							header: 'Site Label',
							dataIndex: 'seo_scanad_sitelabel',
							width: 300,
							renderer: Xeon.renderText,
							editor: new Ext.form.TextField()
						}]
					}));
	}

}


Ext.ux.form.setActif = Ext.extend(Ext.ux.form.ComboBox, {
	data: [
		[0, lang.ASSOCIE_NO],
		[1, lang.ASSOCIE_YES]
	]
});