
//
// Interface contact v1.0.0
// Auteur : Thierry Marianne
//

Xeon.module.widget =
{
	allowDrop: function(tree, node, target) {
		return true;
	},

	// getAction: function(v)
	// {
	// 	if (v == 0) return lang.xeon.mod_contact.state_pending;
	// 	if (v == 1) return lang.xeon.mod_contact.state_processed;
	// 	if (v == 2) return lang.xeon.mod_contact.state_archived;
	// 	else return false;
	// },
		
	makeShellForm: function(ct, item, data)
	{
		if (!item.add)
		{
			var items = [];

			items.push({
				fieldLabel: lang.xeon.name,
				allowBlank: false,
				name: 'name',
				value: data.db.name
			},{
				fieldLabel: lang.xeon.mod_widget.type,
				allowBlank: false,
				name: 'type',
				value: data.db.type,
				disabled : true
			},{
				xtype: 'listbox',
                fieldLabel: lang.xeon.mod_widget.category,               
                itype: 'widget.category',
                name: 'category',
				link:  data.db.category,
                value: data.db.category,
                displayValue: data.db.category_name,
				allowBlank: false,
			});

			ct.add(new Ext.ux.FormPanel({
	            title: lang.xeon.mod_widget.widget_general,
	            iconCls: 'i-information',
				items: items
			}));

			if(data.config.length > 0)
			{
				var items = [];

				for(var i=0;i<data.config.length;i++)
	            {
	                items.push({
	                    xtype: 'listbox',
	                    fieldLabel: data.routes[i].path,               
	                    itype: 'cms.links',
	                    name: data.routes[i].name,
	                    value: data.db[data.routes[i].name],
	                    displayValue: data.db[data.routes[i].name + '_title']
	                });
	            }

				ct.add(new Ext.ux.FormPanel({
		            title: lang.xeon.mod_widget.widget_config,
		            iconCls: 'i-color-wheel',
					items: items
				}));
			}
		}
		else
		{
			var items = [];
			items.push({
				fieldLabel: lang.xeon.name,
				allowBlank: false,
				name: 'name',
				value: data.db.name
			},{
				xtype: 'listbox',
                fieldLabel: lang.xeon.mod_widget.type,               
                itype: 'widget.type',
                name: 'type',
				link:  data.db.type,
                value: data.db.type_name,
				allowBlank: false,
			},{
				xtype: 'listbox',
                fieldLabel: lang.xeon.mod_widget.category,               
                itype: 'widget.category',
                name: 'category',
				link:  data.db.category,
                value: data.db.category,
				allowBlank: false,
			});
			
			ct.add(new Ext.ux.FormPanel({
	            iconCls: 'i-brick',
				items: items
			}));
		}

	},

	// makeIndividualForm: function(ct, item, data)
	// {
	// 	var items = [];
	// 	items.push({
	// 			xtype: 'listbox',
 //                fieldLabel: lang.xeon.mod_contact.title,               
 //                itype: 'contact.title',
 //                name: 'title',
	// 			link:  data.db.title,
 //                value: data.db.title,
 //                displayValue: data.db.title_title
	// 		},{
	// 			fieldLabel: lang.xeon.mod_contact.first_name,
	// 			allowBlank: false,
	// 			name: 'first_name',
	// 			value: data.db.first_name
	// 		},{
	// 			fieldLabel: lang.xeon.mod_contact.last_name,
	// 			allowBlank: false,
	// 			name: 'last_name',
	// 			value: data.db.last_name
	// 		},{
	// 			fieldLabel: lang.xeon.email,
	// 			allowBlank: false,
	// 			name: 'email',
	// 			value: data.db.email
	// 		},{
	// 			fieldLabel: lang.xeon.mod_contact.address,
	// 			allowBlank: true,
	// 			name: 'address',
	// 			value: data.db.address
	// 		},{
	// 			fieldLabel: lang.xeon.mod_contact.address2,
	// 			allowBlank: true,
	// 			name: 'address2',
	// 			value: data.db.address2
	// 		},{
	// 			fieldLabel: lang.xeon.mod_contact.address2,
	// 			allowBlank: true,
	// 			name: 'address3',
	// 			value: data.db.address3
	// 		},{
	// 			fieldLabel: lang.xeon.mod_contact.postal_code,
	// 			allowBlank: true,
	// 			name: 'zip_code',
	// 			value: data.db.zip_code
	// 		},{
	// 			fieldLabel: lang.xeon.mod_contact.city,
	// 			allowBlank: true,
	// 			name: 'city',
	// 			value: data.db.city
	// 		},{
	// 			fieldLabel: lang.xeon.mod_contact.phone,
	// 			allowBlank: true,
	// 			name: 'phone',
	// 			value: data.db.phone
	// 		}/*,{
	// 			fieldLabel: lang.xeon.mod_contact.fax,
	// 			allowBlank: true,
	// 			name: 'fax',
	// 			value: data.db.fax
	// 		}*/,{
	// 			fieldLabel: lang.xeon.mod_contact.subject,
	// 			allowBlank: true,
	// 			name: 'object',
	// 			value: data.db.object
	// 		},{
	// 			xtype: 'textarea',
	// 			fieldLabel: lang.xeon.mod_contact.com,
	// 			name: 'message',
	// 			height: 200,
	// 			value: data.db.message
	// 		},{
	// 			xtype: 'listbox',
 //                fieldLabel: lang.xeon.mod_contact.state,               
 //                itype: 'contact.status',
 //                name: 'status',
	// 			link:  data.db.status,
 //                value: data.db.status,
 //                displayValue: data.db.status_title
	// 		});
		
	// 		ct.add(new Ext.ux.FormPanel({
	// 			title: lang.xeon.mod_contact.block_ind,
	// 			iconCls: 'i-user',
	// 			items: items
	// 	}));
	// },

	// makeEmailForm: function(ct, item, data){
	// 	 var items = [{
 //            fieldLabel: lang.xeon.email,
 //            name: 'email',
 //            allowBlank: false,
 //            value: data.db.email
 //        }];

 //         ct.add(new Ext.ux.FormPanel({
 //                items: items
 //            }));
	// }
}

// Ext.ux.form.setAction = Ext.extend(Ext.ux.form.ComboBox, {
// 	data: [
// 		[0, lang.xeon.mod_contact.state_pending],
// 		[1, lang.xeon.mod_contact.state_processed],
// 		[2, lang.xeon.mod_contact.state_archived]
// 	]
// });