<?php

namespace GM\ReviewBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Comment
 *
 * @ORM\Table(name="gm_review__comment")
 * @ORM\Entity(repositoryClass="GM\CmsBundle\Entity\CommentRepository")
 */
class Comment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="GM\ReviewBundle\Entity\Info", cascade={"persist","remove"})
     */
    private $info;

    /**
     * @ORM\ManyToOne(targetEntity="GM\ReviewBundle\Entity\Comment")
     */
    private $parent;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text")
     * @Assert\NotBlank()
     * @Assert\Length(min = "3")
     */
    private $comment;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    
        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set info
     *
     * @param \GM\ReviewBundle\Entity\Info $info
     * @return Comment
     */
    public function setInfo(\GM\ReviewBundle\Entity\Info $info = null)
    {
        $this->info = $info;
    
        return $this;
    }

    /**
     * Get info
     *
     * @return \GM\ReviewBundle\Entity\Info 
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * Set parent
     *
     * @param \GM\ReviewBundle\Entity\Comment $parent
     * @return Comment
     */
    public function setParent(\GM\ReviewBundle\Entity\Comment $parent = null)
    {
        $this->parent = $parent;
    
        return $this;
    }

    /**
     * Get parent
     *
     * @return \GM\ReviewBundle\Entity\Comment 
     */
    public function getParent()
    {
        return $this->parent;
    }
}