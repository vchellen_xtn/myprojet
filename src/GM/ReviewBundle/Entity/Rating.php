<?php

namespace GM\ReviewBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rating
 *
 * @ORM\Table(name="gm_review__rating")
 * @ORM\Entity(repositoryClass="GM\CmsBundle\Entity\RatingRepository")
 */
class Rating
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="GM\ReviewBundle\Entity\Review", inversedBy="rating")
     */
    private $review;

    /**
     * @var smallint
     *
     * @ORM\Column(name="rate", type="smallint")
     */
    private $rate;

    /**
     * @ORM\OneToOne(targetEntity="GM\ReviewBundle\Entity\Comment")
     */
    private $comment;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rate
     *
     * @param integer $rate
     * @return Rating
     */
    public function setRate($rate)
    {
        $this->rate = $rate;
    
        return $this;
    }

    /**
     * Get rate
     *
     * @return integer 
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * Set review
     *
     * @param \GM\ReviewBundle\Entity\Review $review
     * @return Rating
     */
    public function setReview(\GM\ReviewBundle\Entity\Review $review = null)
    {
        $this->review = $review;
    
        return $this;
    }

    /**
     * Get review
     *
     * @return \GM\ReviewBundle\Entity\Review 
     */
    public function getReview()
    {
        return $this->review;
    }

    /**
     * Set comment
     *
     * @param \GM\ReviewBundle\Entity\Comment $comment
     * @return Rating
     */
    public function setComment(\GM\ReviewBundle\Entity\Comment $comment = null)
    {
        $this->comment = $comment;
    
        return $this;
    }

    /**
     * Get comment
     *
     * @return \GM\ReviewBundle\Entity\Comment 
     */
    public function getComment()
    {
        return $this->comment;
    }
}