<?php

namespace GM\ReviewBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Review
 *
 * @ORM\Table(name="gm_review")
 * @ORM\Entity(repositoryClass="GM\ReviewBundle\Entity\ReviewRepository")
 */
class Review
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="GM\ReviewBundle\Entity\Info")
     */
    private $info;

    /**
     * @ORM\OneToOne(targetEntity="GM\ReviewBundle\Entity\Comment")
     */
    private $comment;

    /**
     * @ORM\OneToMany(targetEntity="GM\ReviewBundle\Entity\Rating", mappedBy="review")
     */
    private $rating;

    public function __construct()
    {
        $this->rating = new ArrayCollection();
    }

}