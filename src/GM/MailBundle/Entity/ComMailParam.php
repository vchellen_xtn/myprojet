<?php

// namespace GM\MailBundle\Entity;

// use Doctrine\ORM\Mapping as ORM;

// /**
//  * ComMailParam
//  *
//  * @ORM\Table()
//  * @ORM\Entity(repositoryClass="GM\CmsBundle\Entity\ComMailParamRepository")
//  */
// class ComMailParam
// {
//     /**
//      * @var integer
//      *
//      * @ORM\Column(name="id", type="integer")
//      * @ORM\Id
//      * @ORM\GeneratedValue(strategy="AUTO")
//      */
//     private $id;

//     /**
//      * @var string
//      *
//      * @ORM\Column(name="paramName", type="string", length=20)
//      */
//     private $paramName;

//     /**
//      * @var string
//      *
//      * @ORM\Column(name="name", type="string", length=75)
//      */
//     private $name;

//     /**
//      * @var integer
//      *
//      * @ORM\Column(name="type", type="smallint")
//      */
//     private $type;

//     /**
//      * @var integer
//      *
//      * @ORM\Column(name="paramMail", type="integer")
//      */
//     private $paramMail;


//     /**
//      * Get id
//      *
//      * @return integer 
//      */
//     public function getId()
//     {
//         return $this->id;
//     }

//     /**
//      * Set paramName
//      *
//      * @param string $paramName
//      * @return ComMailParam
//      */
//     public function setParamName($paramName)
//     {
//         $this->paramName = $paramName;
    
//         return $this;
//     }

//     /**
//      * Get paramName
//      *
//      * @return string 
//      */
//     public function getParamName()
//     {
//         return $this->paramName;
//     }

//     /**
//      * Set name
//      *
//      * @param string $name
//      * @return ComMailParam
//      */
//     public function setName($name)
//     {
//         $this->name = $name;
    
//         return $this;
//     }

//     /**
//      * Get name
//      *
//      * @return string 
//      */
//     public function getName()
//     {
//         return $this->name;
//     }

//     /**
//      * Set type
//      *
//      * @param integer $type
//      * @return ComMailParam
//      */
//     public function setType($type)
//     {
//         $this->type = $type;
    
//         return $this;
//     }

//     /**
//      * Get type
//      *
//      * @return integer 
//      */
//     public function getType()
//     {
//         return $this->type;
//     }

//     /**
//      * Set paramMail
//      *
//      * @param integer $paramMail
//      * @return ComMailParam
//      */
//     public function setParamMail($paramMail)
//     {
//         $this->paramMail = $paramMail;
    
//         return $this;
//     }

//     /**
//      * Get paramMail
//      *
//      * @return integer 
//      */
//     public function getParamMail()
//     {
//         return $this->paramMail;
//     }
// }
