<?php

// namespace GM\MailBundle\Entity;

// use Doctrine\ORM\Mapping as ORM;

// /**
//  * ComMailTags
//  *
//  * @ORM\Table()
//  * @ORM\Entity(repositoryClass="GM\CmsBundle\Entity\ComMailTagsRepository")
//  */
// class ComMailTags
// {
//     /**
//      * @var integer
//      *
//      * @ORM\Column(name="id", type="integer")
//      * @ORM\Id
//      * @ORM\GeneratedValue(strategy="AUTO")
//      */
//     private $id;

//     /**
//      * @var integer
//      *
//      * @ORM\Column(name="type", type="integer")
//      */
//     private $type;

//     /**
//      * @var string
//      *
//      * @ORM\Column(name="name", type="string", length=255)
//      */
//     private $name;

//     /**
//      * @var string
//      *
//      * @ORM\Column(name="linkedField", type="string", length=50)
//      */
//     private $linkedField;

//     /**
//      * @var integer
//      *
//      * @ORM\Column(name="cat", type="integer")
//      */
//     private $cat;


//     /**
//      * Get id
//      *
//      * @return integer 
//      */
//     public function getId()
//     {
//         return $this->id;
//     }

//     /**
//      * Set type
//      *
//      * @param integer $type
//      * @return ComMailTags
//      */
//     public function setType($type)
//     {
//         $this->type = $type;
    
//         return $this;
//     }

//     /**
//      * Get type
//      *
//      * @return integer 
//      */
//     public function getType()
//     {
//         return $this->type;
//     }

//     /**
//      * Set name
//      *
//      * @param string $name
//      * @return ComMailTags
//      */
//     public function setName($name)
//     {
//         $this->name = $name;
    
//         return $this;
//     }

//     /**
//      * Get name
//      *
//      * @return string 
//      */
//     public function getName()
//     {
//         return $this->name;
//     }

//     /**
//      * Set linkedField
//      *
//      * @param string $linkedField
//      * @return ComMailTags
//      */
//     public function setLinkedField($linkedField)
//     {
//         $this->linkedField = $linkedField;
    
//         return $this;
//     }

//     /**
//      * Get linkedField
//      *
//      * @return string 
//      */
//     public function getLinkedField()
//     {
//         return $this->linkedField;
//     }

//     /**
//      * Set cat
//      *
//      * @param integer $cat
//      * @return ComMailTags
//      */
//     public function setCat($cat)
//     {
//         $this->cat = $cat;
    
//         return $this;
//     }

//     /**
//      * Get cat
//      *
//      * @return integer 
//      */
//     public function getCat()
//     {
//         return $this->cat;
//     }
// }
