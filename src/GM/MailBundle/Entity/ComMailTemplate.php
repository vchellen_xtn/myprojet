<?php

// namespace GM\MailBundle\Entity;

// use Doctrine\ORM\Mapping as ORM;

// /**
//  * ComMailTemplate
//  *
//  * @ORM\Table()
//  * @ORM\Entity(repositoryClass="GM\CmsBundle\Entity\ComMailTemplateRepository")
//  */
// class ComMailTemplate
// {
//     /**
//      * @var integer
//      *
//      * @ORM\Column(name="id", type="integer")
//      * @ORM\Id
//      * @ORM\GeneratedValue(strategy="AUTO")
//      */
//     private $id;

//     /**
//      * @var string
//      *
//      * @ORM\Column(name="name", type="string", length=75)
//      */
//     private $name;

//     /**
//      * @var integer
//      *
//      * @ORM\Column(name="type", type="integer")
//      */
//     private $type;

//     /**
//      * @var string
//      *
//      * @ORM\Column(name="tags", type="string", length=255)
//      */
//     private $tags;

//     /**
//      * @var string
//      *
//      * @ORM\Column(name="content", type="text")
//      */
//     private $content;


//     /**
//      * Get id
//      *
//      * @return integer 
//      */
//     public function getId()
//     {
//         return $this->id;
//     }

//     /**
//      * Set name
//      *
//      * @param string $name
//      * @return ComMailTemplate
//      */
//     public function setName($name)
//     {
//         $this->name = $name;
    
//         return $this;
//     }

//     /**
//      * Get name
//      *
//      * @return string 
//      */
//     public function getName()
//     {
//         return $this->name;
//     }

//     /**
//      * Set type
//      *
//      * @param integer $type
//      * @return ComMailTemplate
//      */
//     public function setType($type)
//     {
//         $this->type = $type;
    
//         return $this;
//     }

//     /**
//      * Get type
//      *
//      * @return integer 
//      */
//     public function getType()
//     {
//         return $this->type;
//     }

//     /**
//      * Set tags
//      *
//      * @param string $tags
//      * @return ComMailTemplate
//      */
//     public function setTags($tags)
//     {
//         $this->tags = $tags;
    
//         return $this;
//     }

//     /**
//      * Get tags
//      *
//      * @return string 
//      */
//     public function getTags()
//     {
//         return $this->tags;
//     }

//     /**
//      * Set content
//      *
//      * @param string $content
//      * @return ComMailTemplate
//      */
//     public function setContent($content)
//     {
//         $this->content = $content;
    
//         return $this;
//     }

//     /**
//      * Get content
//      *
//      * @return string 
//      */
//     public function getContent()
//     {
//         return $this->content;
//     }
// }
