<?php

// namespace GM\MailBundle\Entity;

// use Doctrine\ORM\Mapping as ORM;

// /**
//  * ComMailType
//  *
//  * @ORM\Table()
//  * @ORM\Entity(repositoryClass="GM\CmsBundle\Entity\ComMailTypeRepository")
//  */
// class ComMailType
// {
//     /**
//      * @var integer
//      *
//      * @ORM\Column(name="id", type="integer")
//      * @ORM\Id
//      * @ORM\GeneratedValue(strategy="AUTO")
//      */
//     private $id;

//     /**
//      * @var string
//      *
//      * @ORM\Column(name="name", type="string", length=255)
//      */
//     private $name;

//     /**
//      * @var string
//      *
//      * @ORM\Column(name="froms", type="string", length=75)
//      */
//     private $froms;

//     /**
//      * @var string
//      *
//      * @ORM\Column(name="tos", type="string", length=255)
//      */
//     private $tos;

//     /**
//      * @var string
//      *
//      * @ORM\Column(name="cc", type="string", length=255)
//      */
//     private $cc;

//     /**
//      * @var string
//      *
//      * @ORM\Column(name="content", type="text")
//      */
//     private $content;


//     /**
//      * Get id
//      *
//      * @return integer 
//      */
//     public function getId()
//     {
//         return $this->id;
//     }

//     /**
//      * Set name
//      *
//      * @param string $name
//      * @return ComMailType
//      */
//     public function setName($name)
//     {
//         $this->name = $name;
    
//         return $this;
//     }

//     /**
//      * Get name
//      *
//      * @return string 
//      */
//     public function getName()
//     {
//         return $this->name;
//     }

//     /**
//      * Set froms
//      *
//      * @param string $froms
//      * @return ComMailType
//      */
//     public function setFroms($froms)
//     {
//         $this->froms = $froms;
    
//         return $this;
//     }

//     /**
//      * Get froms
//      *
//      * @return string 
//      */
//     public function getFroms()
//     {
//         return $this->froms;
//     }

//     /**
//      * Set tos
//      *
//      * @param string $tos
//      * @return ComMailType
//      */
//     public function setTos($tos)
//     {
//         $this->tos = $tos;
    
//         return $this;
//     }

//     /**
//      * Get tos
//      *
//      * @return string 
//      */
//     public function getTos()
//     {
//         return $this->tos;
//     }

//     /**
//      * Set cc
//      *
//      * @param string $cc
//      * @return ComMailType
//      */
//     public function setCc($cc)
//     {
//         $this->cc = $cc;
    
//         return $this;
//     }

//     /**
//      * Get cc
//      *
//      * @return string 
//      */
//     public function getCc()
//     {
//         return $this->cc;
//     }

//     /**
//      * Set content
//      *
//      * @param string $content
//      * @return ComMailType
//      */
//     public function setContent($content)
//     {
//         $this->content = $content;
    
//         return $this;
//     }

//     /**
//      * Get content
//      *
//      * @return string 
//      */
//     public function getContent()
//     {
//         return $this->content;
//     }
// }
