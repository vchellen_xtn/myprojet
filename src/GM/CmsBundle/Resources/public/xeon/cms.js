
//
// Interface cms v3.0.11
// Auteur : Sylvain Durozard
//

Xeon.module.cms =
{
    allowDrop: function(tree, node, target) {
        return true;
    },
	copyMobile: function (item){
		
		if(item){
			Ext.Ajax.request({
				params: {
					cmd: 'cms.copyMobile',
					item: item
				},
				before: function() {
					Xeon.ui.view.el.mask(lang.xeon.loading, 'x-mask-loading');
				},
				success: function(a)
				{
					Xeon.callback('cms','makeCopyMobileForm', item, a.json);
				},
				callback: function() {
					Xeon.ui.view.el.unmask();
				}
			});
		}else{
			alert(lang.xeon.mod_cms.error_copy_mobile_no_select);
		}
	},
	makeCopyMobileForm: function(id,data){
		
		var item = Xeon.getId(id);
		
		var ct = new Ext.ux.Window({
                    itemId: 'cms.mobile.'+item.id,
                    title: data.title,
                    iconCls: 'i-page-white-copy'
                });
				
		ct.add(new Ext.ux.FormPanel({
            command: 'xeon.copyItem',
            items: [{
                xtype: 'listbox',
                fieldLabel: lang.xeon.target,
                allowBlank: false,
                itype: 'cms.mobile',
                name: 'pge_parent'
            },{
                fieldLabel: lang.xeon.title,
                allowBlank: false,
                name: 'pge_title',
                value: data.db.pge_title
            }]
        }));
		ct.show();
	},
       makePagemRoot:function(ct,item,data){
        t=new Ext.ux.grid.GridPanel({
            title: lang.xeon.mod_cms.page_list,
			 mobile:data.mobile,
            iconCls: 'i-page-white-stack',
            itype: 'cms.mobile',
            rowId: 'id',
            paging:  true,
            quickSearch: true,
            search:engine.getSearchCmsItems(data),
            columns: [{
                header: lang.xeon.id,
                dataIndex: 'id',
                dataType: 'int',
                width: 60
            },{
                header: lang.xeon.title,
                dataIndex: 'title',
                width: 200,
                itemLink: ['cms.pge', 'id']
            },{
                header: lang.xeon.hits,
                dataIndex: 'hit',
                dataType: 'int'
            },{
                header: lang.xeon.last_edit,
                dataIndex: 'edit_date',
                width: 200
            },{
                header: lang.xeon.status,
                dataIndex: 'active',
                width: 60,
                //renderer: Xeon.renderStatus,
                renderer: this.renderYesorNo,
                editor: new Ext.ux.form.ToggleBox()
            }]
        });
		
		ct.add(t);
        if (data.tag)
        {
            ct.add(new Ext.ux.grid.GridPanel({
                title: lang.xeon.tag_list,
                iconCls: 'i-tag-blue',
                itype: 'xeon.tag',
                rowId: 'tag_id',
                reloadAfterEdit: true,
                quickSearch: true,
                columns: [{
                    header: lang.xeon.tag,
                    dataIndex: 'tag_text',
                    width: 200,
                    editor: new Ext.form.TextField({
                        allowBlank: false
                    })
                },{
                    header: lang.xeon.usage,
                    dataIndex: 'tag_count',
                    dataType: 'int'
                }]
            }));
        }

        ct.add(new Ext.ux.InfoPanel({
            title: lang.xeon.info,
            iconCls: 'i-information',
            items: [{
                infoLabel: lang.xeon.last_edit,
                value: data.info.last
            },{
                infoLabel: lang.xeon.mod_cms.page_count,
                value: data.info.total
            },{
                infoLabel: lang.xeon.hits,
                value: data.info.hits
            }]
        }));

        if (data.com)
        {
            ct.add(new Ext.ux.CommentPanel({
                title: lang.xeon.comment_waiting,
                iconCls: 'i-comments',
                data: data.com,
                root: true
            }));
        }
    },
    makeImportForm: function(ct,item,data) {
        ct.add(new Ext.ux.FormPanel({
            items:[
            {
                xtype:'listbox',
                itype:'cms.country',
                name:'pge_lang',
                fieldLabel:lang.xeon.mod_cms.import_land,
            },{
                xtype:'listbox',
                fieldLabel:lang.xeon.mod_cms.page,
                itype:'cms.byct',
                link: 'pge_lang',
                name:'pge_id'
            }
            ]
        }));
    },

    renderYesorNo: function(v, m, r)
    {
        m.css = (v == 1) ? 'x-button i-accept' : 'x-button i-delete';
        m.css = (v == -1) ? 'x-button i-loading' : m.css;
    },
    makePageRoot: function(ct, item, data)
    {
        ct.add(new Ext.ux.grid.GridPanel({
            title: lang.xeon.mod_cms.page_list,
			 mobile:data.mobile,
            iconCls: 'i-page-white-stack',
            itype: 'cms.pge',
            rowId: 'id',
            paging:  true,
            quickSearch: true,
            search:engine.getSearchCmsItems(data),
            columns: [{
                header: lang.xeon.id,
                dataIndex: 'id',
                dataType: 'int',
                width: 60
            },{
                header: lang.xeon.title,
                dataIndex: 'title',
                width: 200,
                itemLink: ['cms.pge', 'id']
            },{
                header: lang.xeon.hits,
                dataIndex: 'hit',
                dataType: 'int'
            },{
                header: lang.xeon.last_edit,
                dataIndex: 'edit_date',
                width: 200
            },{
                header: lang.xeon.status,
                dataIndex: 'active',
                width: 60,
                //renderer: Xeon.renderStatus,
                renderer: this.renderYesorNo,
                editor: new Ext.ux.form.ToggleBox()
            }]
        }));

        if (data.tag)
        {
            ct.add(new Ext.ux.grid.GridPanel({
                title: lang.xeon.tag_list,
                iconCls: 'i-tag-blue',
                itype: 'xeon.tag',
                rowId: 'tag_id',
                reloadAfterEdit: true,
                quickSearch: true,
                columns: [{
                    header: lang.xeon.tag,
                    dataIndex: 'tag_text',
                    width: 200,
                    editor: new Ext.form.TextField({
                        allowBlank: false
                    })
                },{
                    header: lang.xeon.usage,
                    dataIndex: 'tag_count',
                    dataType: 'int'
                }]
            }));
        }

        ct.add(new Ext.ux.InfoPanel({
            title: lang.xeon.info,
            iconCls: 'i-information',
            items: [{
                infoLabel: lang.xeon.last_edit,
                value: data.info.last
            },{
                infoLabel: lang.xeon.mod_cms.page_count,
                value: data.info.total
            },{
                infoLabel: lang.xeon.hits,
                value: data.info.hits
            }]
        }));

        ct.add(new Ext.ux.FormPanel({
            title: lang.xeon.mod_cms.direct_access,
            iconCls: 'i-page-white-go',
            items: [{
                fieldLabel: lang.xeon.id,
                allowBlank: false,
                name: 'id'
            }],

            submit: function()
            {
                var p = this;
                if (!p.form.isValid()) return;

                Xeon.loadItem('cms.pge.'+p.items.get(0).getValue().trim());
            }
        }));

        if (data.com)
        {
            ct.add(new Ext.ux.CommentPanel({
                title: lang.xeon.comment_waiting,
                iconCls: 'i-comments',
                data: data.com,
                root: true
            }));
        }
    },
    
    makeMobileForm: function(ct, item, data)
    {
		if (item.add)
        {
			 var items = [];
			 items.push({
                xtype: 'listbox',
                fieldLabel: lang.xeon.parent,
                allowBlank: false,
                itype: 'cms.mobile',
                name: 'pge_parent',
                value: data.db.pge_parent,
                displayValue: data.db.pge_parent_title
            });
			items.push({
				fieldLabel: lang.xeon.title,
				allowBlank: false,
				name: 'pge_title',
				value: data.db.pge_title
			});

			ct.add(new Ext.ux.FormPanel({
                items: items
            }));
        
		}else{
			this.makePageForm(ct, item, data);
		}
    },

    makeLinksForm: function(ct, item, data)
    {
        if(item.id == 1)
        {
            var items = [];

            for(var i=0;i<data.routes.length;i++)
            {
                items.push({
                    xtype: 'listbox',
                    fieldLabel: data.routes[i].path,               
                    itype: 'cms.links',
                    name: data.routes[i].name,
                    value: data.db[data.routes[i].name],
                    displayValue: data.db[data.routes[i].name + '_title']
                });
            }

            ct.add(new Ext.ux.FormPanel({
                title: lang.xeon.mod_cms.links,
                iconCls: ct.iconCls,
                items: items
            }));
        }
        else if (item.id == 2)
        {
            var items = [];

            for(var i=0;i<data.elmts.length;i++)
            {
                items.push({
                    xtype: 'listbox',
                    fieldLabel: data.elmts[i].title,               
                    itype: 'cms.links',
                    name: data.elmts[i].id,
                    value: data.db[data.elmts[i].name],
                    displayValue: data.db[data.elmts[i].name + '_title']
                });
            }

            ct.add(new Ext.ux.FormPanel({
                title: lang.xeon.mod_cms.links,
                iconCls: ct.iconCls,
                items: items
            }));
        }
    },
    
    makePageForm: function(ct, item, data)
    {
        var items = [];

        if (item.add)
        {
            items.push({
                xtype: 'listbox',
                fieldLabel: lang.xeon.parent,               
                itype: 'cms.pge',
                name: 'parent',
                value: data.db.parent,
                displayValue: data.db.parent_title
            });
        }

        items.push({
            fieldLabel: lang.xeon.title,
            allowBlank: false,
            name: 'title',
            value: data.db.title
        });

        if (item.add)
        {
            ct.add(new Ext.ux.FormPanel({
                items: items
            }));
        }
        else
        {

            items.push({
                fieldLabel: lang.xeon.full_title,
                allowBlank: true,
                name: 'full_title',
                value: data.db.full_title
            });
        
			items.push({
                xtype: 'listbox',
                fieldLabel: lang.xeon.parent,               
                itype: 'cms.pge',
                name: 'parent',
				link:  data.db.parent,
                value: data.db.parent,
                displayValue: data.db.parent_title
            });
			
			if(data.layer){
				items.push({
                     xtype: 'listbox',
					fieldLabel: lang.xeon.mod_cms.layer,               
					itype: 'cms.layer',
					name: 'layer',
					value: data.db.layer_id,
					displayValue: data.db.layer_tpl
                });
			}
            if (data.tag)
            {
                items.push({
                    xtype: 'tagbox',
                    fieldLabel: lang.xeon.tags,
                    data: data.tag,
                    name: 'tags',
                    value: data.db.tags
                });
            }

            if (data.ath)
            {
                items.push({
                    xtype: 'listbox',
                    fieldLabel: lang.xeon.mod_cms.author,
                    itype: 'user.ath',
                    name: 'pge_author',
                    value: data.db.pge_author,
                    displayValue: data.db.pge_author_name
                });
            }

            items.push({
                xtype: 'mediabox',
                fieldLabel: lang.xeon.image,
                name: 'file',
                value: data.db.file,
                displayValue: data.db.file_title
            });

            items.push({
                xtype: 'textarea',
                fieldLabel: lang.xeon.catcher,
                name: 'catcher',
                value: data.db.catcher
            },{
                xtype: 'togglebox',
                fieldLabel: lang.xeon.status,
                allowBlank: false,
                name: 'active',
                value: data.db.active
            });
	
            ct.add(new Ext.ux.FormPanel({
                title: lang.xeon.mod_cms.page,
                iconCls: (data.db.pge_mobile == 1) ? 'i-phone' : ct.iconCls,
                items: items
            }));

            ct.add(new Ext.ux.InfoPanel({
                title: lang.xeon.info,
                iconCls: 'i-information',
                items: [{
                    infoLabel: lang.xeon.id,
                    value: data.db.id
                },{
                    infoLabel: lang.xeon.url,
                    value: data.info.url,
                    renderer: Xeon.renderLink
                },{
                    infoLabel: lang.xeon.make_date,
                    value: data.db.created_title
                },{
                    infoLabel: lang.xeon.last_edit,
                    value: data.db.last_modified_title
                }]
            }));
			
            var items = [];

            // bloc ref
            if (data.ref) {
                         
                items.push({
                    fieldLabel: lang.xeon.url,
                    name: 'url',
                    value: data.db.url,
                    disabled : (data.ismaster) ? false:(!data.url)
                });
                         
                items.push({
                    fieldLabel: lang.xeon.title_url,
                    name: 'title_url',
                    value: data.db.title_url,
                    disabled : (data.ismaster) ? false:(!data.title_url)
                });
			
                // partie ref acex tooltip (voir ux.js)
                title =  new Ext.form.TextField({
                    id:Math.random().toString(),
                    fieldLabel: lang.xeon.title,
                    name: 'meta_title',
                    value: data.db.meta_title,
                    enableKeyEvents:true,
                    maxLength:70
                });
                description = new Ext.form.TextArea({
                    id:Math.random().toString(),
                    fieldLabel: lang.xeon.description,
                    name: 'meta_description',
                    value: data.db.meta_description,
                    enableKeyEvents:true
                //maxLength:70
                });

                keywords = new Ext.form.TextArea({
                    id:Math.random().toString(),
                    xtype: 'textarea',
                    fieldLabel: lang.xeon.keywords,
                    name: 'meta_keywords',
                    value: data.db.meta_keywords,
                    enableKeyEvents:true
                });
                                
                tp = new Ext.ux.RefTip({
                    maxC:70,
                    cp:title
                });
                tp2 = new Ext.ux.RefTip({
                    maxC:170,
                    cp:description
                });
                tp3 = new Ext.ux.RefTip({
                    maxC:170,
                    cp:keywords
                });

                items.push(title,description,keywords);

                if (data.nofollow) {
                    items.push({
                        xtype:'togglebox',
                        fieldLabel:lang.xeon.mod_cms.noindex,
                        name:'no_index',
                        value:data.db.no_index
                        });
                    items.push({
                        xtype:'togglebox',
                        fieldLabel:lang.xeon.mod_cms.nofollow,
                        name:'no_follow',
                        value:data.db.no_follow
                        });
                    items.push({
                        xtype:'togglebox',
                        fieldLabel:lang.xeon.mod_cms.ref_menunofollow,
                        name:'menu_no_follow',
                        value:data.db.menu_no_follow
                        });
                }
                         
                items.push({
                    fieldLabel: lang.xeon.mod_cms.goto,
                    name: 'goto',
                    value: data.db.goto
                });
        
                items.push({
                    xtype: 'listbox',
                    fieldLabel: lang.xeon.mod_cms.stick,               
                    itype: 'cms.stick',
                    name: 'stick',
                    link:  data.db.stick,
                    value: data.db.stick,
                    displayValue: data.db.stick_title
                });
                        
                ct.add(new Ext.ux.FormPanel({
                    title: lang.xeon.seo,
                    iconCls: 'i-magnifier',
                    collapsed: false,
                    items: items
                }));
            }
            // end bloc ref
             
            if (data.lpd)
            {
                ct.add(new Ext.ux.ListPanel({
                    title: lang.xeon.mod_cms.product_link,
                    iconCls: 'i-database',
                    name: 'pge_link_product',
                    itype: 'shop.prd',
                    data: data.lpd
                }));
            }
            // ct.add(new Ext.ux.LayoutPanel({
            //     title: lang.xeon.layout,
            //     iconCls: 'i-layout',
            //     itype: 'cms.cnt',
            //     titleName: 'full_title',
            //     templates: data.tpl,
            //     data: data.cnt,
            //     previewURL: data.info.url,
            //     mobile : data.mobile
            // }));    

            ct.add(new Ext.ux.WidgetPanel({
                title: lang.xeon.layout,
                itype: 'cms.cnt',
                titleName: 'full_title',
                templates: data.tpl,
                data: data.cnt,
                container_widget: data.db.container,
                previewURL: data.info.url,
                mobile : data.mobile
            }));    
            
            if (data.com)
            {
                ct.add(new Ext.ux.CommentPanel({
                    title: lang.xeon.comments,
                    iconCls: 'i-comments',
                    data: data.com
                }));
            }
        }
    },
	
    makePreviewForm: function(ct, item, data)
    {
        ct.add({
            autoWidth: true,
            frame: false,
            border: false,
            html: '<iframe src="/form.php" style="width: 100%; height: 100%;"></iframe>'
        });
    },

    makePageCopy: function(ct, item, data)
    {
        ct.add(new Ext.ux.FormPanel({
            command: 'xeon.copyItem',
            items: [{
                xtype: 'listbox',
                fieldLabel: lang.xeon.target,
                allowBlank: false,
                itype: 'cms.pge',
                name: 'pge_parent',
                value: data.db.pge_parent,
                displayValue: data.db.pge_parent_title
            },{
                fieldLabel: lang.xeon.title,
                allowBlank: false,
                name: 'pge_title',
                value: data.db.pge_title
            }]
        }));
    },
    makeModuleForm: function(ct, item, data)
    {
        ct.add(new Ext.ux.FormPanel({
            title: lang.xeon.settings,
            iconCls: 'i-cog',
            items: [/*{
                fieldLabel: lang.xeon.mod_cms.page_url,
                name: 'page_url',
                value: data.cfg.page_url
            },*/{
                xtype: 'togglebox',
                fieldLabel: lang.xeon.url_full_title,
                name: 'url_full_title',
                value: data.cfg.url_full_title
            },{
                fieldLabel: lang.xeon.template_list,
                name: 'layout_template',
                value: data.cfg.layout_template
            },{
                xtype: 'togglebox',
                fieldLabel: lang.xeon.comment_check,
                name: 'comment_check',
                value: data.cfg.comment_check
            },{
                fieldLabel: lang.xeon.rating_max,
                name: 'rating_max',
                value: data.cfg.rating_max
            },{
				 xtype: 'togglebox',
                fieldLabel: lang.xeon.mod_cms.layer_mode,
                name: 'plugin_layer_parents',
                value: data.cfg.plugin_layer_parents
            }]
        }));

        ct.add(new Ext.ux.FormPanel({
            title: lang.xeon.options,
            iconCls: 'i-brick',
            items: [{
                xtype: 'togglebox',
                fieldLabel: lang.xeon.mod_cms.page_tags,
                name: 'plugin_page_tag',
                value: data.cfg.plugin_page_tag
            },{
                xtype: 'togglebox',
                fieldLabel: lang.xeon.mod_cms.author,
                name: 'plugin_author',
                value: data.cfg.plugin_author
            },{
                xtype: 'togglebox',
                fieldLabel: lang.xeon.comments,
                name: 'plugin_comment',
                value: data.cfg.plugin_comment
            },{
                xtype: 'togglebox',
                fieldLabel: lang.xeon.mod_cms.product_link,
                name: 'plugin_productlink',
                value: data.cfg.plugin_productlink
            },{
                xtype: 'togglebox',
                fieldLabel: lang.xeon.mod_cms.rating,
                name: 'plugin_rating',
                value: data.cfg.plugin_rating
            },{
                xtype: 'togglebox',
                fieldLabel: lang.xeon.mod_cms.page_mobile,
                name: 'plugin_mobile',
                value: data.cfg.plugin_mobile
            },{
                xtype: 'togglebox',
                fieldLabel: lang.xeon.mod_cms.page_import,
                name: 'plugin_page_import',
                value: data.cfg.plugin_page_import
            }]
        }));
    }
}