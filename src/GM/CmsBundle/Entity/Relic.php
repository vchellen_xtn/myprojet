<?php

namespace GM\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Relic
 *
 * @ORM\Table(name="relic")
 * @ORM\Entity(repositoryClass="GM\CmsBundle\Entity\RelicRepository")
 */
class Relic
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="relic", type="integer")
     */
    private $relic;

    /**
     * @var integer
     *
     * @ORM\Column(name="follow", type="integer", nullable=true)
     */
    private $follow;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set relic
     *
     * @param integer $relic
     * @return Relic
     */
    public function setRelic($relic)
    {
        $this->relic = $relic;
    
        return $this;
    }

    /**
     * Get relic
     *
     * @return integer 
     */
    public function getRelic()
    {
        return $this->relic;
    }

    /**
     * Set follow
     *
     * @param integer $follow
     * @return Relic
     */
    public function setFollow($follow)
    {
        $this->follow = $follow;
    
        return $this;
    }

    /**
     * Get follow
     *
     * @return integer 
     */
    public function getFollow()
    {
        return $this->follow;
    }
}