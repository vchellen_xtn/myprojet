<?php

namespace GM\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Layer
 *
 * @ORM\Table(name="layer")
 * @ORM\Entity(repositoryClass="GM\CmsBundle\Entity\LayerRepository")
 */
class Layer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tpl", type="string", length=100)
     */
    private $tpl;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tpl
     *
     * @param string $tpl
     * @return Layer
     */
    public function setTpl($tpl)
    {
        $this->tpl = $tpl;
    
        return $this;
    }

    /**
     * Get tpl
     *
     * @return string 
     */
    public function getTpl()
    {
        return $this->tpl;
    }
}