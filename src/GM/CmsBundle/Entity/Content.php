<?php

namespace GM\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Content
 *
 * @ORM\Table(name="content")
 * @ORM\Entity(repositoryClass="GM\CmsBundle\Entity\ContentRepository")
 */
class Content
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="GM\CmsBundle\Entity\Page", inversedBy="contents")
     */
    private $page;

    /**
     * @var integer
     *
     * @ORM\Column(name="mobile", type="smallint")
     */
    private $mobile;

    /**
     * @var integer
     *
     * @ORM\Column(name="template", type="smallint")
     */
    private $template;

    /**
     * @var integer
     *
     * @ORM\Column(name="rank", type="integer")
     */
    private $rank;

    /**
     * @var string
     *
     * @ORM\Column(name="data1", type="text")
     */
    private $data1;

    /**
     * @var string
     *
     * @ORM\Column(name="data2", type="text", nullable=true)
     */
    private $data2;

    /**
     * @var string
     *
     * @ORM\Column(name="data3", type="text", nullable=true)
     */
    private $data3;

    /**
     * @var string
     *
     * @ORM\Column(name="data4", type="text", nullable=true)
     */
    private $data4;

    /**
     * @var string
     *
     * @ORM\Column(name="data5", type="text", nullable=true)
     */
    private $data5;

    /**
     * @var string
     *
     * @ORM\Column(name="data6", type="text", nullable=true)
     */
    private $data6;

    public function __construct()
    {
        $this->mobile = 1;
        $this->template = 1;
        $this->rank = 1;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mobile
     *
     * @param integer $mobile
     * @return Content
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return integer
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set template
     *
     * @param integer $template
     * @return Content
     */
    public function setTemplate($template)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Get template
     *
     * @return integer
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Set rank
     *
     * @param integer $rank
     * @return Content
     */
    public function setRank($rank)
    {
        $this->rank = $rank;

        return $this;
    }

    /**
     * Get rank
     *
     * @return integer
     */
    public function getRank()
    {
        return $this->rank;
    }

    /**
     * Set data1
     *
     * @param string $data1
     * @return Content
     */
    public function setData1($data1)
    {
        $this->data1 = $data1;

        return $this;
    }

    /**
     * Get data1
     *
     * @return string
     */
    public function getData1()
    {
        return $this->data1;
    }

    /**
     * Set data2
     *
     * @param string $data2
     * @return Content
     */
    public function setData2($data2)
    {
        $this->data2 = $data2;

        return $this;
    }

    /**
     * Get data2
     *
     * @return string
     */
    public function getData2()
    {
        return $this->data2;
    }

    /**
     * Set data3
     *
     * @param string $data3
     * @return Content
     */
    public function setData3($data3)
    {
        $this->data3 = $data3;

        return $this;
    }

    /**
     * Get data3
     *
     * @return string
     */
    public function getData3()
    {
        return $this->data3;
    }

    /**
     * Set data4
     *
     * @param string $data4
     * @return Content
     */
    public function setData4($data4)
    {
        $this->data4 = $data4;

        return $this;
    }

    /**
     * Get data4
     *
     * @return string
     */
    public function getData4()
    {
        return $this->data4;
    }

    /**
     * Set data5
     *
     * @param string $data5
     * @return Content
     */
    public function setData5($data5)
    {
        $this->data5 = $data5;

        return $this;
    }

    /**
     * Get data5
     *
     * @return string
     */
    public function getData5()
    {
        return $this->data5;
    }

    /**
     * Set data6
     *
     * @param string $data6
     * @return Content
     */
    public function setData6($data6)
    {
        $this->data6 = $data6;

        return $this;
    }

    /**
     * Get data6
     *
     * @return string
     */
    public function getData6()
    {
        return $this->data6;
    }

    /**
     * Set page
     *
     * @param \GM\CmsBundle\Entity\Page $page
     * @return Content
     */
    public function setPage(\GM\CmsBundle\Entity\Page $page = null)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Get page
     *
     * @return \GM\CmsBundle\Entity\Page
     */
    public function getPage()
    {
        return $this->page;
    }
}