<?php

// namespace GM\CmsBundle\Entity;

// use Doctrine\ORM\Mapping as ORM;

// /**
//  * Tag
//  *
//  * @ORM\Table(name="tag")
//  * @ORM\Entity(repositoryClass="GM\CmsBundle\Entity\TagRepository")
//  */
// class Tag
// {
//     /**
//      * @var integer
//      *
//      * @ORM\Column(name="id", type="integer")
//      * @ORM\Id
//      * @ORM\GeneratedValue(strategy="AUTO")
//      */
//     private $id;

//     /**
//      * @var string
//      *
//      * @ORM\Column(name="tag", type="string", length=255)
//      */
//     private $tag;

//     /**
//      * @ORM\ManyToMany(targetEntity="GM\CmsBundle\Entity\Page", inversedBy="tags")
//      * @ORM\JoinColumn(name="tag_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
//      */
//     private $page;
//     /**
//      * Constructor
//      */
//     public function __construct()
//     {
//         $this->page = new \Doctrine\Common\Collections\ArrayCollection();
//     }
    
//     /**
//      * Get id
//      *
//      * @return integer 
//      */
//     public function getId()
//     {
//         return $this->id;
//     }

//     /**
//      * Set tag
//      *
//      * @param string $tag
//      * @return Tag
//      */
//     public function setTag($tag)
//     {
//         $this->tag = $tag;
    
//         return $this;
//     }

//     /**
//      * Get tag
//      *
//      * @return string 
//      */
//     public function getTag()
//     {
//         return $this->tag;
//     }

//     /**
//      * Add page
//      *
//      * @param \GM\CmsBundle\Entity\Page $page
//      * @return Tag
//      */
//     public function addPage(\GM\CmsBundle\Entity\Page $page)
//     {
//         $this->page[] = $page;
    
//         return $this;
//     }

//     /**
//      * Remove page
//      *
//      * @param \GM\CmsBundle\Entity\Page $page
//      */
//     public function removePage(\GM\CmsBundle\Entity\Page $page)
//     {
//         $this->page->removeElement($page);
//     }

//     /**
//      * Get page
//      *
//      * @return \Doctrine\Common\Collections\Collection 
//      */
//     public function getPage()
//     {
//         return $this->page;
//     }
// }