<?php

namespace GM\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;

/**
 * Page
 *
 * @ORM\Table(name="page")
 * @ORM\Entity(repositoryClass="GM\CmsBundle\Entity\PageRepository")
 */
class Page
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="GM\CmsBundle\Entity\Page", inversedBy="children", cascade={"persist"})
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="GM\CmsBundle\Entity\Page", mappedBy="parent", cascade={"remove"})
     * @ORM\JoinColumn(name="children_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $children;

    /**
     * @ORM\ManyToOne(targetEntity="GM\CmsBundle\Entity\Layer")
     * @ORM\JoinColumn(nullable=true)
     */
    private $layer;

    /**
     * @ORM\ManyToMany(targetEntity="GM\CmsBundle\Entity\Page", mappedBy="linkedPages")
     */
    private $linkedWithPage;

    /**
     * @var integer
     *
     * @ORM\ManyToMany(targetEntity="GM\CmsBundle\Entity\Page", inversedBy="linkedWithPage")
     * @ORM\JoinTable(name="linked",
     *      joinColumns={@ORM\JoinColumn(name="page_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="linked_id", referencedColumnName="id", onDelete="CASCADE")})
     */
    private $linkedPages;

    /**
     * @var string
     *
     * @ORM\Column(name="link_product", type="text", nullable=true)
     */
    private $linkProduct;

    /**
     * @ORM\ManyToOne(targetEntity="GM\CmsBundle\Entity\Page", inversedBy="stuck")
     * @ORM\JoinColumn(name="stick_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $stick;

    /**
     * @ORM\OneToMany(targetEntity="GM\CmsBundle\Entity\Page", mappedBy="stick")
     */
    private $stuck;

    /**
     * @ORM\ManyToMany(targetEntity="GM\ReviewBundle\Entity\Comment")
     * @ORM\JoinTable(name="page_comment",
     *      joinColumns={@ORM\JoinColumn(name="page_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="comment_id", referencedColumnName="id", unique=true)}
     *      )
     **/
    private $comments;

    /**
     * @ORM\ManyToMany(targetEntity="GM\SiteBundle\Entity\Card", cascade={"persist","remove"}, fetch="EAGER")
     * @ORM\JoinTable(name="page_card",
     *      joinColumns={@ORM\JoinColumn(name="page_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="card_id", referencedColumnName="id", unique=true)}
     *      )
     */
    private $card;

    /**
     * @var string
     *
     * @ORM\Column(name="route", type="string", length=255, nullable=true)
     */
    private $route;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
        $this->linkedWithPage = new \Doctrine\Common\Collections\ArrayCollection();
        $this->linkedPages = new \Doctrine\Common\Collections\ArrayCollection();
        $this->stuck = new \Doctrine\Common\Collections\ArrayCollection();
        $this->comments = new \Doctrine\Common\Collections\ArrayCollection();
        $this->card = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set linkProduct
     *
     * @param string $linkProduct
     * @return Page
     */
    public function setLinkProduct($linkProduct)
    {
        $this->linkProduct = $linkProduct;
    
        return $this;
    }

    /**
     * Get linkProduct
     *
     * @return string 
     */
    public function getLinkProduct()
    {
        return $this->linkProduct;
    }

    /**
     * Set parent
     *
     * @param \GM\CmsBundle\Entity\Page $parent
     * @return Page
     */
    public function setParent(\GM\CmsBundle\Entity\Page $parent = null)
    {
        $this->parent = $parent;
    
        return $this;
    }

    /**
     * Get parent
     *
     * @return \GM\CmsBundle\Entity\Page 
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add children
     *
     * @param \GM\CmsBundle\Entity\Page $children
     * @return Page
     */
    public function addChildren(\GM\CmsBundle\Entity\Page $children)
    {
        $this->children[] = $children;
    
        return $this;
    }

    /**
     * Remove children
     *
     * @param \GM\CmsBundle\Entity\Page $children
     */
    public function removeChildren(\GM\CmsBundle\Entity\Page $children)
    {
        $this->children->removeElement($children);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set layer
     *
     * @param \GM\CmsBundle\Entity\Layer $layer
     * @return Page
     */
    public function setLayer(\GM\CmsBundle\Entity\Layer $layer = null)
    {
        $this->layer = $layer;
    
        return $this;
    }

    /**
     * Get layer
     *
     * @return \GM\CmsBundle\Entity\Layer 
     */
    public function getLayer()
    {
        return $this->layer;
    }

    /**
     * Add linkedWithPage
     *
     * @param \GM\CmsBundle\Entity\Page $linkedWithPage
     * @return Page
     */
    public function addLinkedWithPage(\GM\CmsBundle\Entity\Page $linkedWithPage)
    {
        $this->linkedWithPage[] = $linkedWithPage;
    
        return $this;
    }

    /**
     * Remove linkedWithPage
     *
     * @param \GM\CmsBundle\Entity\Page $linkedWithPage
     */
    public function removeLinkedWithPage(\GM\CmsBundle\Entity\Page $linkedWithPage)
    {
        $this->linkedWithPage->removeElement($linkedWithPage);
    }

    /**
     * Get linkedWithPage
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLinkedWithPage()
    {
        return $this->linkedWithPage;
    }

    /**
     * Add linkedPages
     *
     * @param \GM\CmsBundle\Entity\Page $linkedPages
     * @return Page
     */
    public function addLinkedPage(\GM\CmsBundle\Entity\Page $linkedPages)
    {
        $this->linkedPages[] = $linkedPages;
    
        return $this;
    }

    /**
     * Remove linkedPages
     *
     * @param \GM\CmsBundle\Entity\Page $linkedPages
     */
    public function removeLinkedPage(\GM\CmsBundle\Entity\Page $linkedPages)
    {
        $this->linkedPages->removeElement($linkedPages);
    }

    /**
     * Get linkedPages
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLinkedPages()
    {
        return $this->linkedPages;
    }

    /**
     * Set stick
     *
     * @param \GM\CmsBundle\Entity\Page $stick
     * @return Page
     */
    public function setStick(\GM\CmsBundle\Entity\Page $stick = null)
    {
        $this->stick = $stick;
    
        return $this;
    }

    /**
     * Get stick
     *
     * @return \GM\CmsBundle\Entity\Page 
     */
    public function getStick()
    {
        return $this->stick;
    }

    /**
     * Add stuck
     *
     * @param \GM\CmsBundle\Entity\Page $stuck
     * @return Page
     */
    public function addStuck(\GM\CmsBundle\Entity\Page $stuck)
    {
        $this->stuck[] = $stuck;
    
        return $this;
    }

    /**
     * Remove stuck
     *
     * @param \GM\CmsBundle\Entity\Page $stuck
     */
    public function removeStuck(\GM\CmsBundle\Entity\Page $stuck)
    {
        $this->stuck->removeElement($stuck);
    }

    /**
     * Get stuck
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getStuck()
    {
        return $this->stuck;
    }

    /**
     * Add comments
     *
     * @param \GM\ReviewBundle\Entity\Comment $comments
     * @return Page
     */
    public function addComment(\GM\ReviewBundle\Entity\Comment $comments)
    {
        $this->comments[] = $comments;
    
        return $this;
    }

    /**
     * Remove comments
     *
     * @param \GM\ReviewBundle\Entity\Comment $comments
     */
    public function removeComment(\GM\ReviewBundle\Entity\Comment $comments)
    {
        $this->comments->removeElement($comments);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set card
     *
     * @param \GM\SiteBundle\Entity\Card $card
     * @return Page
     */
    public function setCard(\GM\SiteBundle\Entity\Card $card = null)
    {
        $this->card = $card;
    
        return $this;
    }

    /**
     * Get card
     *
     * @return \GM\SiteBundle\Entity\Card 
     */
    public function getCard()
    {
        return $this->card;
    }

    /**
     * Add card
     *
     * @param \GM\SiteBundle\Entity\Card $card
     * @return Page
     */
    public function addCard(\GM\SiteBundle\Entity\Card $card)
    {
        $this->card[] = $card;
    
        return $this;
    }

    /**
     * Remove card
     *
     * @param \GM\SiteBundle\Entity\Card $card
     */
    public function removeCard(\GM\SiteBundle\Entity\Card $card)
    {
        $this->card->removeElement($card);
    }

    public function getCardByDomain($domain)
    {
        return $this->getCard()
                    ->filter(
                        function($entry) use ($domain)
                        {
                            if($entry->getDomain() == $domain)
                                return true;

                            return false;   
                        }
                    )->first();
    }

    /**
     * Set route
     *
     * @param string $route
     * @return Page
     */
    public function setRoute($route)
    {
        $this->route = $route;
    
        return $this;
    }

    /**
     * Get route
     *
     * @return string 
     */
    public function getRoute()
    {
        return $this->route;
    }
}