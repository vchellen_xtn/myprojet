<?php

namespace GM\GeoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Zone
 *
 * @ORM\Table(name="zone")
 * @ORM\Entity
 */
class Zone
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @ORM\OneToMany(targetEntity="Country", mappedBy="zone")
     */
    private $countries;

    /**
     * @ORM\ManyToMany(targetEntity="Fast\WalletBundle\Entity\Currency")
     */
    private $currencies;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return CountryZone
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set countries
     *
     * @param integer $countries
     * @return CountryZone
     */
    public function setCountries($countries)
    {
        $this->countries = $countries;
    
        return $this;
    }

    /**
     * Get countries
     *
     * @return integer 
     */
    public function getCountries()
    {
        return $this->countries;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->countries = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add countries
     *
     * @param \GM\SiteBundle\Entity\Country $countries
     * @return CountryZone
     */
    public function addCountrie(\GM\SiteBundle\Entity\Country $countries)
    {
        $this->countries[] = $countries;
    
        return $this;
    }

    /**
     * Remove countries
     *
     * @param \GM\SiteBundle\Entity\Country $countries
     */
    public function removeCountrie(\GM\SiteBundle\Entity\Country $countries)
    {
        $this->countries->removeElement($countries);
    }

    /**
     * Add currencies
     *
     * @param \Fast\WalletBundle\Entity\Currency $currencies
     * @return Zone
     */
    public function addCurrencie(\Fast\WalletBundle\Entity\Currency $currencies)
    {
        $this->currencies[] = $currencies;
    
        return $this;
    }

    /**
     * Remove currencies
     *
     * @param \Fast\WalletBundle\Entity\Currency $currencies
     */
    public function removeCurrencie(\Fast\WalletBundle\Entity\Currency $currencies)
    {
        $this->currencies->removeElement($currencies);
    }

    /**
     * Get currencies
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCurrencies()
    {
        return $this->currencies;
    }
}