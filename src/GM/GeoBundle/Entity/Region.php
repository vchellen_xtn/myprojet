<?php

namespace GM\GeoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Country
 *
 * @ORM\Table(name="region")
 * @ORM\Entity
 */
class Region
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="alpha2", type="string", length=2)
     */
    private $alpha2;

    /**
     * @var string
     *
     * @ORM\Column(name="alpha3", type="string", length=3)
     */
    private $alpha3;

    /*
     * @ORM\ManyToMany(targetEntity="Fast\VatBundle\Entity\Rate", cascade={"persist","remove"})
     * @ORM\JoinTable(name="region_vat_rate",
     *      joinColumns={@ORM\JoinColumn(name="rate_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="region_id", referencedColumnName="id", unique=true)}
     *      )
    private $vatRates;
     */

    /**
     * @ORM\ManyToOne(targetEntity="Country", inversedBy="regions")
     */
    private $country;
}