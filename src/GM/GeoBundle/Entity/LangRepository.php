<?php

namespace GM\GeoBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * LangRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class LangRepository extends EntityRepository
{
	public function findByDomain($domain)
	{
		$qb = $this->createQueryBuilder('l')
					->join('l.domains', 'd')
						->addSelect('d')
					->andWhere('d = ?1')
						->setParameter(1, $domain);

		return $qb->getQuery()
				  ->getResult();
	}
}
