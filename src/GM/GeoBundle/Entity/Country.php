<?php

namespace GM\GeoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Country
 *
 * @ORM\Table(name="country")
 * @ORM\Entity
 */
class Country
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="alpha2", type="string", length=2)
     */
    private $alpha2;

    /**
     * @var string
     *
     * @ORM\Column(name="alpha3", type="string", length=3)
     */
    private $alpha3;

    /*
     * @ORM\ManyToMany(targetEntity="Fast\WalletBundle\Entity\Currency")
    private $currencies;
     */

    /**
     * @ORM\ManyToOne(targetEntity="Zone", inversedBy="countries")
     */
    private $zone;

    /*
     * @ORM\ManyToMany(targetEntity="Fast\VatBundle\Entity\Rate", cascade={"persist","remove"})
     * @ORM\JoinTable(name="country_vat_rate",
     *      joinColumns={@ORM\JoinColumn(name="rate_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="country_id", referencedColumnName="id", unique=true)}
     *      )
    private $vatRates;
     */

    /**
     * @ORM\OneToMany(targetEntity="Region", mappedBy="country")
     */
    private $regions;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->currencies = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set alpha2
     *
     * @param string $alpha2
     * @return Country
     */
    public function setAlpha2($alpha2)
    {
        $this->alpha2 = $alpha2;
    
        return $this;
    }

    /**
     * Get alpha2
     *
     * @return string 
     */
    public function getAlpha2()
    {
        return $this->alpha2;
    }

    /**
     * Set alpha3
     *
     * @param string $alpha3
     * @return Country
     */
    public function setAlpha3($alpha3)
    {
        $this->alpha3 = $alpha3;
    
        return $this;
    }

    /**
     * Get alpha3
     *
     * @return string 
     */
    public function getAlpha3()
    {
        return $this->alpha3;
    }

    /**
     * Add currencies
     *
     * @param \Fast\WalletBundle\Entity\Currency $currencies
     * @return Country

    public function addCurrencie(\Fast\WalletBundle\Entity\Currency $currencies)
    {
        $this->currencies[] = $currencies;
    
        return $this;
    }
     */
    /**
     * Remove currencies
     *
     * @param \Fast\WalletBundle\Entity\Currency $currencies

    public function removeCurrencie(\Fast\WalletBundle\Entity\Currency $currencies)
    {
        $this->currencies->removeElement($currencies);
    }
*/

    /**
     * Get currencies
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCurrencies()
    {
        return $this->currencies;
    }

    /**
     * Set zone
     *
     * @param \GM\GeoBundle\Entity\Zone $zone
     * @return Country
     */
    public function setZone(\GM\GeoBundle\Entity\Zone $zone = null)
    {
        $this->zone = $zone;
    
        return $this;
    }

    /**
     * Get zone
     *
     * @return \GM\GeoBundle\Entity\Zone 
     */
    public function getZone()
    {
        return $this->zone;
    }
}