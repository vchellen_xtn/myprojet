
//
// Extensions de l'interface Xeon
// Auteur : Sylvain Durozard
//

Ext.ns('Ext.ux.form');
Ext.ns('Ext.ux.grid');
Ext.ns('Ext.ux.xeon');

Array.prototype.pushArray = function(arr) {
    this.push.apply(this, arr);
};

Ext.ux.Window = Ext.extend(Ext.Window, {
    layout: 'fit',
    resizable: false,
    buttonAlign: 'center',
    border: false,
    modal: true,
    width: 560,
    defaults: {
        frame: true
    //border: true,
    //autoHeight: true
    }
});

Ext.ux.xeon.LayoutWindow = Ext.extend(Ext.Window, {
    cls: 'x-layoutwindow',
    layout: 'fit',
    buttonAlign: 'center',
    border: false,
    modal: true,
    width: 700,
    buttons: [{
        text: lang.xeon.edit,
        handler: function()
        {
            var w = this.ownerCt.ownerCt;
            var p = w.getComponent(0);
            
            if (!p.form.isValid()) return;
            if (p.bwrap.isMasked()) return;
            
            tinyMCE.triggerSave();
            p.form.applyDataArray();

            Ext.Ajax.request({
                form: p.form.el.dom,
                params: p.params,
                before: function() {
                    p.bwrap.mask(lang.xeon.saving, 'x-mask-loading');
                },
                success: p.success,
                callback: function(o, s, a)
                {
                    p.bwrap.unmask();
                    if (!a.error) w.close();
                }
            });
        }
    },{
        text: lang.xeon.cancel,
        handler: function() {
            this.ownerCt.ownerCt.close();
        }
    }]
});

Ext.ux.xeon.CenterPanel = Ext.extend(Ext.Panel, {
    autoScroll: true,
    closable: true,
    title: lang.xeon.loading,
    iconCls: 'i-loading',
    //tbar: ['buttons'],
    defaults: {
        frame: true,
        //autoWidth: true,
        width: 750,
        collapsible: true,
        titleCollapse: true,
        hideCollapseTool: true,
        animCollapse: false,
        style: {
            margin: '20px'
        }
    },
    
    makeForm: function()
    {
        var tab = this;
        var item = Xeon.getId(tab.itemId);
        
        Xeon.ui.center.setActiveTab(tab);
        tab.body.setVisible(false);
        tab.bwrap.mask(lang.xeon.loading, 'x-mask-loading');
    
        Ext.Ajax.request({
            params: {
                cmd: 'xeon.makeForm',
                item: tab.itemId
            },
            success: function(a)
            {
                tab.setTitle(a.json.title, a.json.icon);
    
                tab.add({
                    frame: false,
                    border: false,
                    html: '<h2>'+a.json.title+'</h2>'
                });
                
                Xeon.callback(item.module, a.json.callback, tab, item, a.json);
    
                tab.doLayout(true);
                tab.body.setVisible(true);
                tab.bwrap.unmask();
            },
            callback: function(o, s, a)
            {
                if (a.error) {
                    Xeon.ui.center.remove(tab);
                }
            }
        });
    },
    
    reloadItem: function()
    {
        this.removeAll();
        this.setTitle(lang.xeon.loading, 'i-loading');
        //this.reloadItem();
        this.makeForm();
    }
});

Ext.ux.xeon.TreePanel = Ext.extend(Ext.tree.TreePanel, {
    iconCls: '',
    autoScroll: true,
    collapsed: true,
    hideCollapseTool: true,
    border: false,
    nodeState : [],
    
    initComponent: function()
    {
        var p = this;
    
        var item = Xeon.getId(p.itemId);
        
       
        
        p.tbar.push('->',{
            tooltip: lang.xeon.tree_expand,
            iconCls: 'i-expand-all',
            handler: p.expandAll,
            scope: p
        },{
            tooltip: lang.xeon.tree_collapse,
            iconCls: 'i-collapse-all',
            handler: p.collapseAll,
            scope: p
        },{
            tooltip: lang.xeon.tree_reload,
            iconCls: 'i-refresh',
            handler: p.reloadData,
            scope: p
        });
        
        p.loader = new Ext.tree.TreeLoader({
            dataUrl: Ext.Ajax.url,
            baseParams : {
                cmd: 'xeon.makeTree',
                item: p.itemId
            },
            createNode: function(n)
            {
                if (n.item) n.id = n.item;
                return !n.children ? new Ext.tree.TreeNode(n) : new Ext.tree.AsyncTreeNode(n);
            }
        });
        if (p.sort)
        {
            p.ddAppendOnly = true;
            
            new Ext.tree.TreeSorter(p, {
                folderSort: true
            });
        }
        
        p.root = new Ext.tree.AsyncTreeNode(p.root);
        p.selModel = new Ext.tree.DefaultSelectionModel();
        
        p.selModel.on('selectionchange', function(s, n)
        {
            var allowCopy = true;
            var allowDelete = true;
        
            if (n !== null && n.attributes.item && !n.isRoot)
            {
                if (typeof(n.attributes._copy) == 'boolean') allowCopy = n.attributes._copy;
                if (typeof(n.attributes._delete) == 'boolean') allowDelete = n.attributes._delete;
            }
            else {
                allowCopy = allowDelete = false;
            }
        
            p.topToolbar.items.each(function(b)
            {
                if (b.action == 'copy') b.setDisabled(!allowCopy);
                if (b.action == 'delete') b.setDisabled(!allowDelete);
            });
        });
        
        p.on('render', function(p)
        {
            
            var $fl = false;
            p.topToolbar.items.each(function(b)
            {
                if(b.action){
                    switch(b.action){
                        case 'mfilter':
                            b.setDisabled(!$fl);
                            break;
                        case 'add':
                            b.setHandler(p.addItem, p);
                            break;
                        case 'copy':
                            b.setHandler(p.copyItem, p);
                            break;
                        case 'delete':
                            b.setHandler(p.deleteItem, p);
                            break;
                        case 'import':
                            b.setHandler(p.importItem, p);
                            break;
                        default:
                            // alert(b.action);
                            b.setHandler(p.customizeAction, p,b.action);
                            
                            break;
                    }
                }
                if (b.type == 'mobile') $fl = true;
            });
            
            
          
        });
        
        p.on('click', function(n, e)
        {
            /*var el = Ext.get(e.target);
            
            if (el.hasClass('x-tree-node-icon') && !n.isRoot)
            {
                Ext.Ajax.request({
                    params: {
                        cmd: 'xeon.toggleItem',
                        item: n.attributes.item
                    },
                    before: function() {
                        el.addClass('i-loading');
                    },
                    success: function(a)
                    {
                        if (a.json.title)
                        {
                            n.setText(a.json.title);
                        }
                        
                        if (a.json.icon)
                        {
                            el.replaceClass(n.attributes.iconCls, a.json.icon);
                            n.attributes.iconCls = a.json.icon;
                        }
                    },
                    callback: function() {
                        el.removeClass('i-loading');
                    }
                });
            }
            else
            {*/
                   
            if (n.attributes.item) {
                Xeon.loadItem(n.attributes.item);
            }
        /*}*/
        });

        p.on('expand', function(p)
        {
            // console.log(p);
            // console.log('setCookie tree');
            Xeon.setCookie('tree', p.itemId);
            // console.log('setCookie end');
            p.root.expand();
        });

        p.on('beforeappend', p.checkNodeState);
        p.on('expandnode', p.setNodeState);
        p.on('collapsenode', p.setNodeState);
        p.on('nodedragover', p.checkAllowDrop);
        p.on('movenode', p.moveItem);
        
        Ext.ux.xeon.TreePanel.superclass.initComponent.call(this);
    },
    customizeAction:function(b,t,v){
        var item = Xeon.getId(this.itemId);
        
        if(this.selModel.selNode){
        
            Xeon.callpersonalisedCommand(b.action,this.selModel.selNode.attributes.item,item.module);
        }else{           
            Xeon.callpersonalisedCommand(b.action,null,item.module);
        }
        
    },
    importItem: function(b)
    {
        var item = Xeon.getId(this.itemId);
        Xeon.importItem(item.module+'.'+'import'+'.0', null);
    },

    addItem: function(b)
    {
        var n = this.selModel.selNode;
        var item = Xeon.getId(this.itemId);
        
        Xeon.loadItem(item.module+'.'+b.type+'.0', n ? n.attributes.item : null);
    },
    
    copyItem: function()
    {
        Xeon.copyItem(this.selModel.selNode.attributes.item);
    },
    
    moveItem: function(t, n, oldParent, newParent, i)
    {
        Ext.Ajax.request({
            params: {
                cmd: 'xeon.moveItem',
                item: n.attributes.item,
                target: newParent.attributes.item,
                rank: i+1
            }
        });
    },

    deleteItem: function()
    {
        var p = this;
        var n = p.selModel.selNode;

        if (n !== null)
        {
            Ext.Msg.confirm(lang.xeon.confirm, String.format(lang.xeon.delete_this, n.text), function(btn)
            {
                if (btn == 'yes')
                {
                    if(n.attributes.item.substring(0,3) == "cms")
                    {
                        //Form panel pour les divers choix
                        var return_page = "parent";
                        var this_formpanel = new Ext.form.FormPanel({
                            cls: 'x-formpanel',
                            labelWidth: 180,
                            buttonAlign: 'center',
                            name:'newlink',
                            items: [
                            {
                                fieldLabel: lang.xeon.msg_delete_newlink1,
                                xtype : 'checkbox',
                                id : 'newlink1',
                                checked : true,
                                handler: function()
                                {
                                    if(Ext.getCmp("newlink1").checked)
                                    {
                                        document.getElementById("newlink2").checked = false;
                                        
                                            
                                        Ext.getCmp("newlink2").checked = false;
                                        

                                        return_page = "parent";
                                    }
                                }
                            },
                            {
                                fieldLabel: lang.xeon.msg_delete_newlink2,
                                id : 'newlink2',
                                xtype : 'checkbox',
                                handler: function()
                                {
                                    if(Ext.getCmp("newlink2").checked)
                                    {
                                        document.getElementById("newlink1").checked = false;
                                            

                                        Ext.getCmp("newlink1").checked = false;
                                        
                                            
                                        return_page = "home";
                                    }
                                }
                            },
                            {
                                fieldLabel: lang.xeon.msg_delete_newlink3_2,
                                id :  'newlink3_2',
                                width: 50,
                                onFocus: function ()
                                {
                                    document.getElementById("newlink1").checked = false;
                                    document.getElementById("newlink2").checked = false;
                                    return_page = "another_id";
                                }
                            //readOnly: true
                            }
                            ],
                            buttons: [{
                                text: lang.xeon.ok,
                                handler: function() {

                                    if(return_page != "another_id")
                                        returnPage = return_page;
                                    else
                                        returnPage = document.getElementById("newlink3_2").value;
                                        

                                    //On enregistre les info de la redirection
                                    Ext.Ajax.request({
                                        params: {
                                            cmd: 'cms.saveDeletePage',
                                            item: n.attributes.item,
                                            returnPage : returnPage
                                        }
                                    });

                                    //On supprime la page
                                    Ext.Ajax.request({
                                        params: {
                                            cmd: 'xeon.deleteItem',
                                            item: n.attributes.item
                                        }
                                    });

                                    win.close();

                                    var tab = Xeon.ui.center.getComponent(n.attributes.item);
                                    if (tab) Xeon.ui.center.remove(tab);

                                    n.remove();
                                }
                            },{
                                text: lang.xeon.cancel,
                                handler: function() {
                                    win.close();
                                }
                            }]
                        });


                        //Pop-up pour le formpanel
                        var win = new Ext.Window({
                            title: lang.xeon.msg_delete_newlink,
                            //width: 430,
                            //height: 200,
                                                       
                            items: this_formpanel
                        });

                        win.show();

                    }
                    else
                    {
                        Ext.Ajax.request({
                            params: {
                                cmd: 'xeon.deleteItem',
                                item: n.attributes.item
                            }
                        });

                        var tab = Xeon.ui.center.getComponent(n.attributes.item);
                        if (tab) Xeon.ui.center.remove(tab);

                        n.remove();
                    }
            
                }
            });
        }
    },
    
    reloadData: function()
    {
        this.selModel.clearSelections();
        this.loader.baseParams = {
                    cmd: 'xeon.makeTree',
                    item: this.itemId
                };
        this.root.reload();
    },
    
    mFilterReload: function()
    {
        this.selModel.clearSelections();
        this.loader.baseParams = {
                    cmd: 'xeon.makeTree',
                    item: this.itemId,
                    mobile: true
                };
        this.root.reload();
    },
    
    collapseAll: function(b)
    {
        this.root.collapseChildNodes(true);
    },
    
    checkNodeState: function(t, p, n)
    {
        if (this.nodeState[n.attributes.item]) {
            n.expanded = true;
        }
    },

    setNodeState: function(n) {
        this.nodeState[n.attributes.item] = n.expanded;
    },

    checkAllowDrop: function(e)
    {
        var item = Xeon.getId(this.itemId);

               
        if (Xeon.module[item.module] && typeof Xeon.module[item.module]['allowDrop'] == 'function')
        {
            var target = (e.point == 'append') ? e.target : e.target.parentNode;
            return Xeon.module[item.module]['allowDrop'].call(this, e.tree, e.dropNode, target) || false;
        }
        
        return false;
    }
});

Ext.ux.FormPanel = Ext.extend(Ext.FormPanel, {
    resize:false,
    cls: 'x-formpanel',
    labelWidth: 180,
    buttonAlign: 'center',
    boxComponentId:false,
    buttons: [{
        text: lang.xeon.submit,
        handler: function(b) {
            this.ownerCt.ownerCt.submit();
        }
    },{
        text: lang.xeon.cancel,
        handler: function(b)
        {
            var p = b.ownerCt.ownerCt;

            if (p.ownerCt.baseCls == 'x-window' && p.ownerCt.closable) {
                p.ownerCt.close();
            } else {
                p.form.reset();
            }
        }
    }],
    
    initComponent: function()
    {
        var p = this;
        
        if (this.resize) this.labelWidth = this.resize;
                
        p.keys = [{
            key: [10, 13],
            handler: function(k, e)
            {
                if (e.target.type != 'textarea') {
                    p.submit();
                }
            }
        }];
        
        if (p.edit == false)
        {
            p.on('render', function()
            {
                Ext.each(p.buttons, function(b) {
                    b.setDisabled(true);
                });
            });
        }
        
        Ext.ux.FormPanel.superclass.initComponent.call(this);
    },
    
    submit: function()
    {
        var p = this;
        var item = p.ownerCt.itemId;
        
               
        if (!p.form.isValid()) return;
        if (p.bwrap.isMasked()) return;
        
        p.form.applyDataArray();
        
        Ext.Ajax.request({
            form: p.form.el.dom,
            params: p.params || {
                cmd: p.command || 'xeon.saveData',
                item: p.itemId || item
            },
            before: function() {
                p.bwrap.mask(lang.xeon.saving, 'x-mask-loading');
            },
            success: p.success || Ext.emptyFn,
            callback: function(o, s, a)
            {
                p.bwrap.unmask();

                if (!a.error)
                {
                    if (p.boxComponentId) {
                        s = Ext.getCmp(p.boxComponentId);
                        s.getEl().update(a.json.db.html);
                    }

                    Xeon.addItem(a.json.item, a);
                    
                    if (p.ownerCt && p.ownerCt.baseCls == 'x-window') {
                        p.ownerCt.close();
                        Xeon.activeGrid.store.reload();
                    }
                }
                               
            }
        });
    }
});

Ext.ux.ListPanel = Ext.extend(Ext.ux.FormPanel, {
    initComponent: function()
    {
        var Record = Ext.data.Record.create(['v', 'd']);
        
        var ms = new Ext.ux.form.MultiSelect({
            fieldLabel: this.fieldLabel || this.title,
            name: this.name,
            dataFields: ['v', 'd'],
            valueField: 'v',
            displayField: 'd',
            data: this.data,
            onViewClick: Ext.emptyFn,
            
            onViewDblClick: function(d, i)
            {
                ms.view.store.remove(ms.view.store.getAt(i));
                ms.setValue(ms.getValue());
            },
            
            setValue: function(v) {
                ms.hiddenField.dom.value = v;
            },
            
            getValue: function()
            {
                var data = [];
                
                ms.view.store.each(function(r) {
                    data.push(r.data.v);
                });
                
                return data.join(',');
            },
            
            reset: function()
            {
                ms.view.store.loadData(this.data);
                ms.setValue(ms.getValue());
            }
        });
        
        var lb = new Ext.ux.form.ListBox({
            fieldLabel: lang.xeon.add_item,
            itype: this.itype,
            reset: Ext.emptyFn,
            
            onSelect: function(r)
            {
                            
                //console.log(r.data.v, typeof r.data.v);
                
                if (ms.view.store.find('v', r.data.v) == -1)
                {
                    var r = new Record({
                        v: r.data.v,
                        d: r.data.d
                    });
                    ms.view.store.add(r);
                    ms.setValue(ms.getValue());
                //ms.view.store.sort('d', 'ASC');
                }
                
                this.collapse();
            }
        });
        
        lb.on('render', function() {
            this.el.dom.removeAttribute('name');
        });
        
        this.items = [ms, lb];
        Ext.ux.ListPanel.superclass.initComponent.call(this);
    }
});

Ext.ux.InfoPanel = Ext.extend(Ext.Panel, {
    cls: 'x-infopanel',
    
    initComponent: function()
    {
        if (this.items)
        {
            var table = '';
            
            Ext.each(this.items, function(i)
            {
                if (i.renderer) i.value = i.renderer.call(this, i.value);
                table += '<tr><th>'+i.infoLabel+' :</th><td>'+i.value+'</td></tr>';
            });
    
            this.html = '<table>'+table+'</table>';
            delete this.items;
        }
        
        Ext.ux.InfoPanel.superclass.initComponent.call(this);
    }
});

Ext.ux.SliderTip = Ext.extend(Ext.Tip, {
    minWidth: 10,
    offsets : [0, -10],
    tipText: '{0}',

    init : function(slider){
        slider.on('dragstart', this.onSlide, this,{
            buffer:50
        });
        slider.on('drag', this.onSlide, this,{
            buffer:50
        });
        slider.on('dragend', this.hide, this,{
            buffer:50
        });
        slider.on('destroy', this.destroy, this);
    },

    onSlide : function(slider){
        this.show();
        var v = slider.getValue();
        if (v == this.lastValue)
            return;
        this.lastValue=v;
        this.body.update(this.getText(slider));
        this.doAutoWidth();
        this.el.alignTo(slider.thumb, 'b-t?', this.offsets);
    },

    getText : function(slider){
        return String.format(this.tipText, slider.getValue());
    }
});

Ext.ux.grid.GridPanel = Ext.extend(Ext.grid.EditorGridPanel, {
    height: 350,
    optionCell:false,
    vdata : false,
    enableHdMenu: false,
    mobile: false,
    stripeRows: true,
    reloadAfterEdit: false,
    reloadAfterErrorEdit: false,
    redirectAfterDelete: false,
    configRedirectionAfterDelete: {
        grantFreeInput: true,
        textFreeInput: lang.xeon.free_input_id,
        choices: []
    },
    pageSize: 50,
    conditionalRowId: false,
    needToolbar: 1,
    enableDelete: false,
    searchParams: {
        search: ''
    },
    engine:{},
    search:[],
    showIconRefresh: true,
    noRefreshToolbar: false,
    tools: [{
        id: 'maximize',
        qtip: lang.xeon.maximize,
        handler: function(e, t, p)
        {
            Xeon.ui.tree.collapse();
            p.expand();
            p.setSize(window.innerWidth-64, window.innerHeight-134);
            p.ownerCt.body.dom.scrollTop = (p.el.dom.offsetTop - 20);
        }
    }],
    
    initComponent: function()
    {
        var p = this;
               
        var fields = [p.rowId];
               
        //p.autoExpandColumn = p.columns.length;
        p.extraColumns = p.columns;
        Ext.each(p.columns, function(c)
        {
            if (c.dataType == 'date') {

                fields.push({
                    name: c.dataIndex,
                    type: c.dataType,
                    dateFormat: 'Y-m-d'
                });
                if (c.renderFormat)
                    c.renderer = function(date){
                        return date.format(c.renderFormat);
                    };
            }
            else
                fields.push({
                    name: c.dataIndex,
                    type: c.dataType
                });
            
            if (p.enableDragDrop) {
                c.sortable = false;
            }
            
            if (c.itemLink)
            {
                c.renderer = function(v, m, r) {
                    return v ? '<a class="x-link" onclick="Xeon.loadItem(\''+c.itemLink[0]+'.'+r.json[c.itemLink[1]]+'\');">'+v+'</a>' : '';
                };
            }
            
            if (c.iconCls)
            {
                c.renderer = function(v, m, r) {
                    m.css = (c.handler ? 'x-button ' : 'x-icon ')+c.iconCls;
                }
            }
        });
        
        if (p.selModel !== false)
        {
            if (!p.selModel)
            {
                p.selModel = new Ext.grid.CheckboxSelectionModel({
                    moveEditorOnEnter: false
                });
            }
            
            p.columns.unshift(p.selModel);
        }
        
        if (!p.colModel)
        {
            p.colModel = new Ext.grid.ColumnModel({
                columns: p.columns,
                defaults: {
                    sortable: true
                }
            });
            
            delete p.columns;
        }
        
        if (!p.store)
        {
             p.extraFields = fields;
            p.store = new Ext.data.JsonStore({
                idProperty: p.rowId,
                root: 'data',
                fields: fields,
                autoLoad: true,
                proxy: new Ext.data.HttpProxy({
                    url: Ext.Ajax.url,
                    before: function() {
                        p.bwrap.mask(lang.xeon.loading, 'x-mask-loading');
                    },
                    after: function() {
                        p.bwrap.unmask();
                    }
                }),
                listeners:{
                    metachange : function(store, meta)
                    {
                        // VERSION 2
                        var col = [];
                        col.push(new Ext.grid.CheckboxSelectionModel({
                            moveEditorOnEnter: false
                        }));
                        Ext.each(meta.columns, function(v)
                        {
                            col.push(v);
                        });
                        p.reconfigure(store, new Ext.grid.ColumnModel({
                            columns: col,
                            defaults: {
                                sortable: true
                            }
                        }));
                    }
                }
            });
        }
        
        p.store.on('beforeload', function()
        {
            // console.log(p.searchParams);
            p.store.baseParams = Ext.apply({
                cmd: 'xeon.makeGrid',
                item: p.itype,
                parent: p.parentId ? p.parentId : p.ownerCt.itemId,
                mobile:p.mobile
            }, p.searchParams);
        });

        p.store.on('load', function(s)
        {
            if (p.needToolbar == 1)
            {
                Ext.each(s.reader.jsonData.toolbar, function(b, i)
                {
                    b.handler = p[b.action];
                    b.scope = p;
                    p.topToolbar.insertButton(i, b);
                });
                
                p.needToolbar = 0;
                p.topToolbar.doLayout();
            }
        });
        
        p.tbar = [];

        if(p.showIconRefresh)
        {
            p.tbar.push({
                text: lang.xeon.reload,
                iconCls: 'i-refresh',
                handler: p.reloadData,
                scope: p
            });
        }
        if (p.search)
        {
            p.tbar.push({
                text: lang.xeon.search,
                iconCls: 'i-magnifier',
                handler: p.displaySearch,
                scope: p
            });
        }
        
        if (p.quickSearch)
        {
            var search = function()
            {
                p.searchParams = {
                    search: p.searchField.getValue()
                };
                p.store.load();
                
                if (p.searchWindow) {
                    p.searchWindow.getComponent(0).form.reset();
                }
            }
            
            if (p.quickSearch === true)
            {
                var task = new Ext.util.DelayedTask(search);
                
                p.searchField = new Ext.form.TextField({
                    width: 120,
                    enableKeyEvents: true,
                    emptyText: lang.xeon.quick_search
                });

                new Ext.ux.RefTip({
                    offsets : [140, -17],
                    maxC:70,
                    cp:p.searchField,
                    auto:false,
                    tipText:lang.xeon.quick_search_tip
                });
                                
                p.searchField.on('keyup', function() {
                    task.delay(500);
                });
            }
            else
            {
                p.searchField = p.quickSearch;
                p.searchField.width = 120;
                
                p.searchField.on('select', search);
            }
            
            p.searchParams.search = p.searchField.getValue();
            p.tbar.push('->', p.searchField);
        }
        
        if (p.paging)
        {
            p.bbar = new Ext.PagingToolbar({
                pageSize: p.pageSize,
                store: p.store,
                displayInfo: true,
                listeners:{
                    afterrender : function()
                    {
                        if(p.noRefreshToolbar)
                        {
                            this.refresh.hide();
                        }
                    }
                }
            });
            
            p.store.remoteSort = true;
        }
        
        p.selModel.on('selectionchange', function(s)
        {
            var allowCopy = true;
            var allowDelete = true;

            if (s.getSelections().length != 0)
            {
                Ext.each(s.getSelections(), function(r)
                {
                    if (typeof(r.json._copy) == 'boolean') allowCopy &= r.json._copy;
                    
                    if( p.enableDelete == false )
                        if (typeof(r.json._delete) == 'boolean') allowDelete &= r.json._delete;
                });
            }
            else
            {
                allowCopy = allowDelete = false;
            }
            
            if (s.getSelections().length != 1) {
                allowCopy = false;
            }

            p.topToolbar.items.each(function(b)
            {
                if (b.action == 'copyItem') b.setDisabled(!allowCopy);
                if (b.action == 'deleteItem') b.setDisabled(!allowDelete);
            });
        });
        
        p.on('cellclick', function(p, i, c, e)
        {
            if (p.colModel.columns[c].handler) {
                p.colModel.columns[c].handler.call(this, p, p.store.getAt(i), i, c);
            }
        });
        
        p.on('beforeedit', function(e)
        {
            var allowEdit = true;
            
            if (typeof(e.record.json._edit) == 'boolean') {
                allowEdit = e.record.json._edit;
            }
            
            return allowEdit;
        });

        p.on('afteredit', function(e)
        {
            var iType=((e.grid.extraColumns[e.column].itype) ? e.grid.extraColumns[e.column].itype : (p.optionCell ? 'shop.opt':p.itype));
            var id=(p.conditionalRowId) ? p.conditionalRowId(e) : e.record.id;
            Ext.Ajax.request({
                params: {
                    cmd: 'xeon.saveCell',
                    item: iType+'.'+id,
                    field: e.field,
                    value: (e.value instanceof Date) ? e.value.format(e.record.fields.items[e.column].dateFormat) : e.value
                },
                success: function(a)
                {
                    e.record.commit();
                    
                    if (p.reloadAfterEdit) {
                        p.store.reload();
                    }
                },
                callback : function(o, s, a)
                {
                    if (a.json.tree)
                    {
                        var tree = Xeon.ui.tree.getComponent(a.json.tree);
                        if (tree) tree.root.reload();
                    }
                    if (p.reloadAfterErrorEdit) {
                        p.store.reload();
                    }  
                }
            });
        });
        
        p.on('beforedestroy', function()
        {
            if (p.searchWindow) {
                p.searchWindow.destroy();
            }
        });
        
               
        Ext.ux.grid.GridPanel.superclass.initComponent.call(this);
              
    },
    resetColumns: function(){
        this.extraColumns = [];
        var colModel = new Ext.grid.ColumnModel({columns: this.extraColumns,defaults: {sortable: true}});
        this.reconfigure(this.store, colModel);
    },
    addColumns: function(config){
      var p=this;
      Ext.each(config,function(c){p.extraColumns.push(c)});
     
      var colModel = new Ext.grid.ColumnModel({columns: this.extraColumns,defaults: {sortable: true}});
      if(!this.view){this.view=new Ext.grid.GridView(this.viewConfig)}
      Ext.each(config,function(c){p.extraFields.push({name: c.dataIndex,type:undefined})});          

            
     this.reconfigure(this.store, colModel);
    },
    onRender: function(ct, position)
    {
        var p = this;
        
        Ext.ux.grid.GridPanel.superclass.onRender.call(this, ct, position);

        if (p.enableDragDrop)
        {
            p.ddGroup = 'GridDD';

            new Ext.dd.DropTarget(p.getView().scroller.dom, {
                ddGroup: p.ddGroup,
                notifyDrop: function(dd, e, data)
                {
                    var sm = p.getSelectionModel();
                    var s = sm.getSelections();
                    var i = dd.getDragData(e).rowIndex;
                    
                    if ((typeof i != 'undefined') && (s.length == 1))
                    {
                        var r = p.store.getById(s[0].id);
                        
                        p.store.remove(p.store.getById(s[0].id));
                        p.store.insert(i, r);
                        sm.selectRecords(s);
                        
                        Ext.Ajax.request({
                            params: {
                                cmd: 'xeon.moveItem',
                                item: p.itype+'.'+r.id,
                                target: false,
                                rank: i+1
                            },
                            success: function()
                            {
                                if(p.reloadAfterEdit)
                                {
                                    p.reloadData();
                                }
                            }   
                        });
                    }
                }
            });
        }
        
        new Ext.Resizable(p.el, {
            minWidth: 700,
            handles: 's,e,se',
            transparent: true,
            resizeElement: function()
            {
                var box = this.proxy.getBox();
                p.updateBox(box);
                p.el.setLeftTop(0, 0);
                return box;
            }
        });
    },
    
    addItem: function() {
        Xeon.loadItem(this.itype+'.0', this.ownerCt.itemId, this);
    },
    
    copyItem: function()
    {
        var p = this;
        var r = p.selModel.getSelected();
        
        Xeon.copyItem(p.itype+'.'+r.id);
    },
    
    deleteItem: function()
    {
        // console.log('test deleteItem');
        var p = this;
        var this_formpanel;
        var list = [];
        var count = p.selModel.getCount();

        if (count > 0)
        {
            var text = (count > 1) ? String.format(lang.xeon.delete_items, count) : lang.xeon.delete_item;
                
            Ext.Msg.confirm(lang.xeon.confirm, text, function(btn)
            {
                if (btn == 'yes')
                {
                    
                    if(p.redirectAfterDelete){
                        //QUESTION DE REDIRECTION AVANT SUPPRESSION
                        
                        //Configuration du popup
                        var return_page = "";
                        var other_selection = false;
                        var columns=[];
                        
                        //Ajout de toutes les colonnes
                        for(var i=0;i<p.configRedirectionAfterDelete.choices.length;i++){
                            var obj={
                                fieldLabel: p.configRedirectionAfterDelete.choices[i].text,
                                xtype : 'radio',
                                name: 'redirect',
                                value:  p.configRedirectionAfterDelete.choices[i].value,
                                listeners:{
                                    change: function(){
                                        this_formpanel.getComponent(p.configRedirectionAfterDelete.choices.length).setValue('');
                                    }
                                }
                            };
                            if(typeof( p.configRedirectionAfterDelete.choices[i].default)!='undefined')
                                obj.value=p.configRedirectionAfterDelete.choices[i].default;
                                    
                            columns.push(obj);
                        }
                        
                        //Ajout colonne par défaut
                        if(p.configRedirectionAfterDelete.grantFreeInput){
                            columns.push({
                                fieldLabel: p.configRedirectionAfterDelete.textFreeInput,
                                width: 50,
                                name: 'freechoice',
                                listeners:{
                                    change: function(){
                                        if(this_formpanel.getComponent(p.configRedirectionAfterDelete.choices.length).getValue()!=''){
                                            for(var i=0;i<p.configRedirectionAfterDelete.choices.length;i++)
                                                this_formpanel.getComponent(i).setValue(false);
                                        }
                                    }
                                }
                            });
                        }
                        
                        this_formpanel = new Ext.form.FormPanel({
                            cls: 'x-formpanel',
                            labelWidth: 180,
                            buttonAlign: 'center',
                            name:'newlink',
                            items: columns,
                            buttons: [{
                                text: lang.xeon.ok,
                                handler: function() {
                                    var valeur='';
                                    if(p.configRedirectionAfterDelete.grantFreeInput)
                                        valeur=this_formpanel.getComponent(p.configRedirectionAfterDelete.choices.length).getValue();
                                        
                                    if(valeur==''){
                                        for(var i=0;i<p.configRedirectionAfterDelete.choices.length;i++){
                                            if(this_formpanel.getComponent(i).getValue())
                                                valeur=p.configRedirectionAfterDelete.choices[i].value;
                                        }
                                    }
                                    if(valeur==''){
                                        alert(lang.xeon.erreur_oubli_selection);
                                    }else{
                                        Ext.each(p.selModel.getSelections(), function(r)
                                        {
                                            var tab = Xeon.ui.center.getComponent(p.itype+'.'+r.id);
                                            if (tab) Xeon.ui.center.remove(tab);
                    
                                            if (r.data.opt_id == null) r.id = 'product+'+r.data.prd_id;
                                            list.push(r.id);
                                            p.store.remove(r);
                                        });
                                        //On supprime l'enregistrement
                                        Ext.Ajax.request({
                                            params: {
                                                cmd: 'xeon.deleteItem',
                                                item: p.itype+'.'+list.join('-'),
                                                parent: p.ownerCt.itemId,
                                                answer: valeur
                                            },
                                            success: function()
                                            {
                                                if (p.reloadAfterEdit) {
                                                    p.reloadData();
                                                }
                                            }
                                        });
                                            
                                        win.close();
                                    }
                                }
                            },{
                                text: lang.xeon.cancel,
                                handler: function() {
                                    win.close();
                                }
                            }]
                        });
                        
                        //Affichage du popup
                        var win = new Ext.Window({
                            title: lang.xeon.msg_delete_newlink,
                            items: this_formpanel
                        });
                        win.show();
                    }else{
                        //SUPPRESSION CLASSIQUE
                        Ext.each(p.selModel.getSelections(), function(r)
                        {
                            var tab = Xeon.ui.center.getComponent(p.itype+'.'+r.id);
                            if (tab) Xeon.ui.center.remove(tab);
                            
                            list.push(r.id);
                            p.store.remove(r);
                        });
                        
                        Ext.Ajax.request({
                            params: {
                                cmd: 'xeon.deleteItem',
                                item: p.itype+'.'+list.join('-'),
                                parent: p.ownerCt.itemId
                            },
                            success: function()
                            {
                                if (p.reloadAfterEdit) {
                                    p.reloadData();
                                }
                            }
                        });
                    }
                }
            });
        }
    },
    
    exportData: function(type)
    {
        var p = this;
        var rows = [];
        
        Ext.each(p.selModel.getSelections(), function(r) {
            rows.push(r.id);
        });
        var cmd = 'xeon.exportGrid';
        if(type == 'pdf')
        {
            cmd = 'xeon.exportPDFGrid';
        }

        Ext.Ajax.request({
            params: Ext.apply({
                cmd: cmd,
                item: p.itype,
                parent: p.ownerCt.itemId,
                rows: rows.join(',')
            }, p.searchParams),
            before: function() {
                p.bwrap.mask(lang.xeon.loading, 'x-mask-loading');
            },
            success: function() {
                if(type == 'pdf')
                {
                    Xeon.getFilePDF();
                }
                else
                {
                    Xeon.getFile();   
                }
            },
            callback: function() {
                p.bwrap.unmask();
            }
        });
    },
    exportPDFData : function()
    {
        this.exportData('pdf');
    },
    reloadData: function() {
        this.store.load();
    },
    
    displaySearch: function()
    {
        var p = this;

        if (!p.searchWindow)
        {

            var fp = new Ext.ux.FormPanel({
                items: this.search,
                buttons: [{
                    text: lang.xeon.ok,
                    handler: function() {
                        fp.submit();
                    }
                },{
                    text: lang.xeon.clear,
                    handler: function() {
                        p.searchWindow.getComponent(0).form.reset();
                    }
                }],
                
                submit: function()
                {
                    p.searchParams = {};
                    
                    var f = fp.form.el.dom.elements;
                                       
                    for (var i =0; i < fp.items.items.length; i++)
                    {
                        if (fp.items.items[i].getXType() == 'compositefield'){
                            for (var c =0; c < fp.items.items[i].items.items.length; c++)
                            {
                                item = fp.items.items[i].items.items[c];
                                //todo work only with special components (slider, date...)
                                if (item.getValue() > 0)
                                    p.searchParams['search['+item.name+']'] = item.getValue();
                            }
                        }
                    }
                                           
                    for (var i=0; i<f.length; i++)
                    {
                        var v = Ext.util.Format.trim(f[i].value);
        
                        if (v != '' && !isNumeric(f[i].name) && f[i].name) {
                            p.searchParams['search['+f[i].name+']'] = v;
                        }
                    }
                    
                    p.store.load();
                    p.searchWindow.hide();
                    
                    if (p.searchField) {
                        p.searchField.reset();
                    }
                }
            });
            
            p.searchWindow = new Ext.ux.Window({
                title: lang.xeon.search,
                iconCls: 'i-magnifier',
                closeAction: 'hide',
                id: Ext.id(),
                items: fp
            });
        }
        
        p.searchWindow.show();
    }
});

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

Ext.ux.DataPanel = Ext.extend(Ext.Panel, {
    cls: 'x-datapanel',
    selectedIndex: false,
    Record: Ext.data.Record.create(['item', 'html', 'info']),

    initComponent: function()
    {
        var p = this;
        
        p.items = p.view = new Ext.DataView({
            singleSelect: true,
            trackOver: true,
            emptyText: 'Aucun',
            itemSelector: 'div.bloc',
            store: new Ext.data.ArrayStore({
                fields: ['item', 'html', 'info'],
                data: p.data
            }),
            tpl: new Ext.XTemplate(
                '<tpl for=".">',
                '<div class="{cls}">',
                '{html}',
                '</div>',
                '</tpl>'
                ),
            prepareData: function(d, i, r)
            {
                d.cls = (p.view.store.indexOf(r) == 0) ? 'bloc first' : 'bloc';
                return d;
            }
        });

        Ext.ux.DataPanel.superclass.initComponent.call(this);
    },

    onRender: function(ct, position)
    {
        Ext.ux.DataPanel.superclass.onRender.call(this, ct, position);

        var p = this;

        if (typeof p.getToolbar == 'function')
        {
            p.toolbar = new Ext.Toolbar({
                cls: 'x-datapanel-toolbar',
                renderTo: p.body.dom,
                items: p.getToolbar(p)
            });
            
            p.view.on('mouseenter', p.showToolbar, p);
    
            Ext.EventManager.on(p.el.dom.parentNode, 'mouseover', function(e, t, o) {
                if (t == this) p.hideToolbar();
            });
        }
    },

    highlight: function(i)
    {
        var bloc = Ext.get(this.view.getNode(i));
        bloc.scrollIntoView(this.ownerCt.body);
        bloc.stopFx();
        bloc.highlight('#c3daf9', {
            block: true
        });
    },
    
    getSelectedIndex: function() {
        return this.selectedIndex;
    },
    
    getSelectedRecord: function() {
        return this.view.store.getAt(this.selectedIndex);
    },
    
    showToolbar: function(d, i, n)
    {
        var p = this;
        var bloc = Ext.get(n);
        var show = true;
        
        p.hideToolbar();
        
        if (typeof p.checkToolbar == 'function') {
            show = p.checkToolbar(d.store, i, p.toolbar.items);
        }
        
        if (show !== false)
        {
            var o = bloc.getOffsetsTo(bloc.parent().dom);
            p.toolbar.el.dom.style.top = (o[1]+5)+'px';
            p.toolbar.el.dom.style.right = o[0]+'px';
            p.toolbar.el.dom.style.display = 'block';
        }
        
        p.selectedIndex = i;
    },
    
    hideToolbar: function() {
        this.toolbar.el.dom.style.display = 'none';
    }
});

Ext.ux.LayoutPanel = Ext.extend(Ext.ux.DataPanel, {
    cls: 'x-datapanel x-layoutpanel',
    
    initComponent: function()
    {
        var p = this;
        
        p.tbar = [{
            text: lang.xeon.preview_page,
            iconCls: 'i-eye',
            handler: p.previewPage,
            scope: p
        }/*,{
            text: lang.XEON_ACCEPT_CHANGE,
            iconCls: 'i-accept',
            disabled: true
        },{
            text: lang.XEON_DISCARD_CHANGE,
            iconCls: 'i-delete',
            disabled: true
        }*/];

        Ext.ux.LayoutPanel.superclass.initComponent.call(this);
    },
    
    editData: function(r, index, onUpdate)
    {
        var p = this;
        var db = r.data.info;
        var tpl = db.template;
        
        var params = {
            cmd: 'xeon.saveData',
            item: r.data.item,
            parent: p.ownerCt.itemId,
            'data[template]': tpl,
            'data[rank]': index
        };
        
        var success = function(a) {
            onUpdate.call(this, a.json);
        };
        
        if (tpl == 1)
        {
            new Ext.ux.Window({
                itemId: r.data.item,
                title: lang.xeon.title,
                iconCls: 'i-text-heading-1',
                items: new Ext.ux.FormPanel({
                    params: params,
                    success: success,
                    items: [{
                        fieldLabel: lang.xeon.title,
                        allowBlank: false,
                        name: 'data1',
                        value: db.data1
                    },{
                        xtype: 'combobox',
                        fieldLabel: lang.xeon.level,
                        allowBlank: false,
                        data: [[1, lang.xeon.title1], [2, lang.xeon.title2], [3, lang.xeon.title3]],
                        name: 'data2',
                        value: db.data2
                    }]
                })
            }).show();
        }
        
        if (tpl == 2)
        {
            new Ext.ux.xeon.LayoutWindow({
                title: lang.xeon.catcher,
                iconCls: 'i-text-dropcaps',
                items: new Ext.FormPanel({
                    params: params,
                    success: success,
                    layout: 'fit',
                    height: 300,
                    border: false,
                    items: new Ext.form.TextArea({
                        name: 'data1',
                        value: db.data1
                    })
                })
            }).show();
        }
        
        if (tpl == 3)
        {
            new Ext.ux.xeon.LayoutWindow({
                title: lang.xeon.media,
                iconCls: 'i-image',
                items: new Ext.FormPanel({
                    params: params,
                    success: success,
                    layout: 'fit',
                    height: 324,
                    border: false,
                    items: new Ext.ux.MediaPanel({
                        name: 'data1',
                        value: db.data1
                    })
                })
            }).show();
        }
        
        if (tpl == 4)
        {
            new Ext.ux.xeon.LayoutWindow({
                title: lang.xeon.text1,
                iconCls: 'i-text-align-left',
                items: new Ext.FormPanel({
                    params: params,
                    success: success,
                    layout: 'fit',
                    height: 349,
                    border: false,
                    items: new Ext.TabPanel({
                        plain: true,
                        enableTabScroll: true,
                        activeTab: 0,
                        tabWidth: 160,
                        minTabWidth: 160,
                        resizeTabs: true,
                        items: [{
                            xtype: 'editorpanel',
                            title: lang.xeon.text,
                            name: 'data1',
                            value:  db.data1
                        },{
                            xtype: 'mediapanel',
                            title: lang.xeon.media,
                            name: 'data2',
                            value: db.data2,
                            alignName: 'data3',
                            alignValue: db.data3
                        }]
                    })
                })
            }).show();
        }
        
        if (tpl == 5)
        {
            new Ext.ux.xeon.LayoutWindow({
                title: lang.xeon.text2,
                iconCls: 'i-text-columns',
                items: new Ext.FormPanel({
                    params: params,
                    success: success,
                    layout: 'fit',
                    height: 349,
                    border: false,
                    items: new Ext.TabPanel({
                        plain: true,
                        enableTabScroll: true,
                        activeTab: 0,
                        tabWidth: 160,
                        minTabWidth: 160,
                        resizeTabs: true,
                        items: [{
                            xtype: 'editorpanel',
                            title: lang.xeon.text_col1,
                            name: 'data1',
                            value: db.data1
                        },{
                            xtype: 'mediapanel',
                            title: lang.xeon.media_col1,
                            name: 'data3',
                            value: db.data3,
                            alignName: 'data5',
                            alignValue: db.data5
                        },{
                            xtype: 'editorpanel',
                            title: lang.xeon.text_col2,
                            name: 'data2',
                            value: db.data2
                        },{
                            xtype: 'mediapanel',
                            title: lang.xeon.media_col2,
                            name: 'data4',
                            value: db.data4,
                            alignName: 'data6',
                            alignValue: db.data6
                        }]
                    })
                })
            }).show();
        }
        
        if (tpl == 6)
        {
            new Ext.ux.xeon.LayoutWindow({
                title: lang.xeon.text3,
                iconCls: 'i-text-columns-3',
                items: new Ext.FormPanel({
                    params: params,
                    success: success,
                    layout: 'fit',
                    height: 349,
                    border: false,
                    items: new Ext.TabPanel({
                        plain: true,
                        enableTabScroll: true,
                        activeTab: 0,
                        tabWidth: 160,
                        minTabWidth: 160,
                        resizeTabs: true,
                        items: [{
                            xtype: 'editorpanel',
                            title: lang.xeon.text_col1,
                            name: 'data1',
                            value: db.data1
                        },{
                            xtype: 'editorpanel',
                            title: lang.xeon.text_col2,
                            name: 'data2',
                            value: db.data2
                        },{
                            xtype: 'editorpanel',
                            title: lang.xeon.text_col3,
                            name: 'data3',
                            value: db.data3
                        }]
                    })
                })
            }).show();
        }
        
        if (tpl == 7)
        {
            Ext.Ajax.request({
                params: params,
                success: success
            });
        }
        
        if (tpl == 8)
        {
            new Ext.ux.Window({
                itemId: r.data.item,
                title: lang.xeon.image_slide,
                iconCls: 'i-picture-go',
                items: new Ext.ux.FormPanel({
                    params: params,
                    success: success,
                    items: [{
                        xtype: 'listbox',
                        fieldLabel: lang.media.folder,
                        allowBlank: false,
                        itype: 'media.fd1',
                        name: 'data1',
                        value: db.data1
                    }]
                })
            }).show();
        }
        
        if (tpl == 9)
        {
            new Ext.ux.Window({
                itemId: r.data.item,
                title: lang.xeon.video_slide,
                iconCls: 'i-film-go',
                items: new Ext.ux.FormPanel({
                    params: params,
                    success: success,
                    items: [{
                        xtype: 'listbox',
                        fieldLabel: lang.xeon.mod_media.folder,
                        allowBlank: false,
                        itype: 'media.fd4',
                        name: 'data1',
                        value: db.data1
                    }]
                })
            }).show();
        }
        
        if (tpl == 10)
        {
            new Ext.ux.xeon.LayoutWindow({
                title: lang.xeon.code,
                iconCls: 'i-page-white-code',
                items: new Ext.FormPanel({
                    params: params,
                    success: success,
                    layout: 'fit',
                    height: 300,
                    border: false,
                    items: new Ext.form.TextArea({
                        cls: 'code',
                        name: 'data1',
                        value: db.data1
                    })
                })
            }).show();
        }
    },
    
    getToolbar: function(p)
    {
        var menu = [];
        
        Ext.each(p.templates.split(','), function(i)
        {
            var tpl = Ext.ux.LayoutPanel.templateList[i];
            if (!tpl) return;
            
            menu.push({
                itemId: i,
                text: tpl[0],
                iconCls: tpl[1]
            });
        });
        
        p.templateMenu = new Ext.menu.Menu(menu);
        p.templateMenu.on('click', p.addItem, p);
        
        var tb = [{
            iconCls: 'i-add',
            tooltip: lang.xeon.add_content,
            menu: p.templateMenu
        },{
            iconCls: 'i-page-white-edit',
            tooltip: lang.xeon.edit,
            handler: function() {
                p.editItem.call(this, p, p.getSelectedRecord(), p.getSelectedIndex());
            }
        },{
            iconCls: 'i-phone',
            tooltip: lang.xeon.mobile,
            handler: function() {
                p.mobileItem.call(this, p, p.getSelectedRecord(), p.getSelectedIndex());
            }
        },{
            iconCls: 'i-arrow-blue-up',
            tooltip: lang.xeon.move_up,
            handler: function() {
                p.moveItemUp.call(this, p, p.getSelectedRecord(), p.getSelectedIndex());
            }
        },{
            iconCls: 'i-arrow-blue-down',
            tooltip: lang.xeon.move_down,
            handler: function() {
                p.moveItemDown.call(this, p, p.getSelectedRecord(), p.getSelectedIndex());
            }
        },{
            iconCls: 'i-cross',
            tooltip: lang.xeon.delete,
            handler: function() {
                p.deleteItem.call(this, p, p.getSelectedRecord(), p.getSelectedIndex());
            }
        }];
    
    //console.log(p.mobile)
    if (!p.mobile) tb.splice(2,1);
    
    return tb;
    },
    
    previewPage: function()
    {
        window.open(this.previewURL);
    },
    
    addItem: function(m, b)
    {
        m.hide();
        
        var p = this;
        var i = p.getSelectedIndex()+1;
        
        var r = new p.Record({
            item: p.itype+'.0',
            html: '',
            info: {
                template: b.itemId
            }
        });
        
        p.editData(r, i, function(json)
        {
            r.set('item', json.id);
            r.set('html', json.html);
            r.set('info', '');
            r.set('info', json.db);
            
            p.view.store.insert(i, r);
            p.highlight(i);
        });
    },
    
    editItem: function(p, r, i)
    {
        if (i == 0)
        {
            Ext.MessageBox.show({
                title: lang.xeon.edit_title,
                iconCls: 'i-page-white-edit',
                width: 300,
                prompt: true,
                buttons: Ext.MessageBox.OKCANCEL,
                value: r.data.html,
                fn: function(btn, text)
                {
                    if (btn == 'ok')
                    {
                        r.set('html', text);
                        p.highlight(i);

                        Ext.Ajax.request({
                            params: {
                                cmd: 'xeon.saveCell',
                                item: p.ownerCt.itemId,
                                field: p.titleName,
                                value: text
                            }
                        });
                    }
                }
            });
        }
        else
        {
            p.editData(r, i, function(json)
            {
                r.set('html', json.html);
                r.set('info', '');
                r.set('info', json.db);
                
                p.highlight(i);
            });
        }
    },
    
    moveItemUp: function(p, r, i)
    {
        p.view.store.remove(p.view.store.getAt(i--));
        p.view.store.insert(i, r);
        p.highlight(i);
        p.hideToolbar();

        Ext.Ajax.request({
            params: {
                cmd: 'xeon.moveItem',
                item: r.data.item,
                target: false,
                rank: i
            }
        });
    },
    
    moveItemDown: function(p, r, i)
    {
        p.view.store.remove(p.view.store.getAt(i++));
        p.view.store.insert(i, r);
        p.highlight(i);
        p.hideToolbar();

        Ext.Ajax.request({
            params: {
                cmd: 'xeon.moveItem',
                item: r.data.item,
                target: false,
                rank: i
            }
        });
    },
    
    deleteItem: function(p, r, i)
    {
        Ext.Msg.confirm(lang.xeon.confirm, lang.xeon.delete_content, function(btn)
        {
            if (btn == 'yes')
            {
                Ext.Ajax.request({
                    params: {
                        cmd: 'xeon.deleteItem',
                        item: r.data.item,
                        parent: p.ownerCt.itemId
                    }
                });
                
                p.view.store.remove(r);
                p.hideToolbar();
            }
        });
    },

    mobileItem: function(p, r, i)
    {
       Ext.Msg.confirm(lang.xeon.confirm, lang.xeon.mobile_content, function(btn)
        {
             mobile = (btn == 'yes') ? 1:0;
                Ext.Ajax.request({
                    params: {
                        cmd: 'xeon.saveCell',
                        item: r.data.item,
                        field: 'mobile',
                        value: mobile
                    },
                    success: function(j, t){
                        r.set('html', j.json.html);
                        r.set('info', '');
                        r.set('info', j.json.db);
                        
                        p.hideToolbar();
                        p.highlight(i);
                    }
                });
            
        });
    },

    checkToolbar: function(s, i, t)
    {
        /*console.log('toolbar');
        console.log(s);
        console.log(i);
        console.log(t);*/

        var up = (i > 1);
        var down = ((i != 0) && (i != s.getCount()-1));
        var del = (i != 0);
        
        t.get(4).setDisabled(!del);
        t.get(2).setDisabled(!up);
        t.get(3).setDisabled(!down);
        //if (!this.mobile) t.get(5).setDisabled(!del);
    },
    
    hideToolbar: function()
    {
        Ext.ux.LayoutPanel.superclass.hideToolbar.call(this);
        this.templateMenu.hide();
    }
});

Ext.ux.LayoutPanel.templateList = {
    1: [lang.xeon.title, 'i-text-heading-1'],
    2: [lang.xeon.catcher, 'i-text-dropcaps'],
    3: [lang.xeon.media, 'i-image'],
    4: [lang.xeon.text1, 'i-text-align-left'],
    5: [lang.xeon.text2, 'i-text-columns'],
    6: [lang.xeon.text3, 'i-text-columns-3'],
    7: [lang.xeon.separator, 'i-text-horizontalrule'],
    8: [lang.xeon.image_slide, 'i-picture-go'],
    9: [lang.xeon.video_slide, 'i-film-go'],
    10: [lang.xeon.code, 'i-page-white-code']
};

Ext.ux.EditorPanel = Ext.extend(Ext.Panel, {
    iconCls: 'i-text-align-left',
    layout: 'fit',
    
    initComponent: function()
    {
        this.items = new Ext.ux.TinyMCE({
            tinymceSettings: {
                theme: 'advanced',
                language: 'fr',
                plugins: 'safari,table,paste',
                theme_advanced_buttons1: 'bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,forecolor,backcolor,|,pastetext,pasteword,|,undo,redo,|,bullist,numlist,|,link,unlink,anchor,|,sub,sup,charmap,|,removeformat,code',
                theme_advanced_buttons2: 'tablecontrols',
                theme_advanced_buttons3: '',
                theme_advanced_toolbar_location: 'top',
                theme_advanced_toolbar_align: 'left',
                theme_advanced_statusbar_location: 'bottom',
                theme_advanced_resizing: false,
                content_css: '/src/xeon/style/tinymce.css',
                extended_valid_elements: 'iframe[*]',
                cleanup: true,
                cleanup_on_startup: true,
                verify_html: true,
                convert_urls: false,
                auto_reset_designmode: true,
                inline_styles: true,
                force_p_newlines: false,
                force_br_newlines: true,
                forced_root_block: '',
                force_hex_style_colors: true
            },
            width: 686,
            height: 312,
            name: this.name,
            value: this.value
        });
        
        Ext.ux.EditorPanel.superclass.initComponent.call(this);
    }
});

Ext.ux.CommentPanel = Ext.extend(Ext.ux.DataPanel, {
    root: false,
    
    getToolbar: function(p)
    {
        return [{
            iconCls: 'i-accept',
            tooltip: lang.xeon.approve,
            handler: function()
            {
                var r = p.getSelectedRecord();
                
                Ext.Ajax.request({
                    params: {
                        cmd: 'xeon.saveData',
                        item: r.data.item,
                        'data[com_status]': 1
                    }
                });
                
                if (p.root) {
                    p.view.store.remove(r);
                } else {
                    r.set('info', 1);
                }
                
                p.hideToolbar();
            }
        },{
            iconCls: 'i-delete',
            tooltip: lang.xeon.disapprove,
            handler: function()
            {
                var r = p.getSelectedRecord();
                
                Ext.Ajax.request({
                    params: {
                        cmd: 'xeon.saveData',
                        item: r.data.item,
                        'data[com_status]': 0
                    }
                });
                
                r.set('info', 0);
                p.hideToolbar();
            }
        },{
            iconCls: 'i-cross',
            tooltip: lang.xeon.delete,
            handler: function()
            {
                var r = p.getSelectedRecord();
                
                Ext.Msg.confirm(lang.xeon.confirm, lang.xeon.comment_delete, function(btn)
                {
                    if (btn == 'yes')
                    {
                        Ext.Ajax.request({
                            params: {
                                cmd: 'xeon.deleteItem',
                                item: r.data.item,
                                parent: p.ownerCt.itemId
                            }
                        });
                        
                        p.view.store.remove(r);
                        p.hideToolbar();
                    }
                });
            }
        }];
    },
    
    checkToolbar: function(s, i, t)
    {
        var r = s.getAt(i);

        t.get(0).setDisabled(r.data.info == 1);
        t.get(1).setDisabled(r.data.info == 0);
    }
});

Ext.ux.form.ComboBox = Ext.extend(Ext.form.ComboBox, {
    mode: 'local',
    triggerAction: 'all',
    editable: false,
    forceSelection: true,
    valueField: 'v',
    displayField: 'd',
    tpl: '<tpl for="."><div class="x-combo-list-item{[values.i?" x-icon "+values.i:""]}">{[values.t?values.t:values.d]}</div></tpl>',
    
    initComponent: function()
    {
        this.hiddenName = this.name;

        if (typeof this.store != 'object')
        {
            this.store = new Ext.data.ArrayStore({
                fields: ['v', 'd', 'i', 't'],
                data: this.data
            });
        }

        Ext.ux.form.ComboBox.superclass.initComponent.call(this);
    }
});

Ext.ux.form.ListBox = Ext.extend(Ext.form.ComboBox, {
    triggerAction: 'all',
    forceSelection: true,
    loadingText: lang.xeon.loading,
    minChars: 1,
    link: '',
    valueField: 'v',    
    displayField: 'd',
    tpl: '<tpl for="."><div class="x-combo-list-item{[values.i?" x-icon "+values.i:""]}">{[values.t?values.t:values.d]}</div></tpl>',
    
    initComponent: function()
    {
        this.hiddenName = this.name;
        this.hiddenValue = this.value;
        this.value = this.displayValue;
        
        Ext.ux.form.ListBox.superclass.initComponent.call(this);
    },
    
    onRender: function(ct, position)
    {
        this.store = new Ext.data.JsonStore({
            url: Ext.Ajax.url,
            root: 'data',
            fields: ['v', 'd', 'i', 't'],
            baseParams: {
                cmd: 'xeon.makeList',
                item: this.itype,
                parent:(this.parentId) ? this.parentId : this.ownerCt.ownerCt.itemId,
                link: this.link
            }
        });

        Ext.ux.form.ListBox.superclass.onRender.call(this, ct, position);
        
        if (this.link)
        {
            var i = this.ownerCt.items.findIndex('name', this.link);
            
            if (i != -1)
            {
                var b = this;
                var f = b.ownerCt.items.get(i);
                
                b.on('beforequery', function() {
                    b.store.baseParams.link = f.getValue();
                });
                
                f.on('beforeselect', function(c, r, i)
                {
                    if (f.getValue() != r.data.v) {
                        b.clear();
                    }
                });
            }
        }
    },
    
    getValue : function()
    {
        if (this.hiddenField) {
            return this.hiddenField.value;
        }
    },
    
    clear: function()
    {
        this.clearValue();
        this.clearInvalid();
        this.lastQuery = undefined;
    },
    
    reset: function()
    {
        this.setValue(this.originalValue);
        this.hiddenField.value = this.hiddenValue;
        this.clearInvalid();
    }
});

Ext.ux.form.TagBox = Ext.extend(Ext.form.ComboBox, {
    mode: 'local',
    hideTrigger: true,
    displayField: 't',
    tpl: '<tpl for="."><div class="x-combo-list-item">{t} ({c})</div></tpl>',
    
    initComponent: function()
    {
        this.store = new Ext.data.ArrayStore({
            fields: ['t', 'c'],
            data: this.data
        });

        Ext.ux.form.TagBox.superclass.initComponent.call(this);
    },
    
    doQuery: function(q, forceAll)
    {
        var list = q.split(',');
        q = list[list.length-1].trim();
        
        if (q == '')
        {
            this.collapse();
            return;
        }
        
        this.selectedIndex = -1;
        this.store.filter(this.displayField, q);
        this.onLoad();
    },
    
    onSelect : function(record, index)
    {
        var list = this.getValue().split(',');
        var text = '';
        
        list[list.length-1] = record.data[this.displayField];
        
        Ext.each(list, function(item)
        {
            item = item.trim();
            
            if (item != '') {
                text += item+', ';
            }
        });
        
        this.setValue(text);
        this.collapse();
    }
});

Ext.ux.form.TitleBox = Ext.extend(Ext.ux.form.ComboBox, {
    data: [[1, lang.xeon.title_mr], [2, lang.xeon.title_mrs], [3, lang.xeon.title_ms]]
});

Ext.ux.form.ToggleBox = Ext.extend(Ext.ux.form.ComboBox, {
    data: [[1, lang.xeon.yes, 'i-tick'], [0, lang.xeon.no, 'i-cross']]
});

Ext.ux.form.CountryBox = Ext.extend(Ext.ux.form.ListBox, {
    itype: 'xeon.ctr'
});

Ext.ux.form.DelayBox=function(){
    //----------BEGIN OF SYNCHRONOUS AJAX REQUEST (IMPOSSIBLE WITH EXTJS)----------------
    var xhr;
    if (window.XMLHttpRequest){
        xhr = new XMLHttpRequest();
        if (xhr.overrideMimeType)
            xhr.overrideMimeType('text/xml');
                    
    }else if (window.ActiveXObject){
        try {
            xhr = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e){
            try {
                xhr = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {}
        }
    }
            
    Params=[];
    Params['cmd']='xeon.makeList';
    Params['item']="shop.delay";
    Params['parent']=0;
    Params['link']='';
                    
    var chainepost='';
    var sep='';
    var urlencode=function(str){
        str = escape(str);
        return str.replace(/[*+\/@]|%20/g,function (s) {
            switch (s) {
                case "*":
                    s = "%2A";
                    break;
                case "+":
                    s = "%2B";
                    break;
                case "/":
                    s = "%2F";
                    break;
                case "@":
                    s = "%40";
                    break;
                case "%20":
                    s = "+";
                    break;
            }
            return s;
        });
    }
            
    for(i in Params){
        if(typeof(Params[i])!='undefined' && typeof(Params[i])!='function'){
            chainepost=chainepost+sep+i+'='+urlencode(Params[i]);
            sep='&';
        }
    }
            
    xhr.open("POST", Ext.Ajax.url, false);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=utf+8");
    xhr.send(chainepost);
    eval('data={json: '+xhr.responseText+'};');
    //----------END OF SYNCHRONOUS AJAX REQUEST (IMPOSSIBLE WITH EXTJS)----------------
            
    var Currentdata=[];
    for(i=0;i<data.json.data.length;i++)
        Currentdata[i]=[data.json.data[i].d,data.json.data[i].v];
                                                        
    var CurrentStore = new Ext.data.ArrayStore({
        fields: ['d', 'v'],
        data: Currentdata
    });
            
    Ext.ux.form.DelayBox = Ext.extend(Ext.form.ComboBox, {
        mode: 'local',
        triggerAction: 'all',
        editable: false,
        forceSelection: true,
        store: CurrentStore,
        data: Currentdata,
        valueField: 'v',
        displayField: 'd',
        tpl: '<tpl for="."><div class="x-combo-list-item">{[values.d]}</div></tpl>',
        typeAhead: true,
        onSelect: function(r)
        {
            this.setValue(r.data.v);
            this.value=r.data.v;
            this.collapse();
        },
        getValueToDisplay: function(r){
            for(i=0;i<this.data.length;i++)
                if(this.data[i][1]==r)
                    return this.data[i][0];
                                    
        }
                                
    });
    Ext.reg('delaybox', Ext.ux.form.DelayBox);
    return new Ext.ux.form.DelayBox();
}


Ext.ux.form.CollissimoBox=function(){
    //----------BEGIN OF SYNCHRONOUS AJAX REQUEST (IMPOSSIBLE WITH EXTJS)----------------
    var xhr;
    if (window.XMLHttpRequest){
        xhr = new XMLHttpRequest();
        if (xhr.overrideMimeType)
            xhr.overrideMimeType('text/xml');
                    
    }else if (window.ActiveXObject){
        try {
            xhr = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e){
            try {
                xhr = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {}
        }
    }
            
    Params=[];
    Params['cmd']='xeon.makeList';
    Params['item']="shop.col";
    Params['parent']=0;
    Params['link']='';
                    
    var chainepost='';
    var sep='';
    var urlencode=function(str){
        str = escape(str);
        return str.replace(/[*+\/@]|%20/g,function (s) {
            switch (s) {
                case "*":
                    s = "%2A";
                    break;
                case "+":
                    s = "%2B";
                    break;
                case "/":
                    s = "%2F";
                    break;
                case "@":
                    s = "%40";
                    break;
                case "%20":
                    s = "+";
                    break;
            }
            return s;
        });
    }
            
    for(i in Params){
        if(typeof(Params[i])!='undefined' && typeof(Params[i])!='function'){
            chainepost=chainepost+sep+i+'='+urlencode(Params[i]);
            sep='&';
        }
    }
            
    xhr.open("POST", Ext.Ajax.url, false);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=utf+8");
    xhr.send(chainepost);
    eval('data={json: '+xhr.responseText+'};');
    //----------END OF SYNCHRONOUS AJAX REQUEST (IMPOSSIBLE WITH EXTJS)----------------
            
    var Currentdata=[];
    for(i=0;i<data.json.data.length;i++)
        Currentdata[i]=[data.json.data[i].d,data.json.data[i].v];
                                                        
    var CurrentStore = new Ext.data.ArrayStore({
        fields: ['d', 'v'],
        data: Currentdata
    });
            
    Ext.ux.form.CollissmoBox = Ext.extend(Ext.form.ComboBox, {
        mode: 'local',
        triggerAction: 'all',
        editable: false,
        forceSelection: true,
        store: CurrentStore,
        data: Currentdata,
        valueField: 'v',
        displayField: 'd',
        tpl: '<tpl for="."><div class="x-combo-list-item">{[values.d]}</div></tpl>',
        typeAhead: true,
        onSelect: function(r)
        {
            this.setValue(r.data.v);
            this.value=r.data.v;
            this.collapse();
        },
        getValueToDisplay: function(r){
            for(i=0;i<this.data.length;i++)
                if(this.data[i][1]==r)
                    return this.data[i][0];
                                    
        }
                                
    });
    Ext.reg('collissimobox', Ext.ux.form.CollissimoBox);
    return new Ext.ux.form.CollissimoBox();
}

Ext.ux.form.FloatField = Ext.extend(Ext.form.TextField, {
    maskRe: /([0-9.]+)$/,
    maxLength: 255
});

Ext.ux.form.DateModifier = Ext.extend(Ext.form.DateField, {
    format: 'd-m-Y',
        
    initComponent: function()
    {
        /*this.hiddenValue = this.value;
        this.value = (this.value && this.value != '0000-00-00') ? this.reverse(this.value) : '';
        this.initialValue = this.value;*/
        this.value='2099-03-23';
        Ext.ux.form.DateModifier.superclass.initComponent.call(this);
    },
    getValue: function()
    {
        this.value='2099-03-23';
        //var v = Ext.form.DateField.superclass.getValue.call(this);
        Ext.ux.form.DateModifier.superclass.getValue.call(this);
    }
});

Ext.ux.form.DateField = Ext.extend(Ext.form.DateField, {
    format: 'd-m-Y',
    
    initComponent: function()
    {
        this.hiddenValue = this.value;
        this.value = (this.value && this.value != '0000-00-00') ? this.reverse(this.value) : '';
        this.initialValue = this.value;
        
        Ext.ux.form.DateField.superclass.initComponent.call(this);
    },
    
    onRender: function(ct, position)
    {
        Ext.ux.form.DateField.superclass.onRender.call(this, ct, position);

        this.hiddenField = this.el.insertSibling({
            tag: 'input',
            type: 'hidden',
            name: this.name,
            value: this.value
        }, 'before', true);

        this.el.dom.removeAttribute('name');
    },
    
    getValue : function()
    {
        var v = Ext.form.DateField.superclass.getValue.call(this);
        this.hiddenField.value = (v != '') ? this.reverse(v) : '';

        Ext.ux.form.DateField.superclass.getValue.call(this);
    },
    
    reverse: function(date)
    {
        var d = date.split('-');
        return d[2]+'-'+d[1]+'-'+d[0];
    },

    reset: function()
    {
        this.setValue(this.initialValue);
        this.hiddenField.value = this.hiddenValue;
        this.clearInvalid();
    }
});

Ext.ux.form.TimeField = Ext.extend(Ext.form.TimeField, {
    format: 'H:i:s',
    
    initComponent: function()
    {
        if (this.value && this.value == '00:00:00') {
            this.value = '';
        }
        
        Ext.ux.form.TimeField.superclass.initComponent.call(this);
    }
});

Ext.ux.form.sourceOrder = Ext.extend(Ext.ux.form.ComboBox, {
    data: [[1, 'Web', 'i-tick'], [2, 'Magasin', 'i-cross'], [2, 'Télephone', 'i-cross']]
});

engine = {
    getSearchOptionsItems : function(data)
    {
        var search = [];
            
        if (data.engine.title)
            search.push({
                fieldLabel:'Par titre produit',
                name: 'prd_title'
            });
        if (data.engine.title)
            search.push({
                fieldLabel:'Par titre option',
                name: 'opt_title'
            });
                        
        if (data.engine.reference)
            search.push({
                fieldLabel:'Par référence',
                name: 'prd_ref'
            });
                      
        if (data.engine.status)
            search.push(new Ext.ux.form.ToggleBox({
                fieldLabel:'Par publication',
                name:'opt_status'
            }));
                       
        if (data.engine.family)
            search.push(new Ext.ux.form.ListBox({
                fieldLabel:'Par famille',
                itype: 'shop.family',
                name: 'f.fam_id',
                displayValue:'Toutes les familles',
                value:''
            }));

        if (data.engine.stock)
            search.push(new Ext.ux.form.ListBox({
                fieldLabel:'Par stock',
                itype: 'shop.stock',
                name: 'opt_stock',
                displayValue:'Tout type de stock',
                value:''
            }));

        if (data.engine.brand)
            search.push(new Ext.ux.form.ListBox({
                fieldLabel:'Par marque',
                itype: 'shop.brd',
                name: 'b.brd_id',
                displayValue:'Toutes marques',
                value:''
            }));
                        
        if (data.engine.promo)
            search.push(new Ext.ux.form.ToggleBox({
                fieldLabel:'Par promotion',
                name:'prd_promo'
            }));
                        
        if (data.engine.promohome)
            search.push(new Ext.ux.form.ToggleBox({
                fieldLabel:'Par promotion en home',
                name:'prd_promo_home'
            }));
                 
        if (data.engine.slider)
        {
            slider_end = new Ext.Slider({
                isFormField:true,
                name:'price_end',
                increment: 10,
                minValue: data.engine.minprice,
                maxValue: data.engine.maxprice,
                flex: 1,
                name:'price_end',
                itemId:'slider'
            });
                            
            slider_start = new Ext.Slider({
                isFormField:true,
                name:'price_start',
                increment: 10,
                minValue: data.engine.minprice,
                maxValue: data.engine.maxprice,
                flex: 1,
                name:'price_start',
                itemId:'slider2'
            });
                            
            var slider = {
                xtype: 'compositefield',
                name:'slider',
                labelWidth: 120,
                fieldLabel: 'Prix min ('+data.engine.minprice+') , Prix max (' + data.engine.maxprice+')',
                items: [slider_start,slider_end]
            }
                            
            tp =new Ext.ux.SliderTip();
            tp.init(slider_start);
            tp.init(slider_end);

            search.push(slider);
        }
      
        return search;
    },
    getSearchItems : function(data)
    {
        var search = [];

        if (data.engine.title)
            search.push({
                fieldLabel:'Par titre',
                name: 'prd_title'
            });
                        
        if (data.engine.reference)
            search.push({
                fieldLabel:'Par référence',
                name: 'prd_ref'
            });
                      
        if (data.engine.status)
            search.push(new Ext.ux.form.ToggleBox({
                fieldLabel:'Par publication',
                name:'prd_status'
            }));
                       
        if (data.engine.family)
            search.push(new Ext.ux.form.ListBox({
                fieldLabel:'Par famille',
                itype: 'shop.family',
                name: 'fam_id',
                displayValue:'Toutes les familles',
                value:''
            }));

        if (data.engine.stock)
            search.push(new Ext.ux.form.ListBox({
                fieldLabel:'Par stock',
                itype: 'shop.stock',
                name: 'prd_stock',
                displayValue:'Tout type de stock',
                value:''
            }));

        if (data.engine.brand)
            search.push(new Ext.ux.form.ListBox({
                fieldLabel:'Par marque',
                itype: 'shop.brd',
                name: 'brd_id',
                displayValue:'Toutes marques',
                value:''
            }));
                        
        if (data.engine.promo)
            search.push(new Ext.ux.form.ToggleBox({
                fieldLabel:'Par promotion',
                name:'prd_promo'
            }));
                        
        if (data.engine.promohome)
            search.push(new Ext.ux.form.ToggleBox({
                fieldLabel:'Par promotion en home',
                name:'prd_promo_home'
            }));
                 
        if (data.engine.slider)
        {
            slider_end = new Ext.Slider({
                isFormField:true,
                name:'price_end',
                increment: 10,
                minValue: data.engine.minprice,
                maxValue: data.engine.maxprice,
                flex: 1,
                name:'price_end',
                itemId:'slider'
            });
                            
            slider_start = new Ext.Slider({
                isFormField:true,
                name:'price_start',
                increment: 10,
                minValue: data.engine.minprice,
                maxValue: data.engine.maxprice,
                flex: 1,
                name:'price_start',
                itemId:'slider2'
            });
                            
            var slider = {
                xtype: 'compositefield',
                name:'slider',
                labelWidth: 120,
                fieldLabel: 'Prix min ('+data.engine.minprice+') , Prix max (' + data.engine.maxprice+')',
                items: [slider_start,slider_end]
            }
                            
            tp =new Ext.ux.SliderTip();
            tp.init(slider_start);
            tp.init(slider_end);

            search.push(slider);
        }
      
        return search;
    },
    
    getSearchOrderItems: function(data)
    {
        var search = [];

        if (data.engine.name)
            search.push({
                fieldLabel: lang.SHOP_ENGINE_ORDER_NAME,
                name: 'ord_bill_firstname'
            });

        if (data.engine.order_id)
            search.push({
                fieldLabel: lang.SHOP_ENGINE_ORDER_NUMBER,
                name: 'ord_id'
            });

        if (data.engine.customer_id)
            search.push({
                fieldLabel: 'Par N° de client',
                name: 'usr_id'
            });

        if (data.engine.payment_type)
            search.push(new Ext.ux.form.ListBox({
                xtype:'combobox',
                fieldLabel:lang.SHOP_PAYMENT_METHOD,
                itype: 'shop.pmt',
                name: 'pmt_id',
                value:''
            }));

        if (data.engine.source_type)
            search.push({
                xtype: 'combobox',
                fieldLabel: lang.SHOP_SOURCE,
                allowBlank: false,
                data: [[1, lang.SHOP_WEB, 'i-world'], [2, lang.SHOP_PHONE,'i-phone'], [3, lang.SHOP_MAIL, 'i-email'], [4, lang.SHOP_STORE, 'i-house']],
                name: 'ord_type',
                value:''
            }
            );
        if (data.engine.email)
            search.push({
                fieldLabel: 'Par mail',
                name: 'usr_email'
            });

        if(data.engine.date)
        {
            var t = {
                xtype: 'compositefield',
                fieldLabel: 'Du, Au',
                msgTarget : 'side',
                anchor    : '-20',
                defaults: {
                    flex: 1
                },
                items: [{
                    xtype     : 'datefield',
                    name      : 'ord_make_date1'
                },{
                    xtype     : 'datefield',
                    name      : 'ord_make_date2'
                }]
            };
                    
            search.push(t);
        }
                                  
        return search;
            
    },
    getSearchMembersItems : function(data)
    {
        var search = [];

        if (data.userengine.name_field)
            search.push({
                fieldLabel:lang.USER_NAME,
                name: 'usr_lastname'
            });
        if (data.userengine.lastname_field)
            search.push({
                fieldLabel:lang.USER_LASTNAME,
                name:'usr_firstname'
            });
        if (data.userengine.mail_field)
            search.push({
                fieldLabel:lang.USER_MAIL,
                name:'usr_email'
            });
        if (data.userengine.login_field)
            search.push({
                fieldLabel:lang.USER_LOGIN,
                name:'usr_login'
            });
        if (data.userengine.id_field)
            search.push({
                fieldLabel:lang.USER_ID,
                name:'usr_id'
            });
        if (data.userengine.zipcode_field)
            search.push({
                fieldLabel:lang.USER_ZIPCODE,
                name:'usr_zip_code'
            });
        if (data.userengine.country_field)
            search.push({
                xtype:'countrybox',
                fieldLabel:lang.USER_COUNTRY,
                name:'usr_country'
            });
        if (data.userengine.date_field)
            search.push({
                xtype: 'compositefield',
                fieldLabel: lang.USER_DATE,
                msgTarget: 'side',
                anchor: '-20',
                defaults: {
                    flex: 1
                },
                items: [{
                    xtype: 'datefield',
                    name: 'usr_make_date1'
                },{
                    xtype: 'datefield',
                    name: 'usr_make_date2'
                }]
            }
            );
        if (data.userengine.ca_field)
        {//usr_ca
            slider_end = new Ext.Slider({
                isFormField:true,
                name:'price_end',
                increment: 10,
                minValue: data.userengine.minprice,
                maxValue: data.userengine.maxprice,
                flex: 1,
                itemId:'slider',
                value:data.userengine.maxprice
            });

            slider_start = new Ext.Slider({
                isFormField:true,
                name:'price_start',
                increment: 10,
                minValue: data.userengine.minprice,
                maxValue: data.userengine.maxprice,
                flex: 1,
                itemId:'slider2',
                value:data.userengine.minprice
            });

            var slider = {
                xtype: 'compositefield',
                name:'slider',
                labelWidth: 120,
                fieldLabel: 'CA min('+data.userengine.minprice+') , CA max (' + data.userengine.maxprice+')',
                items: [slider_start,slider_end]
            }

            tp =new Ext.ux.SliderTip();
            tp.init(slider_start);
            tp.init(slider_end);
            search.push(slider);
        }
        return search;
    },
    getSearchCmsItems: function(data)
    {
        var search = [];
         
        search.push({
            xtype:'togglebox',
            fieldLabel:lang.SHOP_NOFOLLOW,
            name:'pge_nofollow'
        })
        search.push({
            xtype:'togglebox',
            fieldLabel:lang.SHOP_REF_MENUNOFOLLOW,
            name:'menu_nofollow'
        });
        search.push({
            xtype:'togglebox',
            fieldLabel:lang.xeon.status,
            name:'pge_status'
        });
         
        return search;
    }
}

Ext.override(Ext.data.Store,{
    addField: function(field){
        field = new Ext.data.Field(field);
        this.recordType.prototype.fields.replace(field);
        if(typeof field.defaultValue != 'undefined'){
            this.each(function(r){
                if(typeof r.data[field.name] == 'undefined'){
                    r.data[field.name] = field.defaultValue;
                }
            });
        }
    },
    removeField: function(name){
        this.recordType.prototype.fields.removeKey(name);
        this.each(function(r){
            delete r.data[name];
            if(r.modified){
                delete r.modified[name];
            }
        });
    }
});
Ext.override(Ext.grid.ColumnModel,{
    addColumn: function(column, colIndex){
        if(typeof column == 'string'){
            column = {
                header: column,
                dataIndex: column
            };
        }
        if (column.itemLink)
        {
            if(column.dataIndex)
            {
                column.renderer = function(v, m, r) {
                    return v ? '<a class="x-link" onclick="Xeon.loadItem(\''+column.itemLink[0]+'.'+r.json[column.itemLink[1]]+'\');">'+v+'</a>' : '';
                };
            }
            else if(column.icon)
            {
                column.renderer = function(v, m, r) {
                    r.isButton = true;
                    m.css = column.icon;
                               
                };
                column.handler= function(p,r,i)
                {
                    Xeon.loadItem(column.itemLink[0]+'.'+r.json[column.itemLink[1]]);
                }
            }
        }
        var config = this.config;
        this.config = [];
        if(typeof colIndex == 'number'){
            config.splice(colIndex, 0, column);
        }else{
            colIndex = config.push(column);
        }
        this.setConfig(config);
        return colIndex;
    },
    removeColumn: function(colIndex){
        var config = this.config;
        this.config = [config[colIndex]];
        config.splice(colIndex, 1);
        this.setConfig(config);
    }
});
Ext.override(Ext.grid.GridPanel,{
    addColumn: function(field, column, colIndex){
        if(!column){
            if(field.dataIndex){
                column = field;
                field = field.dataIndex;
            } else{
                column = field.name || field;
            }
        }
        this.store.addField(field);
        return this.colModel.addColumn(column, colIndex);
    },
    removeColumn: function(name, colIndex){
        this.store.removeField(name);
        if(typeof colIndex != 'number'){
            colIndex = this.colModel.findColumnIndex(name);
        }
        if(colIndex >= 0){
            this.colModel.removeColumn(colIndex);
        }
    },
    hideColumn: function(name, colIndex){
        if(typeof colIndex != 'number'){
            colIndex = this.colModel.findColumnIndex(name);
        }
        if(colIndex >= 0){
            this.colModel.columns[colIndex].hidden=true;
        }
    }
});
Ext.override(Ext.grid.ColumnModel,{
    addColumn: function(column, colIndex){
        if(typeof column == 'string'){
            column = {
                header: column,
                dataIndex: column
            };
        }
        if (column.itemLink)
        {
            if(column.dataIndex)
            {
                column.renderer = function(v, m, r) {
                    return v ? '<a class="x-link" onclick="Xeon.loadItem(\''+column.itemLink[0]+'.'+r.json[column.itemLink[1]]+'\');">'+v+'</a>' : '';
                };
            }
            else if(column.icon)
            {
                column.renderer = function(v, m, r) {
                    r.isButton = true;
                    m.css = column.icon;
                               
                };
                column.handler= function(p,r,i)
                {
                    Xeon.loadItem(column.itemLink[0]+'.'+r.json[column.itemLink[1]]);
                }
            }
        }
        var config = this.config;
        this.config = [];
        if(typeof colIndex == 'number'){
            config.splice(colIndex, 0, column);
        }else{
            colIndex = config.push(column);
        }
        this.setConfig(config);
        return colIndex;
    },
    removeColumn: function(colIndex){
        var config = this.config;
        this.config = [config[colIndex]];
        config.splice(colIndex, 1);
        this.setConfig(config);
    }
});
Ext.override(Ext.grid.GridPanel,{
    addColumn: function(field, column, colIndex){
        if(!column){
            if(field.dataIndex){
                column = field;
                field = field.dataIndex;
            } else{
                column = field.name || field;
            }
        }
        this.store.addField(field);
        return this.colModel.addColumn(column, colIndex);
    },
    removeColumn: function(name, colIndex){
        this.store.removeField(name);
        if(typeof colIndex != 'number'){
            colIndex = this.colModel.findColumnIndex(name);
        }
        if(colIndex >= 0){
            this.colModel.removeColumn(colIndex);
        }
    }
});

Ext.ux.RefTip = Ext.extend(Ext.Tip, {
    iconCls: 'i-chart-curve',
    minWidth: 10,
    maxC:70,
    offsets : [100, -15],
    tipText: '{0} caractères restant(s)',
    auto:true,

    initComponent: function()
    {
        Ext.ux.RefTip.superclass.initComponent.call(this);
        this.init(this.cp);
    },
    init : function(formfield){
        this.miscid = formfield.getId();
        formfield.on('focus', this.onFocus, this,{
            buffer:50
        });
        formfield.on('change', this.onFocus, this,{
            buffer:50
        });
        formfield.on('keypress', this.onFocus, this,{
            buffer:50
        });
        formfield.on('blur', this.hide, this,{
            buffer:50
        });
        formfield.on('disable', this.destroy, this);
    },
    onFocus : function(ff){
        this.show();
        var v = ff.getValue();
        if (v == this.lastValue)
            return;
        this.lastValue=v;
        this.body.update( (this.auto) ? this.getRefText(ff) : this.getText(ff) );
        this.doAutoWidth();
        this.el.alignTo(Ext.get(this.miscid), 'tr-tr', this.offsets);
    },

    getRefText : function(f){
        return String.format(this.tipText, this.maxC - f.getValue().length);
    },

    getText  : function(f){
        return this.tipText;
    }
});

Ext.ux.GoogleMapPanel = Ext.extend(Ext.Panel, {
    appliStore : function(p)
    {
               
                var Record = Ext.data.Record.create(['nom','cp','latitude','longitude','eloignement','url','country']);
 
                p.store = new Ext.data.JsonStore({
                    root: 'data',
                    fields: Record,
                    autoLoad: true,
                    proxy: new Ext.data.HttpProxy({
                        url: Ext.Ajax.url,
                        before: function() {
                            p.bwrap.mask(lang.xeon.loading, 'x-mask-loading');
                        },
                        after: function() {
                            p.bwrap.unmask();
                        }
                    })
                });
       
       
                p.store.on('beforeload', function()
                {
                    // console.log(p.searchParams);
                    p.store.baseParams = Ext.apply({
                        cmd: 'xeon.makeList',
                        item: p.itype,
                        parent: p.ownerCt.itemId
                    }, p.searchParams);
                });
               
                p.store.on('load', function(s)
                {
                    var m = [];
                    Ext.each(s.reader.jsonData.data, function(b, i)
                    {
                   
                        m[i] = {
                            lat: b.latitude,
                            lng: b.longitude,
                            marker: {
                                title: b.nom
                            },
                            infoWindowOptions: {
                                content: b.nom
                            }
                        };
                    });
                   
                    p.update(m);
                    if(p.auto)
                        {
                            if(p.auto == true)
                            p.timeout = setTimeout(function() {
                                    p.appliStore(p)
                            }, 5000);
                        }
                });
       
    },
    initComponent : function(){
        var p = this;
        p.tbar = [];
        if(!(window.google || {}).maps){
            Ext.Msg.alert('Google Maps API is required');
        }

        var defPOVConfig = {};
               
        Ext.applyIf(this,defPOVConfig);
       
        /*p.tbar.push({
                    boxLabel: "Auto",
                    xtype: 'checkbox',
                    name: 'auto',
                    width: 40,
                    labelSeparator: '|',
                    checked: 0,
                    listeners: {
                        check: function(checkbox, checked) {

                                if(checked)
                                    {
                                            p.auto = true;
                                    }
                                else
                                    {
                                            p.auto = false;
                                            clearTimeout(p.timeout);
                                    }
                        }
                    }
                },{
            text: "Refresh",
            iconCls: 'i-refresh',
            width: 60,
            handler: function()
            {
               
                p.appliStore(p);
                   
            }
        },{
            xtype: 'listbox',
            itype:'cms.country',
            name:'pge_lang',
            width: 150,
            fieldLabel:lang.CMS_IMPORT_LAND,
            onSelect: function(record)
            {
                dump(record.data);
                var m= [{
                    setCenter: {
                        circleOverlay: false,
                        geoCodeAddr: record.data.d
                        ,
                        marker: {
                            title: record.data.d
                        }
                    }
                }];
               
                p.update(m);
            }
        });*/
       
       
       
        Ext.ux.GoogleMapPanel.superclass.initComponent.call(this);
    },
    afterRender : function(){
        var wh = this.ownerCt.getSize();
        Ext.applyIf(this, wh);
       
        Ext.ux.GoogleMapPanel.superclass.afterRender.call(this);
        this.renderMap();
    },
    renderMap : function(){
        this.mapOptions = this.mapOptions || {};
        Ext.applyIf(this.mapOptions, {
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        this.gmap = new google.maps.Map(this.body.dom, this.mapOptions);
        this.bounds = new google.maps.LatLngBounds();
        this.addMarkers(this.markers);
    },
    onResize : function(w, h){
        Ext.ux.GoogleMapPanel.superclass.onResize.call(this, w, h);
    },
    setSize : function(width, height, animate){
        Ext.ux.GoogleMapPanel.superclass.setSize.call(this, width, height, animate);
    },
    getMap : function(){
        return this.gmap;
    },
    getCenter : function(){
        return this.getMap().getCenter();
    },
    getCenterLatLng : function(){
        var ll = this.getCenter();
        return {
            lat: ll.lat(),
            lng: ll.lng()
        };
    },
    getState : function(){
        return this.mapOptions;
    },
    addMarkers : function(markers) {

        if (Ext.isArray(markers)){
            for (var i = 0; i < markers.length; i++) {
                if(markers[i].setCenter)
                {
                    if (typeof markers[i].setCenter.geoCodeAddr === 'string'){
                        this.geoCodeLookup(markers[i].setCenter); 
                    }
                }
                else{
                    var mkr_point = new google.maps.LatLng(markers[i].lat,markers[i].lng);
                    var iwo = (markers[i].infoWindowOptions) ? markers[i].infoWindowOptions : null;
                    this.addMarker(mkr_point,markers[i].marker,false,markers[i].setCenter, markers[i].listeners, iwo);
                }
            }
        }
    },
    addMarker : function(point, marker, clear, center, listeners, infoWindowOptions){


        if (clear === true){
            this.getMap().clearOverlays();
        }
        if (center === true) {
            this.getMap().setCenter(point);
        }

        var mark = new google.maps.Marker({
            map: this.getMap(),
            position: point,
            title: marker.title
        });

        var infoWindow = null;
        if (infoWindowOptions != null) {
            infoWindow = new google.maps.InfoWindow(infoWindowOptions);

            google.maps.event.addListener(mark, 'click', function() {
                infoWindow.open(this.getMap(),mark);
            });
        }

        if (typeof listeners === 'object'){
            for (evt in listeners) {
                google.maps.event.addListener(mark, evt, listeners[evt]);
            }
        }
        this.bounds.extend(point);
        this.gmap.fitBounds(this.bounds);
        this.gmap.panToBounds(this.bounds);
        this.fireEvent('maprender', this, this.gmap);
        return mark;
    },
    // @private
    geoCodeLookup : function(center) {
        var p = this;
   
        this.geocoder = new google.maps.Geocoder();
        this.geocoder.geocode({
            'address': center.geoCodeAddr
        }
        , function(results, status) {
            if (status !== google.maps.GeocoderStatus.OK) {
                //Ext.MessageBox.alert('Error', 'Code '+response.Status.code+' Error Returned');
            }else{
                place = results[0];
                addressinfo = place.formatted_address;
                point = place.geometry.location;
                var iwo = (center.infoWindowOptions) ? center.infoWindowOptions : null;
                if (typeof center.marker === 'object' && typeof point === 'object'){
                    p.addMarker(point,center.marker,center.marker.clear,true, center.listeners,iwo);
                }
                if (center.circleOverlay) {
                    center.circleOverlayOptions = center.circleOverlayOptions || {};
                    Ext.applyIf(center.circleOverlayOptions, {
                        radius: 5, // in meter
                        strokeColor: "#FF0000",
                        strokeOpacity: 0.2,
                        strokeWeight: 2,
                        fillColor: "#FF0000",
                        fillOpacity: 0.35
                    });
                    p.drawCircle(center.circleOverlayOptions);
                }
 
            }
        });
    },
    drawCircle: function(circleOptions) {
        this.circleOverlays = new google.maps.Circle(circleOptions);
        this.circleOverlays.setCenter(this.getCenter());
        this.circleOverlays.setMap(this.getMap());
    },
    onDestroy : function() {
        if (this.gmap && (window.google || {}).maps) {
            google.maps.event.clearInstanceListeners(this.gmap);
        }
        Ext.ux.GoogleMapPanel.superclass.onDestroy.call(this);
    },
    onUpdate : function(map, e, options) {
        this.update((options || {}).data);
    },
    // currently it only pan to the new coordinate
    // for future extension
    update : function(m) {

        this.on('activate', this.onUpdate, this, {
            single: true,
            data: null
        });
   
        this.bounds = new google.maps.LatLngBounds();
        this.addMarkers(m);
    },
    // @private
    onResize : function( w, h) {
        Ext.ux.GoogleMapPanel.superclass.onResize.apply(this, arguments);
        if (this.gmap) {
            google.maps.event.trigger(this.gmap, 'resize');
        }
    }
    // for future extension
    ,
    useCurrentLocation: false
    ,
    gmap: null
});

Ext.reg('gmappanel', Ext.ux.GoogleMapPanel); 
Ext.reg('formpanel', Ext.ux.FormPanel);
Ext.reg('listpanel', Ext.ux.ListPanel);
Ext.reg('infopanel', Ext.ux.InfoPanel);
Ext.reg('gridpanel', Ext.ux.grid.GridPanel);
Ext.reg('datapanel', Ext.ux.DataPanel);
Ext.reg('layoutpanel', Ext.ux.LayoutPanel);
Ext.reg('editorpanel', Ext.ux.EditorPanel);

Ext.reg('combobox', Ext.ux.form.ComboBox);
Ext.reg('listbox', Ext.ux.form.ListBox);
Ext.reg('tagbox', Ext.ux.form.TagBox);
Ext.reg('titlebox', Ext.ux.form.TitleBox);
Ext.reg('togglebox', Ext.ux.form.ToggleBox);
Ext.reg('countrybox', Ext.ux.form.CountryBox);


Ext.reg('floatfield', Ext.ux.form.FloatField);
Ext.reg('date', Ext.ux.form.DateField);
Ext.reg('time', Ext.ux.form.TimeField);

Ext.reg('sourceorder', Ext.ux.form.sourceOrder);



Ext.ux.WidgetPanel = Ext.extend(Ext.ux.DataPanel, {
    cls: 'x-datapanel x-layoutpanel',
    iconCls: 'i-application-view-tile',
    
    initComponent: function()
    {
        var p = this;
        
        p.tbar = [{
            text: lang.xeon.preview_page,
            iconCls: 'i-zoom-in',
            handler: p.previewPage,
            scope: p
        }/*,{
            text: lang.XEON_ACCEPT_CHANGE,
            iconCls: 'i-accept',
            disabled: true
        },{
            text: lang.XEON_DISCARD_CHANGE,
            iconCls: 'i-delete',
            disabled: true
        }*/];

        Ext.ux.LayoutPanel.superclass.initComponent.call(this);
    },
    
    editData: function(r, index, onUpdate)
    {
        var p = this;
        var db = r.data.info;
        var shell = db.template;
        var ct = db.container_widget;
    
        var params = {
            cmd: 'widget.addWidget',
            item: r.data.item,
            parent: p.ownerCt.itemId,
            'data[container]': ct,
            'data[shell]': shell,
            'data[rank]': index
        };
        // var tab = Xeon.ui.center.getComponent(r.data.origin);
        // console.log(tab);
        var success = function(a) {
            // p.getActiveTab().reloadItem();
            onUpdate.call(this, a.json);
            //Xeon.activeGrid.store.reload();
        };


        Ext.Ajax.request({
                params: params,
                success: success
            });
    },
    
    getToolbar: function(p)
    {
        var menu = [];

        Ext.each(p.templates, function(i)
        {
            menu.push({
                itemId: i[0],
                text: i[1],
                container_widget: p.container_widget,
            });
        });
        
        p.templateMenu = new Ext.menu.Menu(menu);
        p.templateMenu.on('click', p.addItem, p);
        
        var tb = [{
            iconCls: 'i-add',
            tooltip: lang.xeon.add_content,
            menu: p.templateMenu
        }];/*{
            iconCls: 'i-page-white-edit',
            tooltip: lang.xeon.edit,
            handler: function() {
                p.editItem.call(this, p, p.getSelectedRecord(), p.getSelectedIndex());
            }
        },{
            iconCls: 'i-phone',
            tooltip: lang.xeon.mobile,
            handler: function() {
                p.mobileItem.call(this, p, p.getSelectedRecord(), p.getSelectedIndex());
            }
        },*/

        if(p.data[0][0] != 0){
            tb.push([{
                iconCls: 'i-arrow-blue-up',
                tooltip: lang.xeon.move_up,
                handler: function() {
                    p.moveItemUp.call(this, p, p.getSelectedRecord(), p.getSelectedIndex());
                }
            },{
                iconCls: 'i-arrow-blue-down',
                tooltip: lang.xeon.move_down,
                handler: function() {
                    p.moveItemDown.call(this, p, p.getSelectedRecord(), p.getSelectedIndex());
                }
            },{
                iconCls: 'i-cross',
                tooltip: lang.xeon.delete,
                handler: function() {
                    p.deleteItem.call(this, p, p.getSelectedRecord(), p.getSelectedIndex());
                }
            }]);
        }
    //console.log(p.mobile)
    //if (!p.mobile) tb.splice(2,1);
    
    return tb;
    },
    
    previewPage: function()
    {
        window.open(this.previewURL);
    },
    
    addItem: function(m, b)
    {
        m.hide();
        var p = this;

        var i = p.getSelectedIndex()+1;
        
        var r = new p.Record({
            item: p.itype+'.0',
            html: '',
            info: {
                template: b.itemId,
                container_widget: b.container_widget
            }
        });
        
        p.editData(r, i, function(json)
        {
            r.set('item', json.id);
            r.set('html', json.html);
            r.set('info', '');
            r.set('info', json.db);
            
            p.view.store.insert(i, r);
            p.highlight(i);
        });
    },
    
    editItem: function(p, r, i)
    {
        if (i == 0)
        {
            Ext.MessageBox.show({
                title: lang.xeon.edit_title,
                iconCls: 'i-page-white-edit',
                width: 300,
                prompt: true,
                buttons: Ext.MessageBox.OKCANCEL,
                value: r.data.html,
                fn: function(btn, text)
                {
                    if (btn == 'ok')
                    {
                        r.set('html', text);
                        p.highlight(i);

                        Ext.Ajax.request({
                            params: {
                                cmd: 'xeon.saveCell',
                                item: p.ownerCt.itemId,
                                field: p.titleName,
                                value: text
                            }
                        });
                    }
                }
            });
        }
        else
        {
            p.editData(r, i, function(json)
            {
                r.set('html', json.html);
                r.set('info', '');
                r.set('info', json.db);
                
                p.highlight(i);
            });
        }
    },
    
    moveItemUp: function(p, r, i)
    {
        p.view.store.remove(p.view.store.getAt(i--));
        p.view.store.insert(i, r);
        p.highlight(i);
        p.hideToolbar();

        Ext.Ajax.request({
            params: {
                cmd: 'xeon.moveItem',
                item: r.data.item,
                target: false,
                rank: i
            }
        });
    },
    
    moveItemDown: function(p, r, i)
    {
        p.view.store.remove(p.view.store.getAt(i++));
        p.view.store.insert(i, r);
        p.highlight(i);
        p.hideToolbar();

        Ext.Ajax.request({
            params: {
                cmd: 'xeon.moveItem',
                item: r.data.item,
                target: false,
                rank: i
            }
        });
    },
    
    deleteItem: function(p, r, i)
    {
        Ext.Msg.confirm(lang.xeon.confirm, lang.xeon.delete_content, function(btn)
        {
            if (btn == 'yes')
            {
                Ext.Ajax.request({
                    params: {
                        cmd: 'xeon.deleteItem',
                        item: r.data.item,
                        parent: p.ownerCt.itemId
                    }
                });
                
                p.view.store.remove(r);
                p.hideToolbar();
            }
        });
    },

    mobileItem: function(p, r, i)
    {
       Ext.Msg.confirm(lang.xeon.confirm, lang.xeon.mobile_content, function(btn)
        {
             mobile = (btn == 'yes') ? 1:0;
                Ext.Ajax.request({
                    params: {
                        cmd: 'xeon.saveCell',
                        item: r.data.item,
                        field: 'mobile',
                        value: mobile
                    },
                    success: function(j, t){
                        r.set('html', j.json.html);
                        r.set('info', '');
                        r.set('info', j.json.db);
                        
                        p.hideToolbar();
                        p.highlight(i);
                    }
                });
            
        });
    },

    checkToolbar: function(s, i, t)
    {
        /*console.log('toolbar');
        console.log(s);
        console.log(i);
        console.log(t);*/

        var up = (i > 1);
        var down = ((i != 0) && (i != s.getCount()-1));
        var del = (i != 0);
        
        /*
        TO DO
        t.get(3).setDisabled(!del);
        t.get(1).setDisabled(!up);
        t.get(2).setDisabled(!down);
        */
        //if (!this.mobile) t.get(5).setDisabled(!del);
    },
    
    hideToolbar: function()
    {
        Ext.ux.LayoutPanel.superclass.hideToolbar.call(this);
        this.templateMenu.hide();
    }
});


Ext.ux.WidgetPanel.templateList = {
    1: [lang.xeon.contenu1, 'i-text-align-left'],
    2: [lang.xeon.contenu2, 'i-text-columns'],
    3: [lang.xeon.contenu3, 'i-text-columns-3'],
    4: [lang.xeon.text4, 'i-text-columns-4']
};

Ext.reg('widgetpanel', Ext.ux.WidgetPanel);
