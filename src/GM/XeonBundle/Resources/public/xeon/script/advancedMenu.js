/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * @augments
 * @description advanced top menu
 * @name advancedMenu
 * 
 */
Ext.form.ColorField = function(config){
    Ext.form.ColorField.superclass.constructor.call(this, config);
    this.on('render', this.handleRender);
};

Ext.extend(Ext.form.ColorField, Ext.form.TriggerField,  {
    /**
     * @cfg {Boolean} showHexValue
     * True to display the HTML Hexidecimal Color Value in the field
     * so it is manually editable.
     */
    showHexValue : false,
	
    /**
     * @cfg {String} triggerClass
     * An additional CSS class used to style the trigger button.  The trigger will always get the
     * class 'x-form-trigger' and triggerClass will be <b>appended</b> if specified (defaults to 'x-form-color-trigger'
     * which displays a calendar icon).
     */
    triggerClass : 'x-form-color-trigger',
	
    /**
     * @cfg {String/Object} autoCreate
     * A DomHelper element spec, or true for a default element spec (defaults to
     * {tag: "input", type: "text", size: "10", autocomplete: "off"})
     */
    // private
    defaultAutoCreate : {
        tag: "input", 
        type: "text", 
        size: "10",
        autocomplete: "off", 
        maxlength:"6"
    },
	
    /**
	 * @cfg {String} lengthText
	 * A string to be displayed when the length of the input field is
	 * not 3 or 6, i.e. 'fff' or 'ffccff'.
	 */
    lengthText: "Color hex values must be either 3 or 6 characters.",
	
    //text to use if blank and allowBlank is false
    blankText: "Must have a hexidecimal value in the format ABCDEF.",
	
    /**
	 * @cfg {String} color
	 * A string hex value to be used as the default color.  Defaults
	 * to 'FFFFFF' (white).
	 */
    defaultColor: 'FFFFFF',
	
    maskRe: /[a-f0-9]/i,
    // These regexes limit input and validation to hex values
    regex: /[a-f0-9]/i,

    //private
    curColor: 'ffffff',
	
    // private
    validateValue : function(value){
        if(!this.showHexValue) {
            return true;
        }
        if(value.length<1) {
            this.el.setStyle({
                'background-color':'#' + this.defaultColor
            });
            if(!this.allowBlank) {
                this.markInvalid(String.format(this.blankText, value));
                return false
            }
            return true;
        }
        if(value.length!=3 && value.length!=6 ) {
            this.markInvalid(String.format(this.lengthText, value));
            return false;
        }
        this.setColor(value);
        return true;
    },

    // private
    validateBlur : function(){
        return !this.menu || !this.menu.isVisible();
    },
	
    // Manually apply the invalid line image since the background
    // was previously cleared so the color would show through.
    markInvalid : function( msg ) {
        Ext.form.ColorField.superclass.markInvalid.call(this, msg);
        this.el.setStyle({
            'background-image': 'url(../lib/resources/images/default/grid/invalid_line.gif)'
        });
    },

    /**
     * Returns the current color value of the color field
     * @return {String} value The hexidecimal color value
     */
    getValue : function(){
        return this.curValue || this.defaultValue || "FFFFFF";
    },

    /**
     * Sets the value of the color field.  Format as hex value 'FFFFFF'
     * without the '#'.
     * @param {String} hex The color value
     */
    setValue : function(hex){
        Ext.form.ColorField.superclass.setValue.call(this, hex);
        this.setColor(hex);
    },
	
    /**
	 * Sets the current color and changes the background.
	 * Does *not* change the value of the field.
	 * @param {String} hex The color value.
	 */
    setColor : function(hex) {
        this.curColor = hex;
		
        this.el.setStyle( {
            'background-color': '#' + hex,
            'background-image': 'none'
        });
        if(!this.showHexValue) {
            this.el.setStyle({
                'text-indent': '-100px'
            });
            if(Ext.isIE) {
                this.el.setStyle({
                    'margin-left': '100px'
                });
            }
        }
    },
	
    handleRender: function() {
        this.setDefaultColor();
    },
	
    setDefaultColor : function() {
        this.setValue(this.defaultColor);
    },

    // private
    menuListeners : {
        select: function(m, d){
            this.setValue(d);
        },
        show : function(){ // retain focus styling
            this.onFocus();
        },
        hide : function(){
            this.focus();
            var ml = this.menuListeners;
            this.menu.un("select", ml.select,  this);
            this.menu.un("show", ml.show,  this);
            this.menu.un("hide", ml.hide,  this);
        }
    },
	
    //private
    handleSelect : function(palette, selColor) {
        this.setValue(selColor);
    },

    // private
    // Implements the default empty TriggerField.onTriggerClick function to display the ColorPicker
    onTriggerClick : function(){
        if(this.disabled){
            return;
        }
        if(this.menu == null){
            this.menu = new Ext.menu.ColorMenu();
            this.menu.palette.on('select', this.handleSelect, this );
        }
        this.menu.on(Ext.apply({}, this.menuListeners, {
            scope:this
        }));
        this.menu.show(this.el, "tl-bl?");
    }
});
Xeon.module.advancedMenu =
{

    makeOngletRoot: function(ct, item, data)
    {

        ct.add(new Ext.ux.grid.GridPanel({
            title: 'Gestion des Onglets',
            iconCls: 'i-layout',
            itype: 'advancedMenu.onglet',
            rowId: 'f_id',
            paging:  true,
            quickSearch: false,
            search : false,
            reloadAfteEdit: true,
            exportPath: false,
            columns: [{
                header: lang.XEON_ID,
                dataIndex: 'f_id',
                dataType: 'int',
                width: 60
            },{
                header: lang.XEON_TITLE,
                dataIndex: 'f_title',
                width: 200,
                itemLink: ['advancedMenu.onglet', 'f_id']
            },{
                header: "Type",
                dataIndex: 't_title',
                width: 200
            }]
        }));
        var items = [{
            xtype:'button',
            width: 200,
            text: 'Générer le css',
            name: 'fichier_excel',
            align: 'center',
            style:
            {
                marginLeft:'230px'
            },
            handler:function(p)
            {
                Ext.Ajax.request({
                    params: {
                        cmd: 'advancedMenu.css'
                    },
                    success: function(response,p) {  

                    }
                });
            }
        }];

        ct.add(new Ext.FormPanel({
            items: items
        }));
                    
    },
    makeOngletForm: function(ct, item, data)
    {
        var items=[];
        var form;
        if(item.add)
        {
            items = [{
                fieldLabel: "Titre",
                name: 'f_title',
                allowBlank: false
            },{
                xtype: 'listbox',
                fieldLabel: lang.XEON_PARENT,
                allowBlank: false,
                itype: 'advancedMenu.onglet',
                name: 't_id'
            }];
            form = new Ext.ux.FormPanel({
                items: items
            });
            ct.add(form);
        }
        else
        {
            items = [{
                fieldLabel: "Titre",
                name: 'f_title',
                allowBlank: false,
                value: data.db.f_title
            },{
                xtype: 'mediabox',
                fieldLabel: "Logo",
                name: 'f_logo',
                value: data.db.f_logo,
                displayValue: data.db.f_logo
            },{
                xtype: 'mediabox',
                fieldLabel: "Logo Hover",
                name: 'f_logo_hover',
                value: data.db.f_logo_hover,
                displayValue: data.db.f_logo_hover
            },{
                xtype: 'listbox',
                fieldLabel: lang.XEON_PARENT,
                allowBlank: false,
                itype: 'advancedMenu.onglet',
                name: 't_id',
                value: data.db.t_id,
                displayValue: data.db.t_title
            },{
                xtype: 'togglebox',
                fieldLabel: "Afficher les cms dans le footer du top menu",
                name: 'f_cms_footer_show',
                value: data.db.f_cms_footer_show
            },{
                xtype: 'listbox',
                fieldLabel: "CMS Parent Pour le footer",
                allowBlank: false,
                itype: 'cms.pge',
                name: 'pge_footer_id',
                value: data.db.pge_footer_id,
                displayValue: data.db.pge_footer_id
            }];
        
        
            var bf = new Ext.form.ColorField({
                fieldLabel: 'Couleur du texte',
                name:'f_color',
                showHexValue:true,
                defaultColor: data.db.f_color
            });
            var cf = new Ext.form.ColorField({
                fieldLabel: 'Couleur Fond de l\'onglet',
                name:'f_background',
                showHexValue:true,
                defaultColor: data.db.f_background
            });
            
            items.push({
                xtype: 'mediabox',
                fieldLabel: lang.XEON_IMAGE+" du fond",
                name: 'f_background_image',
                value: data.db.f_background_image,
                displayValue: data.db.f_background_image
            });
        
            items.push(cf);
            items.push(bf);
            
            if(data.db.t_id==6)
            {
                items.push(new Ext.form.HtmlEditor({
                    fieldLabel: lang.XEON_DESCRIPTION,
                    name:'ads_html',
                    value:data.db.ads_html,
                    height: 300
                }));
            }
            if(data.db.t_id==4)
            {
                items.push({
                    fieldLabel: "Lien",
                    name: 'f_link',
                    value: data.db.f_link
                },{
                    xtype: 'combobox',
                    data: [ ["", "Pas de cible (pour compatibilité W3C)"], ["_self", "Ouvrir dans la même frame (_self)"], ["_blank", "Ouvrir dans une nouvelle fenêtre (_blank)"], ["_top", "Ouvir dans la même fenêtre (_top)"], ["_parent", "Ouvrir dans la frame parente (_parent)"]],
                    value: data.db.f_target,
                    fieldLabel: "Cible",
                    name: 'f_target'
                },
                {
                    fieldLabel: "Titre referencement",
                    name: 'f_full_title',
                    value: data.db.f_full_title
                });
            }
            if(data.db.t_id==3)
            {
                items.push({
                    xtype: 'mediabox',
                    fieldLabel: "Image",
                    name: 'fle_id',
                    value: data.db.fle_id,
                    displayValue: data.db.fle_id
                },{
                    fieldLabel: "Lien Image",
                    name: 'f_link_image',
                    value: data.db.f_link_image
                },{
                    xtype: 'combobox',
                    data: [ ["", "Pas de cible (pour compatibilité W3C)"], ["_self", "Ouvrir dans la même frame (_self)"], ["_blank", "Ouvrir dans une nouvelle fenêtre (_blank)"], ["_top", "Ouvir dans la même fenêtre (_top)"], ["_parent", "Ouvrir dans la frame parente (_parent)"]],
                    value: data.db.f_target,
                    fieldLabel: "Cible",
                    name: 'f_target'
                });
            }
            
            if(data.db.t_id==5)
            {
                items.push({
                    xtype: 'listbox',
                    fieldLabel: "Page CMS",
                    allowBlank: false,
                    itype: 'cms.pge',
                    name: 'pge_id',
                    value: data.db.pge_id,
                    displayValue: data.db.pge_id
                },{
                    xtype: 'togglebox',
                    fieldLabel: "Afficher les logos des marques (24x24 px)",
                    name: 'f_brand_show_logo',
                    value: data.db.f_brand_show_logo
                });
            }
            
            if(data.db.t_id==1)
            {
                items.push({
                    xtype: 'listbox',
                    fieldLabel: "Catégorie",
                    allowBlank: false,
                    itype: 'compare.family',
                    name: 'fam_id',
                    displayValue: data.db.fam_id
                },{
                    xtype: 'togglebox',
                    fieldLabel: "Afficher les catégories",
                    name: 'f_show_categories',
                    value: data.db.f_show_categories
                },{
                    xtype: 'listbox',
                    fieldLabel: "Page CMS Dans le hover",
                    allowBlank: false,
                    itype: 'cms.pge',
                    name: 'pge_id',
                    value: data.db.pge_id,
                    displayValue: data.db.pge_id
                },{
                    xtype: 'togglebox',
                    fieldLabel: "Catégorie Promotion",
                    name: 'f_show_categeries_promotion',
                    value: data.db.f_show_categeries_promotion
                },{
                    xtype: 'togglebox',
                    fieldLabel: "Afficher les sous sous catégories �  droite des catégories (attention en activant ce mode d'affichage vous devez limiter le nombre de colonnes (si l'affichage des catégories est actif))",
                    name: 'f_show_subcategories',
                    value: data.db.f_show_subcategories
                });
            }
            
            if(data.db.t_id==2)
            {
                items.push({
                    xtype: 'listbox',
                    fieldLabel: "Page CMS",
                    allowBlank: false,
                    itype: 'cms.pge',
                    name: 'pge_id',
                    value: data.db.pge_id,
                    displayValue: data.db.pge_id
                });
            }
            
            
            form = new Ext.ux.FormPanel({
                items: items
            });
            
            form.buttons[0].handler = function(){
                this.ownerCt.ownerCt.submit();
                ct.reloadItem();
            };
            
            
            ct.add(form);
            
            
            if(data.db.t_id==1 || data.db.t_id==2)   
            {
                ct.add(new Ext.ux.grid.GridPanel({
                    title: 'Gestion des Colonne',
                    iconCls: 'i-layout',
                    itype: 'advancedMenu.column',
                    rowId: 'c_id',
                    paging:  true,
                    quickSearch: false,
                    search : false,
                    reloadAfteEdit: true,
                    exportPath: false,
                    columns: [{
                        header: lang.XEON_ID,
                        dataIndex: 'c_id',
                        dataType: 'int',
                        width: 60
                    },{
                        header: lang.XEON_TITLE,
                        dataIndex: 'c_title',
                        width: 200,
                        itemLink: ['advancedMenu.column', 'c_id']
                    }]
                }));
            }

        }

        
    },
    makeColumnForm: function(ct, item, data)
    {
        var items=[];
        var form;
        if(item.add)
        {
            items = [{
                fieldLabel: "Titre",
                name: 'c_title',
                allowBlank: false
            },{
                fieldLabel: "onglet id",
                name: 'f_id',
                xtype: 'hidden',
                allowBlank: false,
                value: data.db.f_id
            }];
            form = new Ext.ux.FormPanel({
                items: items
            });
            ct.add(form);
        }
        else
        {

            var bf = new Ext.form.ColorField({
                fieldLabel: 'Couleur du texte',
                name:'c_color',
                showHexValue:true,
                defaultColor: data.db.c_color
            });
            var cf = new Ext.form.ColorField({
                fieldLabel: 'Fond de la colonne',
                name:'c_background',
                showHexValue:true,
                defaultColor: data.db.c_background
            });
            
            items = [{
                fieldLabel: "Titre",
                name: 'c_title',
                allowBlank: false,
                value: data.db.c_title
            },{
                fieldLabel: "onglet id",
                name: 'f_id',
                xtype: 'hidden',
                allowBlank: false,
                value: data.db.f_id
            },cf,bf];
            form = new Ext.ux.FormPanel({
                items: items
            });
            ct.add(form);
            
            ct.add(new Ext.ux.grid.GridPanel({
                title: 'Gestion des Eléments',
                iconCls: 'i-layout',
                itype: 'advancedMenu.element',
                rowId: 'e_id',
                paging:  true,
                quickSearch: false,
                search : false,
                reloadAfteEdit: true,
                exportPath: false,
                columns: [{
                    header: lang.XEON_ID,
                    dataIndex: 'e_id',
                    dataType: 'int',
                    width: 60
                },{
                    header: lang.XEON_TITLE,
                    dataIndex: 'e_title',
                    width: 200,
                    itemLink: ['advancedMenu.element', 'e_id']
                },{
                    header: "Type",
                    dataIndex: 't_title',
                    width: 200
                }]
            }));
            
        }
    },
    makeElementForm: function(ct, item, data)
    {
        var items=[];
        var form;
        if(item.add)
        {
            items = [{
                fieldLabel: "Titre",
                name: 'e_title',
                allowBlank: false
            },{
                xtype: 'listbox',
                fieldLabel: lang.XEON_PARENT,
                allowBlank: false,
                itype: 'advancedMenu.onglet',
                name: 't_id'
            },{
                fieldLabel: "column id",
                name: 'c_id',
                xtype: 'hidden',
                allowBlank: false,
                value: data.db.c_id
            }];
            form = new Ext.ux.FormPanel({
                items: items
            });
            ct.add(form);
        }
        else
        {
            items = [{
                fieldLabel: "Titre",
                name: 'e_title',
                allowBlank: false,
                value: data.db.e_title
            },{
                fieldLabel: "column id",
                name: 'c_id',
                xtype: 'hidden',
                allowBlank: false,
                value: data.db.c_id
            },{
                xtype: 'mediabox',
                fieldLabel: "Logo",
                name: 'e_logo',
                value: data.db.e_logo,
                displayValue: data.db.e_logo
            },{
                xtype: 'listbox',
                fieldLabel: lang.XEON_PARENT,
                allowBlank: false,
                itype: 'advancedMenu.onglet',
                name: 't_id',
                value: data.db.t_id,
                displayValue: data.db.t_title
            }];
        
        
            var bf = new Ext.form.ColorField({
                fieldLabel: 'Couleur du texte',
                name:'e_color',
                showHexValue:true,
                defaultColor: data.db.e_color
            });

            items.push(bf);
            
            if(data.db.t_id==6)
            {
                items.push(new Ext.form.HtmlEditor({
                    fieldLabel: lang.XEON_DESCRIPTION,
                    name:'ads_html',
                    value:data.db.ads_html,
                    height: 300
                }));
            }
            if(data.db.t_id==4)
            {
                items.push({
                    fieldLabel: "Lien",
                    name: 'e_link',
                    allowBlank: false,
                    value: data.db.e_link
                },{
                    xtype: 'combobox',
                    data: [ ["", "Pas de cible (pour compatibilité W3C)"], ["_self", "Ouvrir dans la même frame (_self)"], ["_blank", "Ouvrir dans une nouvelle fenêtre (_blank)"], ["_top", "Ouvir dans la même fenêtre (_top)"], ["_parent", "Ouvrir dans la frame parente (_parent)"]],
                    value: data.db.e_target,
                    fieldLabel: "Cible",
                    name: 'e_target'
                });
            }
            if(data.db.t_id==3)
            {
                items.push({
                    xtype: 'mediabox',
                    fieldLabel: "Image",
                    name: 'fle_id',
                    value: data.db.fle_id,
                    displayValue: data.db.fle_id
                },{
                    fieldLabel: "Lien Image",
                    name: 'e_link_image',
                    allowBlank: false,
                    value: data.db.e_link_image
                },{
                    xtype: 'combobox',
                    data: [ ["", "Pas de cible (pour compatibilité W3C)"], ["_self", "Ouvrir dans la même frame (_self)"], ["_blank", "Ouvrir dans une nouvelle fenêtre (_blank)"], ["_top", "Ouvir dans la même fenêtre (_top)"], ["_parent", "Ouvrir dans la frame parente (_parent)"]],
                    value: data.db.e_target,
                    fieldLabel: "Cible",
                    name: 'e_target'
                });
            }
            
            if(data.db.t_id==1)
            {
                items.push({
                    xtype: 'listbox',
                    fieldLabel: "Catégorie",
                    allowBlank: false,
                    itype: 'compare.family',
                    name: 'fam_id',
                    displayValue: data.db.fam_id
                },{
                    xtype: 'togglebox',
                    fieldLabel: "Catégories Promotion",
                    name: 'e_show_categeries_promotion',
                    value: data.db.e_show_categeries_promotion
                },{
                    xtype: 'togglebox',
                    fieldLabel: "Afficher les sous catégories",
                    name: 'e_show_subcategories',
                    value: data.db.e_show_subcategories
                });
            }
            
            if(data.db.t_id==2)
            {
                items.push({
                    xtype: 'listbox',
                    fieldLabel: "Page CMS",
                    allowBlank: false,
                    itype: 'cms.pge',
                    name: 'pge_id',
                    value: data.db.pge_id,
                    displayValue: data.db.pge_id
                },{
                    xtype: 'togglebox',
                    fieldLabel: "Afficher les sous pages",
                    name: 'e_show_subpages',
                    value: data.db.e_show_subpages
                });
            }
            
            form = new Ext.ux.FormPanel({
                items: items
            });
            
            form.buttons[0].handler = function(){
                this.ownerCt.ownerCt.submit();
                ct.reloadItem();
            };
            
            ct.add(form);
            
        }
  
    },
    makeOptionForm: function(ct, item, data)
    {
        var form;
        var items=[];
        if(item.id==1)
        {
            items .push({
                xtype: 'mediabox',
                fieldLabel: "Image",
                name: 'fle_id',
                value: data.db.fle_id,
                displayValue: data.db.fle_id
            },{
                fieldLabel: "Titre de lien",
                name: 'o_title',
                allowBlank: false,
                value: data.db.o_title
            },{
                fieldLabel: "lien",
                name: 'o_link',
                allowBlank: false,
                value: data.db.o_link
            },{
                xtype: 'combobox',
                data: [ ["", "Pas de cible (pour compatibilité W3C)"], ["_self", "Ouvrir dans la même frame (_self)"], ["_blank", "Ouvrir dans une nouvelle fenêtre (_blank)"], ["_top", "Ouvir dans la même fenêtre (_top)"], ["_parent", "Ouvrir dans la frame parente (_parent)"]],
                value: data.db.o_target,
                fieldLabel: "Cible",
                name: 'o_target'
            });
            items.push(new Ext.form.HtmlEditor({
                fieldLabel: lang.XEON_DESCRIPTION,
                name:'o_description',
                value:data.db.o_description,
                height: 300
            }));
        }
        else
        {
            items.push({
                xtype: 'listbox',
                fieldLabel: "Page CMS",
                allowBlank: false,
                itype: 'cms.pge',
                name: 'pge_id',
                value: data.db.pge_id,
                displayValue: data.db.pge_id
            });
        }
        form = new Ext.ux.FormPanel({
            items: items
        });
        ct.add(form);


    }
}