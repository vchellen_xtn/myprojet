/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * @author: Mustapha CHELH
 * @version: 0.1
 * @description: récupération des flux 
 */

Xeon.module.flow =
{
    allowDrop: function(tree, node, target) {
        return (node.parentNode.getDepth() == target.getDepth());
    },
    makeFlowRoot: function(ct, item, data)
    {

    },
    makeFlowForm: function(ct, item, data)
    {
        
        if(item.add)
        { 
            if(data.db)
                {
                    var form  =  new Ext.ux.FormPanel({
                        title: data.db.spl_name,
                        iconCls:'i-box',
                        items: [

                        {
                            name:'flow_url',
                            fieldLabel:"Url flux",
                            allowBlank: false,
                            value:""
                        },
                        {
                            name:'spl_id',
                            fieldLabel:"Supplier",
                            xtype: 'hidden',
                            allowBlank: false,
                            value:data.db.spl_id
                        },
                        {
                            xtype: 'listbox',
                            fieldLabel: lang.FLOW_MERCHANT,
                            itype: 'flow.mct.'+data.db.spl_id,
                            name: 'mct_id',
                            allowBlank: false,
                            value: data.db.mct_id,
                            displayValue: data.db.mct_name
                        },
                        {
                            xtype: 'listbox',
                            fieldLabel: lang.FLOW_LINK_TYPE,
                            itype: 'flow.ext',
                            allowBlank: false,
                            name: 'type_id'
                        },
                        {
                            xtype: 'togglebox',
                            fieldLabel: lang.FLOW_LINK_EXTERNE,
                            allowBlank: false,
                            name: 'flow_is_external',
                            value: 1 
                        },
                        {
                            xtype: 'togglebox',
                            fieldLabel: lang.FLOW_LINK_PROMOTION,
                            allowBlank: false,
                            name: 'flow_is_promotion',
                            value: 0 
                        },
                        {
                            xtype: 'togglebox',
                            fieldLabel: lang.XEON_STATUS,
                            allowBlank: false,
                            name: 'flow_status',
                            value: 1
                        }

                        ]

                    });
                    ct.add(form);
                }
                else
                    {
                        var form = new Ext.ux.InfoPanel({
                                width: '210px',
                                height: '400px',
                                collapsible: true,
                                html: lang.FLOW_ERROR_ADD,
                                icon: Ext.MessageBox.WARNING,
                                buttons: Ext.MessageBox.OK
                        });
                        
                        ct.add(form);
                    }
               
            
            
        }
        else
        {     
            var columns = [];
            columns.push(
            {
                header: 'id',
                dataIndex: 'flow_id',
                width: 50
            });
        
            columns.push(
            {
                header: lang.FLOW_LINK,
                dataIndex: 'flow_url',
                width: 170,
                itemLink: ['flow.id', 'flow_id']
            });
        
            columns.push(
            {
                header: lang.FLOW_LINK_TYPE,
                dataIndex: 'type_value',
                width: 100
            });

            columns.push(
            {
                header: lang.XEON_STATUS,
                dataIndex: 'flow_status',
                width: 50
            });
            
            columns.push(
            {
                header: lang.FLOW_MERCHANT,
                dataIndex: 'mct_name',
                width: 120
            });
            
            columns.push(
            {
                header: lang.FLOW_LINK_PROMOTION,
                dataIndex: 'flow_is_promotion',
                width: 150
            });
        
            columns.push(
            {
                header: lang.FLOW_DATE_ADD,
                dataIndex: 'flow_date',
                width: 130
            });
        
            // console.log(data.engine);
            ct.add(new Ext.ux.grid.GridPanel({
                title: "Liste des flux",
                iconCls: 'i-box',
                itype: 'flow.flow',
                rowId: 'flow_id',
                paging: true,
                quickSearch: false,
                search: false,
                columns: columns
            }));
        }
    }
    ,
    makeIdForm: function(ct, item, data)
    {
        
        ct.add(new Ext.ux.FormPanel({
            title: "Lien",
            iconCls:'i-note',
            items: [
            {
                name:'flow_url',
                fieldLabel:"Url flux",
                allowBlank: false,
                value: data.db.flow_url
            },
            {
                xtype: 'listbox',
                fieldLabel: lang.FLOW_SUPPLIER,
                itype: 'flow.flow',
                name: 'spl_id',
                allowBlank: false,
                value: data.db.spl_id,
                displayValue: data.db.spl_name
            },
            {
                xtype: 'listbox',
                fieldLabel: lang.FLOW_LINK_TYPE,
                itype: 'flow.ext',
                allowBlank: false,
                name: 'type_id',
                value: data.db.type_id,
                displayValue: data.db.type_value
            },
            {
                xtype: 'listbox',
                fieldLabel: lang.FLOW_MERCHANT,
                itype: 'flow.mct',
                name: 'mct_id',
                allowBlank: false,
                value: data.db.mct_id,
                displayValue: data.db.mct_name
            },
            {
                xtype: 'togglebox',
                fieldLabel: lang.FLOW_LINK_EXTERNE,
                allowBlank: false,
                name: 'flow_is_external',
                value: data.db.flow_is_external
            },
            {
                xtype: 'togglebox',
                fieldLabel: lang.FLOW_LINK_PROMOTION,
                allowBlank: false,
                name: 'flow_is_promotion',
                value: 0 
            },
            {
                xtype: 'togglebox',
                fieldLabel: lang.XEON_STATUS,
                allowBlank: false,
                name: 'flow_status',
                value: data.db.flow_status
            }
                
            ]
        }));
        
    }
}