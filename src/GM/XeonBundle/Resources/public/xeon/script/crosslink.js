
//
// Interface cms v3.0.11
// Auteur : Sylvain Durozard
//

Xeon.module.crosslink =
{
	allowDrop: function(tree, node, target) {
		return true;
	},
	
	makeKeywordRoot: function(ct, item, data)
	{						
		//grille des kwd liés aux pages cms
		ct.add(new Ext.ux.grid.GridPanel({
			title: lang.CROSSLINK_PAGE_KEYWORDS_LIST,
			iconCls: 'i-page-white-stack',
			itype: 'crosslink.kwrd',
			rowId: 'kwrd_id',
			paging:  true,
			quickSearch: true,
			columns: [{
				header: lang.XEON_TITLE,
				dataIndex: 'kwrd_text',
				width: 200,
				itemLink: ['crosslink.kwrd', 'kwrd_id']
			},{
				header: lang.CROSSLINK_LINKED_PAGE,
				dataIndex: 'pge_title',
				width: 150,
				itemLink: ['cms.pge' , 'id']
			},
			{
				header: lang.CROSSLINK_LINKED_PRODUCT,
				dataIndex: 'prd_title',
				width: 150,
				itemLink: ['shop.prd' , 'id']
			},
			{
				header: lang.XEON_LAST_EDIT,
				dataIndex: 'kwrd_edit_date',
				width: 200
			},{
				header: lang.XEON_STATUS,
				dataIndex: 'kwrd_status',
				width: 60,
				renderer: Xeon.renderStatus,
				editor: new Ext.ux.form.ToggleBox()
			}]
		}));	

		

	},	
	
	makeKeywordproductRoot: function(ct, item, data)
	{

		//grille des kwd liés aux pages produit
		ct.add(new Ext.ux.grid.GridPanel({
			title: lang.CROSSLINK_PRODUCT_KEYWORDS_LIST,
			iconCls: 'i-page-white-stack',
			itype: 'crosslink.kwrdP',
			rowId: 'kwrd_id',
			paging:  true,
			quickSearch: true,
			columns: [{
				header: lang.XEON_TITLE,
				dataIndex: 'kwrd_text',
				width: 200,
				itemLink: ['crosslink.kwrdP', 'kwrd_id']
			},
			{
				header: lang.CROSSLINK_LINKED_PAGE,
				dataIndex: 'pge_title',
				width: 150,
				itemLink: ['cms.pge', 'id']
			},
			{
				header: lang.CROSSLINK_LINKED_PRODUCT,
				dataIndex: 'prd_title',
				width: 150,
				itemLink: ['shop.prd', 'id']
			},{
				header: lang.XEON_LAST_EDIT,
				dataIndex: 'kwrd_edit_date',
				width: 200
			},{
				header: lang.XEON_STATUS,
				dataIndex: 'kwrd_status',
				width: 60,
				renderer: Xeon.renderStatus,
				editor: new Ext.ux.form.ToggleBox()
			}]
		}));

	},

	makeKeywordForm: function(ct, item, data)
	{
		var items = [];
		var kwrd_text = data.db ? data.db.kwrd_text : '';
		var prd_id = data.db ? data.db.id : '';
		var fam_id = data.db ? data.db.id : '';
		var pge_id = data.db ? data.db.id : '';
		//var kwrd_url_link = data.db ? data.db.kwrd_url_link : '';
		var pge_title = data.db ? data.db.pge_title : '';
		var prd_title = data.db ? data.db.prd_title : '';
		var fam_title = data.db ? data.db.fam_title : '';
		var kwrd_status = data.db ? data.db.kwrd_status : 0;
	  	  
		items.push({
			fieldLabel: lang.CROSSLINK_KEYWORD,
			allowBlank: false,
			name: 'kwrd_text',
			value: kwrd_text
		});		
		
		items.push({
				xtype: 'combobox',
				fieldLabel: lang.CROSSLINK_TYPE_LINK,
				data: [[0,''],[1, lang.CROSSLINK_PAGE], [2, lang.CROSSLINK_PRODUCT]],
				tpl: '<tpl for="."><div class="x-combo-list-item">{[values.d == "" ? "&nbsp;" : values.d]}</div></tpl>',
				value:(data.db.kwrd_type_link== 3)?'Produit':data.db.kwrd_type_link,
				allowBlank: true,
				listeners:
				{
					select: function(){
						document.getElementsByName("kwrd_type_link")[0].value;
						
						Ext.Ajax.request({
							params: {
								cmd: 'crosslink.update_type_kwrd',
								id : item.id,
								type_id : document.getElementsByName("kwrd_type_link")[0].value
							},
							success: Ext.emptyFn
						});
						
						
					}
				},
				name: 'kwrd_type_link'
			});
						
		if (item.add)
		{
			ct.add(new Ext.ux.FormPanel({
				items: items
			}));
		}
		else
		{
			if(data.db.kwrd_type_link == '1')
			{
				items.push({
					xtype: 'listbox',
					fieldLabel: lang.CROSSLINK_LINKED_PAGE,
					allowBlank: true,
					itype: 'cms.pgeTitleOrId',
					name: 'id',
					value: pge_id,
					displayValue: pge_title
				});
			}else if(data.db.kwrd_type_link == '2' || data.db.kwrd_type_link == '3')
			{
				items.push({
					xtype: 'listbox',
					fieldLabel: lang.CROSSLINK_LINKED_FAMILY,
					allowBlank: true,
					itype: 'shop.famTitleOrId',
					name: 'idfam',
					value: data.db.id,
					displayValue: data.db.fam_title,
					tpl: '<tpl for="."><div class="x-combo-list-item">{[values.d == "" ? "&nbsp;" : values.d]}</div></tpl>'
				});
				
				items.push({
					xtype: 'listbox',
					fieldLabel: lang.CROSSLINK_LINKED_PRODUCT,
					allowBlank: true,
					itype: 'shop.prdTitleOrId',
					link: 'idfam',
					name: 'idprd',
					value: (data.db.kwrd_type_link == '3')?'':data.db.id,
					displayValue: (data.db.kwrd_type_link == '3')?'':prd_title
				});
				
			}
		
			items.push({
				xtype: 'togglebox',
				fieldLabel: lang.XEON_STATUS,
				allowBlank: false,
				name: 'kwrd_status',
				value: kwrd_status
			});
			
			ct.add(new Ext.ux.FormPanel({
				title: lang.CMS_PAGE,
				iconCls: ct.iconCls,
				items: items
			}));
			
			//Blacklist cms
			ct.add(new Ext.ux.grid.GridPanel({
				title: lang.CROSSLINK_BLACKLIST_CMS,
				iconCls: 'i-page-white-stack',
				itype: 'crosslink.blackcms',
				rowId: 'black_id',
				paging:  true,
				quickSearch: true,
				columns: [{
					header: lang.CMS_PAGE,
					dataIndex: 'pge_title',
					width: 600,
					itemLink: ['cms.pge', 'pge_id']
				}]
			}));
			
			//Blacklist shop
			ct.add(new Ext.ux.grid.GridPanel({
				title: lang.CROSSLINK_BLACKLIST_SHOP,
				iconCls: 'i-page-white-stack',
				itype: 'crosslink.blackshop',
				rowId: 'black_id',
				paging:  true,
				quickSearch: true,
				columns: [{
					header: lang.CMS_PAGE,
					dataIndex: 'prd_title',
					width: 600,
					itemLink: ['shop.prd', 'prd_id']
				}]
			}));

			ct.add(new Ext.ux.InfoPanel({
				title: lang.XEON_INFO,
				iconCls: 'i-information',
				success: function() {
						ct.reloadItem();
					},
				items: [{
					infoLabel: lang.XEON_ID,
					value: data.db.kwrd_id
				},{
					infoLabel: lang.CROSSLINK_LINKED_PAGE_ID,
					value: data.db.id
				},{
					infoLabel: lang.CROSSLINK_LINKED_PAGE_URL,
					value: data.info.url,
					renderer: Xeon.renderLink
				},{
					infoLabel: lang.XEON_MAKE_DATE,
					value: data.info.make
				},{
					infoLabel: lang.XEON_LAST_EDIT,
					value: data.info.edit
				}]
			}));
			
		}
	},	
	
	makeBlacklistcmsForm: function(ct, item, data)
	{
		ct.add(new Ext.ux.FormPanel({
			items: [{
					xtype: 'listbox',
					fieldLabel: lang.CMS_PAGE,
					allowBlank: true,
					itype: 'cms.pgeTitleOrId',
					name: 'id'
				},
				{
					xtype: 'hidden',
					name: 'cross_id',
					value:data.id
				}]
		}));
	},
	
	makeBlacklistshopForm: function(ct, item, data)
	{
		ct.add(new Ext.ux.FormPanel({
			items: [{
						xtype: 'listbox',
						fieldLabel: lang.SHOP_FAMILY,
						allowBlank: true,
						itype: 'shop.famTitleOrId',
						name: 'fam_id',
						tpl: '<tpl for="."><div class="x-combo-list-item">{[values.d == "" ? "&nbsp;" : values.d]}</div></tpl>'
					},
					{
						xtype: 'listbox',
						fieldLabel: lang.SHOP_PRODUCT,
						allowBlank: true,
						itype: 'shop.prdTitleOrId',
						link: 'fam_id',
						name: 'id'
					},
					{
						xtype: 'hidden',
						name: 'cross_id',
						value:data.id
					}]
		}));
	},
	
	
	makeKeywordproductForm: function(ct, item, data)
	{
		this.makeKeywordForm(ct, item, data);
	},
	

	makeModuleForm: function(ct, item, data)
	{
		ct.add(new Ext.ux.FormPanel({
			title: lang.XEON_SETTINGS,
			iconCls: 'i-cog',
			items: [
			{
				fieldLabel: lang.CROSSLINK_NB_CMS,
				name: 'nb_cms',
				value: data.cfg.nb_cms
			},
			{
				fieldLabel: lang.CROSSLINK_NB_SHOP,
				name: 'nb_shop',
				value: data.cfg.nb_shop
			}
			]
		}));
	}
}