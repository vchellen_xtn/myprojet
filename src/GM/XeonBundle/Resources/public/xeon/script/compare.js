//
// Interface compare v2.1.1
// Auteur : Sylvain Durozard
//
Xeon.module.compare =
{
        
    allowDrop: function(tree, node, target) {
        return (node.parentNode.getDepth() == target.getDepth());
    },
    exportOrder: function( cmd, order )
    {
        Ext.Ajax.request({
            params: {
                cmd: cmd,
                order_id: order
            },

            success: function( result ){
                Xeon.getFile();
            }
        });
    },
    myRenderDate: function(v){
        var d = Date.parseDate(v, 'Y-m-d');
        if (!d || v.substr(0, 4) == '0000') return lang.COMPARE_NONE;
        return d.format('d-m-Y H:i:s');
    },
    renderWeight: function(v) {
        return (v < 1) ? (v*1000)+' g' : v+' kg';
    },

    renderWeightG: function(v) {
        return v+' g';
    },
    renderQuantity: function(v) {
        return (v > 1) ? v+' '+lang.COMPARE_PIECES : v+' '+lang.COMPARE_PIECE;
    },

    renderPrice1: function(v) {
        return (v != 0) ? v+' &euro;' : lang.COMPARE_FREE;
    },

    renderPrice2: function(v) {
        return (v != 0) ? v+' &euro;' : lang.XEON_NO;
    },

    renderDiscountType: function(v) {
        return (v == 1) ? lang.COMPARE_AMOUNT : lang.COMPARE_PERCENTAGE;
    },

    renderDiscountValue: function(v, m, r) {
        return (r.data.dsc_type == 1) ? v+' &euro;' : v+' %';
    },
	
    renderOrderStep: function(v)
    {
        if (v == 1) return lang.COMPARE_ESTIMATE;
        if (v == 2) return lang.COMPARE_PAYMENT_TODO;
        if (v == 3) return lang.COMPARE_PROCESS_TODO;
        if (v == 4) return lang.COMPARE_COMPLETED;
        if (v == 5) return lang.COMPARE_CANCELLED;
    },
	
    renderOrderSource: function(v)
    {
        if (v == 1) return lang.COMPARE_WEB;
        if (v == 2) return lang.COMPARE_PHONE;
        if (v == 3) return lang.COMPARE_MAIL;
        if (v == 4) return lang.COMPARE_STORE;
    },

    makeProductRoot: function(ct, item, data)
    {
        var columns = [];
        
        columns.push({
            header: lang.COMPARE_PRODUCT_ID,
            dataIndex: 'prd_id',
            width: 50,
            itemLink: ['compare.prd', 'prd_id']
        });
            
        if (data.brd)
        {
            columns.push({
                header: lang.COMPARE_BRAND,
                dataIndex: 'brd_title',
                width: 100,
                itemLink: ['compare.brd', 'brd_id']
            });
        }
        
        
            
        // add parent cat field
        columns.push({
            header:  'Categorie',
            dataIndex: 'fam_parent_text',
            width: 100,
            itemLink: ['compare.fam', 'fam_parent']
        });

        //if (data.sub_cat)
        //{
        columns.push({
            header:'Sous-Cat',
            dataIndex: 'fam_title',
            width: 100,
            itemLink: ['compare.fam', 'fam_id']
        });
        //}
      
        columns.push({
            header: lang.XEON_TITLE,
            dataIndex: 'prd_title',
            width: 150,
            itemLink: ['compare.prd', 'prd_id']
        });


        if (data.opt)
            columns.push({
                header: lang.XEON_OPT_TITLE,
                dataIndex: 'opt_title',
                width: 150,
                itemLink: ['compare.opt', 'opt_id']
            });

        columns.push(
        {
            header: 'Nouveautes home',
            dataIndex: (data.opt) ? 'opt_new_home' : 'prd_new_home',
            width: 100,
            renderer: this.renderYesorNo,
            editor: new Ext.ux.form.ToggleBox()
        },{
            header: 'Promo home',
            dataIndex: (data.opt) ? 'opt_promo_home' : 'prd_promo_home',
            width: 100,
            renderer: this.renderYesorNo,
            editor: new Ext.ux.form.ToggleBox()
        },{
            header: 'Coup de coeur',
            dataIndex: (data.opt) ? 'opt_flash':'prd_flash',
            width: 100,
            renderer: this.renderYesorNo,
            editor: new Ext.ux.form.ToggleBox()
        },{
            header: 'Top vente Home',
            dataIndex: (data.opt) ? 'opt_topsale_home' : 'prd_topsale_home',
            width: 100,
            renderer: this.renderYesorNo,
            editor: new Ext.ux.form.ToggleBox()
        },{
            header: 'Top budget home',
            dataIndex: (data.opt) ? 'opt_topbudget_home' : 'prd_topbudget_home',
            width: 100,
            renderer: this.renderYesorNo,
            editor: new Ext.ux.form.ToggleBox()
        },{
            header: 'Publier',
            dataIndex: (data.opt) ? 'opt_publish' : 'prd_publish',
            width: 60,
            renderer: this.renderYesorNo,
            editor: new Ext.ux.form.ToggleBox()
        });
                    
        columns.push({
            header: lang.XEON_STATUS,
            dataIndex: 'prd_status',
            width: 60,
            renderer: this.renderYesorNo,
            editor: new Ext.ux.form.ToggleBox()
        });
        
        // console.log(data.engine);
        ct.add(new Ext.ux.grid.GridPanel({
            title: lang.COMPARE_PRODUCT_LIST,
            search : engine.getSearchOptionsItems(data),
            optionCell:data.opt, // if set to true, change the itype to discard changes
            iconCls: 'i-database',
            itype: 'compare.prd',
            rowId: (data.opt) ? 'opt_id':'prd_id',
            paging: true,
            quickSearch: true,
            columns: columns,
            redirectAfterDelete: data.redirect,
            configRedirectionAfterDelete: {
                grantFreeInput: true,
                textFreeInput: lang.XEON_FREE_INPUT_ID,
                choices: [{
                    text: lang.COMPARE_HOME,
                    value: '0'
                },{
                    text: lang.COMPARE_FAMILLE_MERE,
                    value: 'famille'
                }]
            }
        }));
		
        if (data.tag)
        {
            ct.add(new Ext.ux.grid.GridPanel({
                title: lang.XEON_TAG_LIST,
                iconCls: 'i-tag-blue',
                itype: 'xeon.tag',
                rowId: 'tag_id',
                reloadAfterEdit: true,
                quickSearch: true,
                search:false,
                columns: [{
                    header: lang.XEON_TAG,
                    dataIndex: 'tag_text',
                    width: 200,
                    editor: new Ext.form.TextField({
                        allowBlank: false
                    })
                },{
                    header: lang.XEON_USAGE,
                    dataIndex: 'tag_count',
                    dataType: 'int'
                }]
            }));
        }
		
        if (data.com)
        {
            ct.add(new Ext.ux.AvisPanel({
                title: lang.XEON_COMMENT_WAITING,
                iconCls: 'i-comments',
                data: data.com,
                root: true
            }));
        }
    },

    makeInformationForm: function(ct, item, data)
    {
        var media = new Ext.BoxComponent({
            autoEl: {
                tag: 'img',
                src: data.db.url,
                style:"border:1px solid black;"
            },
            style:"position: absolute; top: 10px; left: 510px;"
        });
                        
        ct.add(new Ext.ux.FormPanel({
            boxComponentId:media.id,
            title:lang.COMPARE_ADRESSPRO,
            iconCls:'i-note',
            items:[
            {
                name:'name',
                fieldLabel:lang.COMPARE_NAME,
                value:data.db.name
            },media,
            {
                xtype:'mediabox',
                name:'logo',
                fieldLabel:lang.COMPARE_LOGO,
                value:data.db.logo,
                displayValue: data.db.logotitle
            },
            {
                xtype:'textarea',
                name:'adress',
                fieldLabel:lang.COMPARE_ADRESS,
                value:data.db.adress
            },
            {
                name:'zipcode',
                fieldLabel:lang.COMPARE_ZIPCODE,
                value:data.db.zipcode
            },
            {
                name:'town',
                fieldLabel:lang.COMPARE_TOWN,
                value:data.db.town
            },
            {
                name:'tel1',
                fieldLabel:lang.COMPARE_TEL1,
                value:data.db.tel1
            },
            {
                name:'tel2',
                fieldLabel:lang.COMPARE_TEL2,
                value:data.db.tel2
            },{
                name:'ntva',
                fieldLabel:lang.COMPARE_TVAINTRA,
                value:data.db.tva
            },
            {
                name:'siret',
                fieldLabel:lang.COMPARE_SIRET,
                value:data.db.siret
            }
            ]
        }))
          
    },
        
    makeFamilyForm: function(ct, item, data)
    {
          
        var items = [];
        var columns = [];

        columns.push({
            header: lang.COMPARE_PRODUCT_ID,
            dataIndex: 'prd_id',
            width: 50,
            itemLink: ['compare.prd', 'prd_id']
        });
            
        //add brand field
        if (data.brd)
        {
            columns.push({
                header: lang.COMPARE_BRAND,
                dataIndex: 'brd_title',
                width: 50,
                itemLink: ['compare.brd', 'brd_id']
            });
        }
                
        // add catgeorie field
        columns.push({
            header: 'Categorie',
            dataIndex: 'fam_title',
            width: 100,
            itemLink: ['compare.fam', 'fam_id']
        });
		
        // add parent cat field
        if (data.sub_cat)
        {
            columns.push({
                header: 'Sous-Cat',
                dataIndex: 'fam_parent_text',
                width: 100
            });
        }
                
        columns.push({
            header: lang.XEON_TITLE,
            dataIndex: 'prd_title',
            width: 150,
            itemLink: ['compare.prd', 'prd_id']
        });

        if (data.opt)
            columns.push({
                header: lang.XEON_OPT_TITLE,
                dataIndex: 'opt_title',
                width: 150,
                itemLink: ['compare.opt', 'opt_id']
            });
                
        columns.push({
            header: 'Nouveautes home',
            dataIndex: (data.opt) ? 'opt_new_home' : 'prd_new_home',
            width: 100,
            renderer: this.renderYesorNo,
            editor: new Ext.ux.form.ToggleBox()
        },{
            header: 'Promo home',
            dataIndex: (data.opt) ? 'opt_promo_home' : 'prd_promo_home',
            width: 100,
            renderer: this.renderYesorNo,
            editor: new Ext.ux.form.ToggleBox()
        },{
            header: 'Coup de coeur',
            dataIndex: (data.opt) ? 'opt_flash':'prd_flash',
            width: 100,
            renderer: this.renderYesorNo,
            editor: new Ext.ux.form.ToggleBox()
        },{
            header: 'Top vente Home',
            dataIndex: (data.opt) ? 'opt_topsale_home' : 'prd_topsale_home',
            width: 100,
            renderer: this.renderYesorNo,
            editor: new Ext.ux.form.ToggleBox()
        },{
            header: 'Top budget home',
            dataIndex: (data.opt) ? 'opt_topbudget_home' : 'prd_topbudget_home',
            width: 100,
            renderer: this.renderYesorNo,
            editor: new Ext.ux.form.ToggleBox()
        },{
            header: 'Publier',
            dataIndex: (data.opt) ? 'opt_publish' : 'prd_publish',
            width: 60,
            renderer: this.renderYesorNo,
            editor: new Ext.ux.form.ToggleBox()
        });
        

        if (item.add)
        {
            items.push({
                fieldLabel: lang.XEON_TITLE,
                allowBlank: false,
                name: 'fam_title',
                value: data.db.fam_title
            });

            items.push({
                xtype: 'listbox',
                fieldLabel: lang.XEON_PARENT,
                itype: 'compare.fam',
                name: 'fam_parent',
                itemLink: ['compare.fam', 'fam_id'],
                value: data.db.fam_parent,
                displayValue: data.db.fam_parent_title
            });
                        
            items.push({
                fieldLabel: lang.COMPARE_FULL_TITLE,
                allowBlank: false,
                name: 'fam_full_title',
                value: data.db.fam_full_title
            });
                        
            items.push({
                xtype:'textarea',
                fieldLabel: lang.XEON_CATCHER,
                name: 'fam_catcher',
                value: data.db.fam_catcher
            });
                        
            items.push({
                xtype:'textarea',
                fieldLabel: lang.XEON_DESCRIPTION,
                name: 'fam_description',
                value: data.db.fam_description
            });
                        
            items.push({
                fieldLabel: 'Tags',
                name: 'tag_text',
                value: data.db.tag_text
            });
                        
            items.push({
                xtype: 'mediabox',
                fieldLabel: lang.XEON_IMAGE,
                name: 'fam_media',
                value: data.db.fam_media,
                displayValue: data.db.fam_media_title
            },{
                xtype: 'textfield',
                fieldLabel: 'Taxe',
                allowBlank: false,
                name: 'fam_tax',
                value: (data.db.fam_tax) ? data.db.fam_tax : 19.6
            },{
                xtype: 'togglebox',
                fieldLabel: lang.XEON_STATUS,
                allowBlank: false,
                name: 'fam_status',
                value: 0
            });
        }
        else
        {
                         
            p = new Ext.ux.grid.GridPanel({
                title: lang.COMPARE_PRODUCT_LIST,
                rowId:(data.opt) ? 'opt_id': 'prd_id',
                iconCls: 'i-database',
                itype: 'compare.prd',
                optionCell:data.opt,
                paging:true,
                quickSearch: true,
                columns: columns,
                redirectAfterDelete: data.redirect,
                configRedirectionAfterDelete: {
                    grantFreeInput: true,
                    textFreeInput: lang.XEON_FREE_INPUT_ID,
                    choices: [{
                        text: lang.COMPARE_HOME,
                        value: '0'
                    },{
                        text: lang.COMPARE_FAMILLE_MERE,
                        value: 'famille'
                    }]
                },
                search:engine.getSearchItems(data)
            });
        }
		
		
		
        if (item.add)
        {
            ct.add(new Ext.ux.FormPanel({
                items: items
            }));
        }
        else
        {
                    
            items.push({
                xtype: 'listbox',
                fieldLabel: lang.XEON_PARENT,
                allowBlank: false,
                itype: 'compare.fam',
                name: 'fam_parent',
                value: data.db.fam_parent,
                displayValue: data.db.fam_parent_title
            });
                        
            items.push({
                fieldLabel: lang.XEON_TITLE,
                allowBlank: false,
                name: 'fam_title',
                value: data.db.fam_title
            });
                        
            items.push({
                fieldLabel: lang.COMPARE_FULL_TITLE,
                allowBlank: false,
                name: 'fam_full_title',
                value: data.db.fam_full_title
            });
			
            if (data.tag)
            {
                items.push({
                    xtype: 'tagbox',
                    fieldLabel: lang.XEON_TAGS,
                    data: data.tag,
                    name: 'fam_tags',
                    value: data.db.fam_tags
                });
            }
			
            items.push({
                xtype: 'mediabox',
                fieldLabel: lang.XEON_IMAGE,
                name: 'fam_media',
                value: data.db.fam_media,
                displayValue: data.db.fam_media_title
            },{
                xtype: 'textarea',
                fieldLabel: lang.XEON_CATCHER,
                name: 'fam_catcher',
                value: data.db.fam_catcher
            },{
                xtype: 'textarea',
                fieldLabel: lang.XEON_DESCRIPTION,
                name: 'fam_description',
                value: data.db.fam_description
            },{
                xtype: 'textfield',
                fieldLabel: 'Taxe',
                allowBlank: false,
                name: 'fam_tax',
                value: data.db.fam_tax
            },{
                xtype: 'togglebox',
                fieldLabel: lang.XEON_STATUS,
                allowBlank: false,
                name: 'fam_status',
                value: data.db.fam_status
            });
			
            ct.add(new Ext.ux.FormPanel({
                title: lang.COMPARE_FAMILY,
                iconCls: ct.iconCls,
                items: items
            }));
                        
            if (data.ref) {

                // partie ref acex tooltip (voir ux.js)
                title =  new Ext.form.TextField({
                    fieldLabel: lang.XEON_TITLE,
                    name: 'fam_meta_title',
                    value: data.db.fam_meta_title,
                    enableKeyEvents:true,
                    maxLength:70
                });
                description = new Ext.form.TextArea({
                    xtype:'textarea',
                    fieldLabel: lang.XEON_DESCRIPTION,
                    name: 'fam_meta_description',
                    value: data.db.fam_meta_description,
                    enableKeyEvents:true
                //maxLength:70
                });

                keywords = new Ext.form.TextArea({
                    xtype: 'textarea',
                    fieldLabel: lang.XEON_KEYWORDS,
                    name: 'fam_meta_keywords',
                    value: data.db.fam_meta_keywords,
                    enableKeyEvents:true
                });

                tp = new Ext.ux.RefTip({
                    maxC:70,
                    cp:title
                });
                tp2 = new Ext.ux.RefTip({
                    maxC:160,
                    cp:description
                });
                tp3 = new Ext.ux.RefTip({
                    maxC:160,
                    cp:keywords
                });

                ct.add(new Ext.ux.FormPanel({
                    title: lang.XEON_SEO,
                    iconCls: 'i-magnifier',
                    collapsed: false,//(!data.db.fam_meta_title && !data.db.fam_meta_description && !data.db.fam_meta_keywords),
                    items: [title,description,keywords,{
                        fieldLabel: lang.COMPARE_URL,
                        name: 'fam_url',
                        value: data.db.fam_url,
                        disabled : (data.ismaster) ? false:data.url
                    }]
                }));
            }
                        
            ct.add(p);
                        
            if (data.prd)
            {
                var columns = [];
				
                if (data.brd)
                {
                    columns.push({
                        header: lang.COMPARE_BRAND,
                        dataIndex: 'brd_title',
                        width: 150,
                        itemLink: ['compare.brd', 'brd_id']
                    });
                }
				
                columns.push({
                    header: lang.XEON_TITLE,
                    dataIndex: 'prd_title',
                    width: 200,
                    itemLink: ['compare.prd', 'prd_id']
                },{
                    header: lang.XEON_HITS,
                    dataIndex: 'prd_hits',
                    dataType: 'int'
                },{
                    header: lang.COMPARE_SALES,
                    dataIndex: 'prd_sales',
                    dataType: 'int'
                },{
                    header: lang.XEON_STATUS,
                    dataIndex: 'prd_status',
                    width: 60,
                    renderer: Xeon.renderStatus,
                    editor: new Ext.ux.form.ToggleBox()
                });
				
            /*ct.add(new Ext.ux.grid.GridPanel({
					title: lang.COMPARE_PRODUCT_LIST,
					iconCls: 'i-database',
					itype: 'compare.prd',
					rowId: 'prd_id',
					columns: columns
				}));*/
            }

            ct.add(new Ext.ux.grid.GridPanel({
                title: lang.COMPARE_FEATURE_LIST,
                iconCls: 'i-table',
                itype: 'compare.ftr',
                rowId: 'ftr_id',
                enableDragDrop: true,
                search:false,
                columns: [{
                    header: lang.XEON_TITLE,
                    dataIndex: 'ftr_title',
                    width: 200,
                    editor: new Ext.form.TextField()
                },{
                    header: lang.COMPARE_FEATURE_VALUE,
                    dataIndex: 'ftr_list',
                    width: 400,
                    editor: new Ext.form.TextArea()
                }]
            }));
        }
    },

    makeFeatureForm: function(ct, item, data)
    {
        if (item.add)
        {
            ct.add(new Ext.ux.FormPanel({
                items: [{
                    xtype: 'hidden',
                    name: 'fam_id',
                    value: data.db.fam_id
                },{
                    fieldLabel: lang.XEON_TITLE,
                    allowBlank: false,
                    name: 'ftr_title',
                    value: data.db.ftr_title
                },{
                    xtype: 'textarea',
                    fieldLabel: lang.COMPARE_FEATURE_VALUE,
                    name: 'ftr_list',
                    value: data.db.ftr_list
                }]
            }));
        }
    },
            
    makeProductForm: function(ct, item, data)
    {
           
        var items = [{
            xtype: 'listbox',
            fieldLabel: lang.COMPARE_FAMILY,
            allowBlank: false,
            itype: 'compare.fam',
            name: 'fam_id',
            itemLink: ['compare.fam', 'fam_id'],
            value: data.db.fam_id,
            displayValue: data.db.fam_title
        }];

        if (data.brd)
        {
            items.push({
                xtype: 'listbox',
                fieldLabel: lang.COMPARE_BRAND,
                allowBlank: true,
                itype: 'compare.brd',
                name: 'brd_id',
                value: data.db.brd_id,
                itemLink: ['compare.brd', 'brd_id'],
                displayValue: data.db.brd_title
            });
        }
		
        items.push({
            fieldLabel: lang.XEON_TITLE,
            allowBlank: false,
            name: 'prd_title',
            value: data.db.prd_title
        });
		

        items.push({
            xtype: 'tagbox',
            fieldLabel: lang.XEON_TAGS,
            name: 'prd_tags',
            data:data.tag,
            value: data.db.prd_tags
        });


        items.push({
            xtype: 'mediabox',
            fieldLabel: lang.XEON_IMAGE,
            name: 'prd_media',
            value: data.db.prd_media,
            displayValue: data.db.fle_title
        });
                        
        items.push({
            xtype: 'floatfield',
            fieldLabel: lang.COMPARE_TAX,
            allowBlank: false,
            name: 'prd_tax',
            value: data.db.prd_tax
        },{
            xtype: 'togglebox',
            fieldLabel: lang.COMPARE_PRD_NEW_HOME,
            name: 'prd_new_home',
            value: (data.db.prd_new_home) ? data.db.prd_new_home : 0
        },{
            xtype: 'togglebox',
            fieldLabel: lang.COMPARE_PRD_PROMO_HOME,
            name: 'prd_promo_home',
            value: (data.db.prd_promo_home) ? data.db.prd_promo_home : 0
        });

        if (item.add)
        {
                    
            items.push({
                xtype: 'togglebox',
                fieldLabel: lang.XEON_STATUS,
                allowBlank: false,
                name: 'prd_promo_home',
                value: 0
            });

            ct.add(new Ext.ux.FormPanel({
                items: items
            }));
                       
        }
        else
        {
            items.push({
                xtype: 'togglebox',
                fieldLabel: lang.COMPARE_NEW,
                allowBlank: false,
                name: 'prd_new',
                value: data.db.prd_new
            },{
                xtype: 'togglebox',
                fieldLabel: lang.COMPARE_FLASH,
                allowBlank: false,
                name: 'prd_flash',
                value: data.db.prd_flash
            },{
                xtype: 'textarea',
                fieldLabel: lang.XEON_CATCHER,
                name: 'prd_catcher',
                value: data.db.prd_catcher
            },{
                xtype: 'togglebox',
                fieldLabel: lang.XEON_STATUS,
                allowBlank: false,
                name: 'prd_status',
                value: data.db.prd_status
            });
		
            if (data.pls)
            {
                items.push({
                    xtype: 'date',
                    fieldLabel: lang.XEON_START_DATE,
                    emptyText: lang.COMPARE_NONE,
                    name: 'prd_start_date',
                    value: data.db.prd_start_date
                },{
                    xtype: 'date',
                    fieldLabel: lang.XEON_END_DATE,
                    emptyText: lang.COMPARE_NEVER,
                    name: 'prd_end_date',
                    value: data.db.prd_end_date
                });
            }
						
            ct.add(new Ext.ux.FormPanel({
                title: lang.COMPARE_PRODUCT,
                iconCls: ct.iconCls,
                items: items
            }));

            /*ct.add(new Ext.ux.MediaProductPanel({
                id:Math.random().toString(),
                prdid:item.id,
                iconCls: 'i-page-white-stack',
                mode: 'folder'
            }));*/
                      
            // ct.add(media);
            if (data.ref)
            {
             
                // partie ref acex tooltip (voir ux.js)
                title =  new Ext.form.TextField({
                    id:Math.random().toString(),
                    fieldLabel: lang.XEON_TITLE,
                    name: 'prd_meta_title',
                    value: data.db.prd_meta_title,
                    enableKeyEvents:true
                //maxLength:70
                });
                description = new Ext.form.TextArea({
                    id:Math.random().toString(),
                    fieldLabel: lang.XEON_DESCRIPTION,
                    name: 'prd_meta_description',
                    value: data.db.prd_meta_description,
                    enableKeyEvents:true
                // maxLength:70
                });

                keywords = new Ext.form.TextArea({
                    id:Math.random().toString(),
                    xtype: 'textarea',
                    fieldLabel: lang.XEON_KEYWORDS,
                    name: 'prd_meta_keywords',
                    value: data.db.prd_meta_keywords,
                    enableKeyEvents:true
                });

                tp = new Ext.ux.RefTip({
                    maxC:70,
                    cp:title
                });
                tp2 = new Ext.ux.RefTip({
                    maxC:170,
                    cp:description
                });
                tp3 = new Ext.ux.RefTip({
                    maxC:170,
                    cp:keywords
                });

                console.log(data);
                ct.add(new Ext.ux.FormPanel({
                    title: lang.XEON_SEO,
                    iconCls: 'i-magnifier',
                    collapsed: (!data.db.prd_meta_title && !data.db.prd_meta_description && !data.db.prd_meta_keywords),
                    items: [title,description,keywords,{
                        fieldLabel: lang.COMPARE_URL,
                        name: 'prd_url',
                        value: data.db.prd_url,
                        disabled : (data.ismaster) ? false:data.url
                    }]
                }));
            }
			
            if (data.lfm)
            {
                ct.add(new Ext.ux.ListPanel({
                    title: lang.COMPARE_LINK_FAMILY,
                    iconCls: 'i-folder',
                    name: 'prd_link_family',
                    itype: 'compare.fam',
                    data: data.lfm
                }));
            }
			
            if (data.ftr)
            {
                var items = [];

                Ext.each(data.ftr, function(db)
                {
                    if (db.ftr_list == '')
                    {
                        items.push({
                            fieldLabel: db.ftr_title,
                            name: db.ftr_id,
                            value: db.ftr_value
                        });
                    }
                    else
                    {
                        var data = [];
						
                        Ext.each(db.ftr_list.split('\n'), function(e) {
                            data.push([e, e]);
                        });
						
                        items.push({
                            xtype: 'combobox',
                            fieldLabel: db.ftr_title,
                            data: data,
                            name: db.ftr_id,
                            value: db.ftr_value
                        });
                    }
                });

                ct.add(new Ext.ux.FormPanel({
                    title: lang.COMPARE_PRODUCT_FEATURES,
                    iconCls: 'i-table',
                    command: 'compare.saveProductFeature',
                    items: items
                }));
            }
			
            var columns = [{
                header: lang.XEON_TITLE,
                dataIndex: 'opt_title',
                itemLink: ['compare.opt', 'opt_id']
            },{
                header: lang.COMPARE_PRICE,
                dataIndex: 'opt_price',
                dataType: 'float',
                editor: new Ext.form.TextField(),
                renderer: this.renderPrice1
            },{
                header: lang.COMPARE_PROMOTION,
                dataIndex: 'opt_promotion',
                dataType: 'float',
                editor: new Ext.form.TextField(),
                renderer: this.renderPrice2
            },{
                header: lang.COMPARE_PROMOTION_DATE_BEGIN,
                dataIndex: 'opt_promotion_begin_date',
                dataType: 'date',
                editor: new Ext.ux.form.DateField,
                renderer: Xeon.renderDate
				
            },{
                header: lang.COMPARE_PROMOTION_DATE_END,
                dataIndex: 'opt_promotion_end_date',
                dataType: 'date',
                editor: new Ext.form.DateField({
                    format: 'd-m-Y'
                }),
                renderer: Xeon.renderDate
            }];
			
            if (data.eco)
            {
                columns.push({
                    header: lang.COMPARE_ECOTAX,
                    dataIndex: 'opt_ecotax',
                    dataType: 'float',
                    editor: new Ext.form.TextField(),
                    renderer: this.renderPrice2
                });
            }
			
            if (data.ext)
            {
                columns.push({
                    header: lang.COMPARE_EXTRA,
                    dataIndex: 'opt_extra',
                    dataType: 'float',
                    editor: new Ext.form.TextField(),
                    renderer: this.renderPrice2
                });
            }
			
            if (data.stk)
            {
                columns.push({
                    header: lang.COMPARE_STOCK,
                    dataIndex: 'opt_stock',
                    dataType: 'int',
                    editor: new Ext.form.TextField(),
                    renderer: this.renderQuantity
                });
            }
			
            columns.push({
                header: lang.COMPARE_WEIGHT,
                dataIndex: 'opt_weight',
                dataType: 'float',
                editor: new Ext.ux.form.FloatField(),
                renderer: this.renderWeight
            },{
                header: lang.COMPARE_REFERENCE,
                dataIndex: 'opt_reference',
                editor: new Ext.form.TextField()
            },{
                header: lang.XEON_STATUS,
                dataIndex: 'opt_status',
                width: 60,
                renderer: Xeon.renderStatus,
                editor: new Ext.ux.form.ToggleBox()
            });
			
            if (data.spl)
            {
                columns.push({
                    header: lang.COMPARE_SUPPLIER,
                    dataIndex: 'spl_name',
                    width: 200,
                    itemLink: ['compare.spl', 'spl_id']
                });
            }
            //console.log(data);
            if (data.opt)
            {
                var items = [{
                    xtype: 'hidden',
                    name: 'prd_id',
                    value: data.db.prd_id
                }];

                items.push({
                    fieldLabel: lang.COMPARE_PRICE,
                    allowBlank: false,
                    name: 'prd_price',
                    value: data.db.prd_price
                },{
                    fieldLabel: lang.COMPARE_PROMOTION,
                    name: 'prd_promotion',
                    value: data.db.prd_promotion
                },{
                    xtype: "date",
                    fieldLabel: lang.COMPARE_PROMOTION_DATE_BEGIN,
                    name: 'prd_promotion_start_date',
                    emptyText: lang.COMPARE_NONE,
                    renderer: Xeon.renderDate,
                    editor: new Ext.form.DateField({
                        format: 'd-m-Y'
                    }),
                    value: data.db.prd_promotion_start_date
                },{
                    xtype:  "date",
                    fieldLabel: lang.COMPARE_PROMOTION_DATE_END,
                    name: 'prd_promotion_end_date',
                    emptyText: lang.COMPARE_NONE,
                    renderer: this.myRenderDate,
                    editor: new Ext.form.DateField({
                        format: 'd-m-Y'
                    }),
                    value: data.db.prd_promotion_end_date
                },{
                    fieldLabel: lang.COMPARE_ECOTAX,
                    name: 'prd_ecotax',
                    value: data.db.prd_ecotax
                },{
                    fieldLabel: lang.COMPARE_EXTRA,
                    name: 'prd_extra',
                    value: data.db.prd_extra
                },{
                    fieldLabel: lang.COMPARE_STOCK,
                    allowBlank: false,
                    name: 'prd_stock',
                    value: data.db.prd_stock

                },{
                    fieldLabel: lang.COMPARE_WEIGHT,
                    allowBlank: false,
                    name: 'prd_weight',
                    value: data.db.prd_weight
                },{
                    fieldLabel: lang.COMPARE_REFERENCE,
                    allowBlank: false,
                    name: 'prd_reference',
                    value: data.db.prd_reference
                },{
                    fieldLabel: "Image tracker",
                    allowBlank: false,
                    name: 'prd_img_tracker',
                    value: data.db.prd_img_tracker
                },{
                    fieldLabel: "Url",
                    allowBlank: false,
                    name: 'prd_promo_reduction',
                    value: data.db.prd_url
                });
                
                if(data.prd_promo_reduction)
                {
                    items.push({
                        fieldLabel: "Réduction",
                        allowBlank: false,
                        name: 'prd_promo_reduction',
                        value: data.db.promo_reduction
                    });
                }
                if(data.prd_promo_percent)
                {
                    items.push({
                        fieldLabel: "Pourcentage",
                        allowBlank: false,
                        name: 'prd_promo_percent',
                        value: data.db.prd_promo_percent
                    });
                }
                items.push({
                    fieldLabel: "Prix sans la promotion",
                    allowBlank: false,
                    name: 'prd_price_no_promo',
                    value: data.db.prd_price_no_promo
                },{
                    fieldLabel: "Prix d'origine chez le marchand",
                    allowBlank: false,
                    name: 'prd_origin_price',
                    value: data.db.prd_origin_price
                });

                ct.add(new Ext.ux.FormPanel({
                    title: lang.COMPARE_OPTION,
                    iconCls: ct.iconCls,
                    items: items
                }));
                
            }

            ct.add(new Ext.ux.LayoutPanel({
                previewURL : data.db.prd_url,
                title: lang.XEON_LAYOUT,
                iconCls: 'i-layout',
                itype: 'compare.cnt',
                titleName: 'prd_full_title',
                templates: data.tpl,
                data: data.cnt
            }));
                        
            if (data.lpd)
            {
                ct.add(new Ext.ux.ListPanel({
                    title: lang.COMPARE_LINK_PRODUCT,
                    iconCls: 'i-database',
                    name: 'prd_link_product',
                    itype: 'compare.prd',
                    data: data.lpd
                }));
            }
			
            if (data.lpg)
            {
                ct.add(new Ext.ux.ListPanel({
                    title: lang.COMPARE_LINK_PAGE,
                    iconCls: 'i-page-white',
                    name: 'prd_link_page',
                    itype: 'cms.pge',
                    data: data.lpg
                }));
            }
			
        }
    },
	
    makeProductCopy: function(ct, item, data)
    {
        ct.add(new Ext.ux.FormPanel({
            command: 'xeon.copyItem',
            items: [{
                xtype: 'listbox',
                fieldLabel: lang.XEON_TARGET,
                allowBlank: false,
                itype: 'compare.fam',
                name: 'fam_id',
                value: data.db.fam_id,
                displayValue: data.db.fam_title
            },{
                fieldLabel: lang.XEON_TITLE,
                allowBlank: false,
                name: 'prd_title',
                value: data.db.prd_title
            }]
        }));
    },

    makeBrandRoot: function(ct, item, data)
    {
        ct.add(new Ext.ux.grid.GridPanel({
            title: lang.COMPARE_BRAND_LIST,
            iconCls: 'i-tag-blue',
            itype: 'compare.brd',
            rowId: 'brd_id',
            quickSearch: true,
            columns: [{
                header: lang.XEON_TITLE,
                dataIndex: 'brd_title',
                width: 200,
                itemLink: ['compare.brd', 'brd_id'],
                editor: new Ext.form.TextField({
                    allowBlank: false
                })
            },{
                header: lang.COMPARE_DESCRIPTION,
                dataIndex: 'brd_description',
                editor: new Ext.form.TextArea()
            }]
        }));
    },

    makeBrandForm: function(ct, item, data)
    {
                
        cms = new Ext.ux.form.ListBox({
            fieldLabel:'Lier à une page cms',
            itype:'compare.cmstobrand',
            name:'pge_id',
            displayValue:data.db.pge_title,
            value:data.db.pge_id
        });
                
        var items = [{
            fieldLabel: lang.XEON_TITLE,
            name: 'brd_title',
            allowBlank: false,
            value: data.db.brd_title
        },{
            xtype: 'textarea',
            fieldLabel: lang.COMPARE_DESCRIPTION,
            name: 'brd_description',
            value: data.db.brd_description
        },cms, new Ext.ux.form.ToggleBox({
            fieldLabel:'Actif',
            name:'brd_status'
        })]
                
        var items = [{
            fieldLabel: lang.XEON_TITLE,
            name: 'brd_title',
            allowBlank: false,
            value: data.db.brd_title
        },{
            xtype: 'textarea',
            fieldLabel: lang.COMPARE_DESCRIPTION,
            name: 'brd_description',
            value: data.db.brd_description
        },cms, new Ext.ux.form.ToggleBox({
            fieldLabel:'Actif',
            name:'brd_status',
            displayValue:data.db.brd_status,
            value:data.db.brd_status
        })];
		
        if (item.add)
        {
            ct.add(new Ext.ux.FormPanel({
                items: items
            }));
        }
        else
        {
            items.push({
                xtype: 'mediabox',
                fieldLabel: lang.XEON_IMAGE,
                name: 'brd_media',
                value: data.db.brd_media,
                displayValue: data.db.brd_media_title
            });
			
            ct.add(new Ext.ux.FormPanel({
                title: lang.COMPARE_BRAND,
                iconCls: ct.iconCls,
                items: items
            }));
			
			
            //Pavé référencement si activé dans la configuration
            if(data.brand_ref){
                title =  new Ext.form.TextField({
                    fieldLabel: lang.XEON_TITLE,
                    name: 'brd_meta_title',
                    value: data.db.brd_meta_title,
                    enableKeyEvents:true,
                    maxLength:70
                });
                description = new Ext.form.TextArea({
                    xtype:'textarea',
                    fieldLabel: lang.XEON_DESCRIPTION,
                    name: 'brd_meta_description',
                    value: data.db.brd_meta_description,
                    enableKeyEvents:true
                //maxLength:70
                });
                keywords = new Ext.form.TextArea({
                    xtype: 'textarea',
                    fieldLabel: lang.XEON_KEYWORDS,
                    name: 'brd_meta_keywords',
                    value: data.db.brd_meta_keywords,
                    enableKeyEvents:true
                });

                tp = new Ext.ux.RefTip({
                    maxC:70,
                    cp:title
                });
                tp2 = new Ext.ux.RefTip({
                    maxC:170,
                    cp:description
                });
                tp3 = new Ext.ux.RefTip({
                    maxC:170,
                    cp:keywords
                });

                ct.add(new Ext.ux.FormPanel({
                    title: lang.XEON_SEO,
                    iconCls: 'i-magnifier',
                    collapsed: false,//(!data.db.fam_meta_title && !data.db.fam_meta_description && !data.db.fam_meta_keywords),
                    items: [title,description,keywords]
                }));
            }
        }
    },

    makeSupplierRoot: function(ct, item, data)
    {
        ct.add(new Ext.ux.grid.GridPanel({
            title: lang.COMPARE_SUPPLIER_LIST,
            iconCls: 'i-lorry',
            itype: 'compare.spl',
            rowId: 'spl_id',
            columns: [{
                header: lang.COMPARE_NAME,
                dataIndex: 'spl_name',
                width: 150,
                itemLink: ['compare.spl', 'spl_id']
            },{
                header: lang.COMPARE_REFERENCE,
                dataIndex: 'spl_reference',
                editor: new Ext.form.TextField({
                    allowBlank: false
                })
            },{
                header: lang.COMPARE_ADDRESS,
                dataIndex: 'spl_address',
                width: 150,
                editor: new Ext.form.TextField()
            },{
                header: lang.COMPARE_ZIP_CODE,
                dataIndex: 'spl_zip_code',
                editor: new Ext.form.TextField()
            },{
                header: lang.COMPARE_CITY,
                dataIndex: 'spl_city',
                editor: new Ext.form.TextField()
            },{
                header: lang.COMPARE_COUNTRY,
                dataIndex: 'spl_country'
            },{
                header: lang.COMPARE_PHONE,
                dataIndex: 'spl_phone',
                editor: new Ext.form.TextField()
            }]
        }));
    },
    makeMerchantForm: function(ct, item, data)
    {


  
        var items = [{
            fieldLabel: "Marchand",
            allowBlank: false,
            name: 'mct_name',
            value: data.db.mct_name
        },{
            fieldLabel: lang.COMPARE_REFERENCE,
            allowBlank: false,
            name: 'mct_reference',
            value: data.db.mct_reference
        },{
            fieldLabel: lang.COMPARE_ADDRESS,
            name: 'mct_address',
            value: data.db.mct_address
        },{
            fieldLabel: lang.COMPARE_ZIP_CODE,
            name: 'mct_zip_code',
            value: data.db.mct_zip_code
        },{
            fieldLabel: lang.COMPARE_CITY,
            name: 'mct_city',
            value: data.db.mct_city
        },{
            xtype: 'countrybox',
            fieldLabel: lang.COMPARE_COUNTRY,
            name: 'mct_country',
            value: data.db.mct_country,
            displayValue: data.db.mct_country_name
        },{
            fieldLabel: lang.COMPARE_PHONE,
            name: 'mct_phone',
            value: data.db.mct_phone
        },{
            fieldLabel: "Téléphone Mobile",
            name: 'mct_mobile',
            value: data.db.mct_mobile
        },{
            fieldLabel: "Fax",
            name: 'mct_fax',
            value: data.db.mct_fax
        }
        ];

        

        if (item.add)
        {
            ct.add(new Ext.ux.FormPanel({
                anchor: -20,
                items: [ items ]
            }));
        }
        else
        {
            
            
            ct.add(new Ext.ux.FormPanel({
                title: lang.COMPARE_SUPPLIER,
                iconCls: ct.iconCls,
                items: [ items ]
            }));   
      
  
        }
    },
    makeFamspleditForm: function(ct, item, data)
    {

            var items = [];
       
       
       
       
            items.push({
                fieldLabel: "ID Catégorie chez le fournisseur",
                allowBlank: false,
                xtype: 'hidden',
                width: 50,
                name: 'splfam_id',
                value: data.db.splfam_id
            },
            {
                xtype: 'listbox',
                fieldLabel: "Catégorie",
                allowBlank: true,
                itype: 'compare.family',
                name: 'fam_id',
                value: data.db.fam_id,
                itemLink: ['compare.family', 'fam_id'],
                displayValue: data.db.fam_title
            }/*,{
                xtype: 'togglebox',
                fieldLabel: lang.XEON_STATUS,
                name: 'splfam_status',
                value: val.splfam_status
            }*/);
                
            ct.add(new Ext.ux.FormPanel({
                title: data.db.famspl_title,
                iconCls: ct.iconCls,
                items: [ items ]
            }));

  
    
    },
    makeFamsplForm: function(ct, item, data)
    {

    
    if(item.add)
        {
                var items = [];

                items.push({
                fieldLabel: "ID Catégorie chez le fournisseur",
                allowBlank: false,
                width: 50,
                name: 'splfam_id'
                },{
                xtype: 'hidden',
                name: "spl_id",
                value: data.id
                },
                {
                xtype: 'listbox',
                fieldLabel: "Catégorie",
                allowBlank: true,
                itype: 'compare.family',
                name: 'fam_id',
                itemLink: ['compare.family', 'fam_id']
                }/*,{
                xtype: 'togglebox',
                fieldLabel: lang.XEON_STATUS,
                name: 'splfam_status'
                }*/);

                ct.add(new Ext.ux.FormPanel({
                iconCls: ct.iconCls,
                items: [ items ]
                }));
        }
        else
            {
                var columns = [];

                columns.push(
                {
                        header: "ID Catégorie chez le fournisseur",
                        width: 200,
                        dataIndex: 'splfam_id',
                        itemLink: ['compare.famspledit', 'splfam_id'],
                        sortable: true,
                        value: data.splfam_id
                },        
                {
                        header: "Catégorie",
                        width: 200,
                        dataIndex: 'fam_title',
                        sortable: true,
                        value: data.fam_title
                }/*,{
                        header: lang.XEON_STATUS,
                        dataIndex: 'splfam_status',
                        width: 60,
                        renderer: Xeon.renderStatus,
                        editor: new Ext.ux.form.ToggleBox()
                    }*/);

                // console.log(data.engine);
                ct.add(new Ext.ux.grid.GridPanel({
                    title: "Liste des catégories",
                    optionCell:data.opt, // if set to true, change the itype to discard changes
                    iconCls: 'i-database',
                    rowId: 'id',
                    itype: 'compare.famspl',
                    paging: true,
                    quickSearch: true,
                    columns: columns
                }));
            }
    
    },
    makeSupplierForm: function(ct, item, data)
    {

        var items = [{
            xtype: 'titlebox',
            fieldLabel: lang.CONTACT_TITLE,
            allowBlank: false,
            name: 'spl_title'
	},{
            fieldLabel: lang.CONTACT_FIRM,
            allowBlank: false,
            name: 'spl_name',
            value: data.db.spl_name
        },{
            fieldLabel: lang.CONTACT_LAST_NAME,
            allowBlank: false,
            name: 'spl_lastname',
            value: data.db.spl_lastname
        },{
            fieldLabel: lang.CONTACT_FIRST_NAME,
            allowBlank: false,
            name: 'spl_firstname',
            value: data.db.spl_firstname
        },
        {
            fieldLabel: lang.COMPARE_REFERENCE,
            allowBlank: false,
            name: 'spl_reference',
            value: data.db.spl_reference
        },{
            fieldLabel: lang.COMPARE_ADDRESS,
            name: 'spl_address',
            value: data.db.spl_address
        },{
            fieldLabel: lang.COMPARE_ZIP_CODE,
            name: 'spl_zip_code',
            value: data.db.spl_zip_code
        },{
            fieldLabel: lang.COMPARE_CITY,
            name: 'spl_city',
            value: data.db.spl_city
        },{
            xtype: 'countrybox',
            fieldLabel: lang.COMPARE_COUNTRY,
            name: 'spl_country',
            value: data.db.spl_country,
            displayValue: data.db.spl_country_name
        },{
            fieldLabel: lang.COMPARE_EMAIL,
            name: 'spl_email',
            allowBlank: false,
            value: data.db.spl_email
        },{
            fieldLabel: lang.COMPARE_PHONE,
            name: 'spl_phone',
            value: data.db.spl_phone
        },{
            fieldLabel: lang.COMPARE_PHONE_MOBILE,
            name: 'spl_mobile',
            value: data.db.spl_mobile
        },{
            fieldLabel: lang.COMPARE_FAX,
            name: 'spl_fax',
            value: data.db.spl_fax
        },{
            fieldLabel: lang.COMPARE_ECOTAX,
            name: 'spl_ecotax',
            value: data.db.spl_ecotax
        }
        ,{
            fieldLabel: lang.COMPARE_TAX,
            name: 'spl_tax',
            value: data.db.spl_tax
        },{
            fieldLabel: lang.COMPARE_GAINCLICK,
            xtype: 'togglebox',
            name: 'spl_is_click',
            value: data.db.spl_is_click
        }
        ];
    
        if(data.db.spl_is_click == 0)
        {
            items.push({
                fieldLabel: lang.COMPARE_,
                name: 'spl_percentage_of_sales',
                value: data.db.spl_percentage_of_sales
            });
        }
        if(data.db.spl_is_click == 1)
        {
            items.push({
                fieldLabel: "Prix par clic",
                name: 'spl_price_per_click',
                value: data.db.spl_price_per_click
            });
        }
        
            items.push({
                xtype: 'listbox',
                fieldLabel: 'Catégorie principale',
                name: "spl_favoris_fam",
                itype: 'compare.family',
                itemLink: ['compare.family', 'fam_id'],
                value: data.db.fam_id,
                displayValue: data.db.fam_title

            },{

                fieldLabel: 'Url',
                name: "spl_url",
                value: data.db.spl_url

            },{

                fieldLabel: 'N° TVA intracommunautaire',
                name: "spl_intracommunity_vat",
                value: data.db.spl_intracommunity_vat

            },{

                fieldLabel: 'RCS',
                name: "spl_rcs",
                value: data.db.spl_rcs

            },{

                fieldLabel: 'La liste des rubriques',
                name: "spl_list_family",
                value: data.db.spl_list_family

            });

        if (item.add)
        {
            items.push({
                    fieldLabel: "Mot de passe",
                    inputType: 'password',
                    allowBlank: false,
                    minLength: 3,
                    maxLength: 32,
                    name: 'spl_password'
                });
            ct.add(new Ext.ux.FormPanel({
                    items: items
            }));
        
        }
        else
        {
            
            
            ct.add(new Ext.ux.FormPanel({
                    title: lang.COMPARE_SUPPLIER,
                    iconCls: ct.iconCls,
                    frame: true,
                    items: items
            }));
            
            ct.add(new Ext.ux.FormPanel({
                    title: "Mot de passe",
                    iconCls: ct.iconCls,
                    frame: true,
                    items : [{
                    fieldLabel: "Mot de passe",
                    inputType: 'password',
                    allowBlank: false,
                    minLength: 3,
                    maxLength: 32,
                    name: 'spl_password'
                }]
            }));
            
            
            if (data.com)
            {
                ct.add(new Ext.ux.AvisPanel({
                    title: "Avis",
                    iconCls: 'i-comments',
                    data: data.com
                }));
            }
            
            
            ct.add(new Ext.ux.ListPanel({
                title: "Liste des marchands",
                iconCls: 'i-folder',
                name: 'spl_list_merchant',
                itype: 'compare.mct',
                data: data.mct
            }));
            
    
            
            var items1 = [];
            items1.push({
                xtype: 'hidden',
                name: "Field",
                value: 1
            });
            items1.push({
                xtype: 'hidden',
                name: "spl_id",
                value: data.db.spl_id
            });
            Ext.each(data.champs, function(db)
            {
                var value = db.Field;
                Ext.each(data.filed, function(s)
                {
                   if(s.field_name == db.Field)
                       value = s.field_value;
                
                });
                
                
                if(db.Field != "spl_id")
                {
                    items1.push({
                        fieldLabel: '<table style="background-color: #fff;" border="0" cellspacing="2" cellpadding="2" width="100%">'+
                        '<tbody>'+
                        '<tr>'+
                        '<td style="border: thin solid #6495ed; width: 60%;">'+db.Field+'</td>'+
                        '<td style="border: thin solid #6495ed; width: 40%;">'+db.Type+'</td>'+
                        '</tr>'+
                        '</tbody>'+
                        '</table>',
                        name: db.Field,
                        value: value
                    });
                }

            });
            
            
            var items2 = [];

            Ext.each(data.ftr, function(db)
            {
                var value = db.ftr_title;
                Ext.each(data.filed, function(s)
                {
                   if(s.field_name == db.ftr_title)
                       value = s.field_value;
                
                });
                if(db.ftr_title != "spl_id")
                {
                    items2.push({
                        fieldLabel: '<table style="background-color: #fff;" border="0" cellspacing="2" cellpadding="2" width="100%">'+
                        '<tbody>'+
                        '<tr>'+
                        '<td style="border: thin solid #6495ed; width: 60%;">'+db.ftr_title+' ( '+db.fam_title+' ) </td>'+
                        '</tr>'+
                        '</tbody>'+
                        '</table>',
                        name: "ftr_"+db.ftr_id,
                        value: value
                    });
                }

            });
            
            
            var items3 = [];

            Ext.each(data.pst, function(db)
            {
                var value = db.Field;
                Ext.each(data.filed, function(s)
                {
                   if(s.field_name == db.Field)
                       value = s.field_value;
                
                });
                if(db.Field != "spl_id")
                {
                    items3.push({
                        fieldLabel: '<table style="background-color: #fff;" border="0" cellspacing="2" cellpadding="2" width="100%">'+
                        '<tbody>'+
                        '<tr>'+
                        '<td style="border: thin solid #6495ed; width: 60%;">'+db.Field+'</td>'+
                        '<td style="border: thin solid #6495ed; width: 40%;">'+db.Type+'</td>'+
                        '</tr>'+
                        '</tbody>'+
                        '</table>',
                        name: db.Field,
                        value: value
                    });
                }

            });
            

            
            var field1 = new Ext.form.FieldSet({
                title: 'Informations sur les produits'
                , 
                width: 700
                ,
                labelWidth: 300
                , 
                collapsible: true
                , 
                iconCls: 'i-feed'
                , 
                style: 'background-color: #fff; margin: 30px auto;'
                , 
                defaults: { // applies to every "item" below
                    xtype: 'textfield'
                    , 
                    fieldLabel: 'Test'
                    , 
                    msgTarget: 'under'
                    , 
                    anchor: '-25'
                    , 
                    allowBlank: false
                }
                , 
                items: [
                items1 // will use "defauts" configuration
                ]
            });
            var field2 = new Ext.form.FieldSet({
                title: 'Informations sur les caracteristiques'
                , 
                width: 700
                ,
                labelWidth: 300
                , 
                collapsible: true
                , 
                iconCls: 'i-feed'
                , 
                style: 'background-color: #eee; margin: 30px auto;'
                , 
                defaults: { // applies to every "item" below
                    xtype: 'textfield'
                    , 
                    fieldLabel: 'Test'
                    , 
                    msgTarget: 'under'
                    , 
                    anchor: '-25'
                    , 
                    allowBlank: false
                }
                , 
                items: [
                items2 // will use "defauts" configuration
                ]
            });
            var field3 = new Ext.form.FieldSet({
                title: 'Autres informations'
                , 
                width: 700
                ,
                labelWidth: 300
                , 
                collapsible: true
                , 
                iconCls: 'i-feed'
                , 
                style: 'background-color: #F1F1F1; margin: 30px auto;'
                , 
                defaults: { // applies to every "item" below
                    xtype: 'textfield'
                    , 
                    fieldLabel: 'Test'
                    , 
                    msgTarget: 'under'
                    , 
                    anchor: '-25'
                    , 
                    allowBlank: false
                }
                , 
                items: [
                items3 // will use "defauts" configuration
                ]
            });
                
            ct.add(new Ext.ux.FormPanel({
                title: "Les champs",
                iconCls: ct.iconCls,
                labelWidth: 700,
                items: [
                field1,field3,field2 // will use "defauts" configuration
                ]
            }));
  
        }
    },
	
    makeModuleForm: function(ct, item, data)
    {
        ct.add(new Ext.ux.FormPanel({
            title: lang.XEON_SETTINGS,
            iconCls: 'i-cog',
            items: [{
                fieldLabel: lang.COMPARE_FAMILY_URL,
                name: 'family_url',
                value: data.cfg.family_url
            },{
                fieldLabel: lang.COMPARE_PRODUCT_URL,
                name: 'product_url',
                value: data.cfg.product_url
            },{
                xtype: 'togglebox',
                fieldLabel: lang.XEON_URL_FULL_TITLE,
                name: 'url_full_title',
                value: data.cfg.url_full_title
            },{
                xtype: 'combobox',
                fieldLabel: lang.COMPARE_IMAGE,
                data: [[1, '1'], [2, '2'], [3, '3'], [4, '4']],
                name: 'nb_photo',
                value: (data.cfg.nb_image) ? data.cfg.nb_image : 3
            },{
                xtype: 'combobox',
                fieldLabel: lang.COMPARE_LEVEL,
                data: [[1, '1'], [2, '2'], [3, '3'], [4, '4']],
                name: 'level',
                value: data.cfg.level
            },{
                fieldLabel: lang.COMPARE_DEFAULT_TAX,
                name: 'default_tax',
                value: data.cfg.default_tax
            },{
                xtype: 'combobox',
                fieldLabel: lang.COMPARE_CURRENCY,
                data: [[1, lang.COMPARE_EURO, 'i-money-euro'], [2, lang.COMPARE_DOLLAR, 'i-money-dollar'], [3, lang.COMPARE_POUND, 'i-money-pound'], [4, lang.COMPARE_YEN, 'i-money-yen']],
                name: 'currency',
                value: data.cfg.currency
            },{
                fieldLabel: lang.XEON_TEMPLATE_LIST,
                name: 'layout_template',
                value: data.cfg.layout_template
            },{
                xtype: 'togglebox',
                fieldLabel: lang.XEON_COMMENT_CHECK,
                name: 'comment_check',
                value: data.cfg.comment_check
            },{
                fieldLabel: lang.XEON_RATING_MAX,
                name: 'rating_max',
                value: data.cfg.rating_max
            }]
        }));
		
        ct.add(new Ext.ux.FormPanel({
            title: lang.XEON_OPTIONS,
            iconCls: 'i-brick',
            items: [{
                xtype: 'togglebox',
                fieldLabel: lang.COMPARE_BRANDS,
                name: 'plugin_brand',
                value: data.cfg.plugin_brand
            },{
                xtype: 'togglebox',
                fieldLabel: lang.COMPARE_BRANDS_REF,
                name: 'plugin_brand_ref',
                value: data.cfg.plugin_brand_ref
            },{
                xtype: 'togglebox',
                fieldLabel: lang.COMPARE_SUPPLIERS,
                name: 'plugin_supplier',
                value: data.cfg.plugin_supplier
            },{
                xtype: 'togglebox',
                fieldLabel: lang.COMPARE_FAMILY_TAG,
                name: 'plugin_family_tag',
                value: data.cfg.plugin_family_tag
            },{
                xtype: 'togglebox',
                fieldLabel: lang.COMPARE_PRODUCT_TAG,
                name: 'plugin_product_tag',
                value: data.cfg.plugin_product_tag
            },{
                xtype: 'togglebox',
                fieldLabel: lang.COMPARE_PRODUCT_LIFESPAN,
                name: 'plugin_product_lifespan',
                value: data.cfg.plugin_product_lifespan
            },{
                xtype: 'togglebox',
                fieldLabel: lang.COMPARE_LINK_FAMILY,
                name: 'plugin_link_family',
                value: data.cfg.plugin_link_family
            },{
                xtype: 'togglebox',
                fieldLabel: lang.COMPARE_LINK_PRODUCT,
                name: 'plugin_link_product',
                value: data.cfg.plugin_link_product
            },{
                xtype: 'togglebox',
                fieldLabel: lang.COMPARE_LINK_PAGE,
                name: 'plugin_link_page',
                value: data.cfg.plugin_link_page
            },{
                xtype: 'togglebox',
                fieldLabel: lang.COMPARE_OPTIONS,
                name: 'plugin_option',
                value: data.cfg.plugin_option
            },{
                xtype: 'togglebox',
                fieldLabel: lang.COMPARE_ECOTAX,
                name: 'plugin_ecotax',
                value: data.cfg.plugin_ecotax
            },{
                xtype: 'togglebox',
                fieldLabel: lang.COMPARE_EXTRA,
                name: 'plugin_extra',
                value: data.cfg.plugin_extra
            },{
                xtype: 'togglebox',
                fieldLabel: lang.COMPARE_STOCK,
                name: 'plugin_stock',
                value: data.cfg.plugin_stock
            },{
                xtype: 'togglebox',
                fieldLabel: lang.COMPARE_DELAY,
                name: 'plugin_delay',
                value: data.cfg.plugin_delay
            },{
                xtype: 'togglebox',
                fieldLabel: lang.XEON_COMMENTS,
                name: 'plugin_comment',
                value: data.cfg.plugin_comment
            },{
                xtype: 'togglebox',
                fieldLabel: lang.COMPARE_RATING,
                name: 'plugin_rating',
                value: data.cfg.plugin_rating
            },{
                xtype: 'togglebox',
                fieldLabel: lang.COMPARE_WEIGHTS,
                name: 'plugin_weight',
                value: data.cfg.plugin_weight
            },{
                xtype: 'togglebox',
                fieldLabel: lang.XEON_FIDELITY,
                name: 'plugin_fidelity',
                value: data.cfg.plugin_fidelity
            },{
                xtype: 'togglebox',
                fieldLabel: lang.COMPARE_OPTION,
                name: 'plugin_option',
                value: data.cfg.plugin_option
            },{
                xtype: 'togglebox',
                fieldLabel: lang.COMPARE_STAT,
                name: 'plugin_stat',
                value: data.cfg.plugin_stat
            },{
                xtype: 'togglebox',
                fieldLabel: lang.COMPARE_GIFT_MESSAGE,
                name: 'plugin_gift',
                value: data.cfg.plugin_gift
            },{
                xtype: 'togglebox',
                fieldLabel: lang.COMPARE_GOOGLE_COMPAREPING_XML,
                name: 'plugin_google_compareping',
                value: data.cfg.plugin_redirect_delete_product
            },{
                xtype: 'togglebox',
                fieldLabel: lang.COMPARE_REDIRECT_AFTER_DELETE,
                name: 'plugin_redirect_delete_product',
                value: data.cfg.plugin_redirect_delete_product
            },{
                xtype: 'togglebox',
                fieldLabel: lang.COMPARE_ALERT_STOCK_PROMO,
                name: 'plugin_alert_stock_promo',
                value: data.cfg.plugin_alert_stock_promo
            },{
                fieldLabel: lang.COMPARE_INTERVAL_ALERT_PROMO,
                name: 'plugin_interval_alert_promo',
                value: data.cfg.plugin_interval_alert_promo
            },{
                fieldLabel: 'Email',
                name: 'adress_email',
                value: data.cfg.adress_email
            },{
                xtype: 'togglebox',
                fieldLabel: lang.COMPARE_EDIT_DELAY_STOCK_PRICE,
                name: 'plugin_edit_delay_stock_price',
                value: data.cfg.plugin_edit_delay_stock_price
            }]
        }));
           
        ct.add(new Ext.ux.FormPanel({
            title: lang.XEON_SEARCHENGINE,
            iconCls: 'i-brick',
            itemId:'compareengine',
            items: [{
                xtype: 'togglebox',
                fieldLabel: lang.COMPARE_SLIDER,
                name: 'slider_field',
                value: data.cfg.compareengine.slider_field
            },{
                xtype: 'togglebox',
                fieldLabel: 'Par stock',
                name: 'stock_field',
                value: data.cfg.compareengine.stock_field
            },{
                xtype: 'togglebox',
                fieldLabel: 'Par reference',
                name: 'reference_field',
                value: data.cfg.compareengine.reference_field
            },{
                xtype: 'togglebox',
                fieldLabel: 'Par famille',
                name: 'family_field',
                value: data.cfg.compareengine.family_field
            },{
                xtype: 'togglebox',
                fieldLabel: 'Par status',
                name: 'status_field',
                value: data.cfg.compareengine.status_field
            },{
                xtype: 'togglebox',
                fieldLabel: 'Par marque',
                name: 'brand_field',
                value: data.cfg.compareengine.brand_field
            },{
                xtype: 'togglebox',
                fieldLabel: 'Par promotion',
                name: 'promo_field',
                value: data.cfg.compareengine.promo_field
            },{
                xtype: 'togglebox',
                fieldLabel: 'Par promotion home',
                name: 'promohome_field',
                value: data.cfg.compareengine.promohome_field
            }]
        }));

    },
      
    //GRILLE DES MODIFICATIONS DE PRIS, DELAY ET STOCKS
    makeEditdspRoot: function(ct, item, data){
        //MODIFICATION DU PRIX
        var columns = [{
            header: lang.COMPARE_PRODUCT,
            dataIndex: 'prd_title',
            width: 200,
            itemLink: ['compare.prd', 'prd_id']
        },{
            header: lang.COMPARE_OPTION,
            dataIndex: 'opt_title',
            width: 120,
            itemLink: ['compare.opt', 'opt_id']
        },{
            header: lang.COMPARE_REFERENCE,
            width: 100,
            dataIndex: 'opt_reference'
        },{
            header: lang.COMPARE_PRICE,
            dataIndex: 'opt_price',
            dataType: 'float',
            width: 60,
            editor: new Ext.form.TextField(),
            renderer: this.renderPrice1
        },{
            header: lang.COMPARE_PROMOTION,
            dataIndex: 'opt_promotion',
            dataType: 'float',
            width: 80,
            editor: new Ext.form.TextField(),
            renderer: this.renderPrice2
        },{
            dataType:'date',
            header: lang.COMPARE_PROMOTION_DATE_BEGIN,
            dataIndex: 'opt_promotion_begin_date',
            renderFormat:'d-m-Y',
            editor: new Ext.form.DateField
        },{
            dataType:'date',
            header: lang.COMPARE_PROMOTION_DATE_END,
            dataIndex: 'opt_promotion_end_date',
            renderFormat:'d-m-Y',
            editor: new Ext.form.DateField
        }];
        var items = [{
            xtype: 'hidden',
            name: 'prd_id',
            value: data.db.prd_id
        }];
		
        if (data.spl)
        {
            items.push({
                xtype: 'listbox',
                fieldLabel: lang.COMPARE_SUPPLIER,
                allowBlank: false,
                itype: 'compare.spl',
                name: 'spl_id',
                value: data.db.spl_id,
                displayValue: data.db.spl_name
            });
        }
		
        items.push({
            fieldLabel: lang.XEON_TITLE,
            allowBlank: false,
            name: 'opt_title',
            value: data.db.opt_title
        },{
            fieldLabel: lang.COMPARE_PRICE,
            allowBlank: false,
            name: 'opt_price',
            value: data.db.opt_price
        },{
            fieldLabel: lang.COMPARE_PROMOTION,
            name: 'opt_promotion',
            value: data.db.opt_promotion
        },{
            xtype: "date",
            fieldLabel: lang.COMPARE_PROMOTION_DATE_BEGIN,
            name: 'opt_promotion_begin_date',
            emptyText: lang.COMPARE_NONE,
            renderer: Xeon.renderDate,
            editor: new Ext.form.DateField({
                format: 'd-m-Y'
            }),
            value: data.db.opt_promotion_date_begin
        },{
            xtype:  "date",
            fieldLabel: lang.COMPARE_PROMOTION_DATE_END,
            name: 'opt_promotion_end_date',
            emptyText: lang.COMPARE_NONE,
            renderer: this.myRenderDate,
            editor: new Ext.form.DateField({
                format: 'd-m-Y'
            }),
            value: data.db.opt_promotion_date_end
        });
		
        if (data.eco)
        {
            items.push({
                fieldLabel: lang.COMPARE_ECOTAX,
                name: 'opt_ecotax',
                value: data.db.opt_ecotax
            });
        }
		
        if (data.ext)
        {
            items.push({
                fieldLabel: lang.COMPARE_EXTRA,
                name: 'opt_extra',
                value: data.db.opt_extra
            });
        }
		
        if (data.stk)
        {
            items.push({
                fieldLabel: lang.COMPARE_STOCK,
                allowBlank: false,
                name: 'opt_stock',
                value: data.db.opt_stock
            });
        }
		
        items.push({
            fieldLabel: lang.COMPARE_WEIGHT,
            allowBlank: false,
            name: 'opt_weight',
            value: data.db.opt_weight
        },{
            fieldLabel: lang.COMPARE_REFERENCE,
            allowBlank: false,
            name: 'opt_reference',
            value: data.db.opt_reference
        });
		
        if (item.add)
        {
            ct.add(new Ext.ux.FormPanel({
                items: items
            }));
        }
        else
        {
            items.push({
                xtype: 'mediabox',
                fieldLabel: lang.XEON_IMAGE,
                name: 'opt_media',
                value: data.db.opt_media,
                displayValue: data.db.opt_media_title
            },{
                xtype: 'togglebox',
                fieldLabel: lang.XEON_STATUS,
                allowBlank: false,
                name: 'opt_status',
                value: data.db.opt_status
            });
			
            ct.add(new Ext.ux.FormPanel({
                title: lang.COMPARE_OPTION,
                iconCls: ct.iconCls,
                items: items
            }));
        }
					
        //MODIFICATION DU STOCK
        var columns = [{
            header: lang.COMPARE_PRODUCT,
            dataIndex: 'prd_title',
            width: 200,
            itemLink: ['compare.prd', 'prd_id']
        },{
            header: lang.COMPARE_OPTION,
            dataIndex: 'opt_title',
            width: 120,
            itemLink: ['compare.opt', 'opt_id']
        },{
            header: lang.COMPARE_REFERENCE,
            width: 100,
            dataIndex: 'opt_reference'
        },{
            header: lang.COMPARE_PRICE,
            dataIndex: 'opt_price',
            dataType: 'float',
            width: 60,
            editor: new Ext.form.TextField(),
            renderer: this.renderPrice1
        },{
            header: lang.COMPARE_PROMOTION,
            dataIndex: 'opt_promotion',
            dataType: 'float',
            width: 80,
            editor: new Ext.form.TextField(),
            renderer: this.renderPrice2
        },{
            header: lang.COMPARE_STOCK,
            dataIndex: 'opt_stock',
            width: 80,
            editor: new Ext.form.TextField()
        },{
            header: lang.COMPARE_SEUIL_STOCK,
            dataIndex: 'opt_seuil_stock',
            width: 80,
            editor: new Ext.form.TextField()
        }];
        ct.add(new Ext.ux.grid.GridPanel({
            title: lang.COMPARE_EDIT_STOCK,
            iconCls: 'i-brick',
            itype: 'compare.opt',
            rowId: 'opt_id',
            search: engine.getSearchOptionsItems(data),
            enableDragDrop: true,
            columns: columns,
            paging: true
        }));
		
			
        //MODIFICATION DES DELAIS
        if(data.delay){
            var columns = [{
                header: lang.COMPARE_PRODUCT,
                dataIndex: 'prd_title',
                width: 200,
                itemLink: ['compare.prd', 'prd_id']
            },{
                header: lang.COMPARE_OPTION,
                dataIndex: 'opt_title',
                width: 120,
                itemLink: ['compare.opt', 'opt_id']
            },{
                header: lang.COMPARE_REFERENCE,
                width: 100,
                dataIndex: 'opt_reference'
            },{
                header: lang.COMPARE_PRICE,
                dataIndex: 'opt_price',
                dataType: 'float',
                width: 60,
                editor: new Ext.form.TextField(),
                renderer: this.renderPrice1
            },{
                header: lang.COMPARE_PROMOTION,
                dataIndex: 'opt_promotion',
                dataType: 'float',
                width: 80,
                editor: new Ext.form.TextField(),
                renderer: this.renderPrice2
            },{
                header: lang.COMPARE_DELAY,
                dataIndex: 'del_id',
                width: 80 ,
                editor: new Ext.ux.form.DelayBox() ,
                renderer: function(e){
                    var bin=new Ext.ux.form.DelayBox();
                    return bin.getValueToDisplay(e);
                }
					
            }];
				
            ct.add(new Ext.ux.grid.GridPanel({
                title: lang.COMPARE_EDIT_DELAY,
                iconCls: 'i-brick',
                itype: 'compare.opt',
                rowId: 'opt_id',
                search: engine.getSearchOptionsItems(data),
                enableDragDrop: true,
                columns: columns,
                paging: true
            }));
        }
		
    },
	
    //GRILLE DES ALERTES PROMOTIONS ET STOCKS
    makeAlertRoot: function(ct, item, data)
    {
        var Promo_Grid = new Ext.ux.grid.GridPanel({
            title: lang.COMPARE_ALERT_PROMOS,
            iconCls: 'i-note',
            itype: 'compare.alertp',
            rowId: 'opt_id',
            search: false,
            columns: [{
                header: lang.COMPARE_PRODUCT,
                dataIndex: 'prd_title',
                width: 140,
                itemLink: ['compare.prd', 'prd_id']
            },{
                header: lang.COMPARE_OPTION,
                dataIndex: 'opt_title',
                itemLink: ['compare.opt', 'opt_id']
            },{
                header: lang.COMPARE_REFERENCE,
                dataIndex: 'opt_reference'
				
            },{
                header: lang.COMPARE_PRICE,
                dataIndex: 'opt_price',
                renderer: this.renderPrice1
            },{
                header: lang.COMPARE_PROMOTION,
                dataIndex: 'opt_promotion',
                renderer: this.renderPrice1
            },{
                dataType:'date',
                header: lang.COMPARE_PROMOTION_DATE_END,
                dataIndex: 'opt_promotion_end_date',
                renderFormat:'d-m-Y'
            }
            ]
        });

        ct.add(Promo_Grid);
		
		
        var Stock_Grid = new Ext.ux.grid.GridPanel({
            title: lang.COMPARE_ALERT_STOCK,
            iconCls: 'i-note',
            itype: 'compare.alerts',
            rowId: 'opt_id',
            search: false,
            columns: [{
                header: lang.COMPARE_PRODUCT,
                dataIndex: 'prd_title',
                width: 240,
                itemLink: ['compare.prd', 'prd_id']
            },{
                header: lang.COMPARE_OPTION,
                dataIndex: 'opt_title',
                width: 124,
                itemLink: ['compare.opt', 'opt_id']
            },{
                header: lang.COMPARE_REFERENCE,
                dataIndex: 'opt_reference'
				
            },{
                header: lang.COMPARE_PRICE,
                dataIndex: 'opt_price',
                renderer: this.renderPrice1
            },{
                header: lang.COMPARE_STOCK,
                dataIndex: 'opt_stock'
            }
            ]
        });

        ct.add(Stock_Grid);
    },
        
    makeFidelityRoot: function(ct, item, data)
    {

        fidelity_grid = new Ext.ux.grid.GridPanel({
            title: lang.COMPARE_FIDELITY_LIST,
            iconCls: 'i-note',
            itype: 'compare.fid',
            rowId: 'fid_id',
            columns: [{
                header: lang.COMPARE_FIDELITY_TITLE,
                dataIndex: 'fid_title',
                itemLink: ['compare.fid', 'fid_id']
            },{
                header: lang.COMPARE_FIDELITY_RATIOAP,
                dataIndex: 'fid_ratioAP'
            },{
                header: lang.COMPARE_FIDELITY_RATIOPR,
                dataIndex: 'fid_ratioPR'
            },{
                header: lang.COMPARE_FIDELITY_PROMO,
                dataIndex: 'fid_promo_get',
                renderer: Xeon.renderStatus,
                editor: new Ext.ux.form.ToggleBox()
            },{
                header: lang.COMPARE_MIN_CART,
                dataIndex: 'fid_cart_mini',
                dataType: 'float',
                editor: new Ext.ux.form.FloatField({
                    allowBlank: false
                })
            },{
                header: lang.COMPARE_FIDELITY_USE_OF_POINTS,
                dataIndex: 'fid_points_use',
                dataType: 'float',
                editor: new Ext.ux.form.FloatField({
                    allowBlank: false
                })
            },{
                header: lang.COMPARE_FIDELITY_POINTS_VALIDITY,
                dataIndex: 'fid_valid_duration',
                dataType: 'float',
                editor: new Ext.ux.form.FloatField({
                    allowBlank: false
                })
            },{
                header: lang.COMPARE_START_DATE,
                dataIndex: 'fid_start_date'
            },{
                header: lang.COMPARE_END_DATE,
                dataIndex: 'fid_end_date'
            },{
                header: lang.XEON_STATUS,
                dataIndex: 'fid_status',
                width: 60,
                renderer: Xeon.renderStatus,
                editor: new Ext.ux.form.ToggleBox()
            }]
        });

        ct.add(fidelity_grid);
    },
        
    renderYesorNo: function(v, m, r)
    {
        m.css = (v == 1) ? 'x-button i-accept' : 'x-button i-delete';
        m.css = (v == -1) ? 'x-button i-loading' : m.css;
    },
    makeFidelityForm: function(ct, item, data)
    {
        var items = [{
            fieldLabel: lang.COMPARE_FIDELITY_TITLE,
            allowBlank: false,
            name: 'fid_title',
            value: data.db.fid_title
        },{
            fieldLabel: lang.COMPARE_FIDELITY_RATIOAP,
            allowBlank: false,
            name: 'fid_ratioAP',
            value: data.db.fid_ratioAP,
            width: 80
        },{
            xtype:'tbtext',
            text: lang.COMPARE_FIDELITY_RATIOAP_TEXT,
            allowBlank: false,
            width: 300,
            style:"float:left;margin-left:280px;margin-top :-20px;"
        },{
            fieldLabel: lang.COMPARE_FIDELITY_RATIOPR,
            allowBlank: false,
            name: 'fid_ratioPR',
            value: data.db.fid_ratioPR,
            width: 80
        },{
            xtype:'tbtext',
            text: lang.COMPARE_FIDELITY_RATIOPR_TEXT,
            allowBlank: false,
            width: 300,
            style:"float:left;margin-left:280px;margin-top :-20px;"
        },{
            xtype: 'togglebox',
            fieldLabel: lang.COMPARE_FIDELITY_GAIN_ON_PROMO,
            allowBlank: false,
            name: 'fid_promo_get',
            value: data.db.fid_promo_get
        },{
            xtype: 'floatfield',
            fieldLabel: lang.COMPARE_MIN_CART,
            allowBlank: false,
            name: 'fid_cart_mini',
            value: data.db.fid_cart_mini
        },{
            xtype: 'togglebox',
            fieldLabel: lang.COMPARE_FIDELITY_GAIN_OVER_PROMO,
            allowBlank: false,
            name: 'fid_promo_use',
            value: data.db.fid_promo_use
        },{
            xtype: 'listbox',
            fieldLabel: lang.COMPARE_FIDELITY_USE_OF_POINTS,
            allowBlank: false,
            itype: 'compare.fid',
            name: 'fid_points_use',
            value: data.db.fid_points_use,
            displayValue: data.db.fid_points_use
        },{
            fieldLabel: lang.COMPARE_FIDELITY_POINTS_VALIDITY,
            allowBlank: true,
            name: 'fid_valid_duration',
            value: data.db.fid_valid_duration
        },{
            xtype: 'date',
            fieldLabel: lang.COMPARE_FIDELITY_START_DATE,
            emptyText: lang.COMPARE_NONE,
            name: 'fid_start_date',
            value: data.db.fid_start_date
        },{
            xtype: 'date',
            fieldLabel: lang.COMPARE_FIDELITY_END_DATE,
            emptyText: lang.COMPARE_NEVER,
            name: 'fid_end_date',
            value: data.db.fid_end_date
        }];

        if (item.add)
        {
            ct.add(new Ext.ux.FormPanel({
                items: items
            }));
        }
        else
        {
            items.push({
                xtype: 'togglebox',
                fieldLabel: lang.XEON_STATUS,
                allowBlank: false,
                name: 'fid_status',
                value: data.db.fid_status
            });

            ct.add(new Ext.ux.FormPanel({
                title: lang.COMPARE_FIDELITY,
                iconCls: ct.iconCls,
                items: items
            }));
                        
                       
        /*ct.add(new Ext.ux.ListPanel({
				title: lang.COMPARE_AFFECTED_FAMILY,
				iconCls: 'i-folder',
				name: 'dsc_family_list',
				itype: 'compare.fam',
				data: data.fam
			}));*/
        }
    }
	
}

Ext.override(Ext.form.Field, {
	
    showContainer: function() {
        this.setVisible(true);
		
        this.enable();
		
        this.getEl().up('.x-form-item').setDisplayed(true);
    },

    hideContainer: function() {
        this.disable();
		
        this.setVisible(false);
		
        this.getEl().up('.x-form-item').setDisplayed(false);
    },
	
    setContainerVisible: function(visible) {
        if (visible)
            this.showContainer();
        else
            this.hideContainer();
		
        return this;
    }
});

Ext.ux.AvisPanel = Ext.extend(Ext.ux.DataPanel, {
    root: false,
	
    getToolbar: function(p)
    {
        return [{
                
                
            iconCls: 'i-accept',
            tooltip: lang.XEON_APPROVE,
            handler: function()
            {
                var r = p.getSelectedRecord();
				
                Ext.Ajax.request({
                    params: {
                        cmd: 'xeon.saveData',
                        item: r.data.item,
                        'data[com_status]': 1
                    }
                });	
                Ext.select('.success').setStyle('background-color', '#DFF2BF');
                Ext.select('.error').setStyle('background-color', '#DFF2BF');
                Ext.select('.success').setStyle('border','1px solid #4F8A10');
                Ext.select('.error').setStyle('border','1px solid #4F8A10'); 
                /*return;
                if (p.root) {
                    p.view.store.remove(r);
                } else {
                    r.set('info', 1);
                }*/
		//r.set('info', 1);		
                //p.hideToolbar();

            }
        },{
            iconCls: 'i-delete',
            tooltip: lang.XEON_DISAPPROVE,
            handler: function()
            {
                var r = p.getSelectedRecord();
				
                Ext.Ajax.request({
                    params: {
                        cmd: 'xeon.saveData',
                        item: r.data.item,
                        'data[com_status]': 0
                    }
                });
				
                /*r.set('info', 0);
                p.hideToolbar();*/
                Ext.select('.success').setStyle('background-color', '#FFBABA');
                Ext.select('.error').setStyle('background-color', '#FFBABA');
                Ext.select('.success').setStyle('border','1px solid #D8000C');
                Ext.select('.error').setStyle('border','1px solid #D8000C');
            }
        },{
            iconCls: 'i-cross',
            tooltip: lang.XEON_DELETE,
            handler: function()
            {
                var r = p.getSelectedRecord();
				
                Ext.Msg.confirm(lang.XEON_CONFIRM, lang.XEON_COMMENT_DELETE, function(btn)
                {
                    if (btn == 'yes')
                    {
                        Ext.Ajax.request({
                            params: {
                                cmd: 'xeon.deleteItem',
                                item: r.data.item,
                                parent: p.ownerCt.itemId
                            }
                        });
						
                        p.view.store.remove(r);
                        
                        p.hideToolbar();
                    }
                });
            }
        }];
        

    }
});