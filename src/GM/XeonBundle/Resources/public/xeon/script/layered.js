//
// Interface compare v0.1
// Auteur : Mustapha CHELH
//
// dump(obj) for fun ( Equivalent de var_dump() en php)

function dump(obj) {
    var out = '';
    for (var i in obj) {
        out += i + ": " + obj[i] + "\n";
    }

    alert(out);

    // or, if you wanted to avoid alerts...

    var pre = document.createElement('pre');
    pre.innerHTML = out;
    document.body.appendChild(pre)
}
function isArray(obj){
    return(typeof(obj.length)=="undefined")?false:true;
}
function RootPanel(ct, item, data, type)
{

    ct.add(new Ext.ux.InfoPanel({
        region: 'west',
        title: lang.LAYERED_INFORMATION,
        iconCls: 'i-layered',
        html: lang.LAYERED_INFORMATION_TEXT
    }));

    var columns = [];


    columns.push({
        header: lang.LAYERED_ID_MODEL,
        dataIndex: 'layered_id',
        width: 60,
        dataType: 'int'
    },{
        header: lang.LAYERED_NAME_MODEL,
        dataIndex: 'layered_name',
        width: 200,
        itemLink: ['layered.'+type+'', 'layered_id']
    },{
        header: lang.LAYERED_NUMBER_CATEGORIES,
        dataIndex: 'layered_categories',
        width: 150,
        dataType: 'int'
    },{
        header: lang.LAYERED_DATE_ADD,
        dataIndex: 'layered_date_add',
        width:150
    });


    if(item.type=="root")
    {
        columns.push({
            header: "Type",
            dataIndex: 'module',
            width: 100
        });
    }
    ct.add(new Ext.ux.grid.GridPanel({
        region: 'east',
        title: lang.LAYERED_SHOW_MODEL,
        iconCls: 'i-layered',
        itype: 'layered.'+type+'',
        rowId: 'layered_id',
        paging: true,
        quickSearch: true,
        columns: columns
    }));

}
function makeLayeredForm(ct, item, data, type)
{


    var date = new Date();



    if(item.add)
    {

        if(item.type == "shop")
            var lang_title = lang.LAYERED_SHOP_NAME;
        else if(item.type == "compare")
            var lang_title = lang.LAYERED_COMPARE_NAME;
        else
            var lang_title = "";

        var items = [{
            fieldLabel: lang.LAYERED_ADD_MODEL_TO + " " +lang_title,
            name: 'title',
            value: lang.LAYERED_MODEL + " " + date.toString()
        }];



        var form1 = new Ext.ux.FormPanel({

            items: items

        });

        ct.add(form1);

    }
    else
    {


        var form1 = new Ext.ux.FormPanel({
            title: lang.LAYERED_TITLE_MODEL,
            iconCls: 'i-layered',
            //command: 'layered.saveConfigurationLayeredTitle',
            /*buttons: [{
                            text: "Submit",
                            handler: function() {
                                    alert(dump(this.ownerCt.ownerCt.getForm().getValues()));
                            }
                            }],*/
            items: [{
                fieldLabel: lang.LAYERED_MODEL_NAME,
                name: 'titre',
                value: data.title
            }]

        });

        var LayeredGroupCategories = Ext.extend(Ext.form.CheckboxGroup, {
            columns: 1,
            onDisable: function()
            {
                this.items.each(function(c)
                {
                    c.disable();
                    c.setValue(false);
                });
            },

            onEnable: function()
            {
                this.items.each(function(c)
                {
                    c.enable();
                    c.setValue(true);
                });
            }
        });

        var form2 = new Ext.ux.FormPanel({
            title: lang.LAYERED_CHOICE_CATEGORY,
            iconCls: 'i-layered',
            command: 'layered.saveConfigurationLayeredCategory',
            buttons: [{
                text: "Submit",
                hidden: true,
                handler: function() {
                    //this.ownerCt.ownerCt.submit();
                    Ext.Msg.alert("Message", "Merci !!!");
                }
            }]

        });

        // ajouter le formulaire categorie dans un fieldset
        var FieldSet1 = new Ext.form.FieldSet({
            autoHeight: true,
            collapsible: true,
            cls: 'x-fieldset-white',
            title: lang.LAYERED_CATEGORY,
            items: [{
                name: 'id_modele',
                value: item.id,
                hidden: true
            }]
        });



        Ext.each(data.layered, function(f)
        {

            var fieldset = new Ext.form.FieldSet({
                autoHeight: true,
                collapsible: true,
                cls: 'x-fieldset-white',
                //checkboxToggle: true,
                collapsed: f.collapsed,
                collapsedCls: ''
            });


            fieldset.on('expand', function(f)
            {

                f.items.each(function(g) {
                    g.enable();
                });

            });

            fieldset.on('collapse', function(f)
            {
                f.items.each(function(g) {
                    g.disable();
                });
            });


            var i = 1;

            Ext.each(f.items, function(g)
            {
                var checkbox = [];

                checkbox.push({
                    boxLabel: g.title,
                    name: 'cat_'+g.id+'',
                    labelSeparator: '',
                    checked: g.checked,
                    listeners: {
                        check: function(checkbox, checked) {

                            Ext.Ajax.request({
                                params: {
                                    cmd: 'layered.update_feature',
                                    id : item.id,
                                    category : g.id,
                                    is_check : checked

                                },
                                success: function(a)
                                {
                                    ct.reloadItem();
                                }

                            });
                        }
                    }
                });

                Ext.each(g.items, function(c)
                {
                    recursive(c, checkbox, Ext, 20, item, ct);
                });



                var group = new LayeredGroupCategories({
                    fieldLabel: g.title,
                    itemCls: (i%2 == 0) ? 'x-checkboxgroup-alt' : '',
                    items: checkbox
                });

                fieldset.add(group);
                i++;
            });
            // ajouter le fieldset dans un info panel



            FieldSet1.add(fieldset);
        });

        // fin catégorie

        var form3 = new Ext.ux.FormPanel({
            title: lang.LAYERED_CHOICE_FEATURE,
            iconCls: 'i-layered',
            command: 'layered.saveConfigurationLayeredFeature'
        /*buttons: [{
                    text: "Submit",
                    handler: function() {
                            alert(dump(this.ownerCt.ownerCt.getForm().getValues()));
                    }
                    }],*/

        });

        // ajouter le formulaire caractéristiques dans un fieldset
        var FieldSet2 = new Ext.form.FieldSet({
            autoHeight: true,
            collapsible: true,
            cls: 'x-fieldset-white',
            //checkboxToggle: true,
            collapsed: 0,
            items: [{
                name: 'id_modele',
                value: item.id,
                hidden: true
            }],
            collapsedCls: ''
        });

        FieldSet2.on('expand', function(f)
        {

            f.items.each(function(g) {
                g.enable();
            });

        });

        FieldSet2.on('collapse', function(f)
        {
            f.items.each(function(g) {
                g.disable();
            });
        });


        var checkbox = [];
        var i = 0;
        Ext.each(data.feature, function(g)
        {


            if(typeof(g) == "object")
            {
                checkbox.push({
                    boxLabel: g.title,
                    name: 'feature_'+g.id+'',
                    labelSeparator: '',
                    checked: g.cheked

                });
                i++;
            }

        });



        var group2 = new LayeredGroupCategories({
            fieldLabel: lang.LAYERED_FEATURE,
            itemCls: 'x-checkboxgroup-alt',
            items: checkbox
        });


        FieldSet2.add(group2);


        form2.add(FieldSet1);
        form3.add(FieldSet2);




        // ajouter le formulaire dans l'interface
        ct.add(form1);
        ct.add(form2);
        if(i>0)
            ct.add(form3);

    }


}

function recursive(k, checkbox, Ext, style_marginBottom, item, ct)
{
    if(typeof(k.items)!='undefined')
    {

        style_marginBottom=style_marginBottom+20;
        Ext.each(k.items, function(s)
        {
            if(typeof(s.label)!='undefined')
            {

                checkbox.push({
                    boxLabel: s.label,
                    name: 'cat_'+s.id+'',
                    labelSeparator: '',
                    checked: s.checked,
                    listeners: {
                        check: function(checkbox, checked) {

                            Ext.Ajax.request({
                                params: {
                                    cmd: 'layered.update_feature',
                                    id : item.id,
                                    category : s.id,
                                    is_check : checked

                                },
                                success: function(a)
                                {
                                    ct.reloadItem();
                                }

                            });
                        }
                    },
                    style: 'margin-left: '+style_marginBottom+'px'
                });
            }

            checkbox = recursive(s, checkbox, Ext, style_marginBottom, item, ct);



        });
    }

    return checkbox;
}

Xeon.module.layered =
{
    makeCompareRoot: function(ct, item, data)
    {
        RootPanel(ct, item, data, "compare");
    },
    makeShopRoot: function(ct, item, data)
    {
        RootPanel(ct, item, data, "shop");
    },

    makeShopForm: function(ct, item, data)
    {
        makeLayeredForm(ct, item, data,item.type);
    },

    makeCompareForm: function(ct, item, data)
    {
        makeLayeredForm(ct, item, data,item.type);
    },

    makeModuleForm: function(ct,item, data)
    {
        var items = [/*{
			fieldLabel: lang.LAYERED_OPTION_SHOW_0_VALUE,
			xtype: 'togglebox',
			allowBlank: false,
			name: 'LAYERED_SHOP_SHOW_0_VALUE',
			width: 60,
			value: data.show_0_value
		},{
			fieldLabel: lang.LAYERED_OPTION_SHOW_0_QTE,
			xtype: 'togglebox',
			allowBlank: false,
			name: 'LAYERED_SHOP_SHOW_0_QTE',
			width: 60,
			value: data.show_0_qte
		},*/{
            fieldLabel: lang.LAYERED_OPTION_SHOW_PRICE+"("+lang.LAYERED_SHOP_NAME+")",
            xtype: 'togglebox',
            allowBlank: false,
            name: 'LAYERED_SHOP_SHOW_PRICE',
            width: 60,
            value: data.cfg.LAYERED_SHOP_SHOW_PRICE
        }/*,{
			fieldLabel: lang.LAYERED_OPTION_SHOW_SUBCATEGORIES,
			xtype: 'togglebox',
			allowBlank: false,
			name: 'LAYERED_SHOP_SHOW_SUBCATEGORIES',
			width: 60,
			value: data.show_subcategories
		}*/,{
            fieldLabel: lang.LAYERED_OPTION_SHOW_BRAND+"("+lang.LAYERED_SHOP_NAME+")",
            xtype: 'togglebox',
            allowBlank: false,
            name: 'LAYERED_SHOP_SHOW_BRAND',
            width: 60,
            value: data.cfg.LAYERED_SHOP_SHOW_BRAND
        }
        ,
        /*{
			fieldLabel: lang.LAYERED_OPTION_SHOW_0_VALUE,
			xtype: 'togglebox',
			allowBlank: false,
			name: 'LAYERED_COMPARESHOW_0_VALUE',
			width: 60,
			value: data.show_0_value
		},{
			fieldLabel: lang.LAYERED_OPTION_SHOW_0_QTE,
			xtype: 'togglebox',
			allowBlank: false,
			name: 'LAYERED_COMPARESHOW_0_QTE',
			width: 60,
			value: data.show_0_qte
		},*/{
            fieldLabel: lang.LAYERED_OPTION_SHOW_PRICE+"("+lang.LAYERED_COMPARE_NAME+")",
            xtype: 'togglebox',
            allowBlank: false,
            name: 'LAYERED_COMPARE_SHOW_PRICE',
            width: 60,
            value: data.cfg.LAYERED_COMPARE_SHOW_PRICE
        }/*,{
			fieldLabel: lang.LAYERED_OPTION_SHOW_SUBCATEGORIES,
			xtype: 'togglebox',
			allowBlank: false,
			name: 'LAYERED_COMPARESHOW_SUBCATEGORIES',
			width: 60,
			value: data.show_subcategories
		}*/,{
            fieldLabel: lang.LAYERED_OPTION_SHOW_BRAND+"("+lang.LAYERED_C_NAME+")",
            xtype: 'togglebox',
            allowBlank: false,
            name: 'LAYERED_COMPARE_SHOW_BRAND',
            width: 60,
            value: data.cfg.LAYERED_COMPARE_SHOW_BRAND
        },{
            xtype: 'togglebox',
            fieldLabel: lang.LAYERED_ACCEPT_SHOP,
            name: 'plugin_shop',
            value: data.cfg.plugin_shop
        },{
            xtype: 'togglebox',
            fieldLabel: lang.LAYERED_ACCEPT_COMPARE,
            name: 'plugin_compare',
            value: data.cfg.plugin_compare
        }
    
        ];
        ct.add(new Ext.ux.FormPanel({
            title: lang.XEON_SETTINGS,
            iconCls: 'i-cog',
            items: items
        }));
    }

}
