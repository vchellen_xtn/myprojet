
//
// Interface contact v1.0.0
// Auteur : Thierry Marianne
//

Xeon.module.contact =
{
	allowDrop: function(tree, node, target) {
		return true;
	},

	getAction: function(v)
	{
		if (v == 0) return lang.CONTACT_STATE_PENDING;
		if (v == 1) return lang.CONTACT_STATE_PROCESSED;
		if (v == 2) return lang.CONTACT_STATE_ARCHIVED;
		else return false;
	},
	
	makeContactRoot: function(ct, item, data)
	{
		var contact_grid = new Ext.ux.grid.GridPanel({
			title: lang.CONTACT_LIST_IND,
			iconCls: 'i-vcard',
			itemId: 'contact_grid',
			itype: 'contact.cnt',
			reloadAfterEdit: true,
			rowId: 'prf_id',
			paging: true,
			quickSearch: true,
			reloadAfteEdit: true,
			exportPath: true,
			search: [{
					fieldLabel: lang.XEON_FIRSTNAME,
					name: 'prf_first_name'
				},{
					fieldLabel: lang.XEON_LASTNAME,
					name: 'prf_last_name'
				},{
					fieldLabel: lang.XEON_EMAIL,
					name: 'prf_email'
			}],
			columns: [{
					header: lang.XEON_DATE,
					dataIndex: 'prf_date',
					width: 93
				},{
					header: lang.XEON_FIRSTNAME,
					dataIndex: 'prf_first_name',
					itemLink: ['contact.ind', 'prf_id']
				},{
					header: lang.XEON_LASTNAME,
					dataIndex: 'prf_last_name',
					itemLink: ['contact.ind', 'prf_id']
				},{
					header: lang.CONTACT_EMAIL,
					dataIndex: 'prf_email',
					width: 150,
					renderer: Xeon.renderEmail
				},{
					header: lang.CONTACT_COM,
					dataIndex: 'prf_message',
					width: 200
				},{
					header: lang.CONTACT_STATE,
					width: 80,
					dataIndex: 'prf_status',
					renderer: this.getAction,
					editor: new Ext.ux.form.setAction(),
					align: 'left'
			}]
		});

		ct.add(contact_grid);
		
		
		var email_grid = new Ext.ux.grid.GridPanel({
			title: lang.CONTACT_LIST_EMAIL,
			iconCls: 'i-vcard',
			itemId: 'email_grid',
			itype: 'contact.cne',
			reloadAfterEdit: true,
			rowId: 'cne_id',
			paging: true,
			quickSearch: false,
			reloadAfteEdit: true,
			exportPath: false,
			search: false,
			columns: [{
					header: lang.CONTACT_EMAIL,
					dataIndex: 'cne_email',
					width: 150,					
					editor: new Ext.form.TextField({
                        allowBlank: false
                    })
				}]
		});

		ct.add(email_grid);
	},
		
	makeContactForm: function(ct, item, data)
	{
		if (!item.add)
		{
			var thisGrid = new Ext.ux.grid.GridPanel({
				title: lang.CONTACT_LIST_IND,
				exportPath: true,
				iconCls: 'i-vcard',
				itype: 'contact.cnt',
				height: 450,
				paging: true,
				quickSearch: true,
				reloadAfteEdit: true,
				rowId: 'prf_id',
				search: [{
					fieldLabel: lang.XEON_FIRSTNAME,
					name: 'prf_first_name'
				},{
					fieldLabel: lang.XEON_LASTNAME,
					name: 'prf_last_name'
				},{
					fieldLabel: lang.XEON_EMAIL,
					name: 'prf_email'
				}],
				columns: [{
						header: lang.XEON_FIRSTNAME,
						dataIndex: 'prf_first_name',
						itemLink: ['contact.ind', 'prf_id']
					},{
						header: lang.XEON_LASTNAME,
						dataIndex: 'prf_last_name',
						itemLink: ['contact.ind', 'prf_id']
					},{
						header: lang.CONTACT_EMAIL,
						dataIndex: 'prf_email',
						width: 150,
						renderer: Xeon.renderEmail
					},{
						header: lang.CONTACT_COM,
						dataIndex: 'prf_message',
						width: 150
					},{
						header: 'Date',
						dataIndex: 'prf_date',
						width: 100
					},{
						header: lang.CONTACT_STATE,
						dataIndex: 'prf_status',
						renderer: this.getAction,
						editor: new Ext.ux.form.setAction(),
						align: 'left'
				}]
			});
			
			ct.add(thisGrid);
		}
		else
		{
			var items = [];
			items.push({
				xtype: 'titlebox',
				fieldLabel: lang.CONTACT_TITLE,
				allowBlank: false,
				name: 'prf_title'
			},{
				fieldLabel: lang.CONTACT_FIRST_NAME,
				allowBlank: false,
				name: 'prf_first_name'
			},{
				fieldLabel: lang.CONTACT_LAST_NAME,
				allowBlank: false,
				name: 'prf_last_name'
			},{
				fieldLabel: lang.XEON_EMAIL,
				allowBlank: false,
				name: 'prf_email'
			},{
				fieldLabel: lang.CONTACT_ADDRESS,
				allowBlank: true,
				name: 'prf_address'
			},{
				fieldLabel: lang.CONTACT_POSTAL_CODE,
				allowBlank: true,
				name: 'prf_postal_code'
			},{
				fieldLabel: lang.CONTACT_CITY,
				allowBlank: true,
				name: 'prf_city'
			},{
				fieldLabel: lang.CONTACT_PHONE,
				allowBlank: true,
				name: 'prf_phone'
			},{
				fieldLabel: lang.CONTACT_FAX,
				allowBlank: true,
				name: 'prf_fax'
			},{
				fieldLabel: lang.CONTACT_FIRM,
				name: 'prf_company'
			},{
				xtype: 'textarea',
				fieldLabel: lang.CONTACT_COM,
				name: 'prf_message',
				height: 200
			});

			ct.add(new Ext.ux.FormPanel({
				iconCls: 'i-user',
				items: items
			}));
		}
	},

	makeIndividualForm: function(ct, item, data)
	{
		var items = [];
		items.push({
				xtype: 'titlebox',
				fieldLabel: lang.CONTACT_TITLE,
				allowBlank: false,
				name: 'prf_title',
				value: data.db.prf_title
			},{
				fieldLabel: lang.CONTACT_FIRST_NAME,
				allowBlank: false,
				name: 'prf_first_name',
				value: data.db.prf_first_name
			},{
				fieldLabel: lang.CONTACT_LAST_NAME,
				allowBlank: false,
				name: 'prf_last_name',
				value: data.db.prf_last_name
			},{
				fieldLabel: lang.XEON_EMAIL,
				allowBlank: false,
				name: 'prf_email',
				value: data.db.prf_email
			},{
				fieldLabel: lang.CONTACT_ADDRESS,
				allowBlank: true,
				name: 'prf_address',
				value: data.db.prf_address
			},{
				fieldLabel: lang.CONTACT_POSTAL_CODE,
				allowBlank: true,
				name: 'prf_postal_code',
				value: data.db.prf_postal_code
			},{
				fieldLabel: lang.CONTACT_CITY,
				allowBlank: true,
				name: 'prf_city',
				value: data.db.prf_city
			},{
				fieldLabel: lang.CONTACT_PHONE,
				allowBlank: true,
				name: 'prf_phone',
				value: data.db.prf_phone
			},{
				fieldLabel: lang.CONTACT_FAX,
				allowBlank: true,
				name: 'prf_fax',
				value: data.db.prf_fax
			},{
				xtype: 'textarea',
				fieldLabel: lang.CONTACT_COM,
				name: 'prf_message',
				height: 200,
				value: data.db.prf_message
			},{
				xtype: 'combobox',
				fieldLabel: lang.CONTACT_STATE,
				name: 'prf_status',
				value: data.db.prf_status,
				data: [
					[0, lang.CONTACT_STATE_PENDING],
					[1, lang.CONTACT_STATE_PROCESSED],
					[2, lang.CONTACT_STATE_ARCHIVED]	
				]
			});
		
			ct.add(new Ext.ux.FormPanel({
				title: lang.CONTACT_BLOCK_IND,
				iconCls: 'i-user',
				items: items
		}));
	},
	makeEmailForm: function(ct, item, data){
		 var items = [{
            fieldLabel: lang.XEON_EMAIL,
            name: 'cne_email',
            allowBlank: false,
            value: data.db.cne_email
        }];

         ct.add(new Ext.ux.FormPanel({
                items: items
            }));
	}
}

Ext.ux.form.setAction = Ext.extend(Ext.ux.form.ComboBox, {
	data: [
		[0, lang.CONTACT_STATE_PENDING],
		[1, lang.CONTACT_STATE_PROCESSED],
		[2, lang.CONTACT_STATE_ARCHIVED]
	]
});