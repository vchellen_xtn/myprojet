
//
// Classe com v1.2
// Auteur : Maxime TCHOUANMOU
Xeon.module.com =
{
    makeEmailForm : function(ct, item, data)
    {
		editor=new Ext.ux.form.HtmlEditor.Tags;
		editor.tagCat='com.mail.'+data.db.id;
		
         ct.add(new Ext.ux.FormPanel({
            labelWidth:95,
            title: 'Editeur template mail',
            iconCls:'i-wand',
            tools: [{id: 'help',qtip: lang.COM_HELP_MAIL}],
            items:[
             {fieldLabel : 'Expediteur',name:'from',value:data.db.from, disabled:true},
             {fieldLabel:'Cc',name:'cc',value:data.db.cc},
             {
                xtype: 'htmleditor',
                //enableFont: false,
                //enableFontSize: false,
                enableAlignments: false,
                enableLists: false,
                //fieldLabel: lang.COM_MAIL_MODEL_LABEL,
                allowBlank: true,
                width: 600,
                height: 850,
                name:lang.COM_MODEL_NAME,
                value: data.db.content,
                plugins:[
                      new Ext.ux.form.HtmlEditor.Divider(),
                      editor
                ]
            },]
        }));
    },
    
    makeEmailRoot : function(ct, item, data)
    {
    },
    
    makeNewsletterRoot: function(ct, item, data)
    {
            ct.add(new Ext.Panel({
                    title: lang.COM_NEWSLETTER_MODEL,
                    iconCls: 'i-image',
                    bodyStyle: {
                            padding: '10px 0',
                            textAlign: 'center',
                            background: '#ffffff'
                    },
                    html: data.html
            }));
    },

    makeModuleForm: function(ct, item, data)
    {
            ct.add(new Ext.ux.FormPanel({
                    title: lang.XEON_SETTINGS,
                    iconCls: 'i-cog',
                    items: [{
                    	fieldLabel: lang.COM_ARTICLES_COUNT,
                    	name: 'art_count',
                    	value: data.cfg.art_count
                    },{
                    	fieldLabel: lang.COM_PRODUCTS_COUNT,
                    	name: 'pdt_count',
                    	value: data.cfg.pdt_count
                    },{
                    	fieldLabel: lang.COM_FREE_AREA_COUNT,
                    	name: 'free_area_count',
                    	value: data.cfg.free_area_count
                    },{
                    	xtype: 'mediabox',
                    	fieldLabel: lang.COM_NEWSLETTER_MODEL,
                    	name: 'newsletter_model',
                    	value: data.cfg.newsletter_model,
                    	//displayValue: data.cfg.newsletter_model_txt
                    	displayValue: 'Media'
                    },{
                        fieldLabel: lang.COM_CLASS_TEMPLATE,
                        name: 'layout_newsletter',
                        value: data.cfg.layout_newsletter
                    }]
            }));
    
    },

    makeNewsletterForm: function(ct, item, data)
    {
       if(data['preview'])
        {                
            ct.add({
                    frame: false,
                    border: false,
                    width: 800,
                    html: data.data
                });
        }
        else
        {
        	var thisItem = Xeon.getId(ct.itemId);
        	
            var newsTitle = data.db ? data.db.news_title : '';
            var newsEditDate = data.db ? data.db.news_edit_date : '';
            var panelTitle = data.db ? lang.COM_HEAD : '';

            if(!item.add)
            {
                // Zone d'infos de la newsletter
                var infoPanel = new Ext.ux.InfoPanel({
                    iconCls: 'i-information',
                    title: lang.COM_INFO,
                    buttonAlign: 'center',
                    items: [{
	                            infoLabel: lang.COM_NEWSLETTER_MAKE_DATE,
	                            value: data.db.news_make_date
	                        }],
                    buttons: [{
	                    	text: lang.COM_PREVIEW_NEWSLETTER,
	                    	handler: function(){
		                    	Xeon.loadItem(ct.itemId+'-PREVIEW');
		                    }
	                    },{
	                    	text: lang.COM_SAVE_NEWSLETTER,
	                    	handler: function(){
	                    		infoPanel.submit();
	                    }
	                    }],
                    submit: function()
                    {
                        Ext.Ajax.request({
                                        params: {
                                                cmd: 'com.showNewsletter',
                                                item: ct.itemId
                                        },
                                        before: function() {
                                                infoPanel.bwrap.mask(lang.XEON_LOADING, 'x-mask-loading');
                                        },
                                        success: function( r ) {
                                                Xeon.getFile();
                                        },
                                        callback: function() {
                                                infoPanel.bwrap.unmask();
                                        }
                                });
                    }
                })
                ct.add(infoPanel);

            }
            

            // Zone de MAJ des infos de la newsletter
            var headPanel = new Ext.ux.FormPanel({
                iconCls: 'i-tag-blue',
                title: panelTitle,
                items: [{
                        name: 'news_title',
                        fieldLabel: lang.COM_NEWSLETTER_TITLE,
                        value: newsTitle
                },{
                        xtype: 'datefield',
                        name: 'news_edit_date',
                        value: newsEditDate,
                        fieldLabel: lang.COM_NEWSLETTER_EDIT_DATE
                }]
            })
            ct.add(headPanel);

            if(!item.add)
            {
                // Affichage des champs d'ajout d'articles
                if(data.art_count > 0)
                {

                    var items = [];
                    for(var i = 1; i <= data.art_count; i++)
                    {
                        items.push({
                                fieldLabel: i == -1 ? lang.COM_FIRST_ARTICLE : lang.COM_ARTICLE+" "+i,
                                emptyText: lang.COM_SELECT_ARTICLE,
                                allowBlank: true,
                                itype: 'cms.pge',
                                name: 'cnt_art['+i+']',
                                value: data.articles[i] ? data.articles[i].pge_id : ""
                        });
                    }

                    var artPanel = new Ext.ux.FormPanel({
                        iconCls: 'i-page-white-copy',
                        title: lang.COM_NEWSLETTER_ARTICLES,
                        items: [
                            items,
                            {
                                xtype: 'hidden',
                                name: 'cnt_type',
                                value: 0
                            }]
                    })
                    ct.add(artPanel);
                }


                // Affichage des champs d'ajout de produits
                if(data.pdt_count > 0)
                {
                    var itemsPdt = [];
                    for(i = 1; i <= data.pdt_count; i++)
                    {
                        itemsPdt.push({
                				xtype: 'listbox',
                                fieldLabel: lang.COM_PRODUCT+" "+i,
                                itype: 'shop.prd',
                                name: 'cnt_pdt['+i+']',
                                value: data.products[i] ? data.products[i].prd_id : "",
                                displayValue: data.products[i] ? data.products[i].prd_title : ""
                        });
                    }
                    
                    
                    var pdtPanel = new Ext.ux.FormPanel({
                        iconCls: 'i-box',
                        title: lang.COM_NEWSLETTER_PRODUCTS,
                        items: [itemsPdt,
                                {
                        			xtype: 'hidden',
                                    name: 'cnt_type',
                                    value: 1
                                }]
                    });
                    ct.add(pdtPanel);
                }

                // Affichage des champs libres
                if(data.free_area_count > 0)
                {
                    var comboBox = new Array;
                    var mediaBox = new Array;
                    var areaBox = new Array;
                    var mediaLink = new Array;

                    for(var i = 1; i <= data.free_area_count; i++)
                    {

                        var cnt_media_link = (data.freeArea[i]) ? data.freeArea[i].cnt_media_link : "";
                        var cnt_text = (data.freeArea[i]) ? data.freeArea[i].cnt_text : "";
                        var cnt_media = (data.freeArea[i]) ? data.freeArea[i].cnt_media : "";
                        var fle_title = (data.freeArea[i]) ? data.freeArea[i].fle_title : "";
                        var cnt_type = (data.freeArea[i]) ? data.freeArea[i].cnt_type : "";

                        comboBox[i] = new Ext.ux.form.ComboBox({
                            fieldLabel: lang.COM_CONTENT_TYPE,
                            emptyText: lang.COM_SELECT_TYPE,
                            num: i,
                            allowBlank: true,
                            value: cnt_type,
                            name: 'cnt_type',
                            data: [[2, lang.COM_IMAGE], [3, lang.COM_TEXT], [4, lang.COM_IMAGE_TEXT]]
                        });

                        comboBox[i].on('select', function(){
                            switch (this.getValue()) {
                                case 2:
                                    mediaBox[this.num].showContainer();
                                    mediaLink[this.num].showContainer();
                                    areaBox[this.num].hideContainer();
                                    break;

                                case 3:
                                    mediaBox[this.num].hideContainer();
                                    mediaLink[this.num].hideContainer();
                                    areaBox[this.num].showContainer();
                                    break;

                                case 4:
                                    mediaBox[this.num].showContainer();
                                    areaBox[this.num].showContainer();
                                    mediaLink[this.num].showContainer();
                                    break;

                                default:
                                    break;
                            }
                        });

                        mediaBox[i] = new Ext.ux.form.MediaBox({
                            fieldLabel: lang.COM_CONTENT_MEDIA,
                            name: 'cnt_media',
                            allowBlank: false,
                            type: cnt_type,
                            value: cnt_media,
                            displayValue: fle_title
                        });

                        mediaBox[i].on('render', function(){
                            if(this.type == 3 || this.type ==  0)
                                this.hideContainer();
                        });
                        
                        areaBox[i] = new Ext.form.HtmlEditor({
                            fieldLabel: lang.COM_CONTENT_AREA,
                            enableFont : false,
                            enableFontSize : true,
                            name: 'cnt_text',
                            allowBlank: false,
                            width: 400,
                            type: cnt_type,
                            value: cnt_text
                        });

                        areaBox[i].on('render', function(){
                            if(this.type == 2 || this.type == 0)
                                this.hideContainer();
                        });

                        mediaLink[i] = new Ext.form.TextField({
                            fieldLabel: lang.COM_MEDIA_LINK,
                            name: 'cnt_media_link',
                            allowBlank: false,
                            type: cnt_type,
                            value: cnt_media_link
                        });

                        mediaLink[i].on('render', function(){
                            if(this.type == 3 || this.type == 0)
                                this.hideContainer();
                        });

                        ct.add(new Ext.ux.FormPanel({
                            iconCls: 'i-textfield',
                            title: lang.COM_NEWSLETTER_AREA+" "+i,
                            items: [
                                comboBox[i],
                                areaBox[i],
                                mediaBox[i],
                                mediaLink[i],
                                {
                                    xtype: 'hidden',
                                    name: 'cnt_index',
                                    value: i
                                }
                            ]
                        }));
                    }
                }
            }
        }
    }
    
}

//Pour pouvoir masquer une ligne complete d'un formulaire
Ext.override(Ext.form.Field, {
	
	showContainer: function() 
	{
		this.setVisible(true);
		
		this.enable();
	
		this.getEl().up('.x-form-item').setDisplayed(true);
	},
	
	hideContainer: function() 
	{
		this.disable(); 
		
		this.setVisible(false);
	
		this.getEl().up('.x-form-item').setDisplayed(false);
	},
	
	setContainerVisible: function(visible) 
	{
		if (visible)
			this.showContainer();
		else
			this.hideContainer();
		
		return this;
	}
});

