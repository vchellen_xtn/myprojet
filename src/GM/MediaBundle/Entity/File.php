<?php

namespace GM\MediaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * File
 *
 * @ORM\Table(name="gm_media__file")
 * @ORM\Entity(repositoryClass="GM\MediaBundle\Entity\FileRepository")
 */
class File
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    protected $file;

    /**
     * @ORM\ManyToOne(targetEntity="GM\MediaBundle\Entity\Folder", inversedBy="files")
     */
    private $folder;

    /**
     * @ORM\OneToMany(targetEntity="GM\MediaBundle\Entity\File", mappedBy="preview")
     * @ORM\OrderBy({"title" = "ASC"})
     */
    private $owners;

    /**
     * @ORM\ManyToOne(targetEntity="GM\MediaBundle\Entity\File", inversedBy="owners", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="preview_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $preview;

    /**
     * @ORM\ManyToOne(targetEntity="Totem\RequestBundle\Entity\Request")
     */
    private $request;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="caption", type="string", length=255, nullable=true)
     */
    private $caption;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255, nullable=true)
     */
    private $path;

    /**
     * @var integer
     *
     * @ORM\Column(name="level", type="integer", nullable=true)
     */
    private $level;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=true)
     */
    private $link;

    /**
     * @var integer
     *
     * @ORM\Column(name="link_target", type="smallint", nullable=true)
     */
    private $linkTarget;

    /**
     * @ORM\ManyToOne(targetEntity="GM\MediaBundle\Entity\Ext")
     */
    private $ext;

    /**
     * @var integer
     *
     * @ORM\Column(name="width", type="integer", nullable=true)
     */
    private $width;

    /**
     * @var integer
     *
     * @ORM\Column(name="height", type="integer", nullable=true)
     */
    private $height;

    /**
     * @var integer
     *
     * @ORM\Column(name="size", type="integer", nullable=true)
     */
    private $size;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="make_date", type="datetime")
     */
    private $makeDate;

    /**
     * @ORM\ManyToOne(targetEntity="GM\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="make_user_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $makeUser;

    public function __construct()
    {
        $this->makeDate = new \DateTime();
        $this->owners = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fldId
     *
     * @param integer $fldId
     * @return MediaFile
     */
    public function setFldId($fldId)
    {
        $this->fldId = $fldId;
    
        return $this;
    }

    /**
     * Get fldId
     *
     * @return integer 
     */
    public function getFldId()
    {
        return $this->fldId;
    }

    /**
     * Set fleTitle
     *
     * @param string $fleTitle
     * @return MediaFile
     */
    public function setFleTitle($fleTitle)
    {
        $this->fleTitle = $fleTitle;
    
        return $this;
    }

    /**
     * Get fleTitle
     *
     * @return string 
     */
    public function getFleTitle()
    {
        return $this->fleTitle;
    }

    /**
     * Set fleCaption
     *
     * @param string $fleCaption
     * @return MediaFile
     */
    public function setFleCaption($fleCaption)
    {
        $this->fleCaption = $fleCaption;
    
        return $this;
    }

    /**
     * Get fleCaption
     *
     * @return string 
     */
    public function getFleCaption()
    {
        return $this->fleCaption;
    }

    /**
     * Set fleDescription
     *
     * @param string $fleDescription
     * @return MediaFile
     */
    public function setFleDescription($fleDescription)
    {
        $this->fleDescription = $fleDescription;
    
        return $this;
    }

    /**
     * Get fleDescription
     *
     * @return string 
     */
    public function getFleDescription()
    {
        return $this->fleDescription;
    }

    /**
     * Set fleUrl
     *
     * @param string $fleUrl
     * @return MediaFile
     */
    public function setFleUrl($fleUrl)
    {
        $this->fleUrl = $fleUrl;
    
        return $this;
    }

    /**
     * Get fleUrl
     *
     * @return string 
     */
    public function getFleUrl()
    {
        return $this->fleUrl;
    }

    /**
     * Set flePath
     *
     * @param string $flePath
     * @return MediaFile
     */
    public function setFlePath($flePath)
    {
        $this->flePath = $flePath;
    
        return $this;
    }

    /**
     * Get flePath
     *
     * @return string 
     */
    public function getFlePath()
    {
        return $this->flePath;
    }

    /**
     * Set fleLevel
     *
     * @param integer $fleLevel
     * @return MediaFile
     */
    public function setFleLevel($fleLevel)
    {
        $this->fleLevel = $fleLevel;
    
        return $this;
    }

    /**
     * Get fleLevel
     *
     * @return integer 
     */
    public function getFleLevel()
    {
        return $this->fleLevel;
    }

    /**
     * Set fleLink
     *
     * @param string $fleLink
     * @return MediaFile
     */
    public function setFleLink($fleLink)
    {
        $this->fleLink = $fleLink;
    
        return $this;
    }

    /**
     * Get fleLink
     *
     * @return string 
     */
    public function getFleLink()
    {
        return $this->fleLink;
    }

    /**
     * Set fleLinkTarget
     *
     * @param integer $fleLinkTarget
     * @return MediaFile
     */
    public function setFleLinkTarget($fleLinkTarget)
    {
        $this->fleLinkTarget = $fleLinkTarget;
    
        return $this;
    }

    /**
     * Get fleLinkTarget
     *
     * @return integer 
     */
    public function getFleLinkTarget()
    {
        return $this->fleLinkTarget;
    }

    /**
     * Set fleExt
     *
     * @param string $fleExt
     * @return MediaFile
     */
    public function setFleExt($fleExt)
    {
        $this->fleExt = $fleExt;
    
        return $this;
    }

    /**
     * Get fleExt
     *
     * @return string 
     */
    public function getFleExt()
    {
        return $this->fleExt;
    }

    /**
     * Set fleWidth
     *
     * @param integer $fleWidth
     * @return MediaFile
     */
    public function setFleWidth($fleWidth)
    {
        $this->fleWidth = $fleWidth;
    
        return $this;
    }

    /**
     * Get fleWidth
     *
     * @return integer 
     */
    public function getFleWidth()
    {
        return $this->fleWidth;
    }

    /**
     * Set fleHeight
     *
     * @param integer $fleHeight
     * @return MediaFile
     */
    public function setFleHeight($fleHeight)
    {
        $this->fleHeight = $fleHeight;
    
        return $this;
    }

    /**
     * Get fleHeight
     *
     * @return integer 
     */
    public function getFleHeight()
    {
        return $this->fleHeight;
    }

    /**
     * Set fleSize
     *
     * @param integer $fleSize
     * @return MediaFile
     */
    public function setFleSize($fleSize)
    {
        $this->fleSize = $fleSize;
    
        return $this;
    }

    /**
     * Get fleSize
     *
     * @return integer 
     */
    public function getFleSize()
    {
        return $this->fleSize;
    }

    /**
     * Set fleMakeDate
     *
     * @param \DateTime $fleMakeDate
     * @return MediaFile
     */
    public function setFleMakeDate($fleMakeDate)
    {
        $this->fleMakeDate = $fleMakeDate;
    
        return $this;
    }

    /**
     * Get fleMakeDate
     *
     * @return \DateTime 
     */
    public function getFleMakeDate()
    {
        return $this->fleMakeDate;
    }

    /**
     * Set fleMakeUser
     *
     * @param integer $fleMakeUser
     * @return MediaFile
     */
    public function setFleMakeUser($fleMakeUser)
    {
        $this->fleMakeUser = $fleMakeUser;
    
        return $this;
    }

    /**
     * Get fleMakeUser
     *
     * @return integer 
     */
    public function getFleMakeUser()
    {
        return $this->fleMakeUser;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return File
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set caption
     *
     * @param string $caption
     * @return File
     */
    public function setCaption($caption)
    {
        $this->caption = $caption;
    
        return $this;
    }

    /**
     * Get caption
     *
     * @return string 
     */
    public function getCaption()
    {
        return $this->caption;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return File
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return File
     */
    public function setUrl($url)
    {
        $this->url = $url;
    
        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return File
     */
    public function setPath($path)
    {
        $this->path = $path;
    
        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set level
     *
     * @param integer $level
     * @return File
     */
    public function setLevel($level)
    {
        $this->level = $level;
    
        return $this;
    }

    /**
     * Get level
     *
     * @return integer 
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return File
     */
    public function setLink($link)
    {
        $this->link = $link;
    
        return $this;
    }

    /**
     * Get link
     *
     * @return string 
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set linkTarget
     *
     * @param integer $linkTarget
     * @return File
     */
    public function setLinkTarget($linkTarget)
    {
        $this->linkTarget = $linkTarget;
    
        return $this;
    }

    /**
     * Get linkTarget
     *
     * @return integer 
     */
    public function getLinkTarget()
    {
        return $this->linkTarget;
    }

    /**
     * Set ext
     *
     * @param string $ext
     * @return File
     */
    public function setExt($ext)
    {
        $this->ext = $ext;
    
        return $this;
    }

    /**
     * Get ext
     *
     * @return string 
     */
    public function getExt()
    {
        return $this->ext;
    }

    /**
     * Set width
     *
     * @param integer $width
     * @return File
     */
    public function setWidth($width)
    {
        $this->width = $width;
    
        return $this;
    }

    /**
     * Get width
     *
     * @return integer 
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set height
     *
     * @param integer $height
     * @return File
     */
    public function setHeight($height)
    {
        $this->height = $height;
    
        return $this;
    }

    /**
     * Get height
     *
     * @return integer 
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set size
     *
     * @param integer $size
     * @return File
     */
    public function setSize($size)
    {
        $this->size = $size;
    
        return $this;
    }

    /**
     * Get size
     *
     * @return integer 
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set makeDate
     *
     * @param \DateTime $makeDate
     * @return File
     */
    public function setMakeDate($makeDate)
    {
        $this->makeDate = $makeDate;
    
        return $this;
    }

    /**
     * Get makeDate
     *
     * @return \DateTime 
     */
    public function getMakeDate()
    {
        return $this->makeDate;
    }

    /**
     * Set makeUser
     *
     * @param integer $makeUser
     * @return File
     */
    public function setMakeUser($makeUser)
    {
        $this->makeUser = $makeUser;
    
        return $this;
    }

    /**
     * Get makeUser
     *
     * @return integer 
     */
    public function getMakeUser()
    {
        return $this->makeUser;
    }

    /**
     * Set folder
     *
     * @param \GM\MediaBundle\Entity\Folder $folder
     * @return File
     */
    public function setFolder(\GM\MediaBundle\Entity\Folder $folder = null)
    {
        $this->folder = $folder;
    
        return $this;
    }

    /**
     * Get folder
     *
     * @return \GM\MediaBundle\Entity\Folder 
     */
    public function getFolder()
    {
        return $this->folder;
    }

    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set preview
     *
     * @param \GM\MediaBundle\Entity\File $preview
     * @return File
     */
    public function setPreview(\GM\MediaBundle\Entity\File $preview = null)
    {
        $this->preview = $preview;
    
        return $this;
    }

    /**
     * Get preview
     *
     * @return \GM\MediaBundle\Entity\File 
     */
    public function getPreview()
    {
        return $this->preview;
    }

    /**
     * @param mixed $request
     */
    public function setRequest($request)
    {
        $this->request = $request;
    }

    /**
     * @return mixed
     */
    public function getRequest()
    {
        return $this->request;
    }


}