<?php

namespace GM\MediaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Folder
 *
 * @ORM\Table(name="gm_media__folder")
 * @ORM\Entity(repositoryClass="GM\MediaBundle\Entity\FolderRepository")
 */
class Folder
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="GM\MediaBundle\Entity\Folder", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="GM\MediaBundle\Entity\Folder", mappedBy="parent")
     */
    private $children;

    /**
     * @ORM\OneToMany(targetEntity="GM\MediaBundle\Entity\File", mappedBy="folder")
     */
    private $files;

    /**
     * @ORM\ManyToOne(targetEntity="GM\SiteBundle\Entity\Domain")
     */
    private $domain;

    /**
     * @ORM\ManyToOne(targetEntity="GM\MediaBundle\Entity\Type")
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="make_date", type="datetime")
     */
    private $makeDate;

    /**
     * @ORM\ManyToOne(targetEntity="GM\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="make_user_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $makeUser;

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->files = new ArrayCollection();
        $this->makeDate = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fldParent
     *
     * @param integer $fldParent
     * @return MediaFolder
     */
    public function setFldParent($fldParent)
    {
        $this->fldParent = $fldParent;
    
        return $this;
    }

    /**
     * Get fldParent
     *
     * @return integer 
     */
    public function getFldParent()
    {
        return $this->fldParent;
    }

    /**
     * Set fldType
     *
     * @param integer $fldType
     * @return MediaFolder
     */
    public function setFldType($fldType)
    {
        $this->fldType = $fldType;
    
        return $this;
    }

    /**
     * Get fldType
     *
     * @return integer 
     */
    public function getFldType()
    {
        return $this->fldType;
    }

    /**
     * Set fldTitle
     *
     * @param string $fldTitle
     * @return MediaFolder
     */
    public function setFldTitle($fldTitle)
    {
        $this->fldTitle = $fldTitle;
    
        return $this;
    }

    /**
     * Get fldTitle
     *
     * @return string 
     */
    public function getFldTitle()
    {
        return $this->fldTitle;
    }

    /**
     * Set fleUrl
     *
     * @param string $fleUrl
     * @return MediaFolder
     */
    public function setFleUrl($fleUrl)
    {
        $this->fleUrl = $fleUrl;
    
        return $this;
    }

    /**
     * Get fleUrl
     *
     * @return string 
     */
    public function getFleUrl()
    {
        return $this->fleUrl;
    }

    /**
     * Set fldMakeDate
     *
     * @param \DateTime $fldMakeDate
     * @return MediaFolder
     */
    public function setFldMakeDate($fldMakeDate)
    {
        $this->fldMakeDate = $fldMakeDate;
    
        return $this;
    }

    /**
     * Get fldMakeDate
     *
     * @return \DateTime 
     */
    public function getFldMakeDate()
    {
        return $this->fldMakeDate;
    }

    /**
     * Set fldMakeUser
     *
     * @param integer $fldMakeUser
     * @return MediaFolder
     */
    public function setFldMakeUser($fldMakeUser)
    {
        $this->fldMakeUser = $fldMakeUser;
    
        return $this;
    }

    /**
     * Get fldMakeUser
     *
     * @return integer 
     */
    public function getFldMakeUser()
    {
        return $this->fldMakeUser;
    }

    /**
     * Set zone
     *
     * @param integer $zone
     * @return Folder
     */
    public function setZone($zone)
    {
        $this->zone = $zone;
    
        return $this;
    }

    /**
     * Get zone
     *
     * @return integer 
     */
    public function getZone()
    {
        return $this->zone;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Folder
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Folder
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Folder
     */
    public function setUrl($url)
    {
        $this->url = $url;
    
        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set makeDate
     *
     * @param \DateTime $makeDate
     * @return Folder
     */
    public function setMakeDate($makeDate)
    {
        $this->makeDate = $makeDate;
    
        return $this;
    }

    /**
     * Get makeDate
     *
     * @return \DateTime 
     */
    public function getMakeDate()
    {
        return $this->makeDate;
    }

    /**
     * Set makeUser
     *
     * @param integer $makeUser
     * @return Folder
     */
    public function setMakeUser($makeUser)
    {
        $this->makeUser = $makeUser;
    
        return $this;
    }

    /**
     * Get makeUser
     *
     * @return integer 
     */
    public function getMakeUser()
    {
        return $this->makeUser;
    }

    /**
     * Set parent
     *
     * @param \GM\MediaBundle\Entity\Folder $parent
     * @return Folder
     */
    public function setParent(\GM\MediaBundle\Entity\Folder $parent = null)
    {
        $this->parent = $parent;
    
        return $this;
    }

    /**
     * Get parent
     *
     * @return \GM\MediaBundle\Entity\Folder 
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add children
     *
     * @param \GM\MediaBundle\Entity\Folder $children
     * @return Folder
     */
    public function addChildren(\GM\MediaBundle\Entity\Folder $children)
    {
        $this->children[] = $children;
    
        return $this;
    }

    /**
     * Remove children
     *
     * @param \GM\MediaBundle\Entity\Folder $children
     */
    public function removeChildren(\GM\MediaBundle\Entity\Folder $children)
    {
        $this->children->removeElement($children);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Add files
     *
     * @param \GM\MediaBundle\Entity\File $files
     * @return Folder
     */
    public function addFile(\GM\MediaBundle\Entity\File $files)
    {
        $this->files[] = $files;
    
        return $this;
    }

    /**
     * Remove files
     *
     * @param \GM\MediaBundle\Entity\File $files
     */
    public function removeFile(\GM\MediaBundle\Entity\File $files)
    {
        $this->files->removeElement($files);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Set domain
     *
     * @param \GM\SiteBundle\Entity\Domain $domain
     * @return Folder
     */
    public function setDomain(\GM\SiteBundle\Entity\Domain $domain = null)
    {
        $this->domain = $domain;
    
        return $this;
    }

    /**
     * Get domain
     *
     * @return \GM\SiteBundle\Entity\Domain 
     */
    public function getDomain()
    {
        return $this->domain;
    }
}