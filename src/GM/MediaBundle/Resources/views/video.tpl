<div class="content">
    <div class="videoFile">
		<h1>{$data.currentFile->fle_title}</h1>
		
		<object width="{$data.width}" height="{$data.height}">
			<param name="movie" value="{$data.movie}"></param>
			<param name="allowFullScreen" value="true"></param>
			<param name="allowScriptAccess" value="always"></param>
			<param name="wmode" value="transparent"></param>
			<param name="flashvars" value="{$data.flashvars}"></param>
			<embed src="{$data.movie}" type="application/x-shockwave-flash" allowFullScreen="true" allowScriptAccess="always" width="{$data.width}" height="{$data.height}" wmode="transparent" flashvars="{$data.flashvars}"></embed>
		</object>
		
	</div>
</div>
<br class="clear" />