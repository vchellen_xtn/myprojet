<div class="content">
    <div class="videoFolder">
		<h1>{$data.currentFolder->fld_title}</h1>
		
		{if ($data.directorylist|count)==0 && ($data.videoslist|count)==0}
			<p>Ce dossier est vide</p>
		{/if}
		{if ($data.directorylist|count)>0}
			<h2>Sous-Dossiers</h2>
			
			{foreach $data.directorylist as $dir}
				<div class="Videofolder">
					<p><a href="{$dir->getVideoFolderURL()}"> {$dir->fld_title}</a></p>
				</div>
			{/foreach}
			<br class="clear" />
		{/if}
		
		{if ($data.videoslist|count)>0}
			<h2>Videos</h2>
			{foreach $data.videoslist as $video}
				<div class="Video">
					<p><a href="{$video->getVideoURL()}"> {$video->fle_title}</a></p>
					<a href="{$video->getVideoURL()}"><img src="{$video->getThumbnailVideo('200x200')}" /></a>
				</div>
			{/foreach}
			<br class="clear" />
		{/if}
	</div>
</div>
<br class="clear" />