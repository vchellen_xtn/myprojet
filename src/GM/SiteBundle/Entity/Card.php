<?php

namespace GM\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use GM\SeoBundle\Entity\Seo;
use GM\StatBundle\Entity\Stat;
use GM\WidgetBundle\Entity\Container;

/**
 * Card
 *
 * @ORM\Table(name="card")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class Card {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="GM\SiteBundle\Entity\Domain")
     */
    private $domain;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="full_title", type="string", length=255, nullable=true)
     */
    private $fullTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="catcher", type="text", nullable=true)
     */
    private $catcher;

    /**
     * @ORM\ManyToOne(targetEntity="GM\MediaBundle\Entity\File")
     */
    private $file;

    /**
     * @var integer
     *
     * @ORM\Column(name="rank", type="integer")
     */
    private $rank;

    /**
     * @ORM\OneToOne(targetEntity="GM\StatBundle\Entity\Stat", cascade={"remove","persist"})
     */
    private $stat;
    
    /*
     * @ORM\OneToOne(targetEntity="GM\SeoBundle\Entity\Seo", cascade={"remove","persist"})
    private $seo;
     */
    
    /**
     * @ORM\OneToOne(targetEntity="GM\WidgetBundle\Entity\Container", cascade={"remove","persist"})
     */
    private $container;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, nullable=true)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="entity", type="string", length=255)
     */
    private $entity;

    /**
     * @var integer
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     */
    private $active;

    public function __construct() {
        $this->rank = 1;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist() {
        if(!$this->seo)
            $this->seo = new Seo();
        if(!$this->stat)
            $this->stat = new Stat();
        if(!$this->container)
            $this->container = new Container();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Card
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set fullTitle
     *
     * @param string $fullTitle
     * @return Card
     */
    public function setFullTitle($fullTitle)
    {
        $this->fullTitle = $fullTitle;
    
        return $this;
    }

    /**
     * Get fullTitle
     *
     * @return string 
     */
    public function getFullTitle()
    {
        return $this->fullTitle;
    }

    /**
     * Set catcher
     *
     * @param string $catcher
     * @return Card
     */
    public function setCatcher($catcher)
    {
        $this->catcher = $catcher;
    
        return $this;
    }

    /**
     * Get catcher
     *
     * @return string 
     */
    public function getCatcher()
    {
        return $this->catcher;
    }

    /**
     * Set rank
     *
     * @param integer $rank
     * @return Card
     */
    public function setRank($rank)
    {
        $this->rank = $rank;
    
        return $this;
    }

    /**
     * Get rank
     *
     * @return integer 
     */
    public function getRank()
    {
        return $this->rank;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Card
     */
    public function setActive($active)
    {
        $this->active = $active;
    
        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set domain
     *
     * @param \GM\SiteBundle\Entity\Domain $domain
     * @return Card
     */
    public function setDomain(\GM\SiteBundle\Entity\Domain $domain = null)
    {
        $this->domain = $domain;
    
        return $this;
    }

    /**
     * Get domain
     *
     * @return \GM\SiteBundle\Entity\Domain 
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set file
     *
     * @param \GM\MediaBundle\Entity\File $file
     * @return Card
     */
    public function setFile(\GM\MediaBundle\Entity\File $file = null)
    {
        $this->file = $file;
    
        return $this;
    }

    /**
     * Get file
     *
     * @return \GM\MediaBundle\Entity\File 
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set stat
     *
     * @param \GM\StatBundle\Entity\Stat $stat
     * @return Card
     */
    public function setStat(\GM\StatBundle\Entity\Stat $stat = null)
    {
        $this->stat = $stat;
    
        return $this;
    }

    /**
     * Get stat
     *
     * @return \GM\StatBundle\Entity\Stat 
     */
    public function getStat()
    {
        return $this->stat;
    }

    /**
     * Set seo
     *
     * @param \GM\SeoBundle\Entity\Seo $seo
     * @return Card
     */
    public function setSeo(\GM\SeoBundle\Entity\Seo $seo = null)
    {
        $this->seo = $seo;
    
        return $this;
    }

    /**
     * Get seo
     *
     * @return \GM\SeoBundle\Entity\Seo 
     */
    public function getSeo()
    {
        return $this->seo;
    }

    /**
     * Set container
     *
     * @param \GM\WidgetBundle\Entity\Container $container
     * @return Card
     */
    public function setContainer(\GM\WidgetBundle\Entity\Container $container = null)
    {
        $this->container = $container;
    
        return $this;
    }

    /**
     * Get container
     *
     * @return \GM\WidgetBundle\Entity\Container 
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * Set entity
     *
     * @param string $entity
     * @return Card
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
    
        return $this;
    }

    /**
     * Get entity
     *
     * @return string 
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Card
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    
        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }
}