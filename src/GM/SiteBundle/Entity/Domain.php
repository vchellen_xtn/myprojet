<?php

namespace GM\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Domain
 *
 * @ORM\Table(name="domain")
 * @ORM\Entity
 */
class Domain
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="GM\SiteBundle\Entity\Site")
     */
    private $site;

    /**
     * @ORM\OneToMany(targetEntity="GM\SiteBundle\Entity\Alias", mappedBy="domain", cascade={"remove"})
     */
    private $alias;

    /**
     * @var string
     *
     * @ORM\Column(name="domain", type="string", length=255)
     */
    private $domain;

    /**
     * @ORM\ManyToOne(targetEntity="GM\GeoBundle\Entity\Country")
     */
    private $country;

    /**
     * @ORM\ManyToOne(targetEntity="GM\GeoBundle\Entity\Lang")
     */
    private $lang;

    /**
     * @var smallint
     *
     * @ORM\Column(name="secure", type="smallint", nullable=true)
     */
    private $secure;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="tagline", type="string", length=255, nullable=true)
     */
    private $tagline;

    /*
     * @ORM\ManyToOne(targetEntity="GM\SkinBundle\Entity\Skin")
    private $skin;
     */

    /*
     * @ORM\ManyToOne(targetEntity="GM\SkinBundle\Entity\Container")
    private $container;
     */

    /**
     * @ORM\ManyToOne(targetEntity="Domain")
     * @ORM\JoinColumn(name="fallback_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $fallback;


    /**
     * @var integer
     *
     * @ORM\Column(name="active", type="smallint", nullable=true)
     */
    private $active;

    public function __construct()
    {
        $this->alias = new ArrayCollection();
    }

    public function isSecure()
    {
        return $this->secure;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Domain
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set secure
     *
     * @param integer $secure
     * @return Domain
     */
    public function setSecure($secure)
    {
        $this->secure = $secure;
    
        return $this;
    }

    /**
     * Get secure
     *
     * @return integer 
     */
    public function getSecure()
    {
        return $this->secure;
    }

    /**
     * Set domain
     *
     * @param string $domain
     * @return Domain
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;
    
        return $this;
    }

    /**
     * Get domain
     *
     * @return string 
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set active
     *
     * @param integer $active
     * @return Domain
     */
    public function setActive($active)
    {
        $this->active = $active;
    
        return $this;
    }

    /**
     * Get active
     *
     * @return integer 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Add alias
     *
     * @param \GM\SiteBundle\Entity\Alias $alias
     * @return Domain
     */
    public function addAlia(\GM\SiteBundle\Entity\Alias $alias)
    {
        $this->alias[] = $alias;
    
        return $this;
    }

    /**
     * Remove alias
     *
     * @param \GM\SiteBundle\Entity\Alias $alias
     */
    public function removeAlia(\GM\SiteBundle\Entity\Alias $alias)
    {
        $this->alias->removeElement($alias);
    }

    /**
     * Get alias
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set country
     *
     * @param \GM\GeoBundle\Entity\Country $country
     * @return Domain
     */
    public function setCountry(\GM\GeoBundle\Entity\Country $country = null)
    {
        $this->country = $country;
    
        return $this;
    }

    /**
     * Set tagline
     *
     * @param string $tagline
     * @return Domain
     */
    public function setTagline($tagline)
    {
        $this->tagline = $tagline;
    
        return $this;
    }

    /**
     * Get tagline
     *
     * @return string 
     */
    public function getTagline()
    {
        return $this->tagline;
    }

    /**
     * Set site
     *
     * @param \GM\SiteBundle\Entity\Site $site
     * @return Domain
     */
    public function setSite(\GM\SiteBundle\Entity\Site $site = null)
    {
        $this->site = $site;
    
        return $this;
    }

    /**
     * Get site
     *
     * @return \GM\SiteBundle\Entity\Site 
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Get country
     *
     * @return \GM\GeoBundle\Entity\Country 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set fallback
     *
     * @param \GM\SiteBundle\Entity\Domain $fallback
     * @return Domain
     */
    public function setFallback(\GM\SiteBundle\Entity\Domain $fallback = null)
    {
        $this->fallback = $fallback;
    
        return $this;
    }

    /**
     * Get fallback
     *
     * @return \GM\SiteBundle\Entity\Domain 
     */
    public function getFallback()
    {
        return $this->fallback;
    }

    /**
     * Set lang
     *
     * @param \GM\GeoBundle\Entity\Lang $lang
     * @return Domain
     */
    public function setLang(\GM\GeoBundle\Entity\Lang $lang = null)
    {
        $this->lang = $lang;
    
        return $this;
    }

    /**
     * Get lang
     *
     * @return \GM\GeoBundle\Entity\Lang 
     */
    public function getLang()
    {
        return $this->lang;
    }
}