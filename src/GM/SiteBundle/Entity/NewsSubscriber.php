<?php

namespace GM\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NewsSubscriber
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="GM\SiteBundle\Entity\NewsSubscriberRepository")
 */
class NewsSubscriber
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="newsTitle", type="smallint")
     */
    private $newsTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="newsLastname", type="string", length=75)
     */
    private $newsLastname;

    /**
     * @var string
     *
     * @ORM\Column(name="newsFirstname", type="string", length=75)
     */
    private $newsFirstname;

    /**
     * @var string
     *
     * @ORM\Column(name="newsEmail", type="string", length=100)
     */
    private $newsEmail;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set newsTitle
     *
     * @param string $newsTitle
     * @return NewsSubscriber
     */
    public function setNewsTitle($newsTitle)
    {
        $this->newsTitle = $newsTitle;
    
        return $this;
    }

    /**
     * Get newsTitle
     *
     * @return string 
     */
    public function getNewsTitle()
    {
        return $this->newsTitle;
    }

    /**
     * Set newsLastname
     *
     * @param string $newsLastname
     * @return NewsSubscriber
     */
    public function setNewsLastname($newsLastname)
    {
        $this->newsLastname = $newsLastname;
    
        return $this;
    }

    /**
     * Get newsLastname
     *
     * @return string 
     */
    public function getNewsLastname()
    {
        return $this->newsLastname;
    }

    /**
     * Set newsFirstname
     *
     * @param string $newsFirstname
     * @return NewsSubscriber
     */
    public function setNewsFirstname($newsFirstname)
    {
        $this->newsFirstname = $newsFirstname;
    
        return $this;
    }

    /**
     * Get newsFirstname
     *
     * @return string 
     */
    public function getNewsFirstname()
    {
        return $this->newsFirstname;
    }

    /**
     * Set newsEmail
     *
     * @param string $newsEmail
     * @return NewsSubscriber
     */
    public function setNewsEmail($newsEmail)
    {
        $this->newsEmail = $newsEmail;
    
        return $this;
    }

    /**
     * Get newsEmail
     *
     * @return string 
     */
    public function getNewsEmail()
    {
        return $this->newsEmail;
    }
}
