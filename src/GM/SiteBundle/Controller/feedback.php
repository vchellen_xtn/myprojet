<?php

use application\front\controllers as CT;
use \application\front\model as MODEL;
use \application\front\workingclass as CLASSES;
use \mvc\Controller\Action as NI;
use \application\helper as HELPER;
class feedback extends CT\skelettonController{
	
	/*
	* display feedback form
	* @Param Integer OrderId
	* @Param string code BN of order
	*/
	public function feedbackOrder($idOrder,$code){
		
		$this->addStyleSheetFile('shop-rating.css');
		$this->addJavascriptFile('shop-rating.js');
		
		$ord=new MODEl\OrderModel($idOrder);
		if($ord->isLoaded()){
			if($ord->checkCNCode($code) && $ord->ord_status==4){
				MODEL\UserModel::setCurrentUserAs($ord->usr_id);
				$this->data['frm']=new HELPER\feedbackform('commentsFeedBack');	
				$this->data['maxRating']=\config::get('shop.rating_max');
				$lst=$ord->getOrderedProduct();
				$this->data['frm']->initiateFromProductList($lst);				
				if($this->data['frm']->check()){
					$data=$this->data['frm']->getData();
					$valid=!\config::get('shop.comment_check');
					$nb=sizeof($lst);
					$util=MODEL\UserModel::getCurrentUser();
					for($i=0;$i<$nb;$i++){
						if(isset($data['message_'.$lst[$i]->opt_id])){
							$com=new MODEL\ShopCommentModel();
							$com->usr_id=$ord->usr_id;
							$com->com_name=$util->usr_firstname.' '.$util->usr_lastname;
							$com->com_email=$util,usr_email;
							$com->com_status=$valid;
							if(\config::get('shop.plugin_option'))
								$com->prd_id=$lst[$i]->getOption()->prd_id;
							else
								$com->prd_id=$lst[$i]->opt_id;
							
							$com->com_text=$data['message_'.$lst[$i]->opt_id];
							$com->com_rate=$data['note_'.$lst[$i]->opt_id];
							$com->createInstance();
						}
					}
					$this->display('thanks.tpl');	
				}else
					$this->display('form.tpl');
				exit;
			}
		}		
		$this->display('error.tpl');
	}
	
	
	private function _getPurchaseForm($purchasedElem){
		$frm=new HELPER\form('commentsFeedBack');		
		$field=$frm->getField('product');
		if($this->data['isOption'])
			$field->defaultValue=$purchasedElem->getOption()->prd_id;
		else
			$field->defaultValue=$purchasedElem->opt_id;
		return $frm;
	}
	
	
}