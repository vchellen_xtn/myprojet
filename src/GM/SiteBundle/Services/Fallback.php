<?php
namespace GM\SiteBundle\Services;

use GM\SiteBundle\Entity\Card;

class Fallback
{
	protected $em;
	protected $domain;

	public function __construct($em, $domain)
	{
		$this->em = $em;
		$this->domain = $domain
	}

	public function set($entity)
	{
		foreach($this->domain->findAll() as $domain)
		{
			if(!$entity->getCardsByDomain($domain))
			{
				if($card = $this->getFallBack($entity, $domain))
				{
					$clone = clone $card;
					$entity->addCard($clone);
				}
			}
		}

		$this->em->persist($entity);
		$this->em->flush();

		return $this->em
					->getRepository('GMSiteBundle:Alias')
					->find($id);
	}

	public function getFallbackCard($entity, $domain, $origin = false)
	{
		// If none fallback defined
		if(!$domain->getFallBack())
			return false;

		// If the fallback has a card
		if($card = $entity->getCardsByDomain($domain->getFallBack()))
			return $card;

		// We log the origin domain for later
		if(!$origin)
			$origin = $domain;

		// Prevent infinite loop
		if($origin == $domain->getFallBack())
			return false;

		// Recursive until we have a result
		return $this->getFallBack($entity, $domain->getFallBack());
	}
}