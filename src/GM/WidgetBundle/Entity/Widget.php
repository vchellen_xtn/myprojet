<?php

namespace GM\WidgetBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Widget
 *
 * @ORM\Table(name="widget")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Widget
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="GM\SiteBundle\Entity\Domain")
     */
    private $domain;

    /**
     * @ORM\ManyToOne(targetEntity="Shell", inversedBy="widgets")
     */
    private $shell;

    /**
     * @ORM\ManyToOne(targetEntity="Container", inversedBy="widgets", cascade={"persist"})
     */
    private $container;

    /**
     * @var integer
     *
     * @ORM\Column(name="entity_id", type="integer", nullable=true)
     */
    private $entityId;

    /**
     * @ORM\ManyToMany(targetEntity="Param", cascade={"persist","remove"})
     * @ORM\JoinTable(name="properties",
     *      joinColumns={@ORM\JoinColumn(name="widget_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="property_id", referencedColumnName="id", unique=true)}
     *      )
     **/
    private $properties;

    /**
     * @ORM\ManyToMany(targetEntity="Widget", cascade={"persist","remove"})
     * @ORM\JoinTable(name="children",
     *      joinColumns={@ORM\JoinColumn(name="widget_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="item_id", referencedColumnName="id")}
     *      )
     */
    private $items;

    /**
     * @var datetime
     *
     * @ORM\Column(name="last_modified", type="datetime")
     */
    private $lastModified;

    /**
     * @var integer
     *
     * @ORM\Column(name="place", type="integer", nullable=true)
     */
    private $place;

    /**
     * @var integer
     *
     * @ORM\Column(name="rank", type="integer")
     */
    private $rank;

    public function __construct()
    {
        $this->properties = new ArrayCollection();
        $this->config = new ArrayCollection();
        $this->items = new ArrayCollection();
        $this->rank = 1;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpdate(){
        $this->lastModified = new \DateTime();
        if($this->getContainer())
            $this->getContainer()->setLastModified($this->lastModified);
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set entityId
     *
     * @param integer $entityId
     * @return Widget
     */
    public function setEntityId($entityId)
    {
        $this->entityId = $entityId;
    
        return $this;
    }

    /**
     * Get entityId
     *
     * @return integer 
     */
    public function getEntityId()
    {
        return $this->entityId;
    }

    /**
     * Set lastModified
     *
     * @param \DateTime $lastModified
     * @return Widget
     */
    public function setLastModified($lastModified)
    {
        $this->lastModified = $lastModified;
    
        return $this;
    }

    /**
     * Get lastModified
     *
     * @return \DateTime 
     */
    public function getLastModified()
    {
        return $this->lastModified;
    }

    /**
     * Set rank
     *
     * @param integer $rank
     * @return Widget
     */
    public function setRank($rank)
    {
        $this->rank = $rank;
    
        return $this;
    }

    /**
     * Get rank
     *
     * @return integer 
     */
    public function getRank()
    {
        return $this->rank;
    }

    /**
     * Set domain
     *
     * @param \GM\SiteBundle\Entity\Domain $domain
     * @return Widget
     */
    public function setDomain(\GM\SiteBundle\Entity\Domain $domain = null)
    {
        $this->domain = $domain;
    
        return $this;
    }

    /**
     * Get domain
     *
     * @return \GM\SiteBundle\Entity\Domain 
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set container
     *
     * @param \GM\WidgetBundle\Entity\Container $container
     * @return Widget
     */
    public function setContainer(\GM\WidgetBundle\Entity\Container $container = null)
    {
        $this->container = $container;
    
        return $this;
    }

    /**
     * Get container
     *
     * @return \GM\WidgetBundle\Entity\Container 
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * Add properties
     *
     * @param \GM\WidgetBundle\Entity\Param $properties
     * @return Widget
     */
    public function addPropertie(\GM\WidgetBundle\Entity\Param $properties)
    {
        $this->properties[] = $properties;
    
        return $this;
    }

    /**
     * Remove properties
     *
     * @param \GM\WidgetBundle\Entity\Param $properties
     */
    public function removePropertie(\GM\WidgetBundle\Entity\Param $properties)
    {
        $this->properties->removeElement($properties);
    }

    /**
     * Get properties
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProperties()
    {
        return $this->properties;
    }

    /**
     * Add items
     *
     * @param \GM\WidgetBundle\Entity\Widget $items
     * @return Widget
     */
    public function addItem(\GM\WidgetBundle\Entity\Widget $items)
    {
        $this->items[] = $items;
    
        return $this;
    }

    /**
     * Remove items
     *
     * @param \GM\WidgetBundle\Entity\Widget $items
     */
    public function removeItem(\GM\WidgetBundle\Entity\Widget $items)
    {
        $this->items->removeElement($items);
    }

    /**
     * Get items
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Set shell
     *
     * @param \GM\WidgetBundle\Entity\Shell $shell
     * @return Widget
     */
    public function setShell(\GM\WidgetBundle\Entity\Shell $shell = null)
    {
        $this->shell = $shell;
    
        return $this;
    }

    /**
     * Get shell
     *
     * @return \GM\WidgetBundle\Entity\Shell 
     */
    public function getShell()
    {
        return $this->shell;
    }

    /**
     * Set place
     *
     * @param integer $place
     * @return Widget
     */
    public function setPlace($place)
    {
        $this->place = $place;
    
        return $this;
    }

    /**
     * Get place
     *
     * @return integer 
     */
    public function getPlace()
    {
        return $this->place;
    }

    public function getItemByPlace($place)
    {
        return $this->getItems()
                    ->filter(
                        function($entry) use ($place)
                        {
                            if($entry->getPlace() == $place)
                                return true;

                            return false;
                        }
                    )->first();
    }
}