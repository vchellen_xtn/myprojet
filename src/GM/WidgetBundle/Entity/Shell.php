<?php

namespace GM\WidgetBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Shell
 *
 * @ORM\Table(name="shell")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Shell
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="GM\SiteBundle\Entity\Site")
     */
    private $site;

    /**
     * @ORM\ManyToOne(targetEntity="Category")
     */
    private $category;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @ORM\ManyToMany(targetEntity="Param", cascade={"persist","remove"})
     * @ORM\JoinTable(name="config",
     *      joinColumns={@ORM\JoinColumn(name="widget_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="config_id", referencedColumnName="id", unique=true)}
     *      )
     **/
    private $config;

    /**
     * @ORM\OneToMany(targetEntity="Widget", mappedBy="shell", cascade={"remove","persist"})
     */
    private $widgets;

    /**
     * @var datetime
     *
     * @ORM\Column(name="last_modified", type="datetime")
     */
    private $lastModified;

    /**
     * @var string
     *
     * @ORM\Column(name="architect", type="boolean", nullable=true)
     */
    private $architect;

    /**
     * @var integer
     *
     * @ORM\Column(name="rank", type="integer")
     */
    private $rank;

    public function __construct()
    {
        $this->widgets = new ArrayCollection();
        $this->config = new ArrayCollection();
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpdate(){
        $this->lastModified = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Shell
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Shell
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set lastModified
     *
     * @param \DateTime $lastModified
     * @return Shell
     */
    public function setLastModified($lastModified)
    {
        $this->lastModified = $lastModified;
    
        return $this;
    }

    /**
     * Get lastModified
     *
     * @return \DateTime 
     */
    public function getLastModified()
    {
        return $this->lastModified;
    }

    /**
     * Set rank
     *
     * @param integer $rank
     * @return Shell
     */
    public function setRank($rank)
    {
        $this->rank = $rank;
    
        return $this;
    }

    /**
     * Get rank
     *
     * @return integer 
     */
    public function getRank()
    {
        return $this->rank;
    }

    /**
     * Set category
     *
     * @param \GM\WidgetBundle\Entity\Category $category
     * @return Shell
     */
    public function setCategory(\GM\WidgetBundle\Entity\Category $category = null)
    {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Get category
     *
     * @return \GM\WidgetBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Add config
     *
     * @param \GM\WidgetBundle\Entity\Param $config
     * @return Shell
     */
    public function addConfig(\GM\WidgetBundle\Entity\Param $config)
    {
        $this->config[] = $config;
    
        return $this;
    }

    /**
     * Remove config
     *
     * @param \GM\WidgetBundle\Entity\Param $config
     */
    public function removeConfig(\GM\WidgetBundle\Entity\Param $config)
    {
        $this->config->removeElement($config);
    }

    /**
     * Get config
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Add widgets
     *
     * @param \GM\WidgetBundle\Entity\Widget $widgets
     * @return Shell
     */
    public function addWidget(\GM\WidgetBundle\Entity\Widget $widgets)
    {
        $this->widgets[] = $widgets;
    
        return $this;
    }

    /**
     * Remove widgets
     *
     * @param \GM\WidgetBundle\Entity\Widget $widgets
     */
    public function removeWidget(\GM\WidgetBundle\Entity\Widget $widgets)
    {
        $this->widgets->removeElement($widgets);
    }

    /**
     * Get widgets
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getWidgets()
    {
        return $this->widgets;
    }

    /**
     * Set site
     *
     * @param \GM\SiteBundle\Entity\Site $site
     * @return Shell
     */
    public function setSite(\GM\SiteBundle\Entity\Site $site = null)
    {
        $this->site = $site;
    
        return $this;
    }

    /**
     * Get site
     *
     * @return \GM\SiteBundle\Entity\Site 
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set architect
     *
     * @param boolean $architect
     * @return Shell
     */
    public function setArchitect($architect)
    {
        $this->architect = $architect;
    
        return $this;
    }

    /**
     * Get architect
     *
     * @return boolean 
     */
    public function getArchitect()
    {
        return $this->architect;
    }
}