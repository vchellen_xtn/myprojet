
//
// Interface user v1.1.1
// Auteur : Sylvain Durozard
//

Xeon.module.user =
{
    allowDrop: function(tree, node, target) {
        return (node.parentNode.getDepth() == target.getDepth());
    },

    makeAdminRoot: function(ct, item, data)
    {
        ct.add(new Ext.ux.grid.GridPanel({
            title: lang.USER_ADMIN_LIST,
            iconCls: 'i-group',
            itype: 'user.adm',
            rowId: 'id',
            quickSearch: true,
            search: false,
            paging: true,
            columns: [{
                header: "Id",
                dataIndex: 'id',
                width: 50,
            },{
                header: lang.gm.lastname,
                dataIndex: 'lastname',
              itemLink: ['user.adm', 'id']
            },{
                header: lang.gm.firstname,
                dataIndex: 'firstname',
              itemLink: ['user.adm', 'id']
            },{
                header: lang.gm.email,
                dataIndex: 'email',
                width: 200,
            },{
                header: 'Date de création',
                dataIndex: 'makedate.date',
                renderer: Xeon.getDate
            },{
              header: lang.gm.status,
              dataIndex: 'enabled',
              width: 60,
              renderer: Xeon.renderStatus,
              editor: new Ext.ux.form.ToggleBox()
          }]
        }));
    },
    
    makeGroupForm: function(ct, item, data)
    {
        var items = [{
            fieldLabel: lang.xeon.mod_user.group_add_libel_title,
            allowBlank: false,
            name: 'title',
            value: data.db.title
        }];
        var field_cate = {
            xtype: 'listbox',
            fieldLabel: lang.xeon.mod_user.group_parent,
            itype: 'user.grp',
            name: 'parent',
            value: data.db.parent,
            displayValue: data.db.parent_title
          };

        if (item.add)
        {
            items.push(field_cate);
            ct.add(new Ext.ux.FormPanel({
                items: items
            }));
        }
        else
        {
            items.push({
                xtype: 'togglebox',
                fieldLabel: lang.xeon.mod_user.group_status,
                allowBlank: false,
                name: 'status',
                value: data.db.status
            },
            {
                xtype: 'togglebox',
                fieldLabel: 'Super Admin',
                allowBlank: false,
                name: 'superAdmin',
                value: data.db.super_admin
            });
            
            ct.add(new Ext.ux.FormPanel({
                title: lang.USER_GROUP,
                iconCls: ct.iconCls,
                items: items
            }));

        ct.add(new Ext.ux.grid.GridPanel({
            title: lang.USER_ADMIN_LIST,
            iconCls: 'i-group',
            itype: 'user.adm',
            rowId: 'id',
            quickSearch: true,
            search: false,
            paging: true,
            columns: [{
                header: "Id",
                dataIndex: 'id',
                width: 50,
            },{
                header: lang.gm.lastname,
                dataIndex: 'lastname',
              itemLink: ['user.adm', 'id']
            },{
                header: lang.gm.firstname,
                dataIndex: 'firstname',
              itemLink: ['user.adm', 'id']
            },{
                header: lang.gm.email,
                dataIndex: 'email',
                width: 200,
            },{
                header: 'Date de création',
                dataIndex: 'makedate.date',
                renderer: Xeon.getDate
            },{
              header: lang.gm.status,
              dataIndex: 'enabled',
              width: 60,
              renderer: Xeon.renderStatus,
              editor: new Ext.ux.form.ToggleBox()
          }]
        }));
        }
    },

    makeAdminForm: function(ct, item, data)
    {
        var items = [];
        var value_parent = '';
        if (item.add)
        {
            items.push({
                xtype: 'listbox',
                fieldLabel: lang.xeon.mod_user.group_parent,
                itype: 'user.grp',
                name: 'grp_id'
            });
        }
        else
        {
            value_parent = eval('lang.'+ data.db_title.name);
        }

        items.push({
            xtype: 'titlebox',
            fieldLabel: lang.gm.title,
            allowBlank: false,
            name: 'title',
            value: value_parent
        },{
            fieldLabel: lang.gm.firstname,
            allowBlank: false,
            name: 'firstname',
            value: data.db.firstname
        },{
            fieldLabel: lang.gm.lastname,
            allowBlank: false,
            name: 'lastname',
            value: data.db.lastname
        },{
            fieldLabel: lang.xeon.mod_user.domain,
            allowBlank: false,
            name: 'domain',
            value: data.db.domain
        },{
            fieldLabel: lang.xeon.mod_user.login,
            allowBlank: false,
            name: 'login',
            value: data.db.username
        },{
            fieldLabel: lang.xeon.mod_user.password,
            inputType: 'password',
            allowBlank: false,
            minLength: 3,
            maxLength: 32,
            name: 'password',
            value: data.db.password
        });

        if (item.add)
        {
            ct.add(new Ext.ux.FormPanel({
                items: items
            }));
        }
        else
        {
            items.push({
                fieldLabel: lang.gm.email,
                vtype: 'email',
                name: 'email',
                value: data.db.email
            },{
                xtype: 'togglebox',
                fieldLabel: lang.gm.status,
                allowBlank: false,
                name: 'status',
                value: data.db.enabled
            });
            
            ct.add(new Ext.ux.FormPanel({
                title: lang.USER_ADMIN,
                iconCls: ct.iconCls,
                items: items
            }));

            var last_login = '';
            if(data.db.last_login != undefined)
            {
                last_login = data.db.last_login.date;
            }
            ct.add(new Ext.ux.InfoPanel({
                title: lang.XEON_INFO,
                iconCls: 'i-information',
                items: [{
                    infoLabel: 'Date de création',
                    value: data.db.makedate.date
                },{
                    infoLabel: 'Date de dernière connection',
                    value: last_login
                }]
            }));
            ct.add(new Ext.ux.ListPanel({
                title: lang.xeon.mod_request_title.group_user,
                iconCls: 'i-page-white',
                name: 'group',
                itype: 'user.grp',
                data: data.db.multi_grp
            }));
            
        }

    },

    renderYesorNo: function(v, m, r)
    {
        m.css = (v == 1) ? 'x-button i-accept' : 'x-button i-delete';
        m.css = (v == -1) ? 'x-button i-loading' : m.css;
    },
        
    makeMemberRoot: function(ct, item, data)
    {
        ct.add(new Ext.ux.grid.GridPanel({
            title: lang.USER_MEMBER_LIST,
            iconCls: 'i-group',
            itype: 'user.mbr',
            rowId: 'usr_id',
            quickSearch: true,
            paging: true,
            search: engine.getSearchMembersItems(data),
            columns: [{
                header: lang.XEON_LASTNAME,
                dataIndex: 'usr_name',
                itemLink: ['user.mbr', 'usr_id']
            },{
                header: lang.XEON_EMAIL,
                dataIndex: 'usr_email',
                width: 130,
                editor: new Ext.form.TextField({
                    vtype: 'email'
                })
            },{
                header: lang.USER_NB_ORDER,
                dataIndex: 'order_total',
                width: 110
            },{
                header: lang.USER_CA_SHORT,
                dataIndex: 'ca',
                width: 80,
                renderer: Xeon.module.shop.renderPrice1
            },{
                header: lang.USER_AVG_CART,
                width: 90,
                dataIndex: 'avg',
                renderer: Xeon.module.shop.renderPrice1
            },{
                header: lang.USER_NEWSLETTER,
                width: 120,
                dataIndex: 'usr_newsletter',
                renderer: this.renderYesorNo,
                editor: new Ext.ux.form.ToggleBox()
            },{
                header: lang.XEON_MAKE_DATE,
                width: 150,
                dataIndex: 'usr_make_date'
            }]
        }));
    },
    
    makePouetForm: function(ct, item, data)
    {
        Xeon.callback(data.module, 'makeMemberRoot', ct, item, data);
    },

    makeModuleForm: function (ct,item,data)
    {
        //console.log(data);
       /* ct.add(new Ext.ux.FormPanel({
            title: lang.USER_TITLEENGINE,
            iconCls: 'i-brick',
            itemId:'userengine',
            items: [{
                xtype: 'togglebox',
                fieldLabel: lang.USER_NAME,
                name: 'name_field',
                value: data.cfg.userengine.name_field
            },{
                xtype: 'togglebox',
                fieldLabel: lang.USER_LASTNAME,
                name: 'lastname_field',
                value: data.cfg.userengine.lastname_field
            },{
                xtype: 'togglebox',
                fieldLabel: lang.USER_MAIL,
                name: 'mail_field',
                value: data.cfg.userengine.mail_field
            },{
                xtype: 'togglebox',
                fieldLabel: lang.USER_LOGIN,
                name: 'login_field',
                value: data.cfg.userengine.login_field
            },{
                xtype: 'togglebox',
                fieldLabel: lang.USER_ID,
                name: 'id_field',
                value: data.cfg.userengine.id_field
            },{
                xtype: 'togglebox',
                fieldLabel: lang.USER_ZIPCODE,
                name: 'zipcode_field',
                value: data.cfg.userengine.zipcode_field
            },{
                xtype: 'togglebox',
                fieldLabel: lang.USER_COUNTRY,
                name: 'country_field',
                value: data.cfg.userengine.country_field
            },{
                xtype: 'togglebox',
                fieldLabel: lang.USER_DATE,
                name: 'date_field',
                value: data.cfg.userengine.date_field
            },{
                xtype: 'togglebox',
                fieldLabel: lang.USER_CA,
                name: 'ca_field',
                value: data.cfg.userengine.ca_field
            }]
            }));*/
    },

    renderUserType: function(v)
    {
        if (v == 0) return 'N/A';
        if (v == 1) return 'T1';
        if (v == 2) return 'T2';
        if (v == 3) return 'T3';
        if (v == 4) return 'T4';
    },
        
    makeMemberForm: function(ct, item, data)
    {
        var items = [{
            xtype: 'titlebox',
            fieldLabel: lang.XEON_TITLE,
            allowBlank: false,
            name: 'usr_title',
            value: data.db.usr_title
        },{
            fieldLabel: lang.XEON_FIRSTNAME,
            allowBlank: false,
            name: 'usr_firstname',
            value: data.db.usr_firstname
        },{
            fieldLabel: lang.XEON_LASTNAME,
            allowBlank: false,
            name: 'usr_lastname',
            value: data.db.usr_lastname
        },{
            fieldLabel: lang.XEON_LOGIN,
            allowBlank: false,
            name: 'usr_login',
            value: data.db.usr_login
        },{
            fieldLabel: lang.XEON_PASSWORD,
            inputType: 'password',
            allowBlank: false,
            minLength: 3,
            maxLength: 32,
            name: 'usr_password',
            value: data.db.usr_password
        }];
        
        if (item.add)
        {
            ct.add(new Ext.ux.FormPanel({
                items: items
            }));
        }
        else
        {
            items.push({
                fieldLabel: lang.XEON_EMAIL,
                vtype: 'email',
                name: 'usr_email',
                value: data.db.usr_email
            },{
                fieldLabel: lang.XEON_ADDRESS_1,
                maxLength: 44,
                name: 'usr_address_1',
                value: data.db.usr_address_1
            },{
                fieldLabel: lang.XEON_ADDRESS_2,
                maxLength: 44,
                name: 'usr_address_2',
                value: data.db.usr_address_2
            },{
                fieldLabel: lang.XEON_ADDRESS_3,
                maxLength: 44,
                name: 'usr_address_3',
                value: data.db.usr_address_3
            },{
                fieldLabel: lang.XEON_ZIP_CODE,
                name: 'usr_zip_code',
                value: data.db.usr_zip_code
            },{
                fieldLabel: lang.XEON_CITY,
                name: 'usr_city',
                value: data.db.usr_city
            },{
                xtype: 'countrybox',
                fieldLabel: lang.XEON_COUNTRY,
                name: 'usr_country',
                value: data.db.usr_country,
                displayValue: data.db.usr_country_name
            },{
                xtype: 'date',
                fieldLabel: lang.USER_BIRTH_DATE,
                name: 'usr_birth_date',
                value: data.db.usr_birth_date
            },{
                fieldLabel: lang.XEON_PHONE,
                name: 'usr_phone',
                value: data.db.usr_phone
            },{
                fieldLabel: lang.XEON_MOBILE,
                name: 'usr_mobile',
                value: data.db.usr_mobile
            },{
                fieldLabel: lang.XEON_FAX,
                name: 'usr_fax',
                value: data.db.usr_fax
            },{
                fieldLabel: lang.XEON_COMPANY,
                name: 'usr_company',
                value: data.db.usr_company
            },{
                xtype: 'combobox',
                fieldLabel: lang.XEON_STATUS,
                data: [[1, lang.XEON_YES, 'i-tick'], [2, lang.USER_WAITING, 'i-time'], [0, lang.XEON_NO, 'i-cross']],
                allowBlank: false,
                name: 'usr_status',
                value: data.db.usr_status
            });
            
            ct.add(new Ext.ux.FormPanel({
                title: lang.USER_MEMBER,
                iconCls: ct.iconCls,
                items: items
            }));
            
           /* ct.add(new Ext.ux.GridPanel({
              title: 'Adresse de livraison',
              iconCls: 'i-script',
              itype: 'shop.ord',//+data.db.usr_id,
              rowId: 'ord_id',
              search : false,
              columns :
             }));*/
            
            ct.add(new Ext.ux.InfoPanel({
                title: lang.XEON_INFO,
                collapsed : true,
                iconCls: 'i-information',
                items: [{
                    infoLabel: lang.XEON_MAKE_DATE,
                    value: data.db.usr_make_date
                },{
                    infoLabel: lang.USER_LAST_LOGIN,
                    value: data.db.usr_last_login
                }]
            }));

                        
            ct.add(new Ext.ux.ListPanel({
                collapsed:true,
                title: lang.USER_ZONES,
                iconCls: 'i-folder',
                name: 'usr_zone_list',
                itype: 'user.zne',
                data: data.zne
            }));

            ct.add(new Ext.ux.grid.GridPanel({
                title: lang.SHOP_ORDER_LIST,
                //needToolbar: 0,
                iconCls: 'i-script',
                itype: 'shop.ord',//+data.db.usr_id,
                rowId: 'ord_id',
                paging: true,
                quickSearch: new Ext.ux.form.ComboBox({
                    emptyText: lang.SHOP_ORDER_STEP,
                    data: [
                    [1, lang.SHOP_ESTIMATE, 'i-page-white-edit'],
                    [2, lang.SHOP_PAYMENT_TODO, 'i-money'],
                    [3, lang.SHOP_PROCESS_TODO, 'i-package'],
                    [4, lang.SHOP_COMPLETED, 'i-tick'],
                    [5, lang.SHOP_CANCELLED, 'i-cross']
                    ],
                    value: 3,
                }),
                search: false,
                columns: [{
                    header: 'Imp BDC',
                    dataIndex: 'ord_printed_status',
                    align: 'center',
                    width:50,
                    renderer : function (v,m,r)
                    {
                        m.css = 'x-button i-printer';
                        r.isButton = true;
                    },
                    handler: function(p,r,i)
                    {
                        Ext.Ajax.request({
                            params: {
                                cmd: 'xeon.exportGrid',
                                item: 'shop.ordertopdf.'+r.id,
                                parent: ''
                            },
                            success: function() {
                                Xeon.getFile();
                            }
                        })
                }
            },{
                header: lang.SHOP_NUMBER,
                dataIndex: 'ord_id',
                dataType: 'int',
                width: 60,
                itemLink: ['shop.ord', 'ord_id']
            },{
                header: lang.XEON_MAKE_DATE,
                dataIndex: 'ord_make_date',
                renderer: Xeon.renderDate
            },{
                header: lang.XEON_MEMBER,
                dataIndex: 'ord_bill_name',
                width: 150,
                itemLink: ['user.mbr', 'usr_id']
            },{
                header: lang.XEON_EMAIL,
                dataIndex: 'ord_bill_email',
                width: 150,
                renderer: Xeon.renderEmail
            },{
                header: lang.SHOP_TOTAL,
                dataIndex: 'ord_total'
            },{
                header: lang.SHOP_WEIGHT,
                dataIndex: 'ord_weight',
                renderer: Xeon.module.shop.renderWeight
            },{
                header: lang.SHOP_PAYMENT_METHOD,
                dataIndex: 'pmt_title',
                itemLink: ['shop.pmt', 'pmt_id']
            },{
                header: lang.SHOP_PAYMENT_DATE,
                dataIndex: 'ord_payment_date',
                align: 'center',
                renderer: function(v, m, r)
                {
                    if (v = Xeon.renderDate(v)) return v;
                    if (r.json.ord_status != 2) return '';

                    m.css = 'x-button i-money';
                    r.isButton = true;
                },
                handler: function(p, r, i)
                {
                    if (!r.isButton) return;
                    p.store.reload();
                    //p.store.remove(r);

                    Ext.Ajax.request({
                        params: {
                            cmd: 'shop.setOrderPayment',
                            id: r.id
                            }
                        });
            }
            },{
                header: lang.SHOP_POSTAGE_METHOD,
                dataIndex: 'pst_title',
                itemLink: ['shop.pst', 'pst_id']
            },{
                header: lang.SHOP_PROCESS_DATE,
                dataIndex: 'ord_process_date',
                align: 'center',
                renderer: function(v, m, r)
                {
                    if (v = Xeon.renderDate(v)) return v;
                    if (r.json.ord_status != 3) return '';

                    m.css = 'x-button i-package';
                    r.isButton = true;
                },
                handler: function(p, r, i)
                {
                    if (!r.isButton) return;
                    p.store.remove(r);

                    Ext.Ajax.request({
                        params: {
                            cmd: 'shop.setOrderProcess',
                            id: r.id
                        }
                    });
                }
            },{

                header: lang.SHOP_MEMO,
                dataIndex: 'ord_memo',
                width: 150,
                renderer: Xeon.renderText,
                editor: new Ext.form.TextArea()
            }]
            }));

    ct.add(new Ext.ux.grid.GridPanel({
        title: lang.SHOP_DISCOUNT_LIST,
        iconCls: 'i-note',
        itype: 'shop.dsc',
        rowId: 'dsc_id',
        search:false,
        columns: [{
            header: lang.SHOP_CODE,
            dataIndex: 'dsc_code',
            width: 150,
            itemLink: ['shop.dsc', 'dsc_id']
        },{
            header: lang.SHOP_TYPE,
            dataIndex: 'dsc_type',
            renderer: Xeon.module.shop.renderDiscountType,
            editor: new Ext.ux.form.ComboBox({
                data: [[1, lang.SHOP_AMOUNT], [2, lang.SHOP_PERCENTAGE]],
                allowBlank: false
            })
        },{
            header: lang.SHOP_DISCOUNT_ASSIGNED_TO,
            dataIndex: 'name',
            dataType: 'string'
        },{
            header: lang.SHOP_VALUE,
            dataIndex: 'dsc_value',
            dataType: 'float',
            renderer: Xeon.module.shop.renderDiscountValue,
            editor: new Ext.ux.form.FloatField({
                allowBlank: false
            })
        },{
             header: lang.SHOP_UNUSED_VALUE,
             dataIndex: 'dsc_unused_value',
             dataType: 'float',
             renderer: Xeon.module.shop.renderDiscountValue
        },{
            header: lang.SHOP_MIN_CART,
            dataIndex: 'dsc_min_cart',
            dataType: 'float',
            renderer: Xeon.module.shop.renderPrice2,
            editor: new Ext.ux.form.FloatField({
                allowBlank: false
            })
        },{
            header: lang.SHOP_START_DATE,
            dataIndex: 'dsc_start_date'/*,
                editor: new Ext.form.DateField({format: 'd-m-Y'})*/
        },{
            header: lang.SHOP_END_DATE,
            dataIndex: 'dsc_end_date'/*,
                editor: new Ext.form.DateField({format: 'd-m-Y'})*/
        },{
            header: lang.XEON_STATUS,
            dataIndex: 'dsc_status',
            width: 60,
            renderer: Xeon.renderStatus,
            editor: new Ext.ux.form.ToggleBox()
        }]
    }));
    
    for (var m in Xeon.module)
    {
        if (m != 'user') {
            Xeon.callback(m, 'makeMemberForm', ct, item, data);
        }
    };
}
},

        
makeAuthorForm: function(ct, item, data)
{
    var items = [{
        fieldLabel: lang.XEON_NAME,
        allowBlank: false,
        name: 'ath_name',
        value: data.db.ath_name
    },{
        fieldLabel: lang.XEON_EMAIL,
        vtype: 'email',
        name: 'ath_email',
        value: data.db.ath_email
    }];
        
    if (!item.add)
    {
        items.push({
            xtype: 'mediabox',
            fieldLabel: lang.XEON_IMAGE,
            name: 'ath_media',
            value: data.db.ath_media,
            displayValue: data.db.ath_media_title
        });
    }

    if (item.add)
    {
        ct.add(new Ext.ux.FormPanel({
            items: items
        }));
    }
    else
    {
        ct.add(new Ext.ux.FormPanel({
            title: lang.USER_AUTHOR,
            iconCls: ct.iconCls,
            items: items
        }));
    }
},
    
makeZoneForm: function(ct, item, data)
{
    var items = [{
        fieldLabel: lang.XEON_TITLE,
        name: 'zne_title',
        allowBlank: false,
        value: data.db.zne_title
    }];
        
    if (item.add)
    {
        ct.add(new Ext.ux.FormPanel({
            items: items
        }));
    }
    else
    {
        items.push({
            xtype: 'togglebox',
            fieldLabel: lang.XEON_STATUS,
            allowBlank: false,
            name: 'zne_status',
            value: data.db.zne_status
        });
            
        ct.add(new Ext.ux.FormPanel({
            title: lang.XEON_ZONE,
            iconCls: ct.iconCls,
            items: items
        }));
            
        ct.add(new Ext.ux.FormPanel({
            title: lang.XEON_SEO,
            iconCls: 'i-magnifier',
            items: [{
                fieldLabel: lang.XEON_TITLE,
                name: 'zne_meta_title',
                value: data.db.zne_meta_title
            },{
                xtype: 'textarea',
                fieldLabel: lang.XEON_DESCRIPTION,
                name: 'zne_meta_description',
                value: data.db.zne_meta_description
            },{
                xtype: 'textarea',
                fieldLabel: lang.XEON_KEYWORDS,
                name: 'zne_meta_keywords',
                value: data.db.zne_meta_keywords
            }]
        }));
    }
}
}