<?php

namespace GM\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Group
 *
 * @ORM\Table(name="group")
 * @ORM\Entity
 */
class Group
{
	/**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @ORM\ManyToOne(targetEntity="Group")
     */
    private $parent;

    /**
     * @ORM\Column(name="status", type="boolean", options={"default": false})
     */
    private $status;

    /**
     * @ORM\Column(name="super_admin", type="boolean", options={"default": false})
     */
    private $superAdmin;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->groups = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Group
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Group
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Add groups
     *
     * @param \GM\UserBundle\Entity\Group $groups
     * @return Group
     */
    public function addGroup(\GM\UserBundle\Entity\Group $groups)
    {
        $this->groups[] = $groups;
    
        return $this;
    }

    /**
     * Remove groups
     *
     * @param \GM\UserBundle\Entity\Group $groups
     */
    public function removeGroup(\GM\UserBundle\Entity\Group $groups)
    {
        $this->groups->removeElement($groups);
    }

    /**
     * Get groups
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * Set parent
     *
     * @param \GM\UserBundle\Entity\Group $parent
     * @return Group
     */
    public function setParent(\GM\UserBundle\Entity\Group $parent = null)
    {
        $this->parent = $parent;
    
        return $this;
    }

    /**
     * Get parent
     *
     * @return \GM\UserBundle\Entity\Group 
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set superAdmin
     *
     * @param boolean $superAdmin
     * @return Group
     */
    public function setSuperAdmin($superAdmin)
    {
        $this->superAdmin = $superAdmin;
    
        return $this;
    }

    /**
     * Get superAdmin
     *
     * @return boolean 
     */
    public function getSuperAdmin()
    {
        return $this->superAdmin;
    }
}