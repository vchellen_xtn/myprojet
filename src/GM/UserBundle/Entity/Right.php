<?php
namespace GM\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="right")
 * @ORM\Entity()
 */
class Right
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="tag", type="string", length=20)
     */
    protected $tag;

    /**
     * @ORM\ManyToOne(targetEntity="RightCategory")
     */
    protected $category;

    /**
     * @ORM\ManyToOne(targetEntity="Group")
     */
    protected $group;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tag
     *
     * @param string $tag
     * @return Right
     */
    public function setTag($tag)
    {
        $this->tag = $tag;
    
        return $this;
    }

    /**
     * Get tag
     *
     * @return string 
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set category
     *
     * @param \GM\UserBundle\Entity\RightCategory $category
     * @return Right
     */
    public function setCategory(\GM\UserBundle\Entity\RightCategory $category = null)
    {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Get category
     *
     * @return \GM\UserBundle\Entity\RightCategory 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set group
     *
     * @param \GM\UserBundle\Entity\Group $group
     * @return Right
     */
    public function setGroup(\GM\UserBundle\Entity\Group $group = null)
    {
        $this->group = $group;
    
        return $this;
    }

    /**
     * Get group
     *
     * @return \GM\UserBundle\Entity\Group 
     */
    public function getGroup()
    {
        return $this->group;
    }
}