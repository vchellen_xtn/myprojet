<?php
namespace GM\UserBundle\Entity;

use Symfony\Component\Security\Core\Role\RoleInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="role")
 * @ORM\Entity(repositoryClass="GM\UserBundle\Entity\RoleRepository")
 */
class Role implements RoleInterface
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="name", type="string", length=30)
     */
    protected $name;

    /**
     * @ORM\Column(name="tag", type="string", length=20, unique=true)
     */
    protected $tag;

    /**
     * @ORM\ManyToOne(targetEntity="GM\SiteBundle\Entity\Site")
     */
    protected $site;

    /**
     * @ORM\ManyToMany(targetEntity="Right")
     */
    protected $rights;


    public function __construct()
    {
        $this->rights = new ArrayCollection();
    }

    /**
     * @see RoleInterface
     */
    public function getRole()
    {
        return $this->tag;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Role
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add rights
     *
     * @param \GM\SiteBundle\Entity\Site $rights
     * @return Role
     */
    public function addRight(\GM\SiteBundle\Entity\Site $rights)
    {
        $this->rights[] = $rights;
    
        return $this;
    }

    /**
     * Remove rights
     *
     * @param \GM\SiteBundle\Entity\Site $rights
     */
    public function removeRight(\GM\SiteBundle\Entity\Site $rights)
    {
        $this->rights->removeElement($rights);
    }

    /**
     * Get rights
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRights()
    {
        return $this->rights;
    }

    /**
     * Set tag
     *
     * @param string $tag
     * @return Role
     */
    public function setTag($tag)
    {
        $this->tag = $tag;
    
        return $this;
    }

    /**
     * Get tag
     *
     * @return string 
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set site
     *
     * @param \GM\SiteBundle\Entity\Site $site
     * @return Role
     */
    public function setSite(\GM\SiteBundle\Entity\Site $site = null)
    {
        $this->site = $site;
    
        return $this;
    }

    /**
     * Get site
     *
     * @return \GM\SiteBundle\Entity\Site 
     */
    public function getSite()
    {
        return $this->site;
    }
}