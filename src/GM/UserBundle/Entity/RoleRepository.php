<?php

namespace GM\UserBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * DomainRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class RoleRepository extends EntityRepository
{
	public function findOneByRight($right)
	{
		$qb = $this->createQueryBuilder('r')
					->join('r.rights', 'rg')
						->addSelect('rg')
					->andWhere('rg = ?1')
						->setParameter(1, $right);

		return $qb->getQuery()
				  ->getOneOrNullResult();
	}
}
