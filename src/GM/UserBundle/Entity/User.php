<?php
namespace GM\UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\AttributeOverrides;
use Doctrine\ORM\Mapping\AttributeOverride;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="GM\SiteBundle\Entity\Site")
     */
    protected $site;

    /**
     * @ORM\ManyToMany(targetEntity="Group")
     */
    protected $parent;

    /**
     * @ORM\ManyToOne(targetEntity="Title")
     */
    protected $title;

    /**
     * @ORM\Column(name="firstname", type="string", length=50)
     */
    protected $firstname;

    /**
     * @ORM\Column(name="lastname", type="string", length=50)
     */
    protected $lastname;

    /**
     * @ORM\Column(name="make_date", type="datetime")
     */
    protected $makedate;

    /**
     * @ORM\ManyToMany(targetEntity="Role")
     */
    protected $gmRoles;

    /**
     * @ORM\Column(name="domaine", type="string", length=50)
     */
    protected $domain;

    /* !!!! IMPORTANT !!!!
    il faut le laisser pour la methode convertAll qui ne prend que les getters
    alors que FOS/UserBundle utilise is...()
    */
    public function getEnabled()
    {
        return $this->isEnabled();
    }
    public function getLocked()
    {
        return $this->isLocked();
    }
    public function getExpired()
    {
        return $this->isExpired();
    }
    public function getExpiresat()
    {
        return $this->isAccountNonExpired();
    }
    public function getCredentialsexpired()
    {
        return $this->isCredentialsExpired();
    }
    public function getCredentialsexpireat()
    {
        return $this->isCredentialsNonExpired();
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->gmRoles = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    
        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    
        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set makedate
     *
     * @param \DateTime $makedate
     * @return User
     */
    public function setMakedate($makedate)
    {
        $this->makedate = $makedate;
    
        return $this;
    }

    /**
     * Get makedate
     *
     * @return \DateTime 
     */
    public function getMakedate()
    {
        return $this->makedate;
    }

    /**
     * Set site
     *
     * @param \GM\SiteBundle\Entity\Site $site
     * @return User
     */
    public function setSite(\GM\SiteBundle\Entity\Site $site = null)
    {
        $this->site = $site;
    
        return $this;
    }

    /**
     * Get site
     *
     * @return \GM\SiteBundle\Entity\Site 
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set parent
     *
     * @param \GM\UserBundle\Entity\Group $parent
     * @return User
     */
    public function setParent(\GM\UserBundle\Entity\Group $parent = null)
    {
        $this->parent = $parent;
    
        return $this;
    }

    /**
     * Get parent
     *
     * @return \GM\UserBundle\Entity\Group 
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set title
     *
     * @param \GM\UserBundle\Entity\Title $title
     * @return User
     */
    public function setTitle(\GM\UserBundle\Entity\Title $title = null)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return \GM\UserBundle\Entity\Title 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Add gmRoles
     *
     * @param \GM\UserBundle\Entity\Role $gmRoles
     * @return User
     */
    public function addGmRole(\GM\UserBundle\Entity\Role $gmRoles)
    {
        $this->gmRoles[] = $gmRoles;
    
        return $this;
    }

    /**
     * Remove gmRoles
     *
     * @param \GM\UserBundle\Entity\Role $gmRoles
     */
    public function removeGmRole(\GM\UserBundle\Entity\Role $gmRoles)
    {
        $this->gmRoles->removeElement($gmRoles);
    }

    /**
     * Get gmRoles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGmRoles()
    {
        return $this->gmRoles;
    }

    /**
     * Get domain
     *
     * @return string 
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set domain
     *
     * @param string $domain
     * @return User
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;
    
        return $this;
    }

    /**
     * Add parent
     *
     * @param \GM\UserBundle\Entity\Group $parent
     * @return User
     */
    public function addParent(\GM\UserBundle\Entity\Group $parent)
    {
        $this->parent[] = $parent;
    
        return $this;
    }

    /**
     * Remove parent
     *
     * @param \GM\UserBundle\Entity\Group $parent
     */
    public function removeParent(\GM\UserBundle\Entity\Group $parent)
    {
        $this->parent->removeElement($parent);
    }

    public function isGranted($role)
    {
        return in_array($role, $this->getRoles());
    }
}