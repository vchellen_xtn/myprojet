<?php

namespace GM\StatBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Page
 *
 * @ORM\Table(name="gm_stat")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class Stat
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="hit", type="integer")
     */
    private $hit;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var integer
     *
     * @ORM\Column(name="make_user", type="integer", nullable=true)
     */
    private $makeUser;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_modified", type="datetime")
     */
    private $lastModified;

    /**
     * @var integer
     *
     * @ORM\Column(name="edit_user", type="integer", nullable=true)
     */
    private $editUser;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="datetime", nullable=true)
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="datetime", nullable=true)
     */
    private $endDate;

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->created = new \DateTime();
        $this->lastModified = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
        $this->lastModified = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set hit
     *
     * @param integer $hit
     * @return Stat
     */
    public function setHit($hit)
    {
        $this->hit = $hit;
    
        return $this;
    }

    /**
     * Get hit
     *
     * @return integer 
     */
    public function getHit()
    {
        return $this->hit;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Stat
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set makeUser
     *
     * @param integer $makeUser
     * @return Stat
     */
    public function setMakeUser($makeUser)
    {
        $this->makeUser = $makeUser;
    
        return $this;
    }

    /**
     * Get makeUser
     *
     * @return integer 
     */
    public function getMakeUser()
    {
        return $this->makeUser;
    }

    /**
     * Set lastModified
     *
     * @param \DateTime $lastModified
     * @return Stat
     */
    public function setLastModified($lastModified)
    {
        $this->lastModified = $lastModified;
    
        return $this;
    }

    /**
     * Get lastModified
     *
     * @return \DateTime 
     */
    public function getLastModified()
    {
        return $this->lastModified;
    }

    /**
     * Set editUser
     *
     * @param integer $editUser
     * @return Stat
     */
    public function setEditUser($editUser)
    {
        $this->editUser = $editUser;
    
        return $this;
    }

    /**
     * Get editUser
     *
     * @return integer 
     */
    public function getEditUser()
    {
        return $this->editUser;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     * @return Stat
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    
        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime 
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     * @return Stat
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    
        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime 
     */
    public function getEndDate()
    {
        return $this->endDate;
    }
}