<?php

namespace GM\StatBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pined
 *
 * @ORM\Table(name="gm_stat__pined")
 * @ORM\Entity
 */
class Pined
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="new", type="boolean")
     */
    private $new;

    /**
     * @var boolean
     *
     * @ORM\Column(name="home", type="boolean")
     */
    private $home;

    /**
     * @var boolean
     *
     * @ORM\Column(name="heart", type="boolean")
     */
    private $heart;

    /**
     * @var boolean
     *
     * @ORM\Column(name="prearent", type="boolean")
     */
    private $prearent;

    /**
     * @var boolean
     *
     * @ORM\Column(name="archive", type="boolean")
     */
    private $archive;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set new
     *
     * @param boolean $new
     * @return Pined
     */
    public function setNew($new)
    {
        $this->new = $new;
    
        return $this;
    }

    /**
     * Get new
     *
     * @return boolean 
     */
    public function getNew()
    {
        return $this->new;
    }

    /**
     * Set home
     *
     * @param boolean $home
     * @return Pined
     */
    public function setHome($home)
    {
        $this->home = $home;
    
        return $this;
    }

    /**
     * Get home
     *
     * @return boolean 
     */
    public function getHome()
    {
        return $this->home;
    }

    /**
     * Set heart
     *
     * @param boolean $heart
     * @return Pined
     */
    public function setHeart($heart)
    {
        $this->heart = $heart;
    
        return $this;
    }

    /**
     * Get heart
     *
     * @return boolean 
     */
    public function getHeart()
    {
        return $this->heart;
    }

    /**
     * Set prearent
     *
     * @param boolean $prearent
     * @return Pined
     */
    public function setPrearent($prearent)
    {
        $this->prearent = $prearent;
    
        return $this;
    }

    /**
     * Get prearent
     *
     * @return boolean 
     */
    public function getPrearent()
    {
        return $this->prearent;
    }

    /**
     * Set archive
     *
     * @param boolean $archive
     * @return Pined
     */
    public function setArchive($archive)
    {
        $this->archive = $archive;
    
        return $this;
    }

    /**
     * Get archive
     *
     * @return boolean 
     */
    public function getArchive()
    {
        return $this->archive;
    }
}
