<?php

namespace Totem\ExportBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class TotemExportBundle extends Bundle
{
	public function getParent()
	{
		return 'EEDataExporterBundle';
	}
}
