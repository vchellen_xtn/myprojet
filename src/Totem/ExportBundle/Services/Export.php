<?php
namespace Totem\ExportBundle\Services;

use EE\DataExporterBundle\Service\DataExporter;
use Symfony\Component\PropertyAccess\PropertyAccess;

class Export extends DataExporter
{
	private $labels;
    private $rowLabel;

    public function setRowLabel($rowLabel)
    {
        $this->rowLabel = ' name="'.$rowLabel.'" ';
    }
    public function clearRowLabel()
    {
        $this->rowLabel = '';   
    }
	public function clearColumns()
	{
		$this->columns = $this->labels = array();
	}

	public function setColumns(Array $columns)
	{
		parent::setColumns($columns);
		if($this->format == 'xml')
		{
			foreach($columns as $key => $label)
			{
                if(!empty($label))
				    $this->labels[$key]= $label;
			}
		}
	}

	public function setData($rows)
    {
        if (empty($this->format)) {
            throw new \RuntimeException('First use setOptions!');
        }
        if (empty($this->columns)) {
            throw new \RuntimeException('First use setColumns to set columns to export!');
        }

        $accessor = PropertyAccess::getPropertyAccessor();
        $separator = $this->separator;
        $escape = $this->escape;
        $hooks = $this->hooks;
        $format = $this->format;

        foreach ($rows as $row) {
            switch ($this->format) {
                case 'csv':
                case 'json':
                    $tempRow = array();
                    break;
                case 'xls':
                case 'html':
                case 'xml':
                    $tempRow = '';
                    break;
            }

            $tempRow = array_map(
                function ($column) use ($row, $accessor, $separator, $escape, $hooks, $format) {
                    return DataExporter::escape(
                        $accessor->getValue($row, $column),
                        $separator,
                        $escape,
                        $column,
                        $hooks,
                        $format
                    );
                },
                $this->columns
            );

            switch ($this->format) {
                case 'csv':
                    $this->data[] = implode($this->separator, $tempRow);
                    break;
                case 'json':
                    $this->data[] = array_combine($this->data[0], $tempRow);
                    break;
                case 'xls':
                case 'html':
                    $this->data .= '<tr>';
                    foreach ($tempRow as $val) {
                        $this->data .= '<td>' . $val . '</td>';
                    }
                    $this->data .= '</tr>';
                    break;
                case 'xml':
                    $this->data .= '<row '.$this->rowLabel.'>';
                    $i = 0;
                    foreach ($tempRow as $val) {
                    	$label = '';
                    	$name = $this->columns[$i];
                    	if(isset($this->labels[$name]) && !empty($this->labels[$name]))
                    	{
                    		$label = ' label="'.DataExporter::escape(
		                        $this->labels[$name],
		                        $separator,
		                        $escape,
		                        $name,
		                        $hooks,
		                        $format
		                    ).'" ';
                    	}
                        $this->data .= '<column name="' . $name . '" '.$label.'>' . $val . '</column>';
                        $i++;
                    }
                    $this->data .= '</row>';
                    break;
            }

        }

        return $this;
    }
}