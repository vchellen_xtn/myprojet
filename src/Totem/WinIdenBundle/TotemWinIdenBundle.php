<?php

namespace Totem\WinIdenBundle;

use Totem\WinIdenBundle\DependencyInjection\Security\Factory\WinIdenFactory;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class TotemWinIdenBundle extends Bundle
{
	public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $extension = $container->getExtension('security');
        $extension->addSecurityListenerFactory(new WinIdenFactory());
    }
}
