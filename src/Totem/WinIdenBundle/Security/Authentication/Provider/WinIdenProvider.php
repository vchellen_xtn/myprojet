<?php

namespace Totem\WinIdenBundle\Security\Authentication\Provider;

use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\NonceExpiredException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Totem\WinIdenBundle\Security\Authentication\Token\WinIdenUserToken;

class WinIdenProvider implements AuthenticationProviderInterface
{
    private $userProvider;

    public function __construct(UserProviderInterface $userProvider)
    {
        $this->userProvider = $userProvider;
    }

    public function authenticate(TokenInterface $token)
    {
        $user = $this->userProvider->loadUserByUsername($token->getUser());

        if ($user && !empty($token->domain) && $user->getDomain() == $token->domain) {
            $authenticatedToken = new WinIdenUserToken($user->getRoles());
            $authenticatedToken->setUser($user);

            return $authenticatedToken;
        }

        throw new AuthenticationException('Vous n\'êtes pas autorisé à accéder au site.');
    }

    public function supports(TokenInterface $token)
    {
        return $token instanceof WinIdenUserToken;
    }
}