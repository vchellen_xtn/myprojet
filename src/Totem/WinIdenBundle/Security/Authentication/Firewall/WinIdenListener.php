<?php

namespace Totem\WinIdenBundle\Security\Authentication\Firewall;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\Firewall\ListenerInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Doctrine\Common\Util\Debug;

use Totem\WinIdenBundle\Security\Authentication\Token\WinIdenUserToken;

class WinIdenListener implements ListenerInterface
{
    protected $securityContext;
    protected $authenticationManager;

    public function __construct(SecurityContextInterface $securityContext, AuthenticationManagerInterface $authenticationManager)
    {
        $this->securityContext = $securityContext;
        $this->authenticationManager = $authenticationManager;
    }

    public function handle(GetResponseEvent $event)
    {
        if ($this->securityContext->getToken() !== null)
        {
            return true;
        }

        $headers = apache_request_headers();

        if (!isset($headers['Authorization'])){
            header('HTTP/1.1 401 Unauthorized');
            header('WWW-Authenticate: NTLM');
            exit;
        }

        $auth = $headers['Authorization'];

        if (substr($auth,0,5) == 'NTLM ') {
            $msg = base64_decode(substr($auth, 5));
            if (substr($msg, 0, 8) != "NTLMSSP\x00")
                die('error header not recognised');

            if ($msg[8] == "\x01") {
                $msg2 = "NTLMSSP\x00\x02"."\x00\x00\x00\x00". // target name len/alloc
                    "\x00\x00\x00\x00". // target name offset
                    "\x01\x02\x81\x01". // flags
                    "\x00\x00\x00\x00\x00\x00\x00\x00". // challenge
                    "\x00\x00\x00\x00\x00\x00\x00\x00". // context
                    "\x00\x00\x00\x00\x30\x00\x00\x00"; // target info len/alloc/offset

                header('HTTP/1.1 401 Unauthorized');
                header('WWW-Authenticate: NTLM '.trim(base64_encode($msg2)));
                exit;
            }
            else if ($msg[8] == "\x03") {
                function get_msg_str($msg, $start, $unicode = true) {
                    $len = (ord($msg[$start+1]) * 256) + ord($msg[$start]);
                    $off = (ord($msg[$start+5]) * 256) + ord($msg[$start+4]);
                    if ($unicode)
                        return str_replace("\0", '', substr($msg, $off, $len));
                    else
                        return substr($msg, $off, $len);
                }
                $user = get_msg_str($msg, 36);
                $domain = get_msg_str($msg, 28);
                $workstation = get_msg_str($msg, 44);
            }
        }

        $token = new WinIdenUserToken();
        $token->setUser($user);

        $token->domain   = $domain;
        $token->workstation    = $workstation;

        try {
            $authToken = $this->authenticationManager->authenticate($token);

            $this->securityContext->setToken($authToken);
        } catch (AuthenticationException $failed) {
            $response = new Response();
            $response->setContent($failed->getMessage());
            $response->setStatusCode(403);
            $event->setResponse($response);

        }
    }
}