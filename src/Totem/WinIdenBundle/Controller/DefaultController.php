<?php

namespace Totem\WinIdenBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('TotemWinIdenBundle:Default:index.html.twig', array('name' => $name));
    }
}
