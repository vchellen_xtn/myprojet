<?php

namespace Totem\CronBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('TotemCronBundle:Default:index.html.twig', array('name' => $name));
    }
}
