<?php
namespace Totem\CronBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use GM\MediaBundle\Entity\Folder as FolderEntity;
use GM\MediaBundle\Entity\File as FileEntity;
use GM\MediaBundle\Entity\Type as TypeEntity;
use Doctrine\Common\Util\Debug as Debug;

class LaunchCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('cron:launch')
            ->setDescription('Lancer une tache cron')
            ->addArgument('id_cron', InputArgument::OPTIONAL, 'Quel est l\'id de tache')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $idCron = $input->getArgument('id_cron');
        $em = $this->getContainer()->get('doctrine')->getEntityManager();

        $e_cron = $em->getRepository('TotemCronBundle:Cron')->find($idCron);
        if(!$e_cron)
        {
            return 'Erreur de cron';
        }

        $e_request = $e_cron->getRequest();
        // on cherche les valeurs definies dans le cron
        $sCron = $this->getContainer()->get('totem_cron.xeon');
        $valuesCrons = $sCron->getValues($idCron);
        
        $values = array('values' => array());
        if(!$valuesCrons)
        {
            // si pas de valeur du cron on utilise les veleurs par defaut
            $defs = $em->getRepository('TotemRequestBundle:Definition')->findByRequest($e_request);
            foreach ($defs as $def)
            {
                $values['values'][$def->getId()] = $def->getDefaultValue();
            }
        }
        else
        {
            $values['values'] = $valuesCrons;
        }
        
        $item = new \stdClass;
        $item->id = $e_request->getId();
        $parent = new \stdClass;
        $parent->type = 'launchSQL';
        $date = new \DateTime();
        $logs = $this->getContainer()->get('totem_request.service.request')->saveData($item, $values, $parent, true);
        $item->id = $logs['log_id'];
        // le fichier resultSQL_($item->id).csv existe
        $this->getContainer()->get('totem_request.service.sql')->exportGridCsv($item,$e_cron->getTitle().'_'.$date->format("Ymd"));
        $dataPDF = $this->getContainer()->get('totem_request.service.sql')->exportPDFGrid($item);
        // generation en PDF
        $this->generatePdf('/tmp/resultSQL_'.$this->transChar(str_replace(' ','_',$e_cron->getTitle()).'_'.$date->format("Ymd")).'.pdf', $dataPDF['data']);
        $repFolder = $em->getRepository('GMMediaBundle:Folder');
        $groups = $e_request->getGroup();
        $folders = array();
        if($groups->count() == 0)
        {
            // on cree si necessaire le folder invite
            $e_folder = $repFolder->findOneByTitle('Invité');
            if(!$e_folder)
            {
                $e_folder = new FolderEntity();
                $e_folder->setTitle('Invité')
                ->setType($em->getRepository('GMMediaBundle:Type')->find(5));
                $em->persist($e_folder);
                $em->flush();
            }
            $folders[] = $e_folder;
        }
        // on recupere les listes des folder.title = group.title lié à la requete
        foreach ($groups as $grp)
        {
            $e_folder = $repFolder->findOneByTitle($grp->getTitle());
            if(!$e_folder)
            {
                $e_folder = new FolderEntity();
                $e_folder->setTitle($grp->getTitle())
                ->setType($em->getRepository('GMMediaBundle:Type')->find(5));
                $em->persist($e_folder);
            }
            $folders[] = $e_folder;
        }
        // on a tous les folders alors on ajout le fichier dans ces folders
        $files = array();
        $date = new \DateTime();
        foreach($folders as $folder)
        {
            $e_file = new FileEntity();
            //$e_file->setFile(array('name'=>'resultSQL_'.$item->id.'.csv', 'type' => 'text/csv', 'tmp_name' => '/tmp/resultSQL_'.$item->id.'.csv'))
            $e_file->setFile(array('name'=>'resultSQL_'.str_replace(' ','_',$e_cron->getTitle()).'_'.$date->format("Ymd").'.csv', 'type' => 'text/csv', 'tmp_name' => '/tmp/resultSQL_'.str_replace(' ','_',$e_cron->getTitle()).'_'.$date->format("Ymd").'.csv'))
            ->setFolder($folder);
            $e_file->setRequest($e_request);
            $em->persist($e_file);
            $files[] = $e_file;
        }
        foreach($folders as $folder)
        {
            $e_file = new FileEntity();
            //$e_file->setFile(array('name'=>'resultSQL_'.$item->id.'.pdf', 'type' => 'application/pdf', 'tmp_name' => '/tmp/resultSQL_'.$item->id.'.pdf'))
            $e_file->setFile(array('name'=>'resultSQL_'.str_replace(' ','_',$e_cron->getTitle()).'_'.$date->format("Ymd").'.pdf', 'type' => 'application/pdf', 'tmp_name' => '/tmp/resultSQL_'.$this->transChar(str_replace(' ','_',$e_cron->getTitle()).'_'.$date->format("Ymd")).'.pdf'))
            ->setFolder($folder);
            $e_file->setRequest($e_request);
            $em->persist($e_file);
            $files[] = $e_file;
        }

        $em->flush();
        
        $output->writeln('fin');
    }
    protected function generatePdf($filePDF, $data)
    {
        $file = '/tmp/file_' . mt_rand().'.html';
        $fp = fopen($file, 'wb');
        fwrite($fp, $data );
        fclose($fp);
        $cmd = "/usr/local/bin/wkhtmltopdf --print-media-type --page-size A4 -R     50 --encoding utf8 $file ".$filePDF;
        $t = shell_exec($cmd);
        @unlink($file);
    }

    protected function transChar($txt)
    {
        $txt = preg_replace('`&([a-zA-Z])([a-zA-Z]+);`i', '$1', htmlentities($txt));
        return $txt;
     }
}