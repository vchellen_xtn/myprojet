<?php

namespace Totem\CronBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cron
 *
 * @ORM\Table(name="totem_cron")
 * @ORM\Entity
 */
class Cron
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @ORM\OneToOne(targetEntity="Frequency")
     */
    private $frequency;

    /**
     * @ORM\ManyToOne(targetEntity="Totem\RequestBundle\Entity\Request")
     */
    private $request;

    /**
     * @ORM\ManyToMany(targetEntity="Totem\RequestBundle\Entity\Variable")
     */
    private $defined;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;

    /**
     * @ORM\Column(name="periode", type="smallint", nullable=true)
     */
    private $periode;

    /**
     * @ORM\OneToMany(targetEntity="Values", mappedBy="cron")
     */
    private $values;

    /**
     * @ORM\Column(name="status", type="boolean")
     */
    private $status = false;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Cron
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set frequency
     *
     * @param integer $frequency
     * @return Cron
     */
    public function setFrequency($frequency)
    {
        $this->frequency = $frequency;
    
        return $this;
    }

    /**
     * Get frequency
     *
     * @return integer 
     */
    public function getFrequency()
    {
        return $this->frequency;
    }

    /**
     * Set request
     *
     * @param integer $request
     * @return Cron
     */
    public function setRequest($request)
    {
        $this->request = $request;
    
        return $this;
    }

    /**
     * Get request
     *
     * @return integer 
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * Set defined
     *
     * @param integer $defined
     * @return Cron
     */
    public function setDefined($defined)
    {
        $this->defined = $defined;
    
        return $this;
    }

    /**
     * Get defined
     *
     * @return integer 
     */
    public function getDefined()
    {
        return $this->defined;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Cron
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->defined = new \Doctrine\Common\Collections\ArrayCollection();
        $this->values = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Set periode
     *
     * @param integer $periode
     * @return Cron
     */
    public function setPeriode($periode)
    {
        $this->periode = $periode;
    
        return $this;
    }

    /**
     * Get periode
     *
     * @return integer 
     */
    public function getPeriode()
    {
        return $this->periode;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Cron
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Add defined
     *
     * @param \Totem\RequestBundle\Entity\Variable $defined
     * @return Cron
     */
    public function addDefined(\Totem\RequestBundle\Entity\Variable $defined)
    {
        $this->defined[] = $defined;
    
        return $this;
    }

    /**
     * Remove defined
     *
     * @param \Totem\RequestBundle\Entity\Variable $defined
     */
    public function removeDefined(\Totem\RequestBundle\Entity\Variable $defined)
    {
        $this->defined->removeElement($defined);
    }

    /**
     * Add values
     *
     * @param \Totem\CronBundle\Entity\Values $values
     * @return Cron
     */
    public function addValue(\Totem\CronBundle\Entity\Values $values)
    {
        $this->values[] = $values;
    
        return $this;
    }

    /**
     * Remove values
     *
     * @param \Totem\CronBundle\Entity\Values $values
     */
    public function removeValue(\Totem\CronBundle\Entity\Values $values)
    {
        $this->values->removeElement($values);
    }

    /**
     * Get values
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getValues()
    {
        return $this->values;
    }
}