<?php

namespace Totem\CronBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Values
 *
 * @ORM\Table(name="totem_cron_values")
 * @ORM\Entity
 */
class Values
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="value", type="string", length=255)
     */
    private $value;

    /**
     * @ORM\ManyToOne(targetEntity="Variable", cascade={"persist"})
     */
    private $variable;

    /**
     * @ORM\ManyToOne(targetEntity="Cron", inversedBy="values")
     */
    private $cron;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return Values
     */
    public function setValue($value)
    {
        $this->value = $value;
    
        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set variable
     *
     * @param \Totem\CronBundle\Entity\Variable $variable
     * @return Values
     */
    public function setVariable(\Totem\CronBundle\Entity\Variable $variable = null)
    {
        $this->variable = $variable;
    
        return $this;
    }

    /**
     * Get variable
     *
     * @return \Totem\CronBundle\Entity\Variable 
     */
    public function getVariable()
    {
        return $this->variable;
    }

    /**
     * Set cron
     *
     * @param \Totem\CronBundle\Entity\Cron $cron
     * @return Values
     */
    public function setCron(\Totem\CronBundle\Entity\Cron $cron = null)
    {
        $this->cron = $cron;
    
        return $this;
    }

    /**
     * Get cron
     *
     * @return \Totem\CronBundle\Entity\Cron 
     */
    public function getCron()
    {
        return $this->cron;
    }
}