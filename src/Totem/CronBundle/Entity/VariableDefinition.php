<?php

namespace Totem\CronBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Variable
 *
 * @ORM\Table(name="totem_cron_variable_definition")
 * @ORM\Entity
 */
class VariableDefinition
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Totem\RequestBundle\Entity\Definition")
     */
    private $definition;

    /**
     * @ORM\ManyToOne(targetEntity="Totem\CronBundle\Entity\Cron")
     */
    private $cron;

    /**
     * @ORM\Column(name="value", type="string", length=255)
     */
    private $value;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return VariableDefinition
     */
    public function setValue($value)
    {
        $this->value = $value;
    
        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set definition
     *
     * @param \Totem\RequestBundle\Entity\Definition $definition
     * @return VariableDefinition
     */
    public function setDefinition(\Totem\RequestBundle\Entity\Definition $definition = null)
    {
        $this->definition = $definition;
    
        return $this;
    }

    /**
     * Get definition
     *
     * @return \Totem\RequestBundle\Entity\Definition 
     */
    public function getDefinition()
    {
        return $this->definition;
    }

    /**
     * Set cron
     *
     * @param \Totem\CronBundle\Entity\Cron $cron
     * @return VariableDefinition
     */
    public function setCron(\Totem\CronBundle\Entity\Cron $cron = null)
    {
        $this->cron = $cron;
    
        return $this;
    }

    /**
     * Get cron
     *
     * @return \Totem\CronBundle\Entity\Cron 
     */
    public function getCron()
    {
        return $this->cron;
    }
}