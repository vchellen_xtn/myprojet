<?php

namespace Totem\CronBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Frequency
 *
 * @ORM\Table(name="totem_cron__frequency")
 * @ORM\Entity
 */
class Frequency
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer")
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="gap", type="integer")
     */
    private $gap;

    /**
     * @var integer
     *
     * @ORM\Column(name="secondaryGap", type="integer")
     */
    private $secondaryGap;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="beginning", type="datetime")
     */
    private $beginning;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ending", type="datetime")
     */
    private $ending;

    /**
     * @ORM\ManyToMany(targetEntity="Day")
     */
    private $days;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Frequency
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set gap
     *
     * @param integer $gap
     * @return Frequency
     */
    public function setGap($gap)
    {
        $this->gap = $gap;
    
        return $this;
    }

    /**
     * Get gap
     *
     * @return integer 
     */
    public function getGap()
    {
        return $this->gap;
    }

    /**
     * Set secondaryGap
     *
     * @param integer $secondaryGap
     * @return Frequency
     */
    public function setSecondaryGap($secondaryGap)
    {
        $this->secondaryGap = $secondaryGap;
    
        return $this;
    }

    /**
     * Get secondaryGap
     *
     * @return integer 
     */
    public function getSecondaryGap()
    {
        return $this->secondaryGap;
    }

    /**
     * Set beginning
     *
     * @param \DateTime $beginning
     * @return Frequency
     */
    public function setBeginning($beginning)
    {
        $this->beginning = $beginning;
    
        return $this;
    }

    /**
     * Get beginning
     *
     * @return \DateTime 
     */
    public function getBeginning()
    {
        return $this->beginning;
    }

    /**
     * Set ending
     *
     * @param \DateTime $ending
     * @return Frequency
     */
    public function setEnding($ending)
    {
        $this->ending = $ending;
    
        return $this;
    }

    /**
     * Get ending
     *
     * @return \DateTime 
     */
    public function getEnding()
    {
        return $this->ending;
    }

    /**
     * Set days
     *
     * @param integer $days
     * @return Frequency
     */
    public function setDays($days)
    {
        $this->days = $days;
    
        return $this;
    }

    /**
     * Get days
     *
     * @return integer 
     */
    public function getDays()
    {
        return $this->days;
    }
}
