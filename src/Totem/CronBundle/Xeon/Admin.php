<?php
namespace Totem\CronBundle\Xeon;
//
// Module cron v1.1.0
use \Totem\CronBundle\Entity\Variable as VariableEntity;
use \Totem\CronBundle\Entity\Values as ValuesEntity;
use \Totem\CronBundle\Entity\Cron as CronEntity;
use \Totem\CronBundle\Entity\VariableDefinition as VariableDefinitionEntity;
use \Totem\RequestBundle\Entity\Request as RequestEntity;

class Admin
{
    private $_periodes = array();
    protected $_security;
    protected $_service;

    public function hasRight()
    {
        $right = $this->_service->get('gm_user.right');
        if($right->isSuperAdmin())
        {
            return true;
        }
        return false;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getIcon()
    {
        return $this->icon;
    }
	// Initialise le module
    function __construct($util, $url)
    {
      $this->transXeon = $util->get('translator_xeon');
      $this->translator = $util->get('translator');
      $this->tools = $util->get('command');
      $this->em = $util->get('em');
      $this->data = $util->get('data');
      $this->error = $util->get('error');
      $this->_security = $util->get('sc');
      $this->_service = $util->get('service');
      $this->url = $url;
		// Paramètres du module
      $this->name = $this->transXeon->get('mod_cron.mod_name');
      $this->version = '1.0';
      $this->author = 'Hieu VU';
      $this->icon = 'i-images';

      $this->right = array(
          'title' => $this->transXeon->get('mod_cron.mod_name'));
        // Commandes de l'interface
      $this->command = array(
        'makeMediaList' => 'mode,folder,file',
        );  
        // Types d'éléments
      $this->type = array(
         'cron' => array(
            'name' => 'cron',
            'tree' => 1,
            'itemIcon' => 'i-page',
            'addText' => $this->transXeon->get('mod_cron.add_cron'),
            'addRight' => 2,
            'deleteRight' => 3
            ),
         'periode' => array(
            'name' => 'periode'
            ),
         'fin' => array(
            'name' => 'fin'
            )
         );



      $this->tree = array(
        1 => array(
            'name' => 'cron',
            'title' => $this->transXeon->get('mod_cron.mod_name'),
            'rootIcon' => 'i-picture',
            'displayRight' => 1
            )
        );

      $this->_periodes = array(
        $this->transXeon->get('mod_cron.periode_hebdo'),
        $this->transXeon->get('mod_cron.periode_mensuelle'));
  }

  function makeCronRoot($item) {

    return array();
}  
function makeCronGrid()
{
    $data = array();

    $rep = $this->em->getRepository('TotemCronBundle:Cron');
    $crons = $rep->findAll();
    foreach ($crons as $cron)
    {
        $data[] = $this->data->convertAll($cron);
    }
    $nb = sizeof($data);
    return array('data'=>$data,'total'=>$nb);
}
function makeCronTree($item) {
    $rep_cron = $this->em->getRepository('TotemCronBundle:Cron');
    $crons = $rep_cron->findAll();
    $data = array();
        // Liste des enfants
    foreach ($crons as $k => $v) {
        $data[] = array(
            'item' => $this->tools->makeId('cron', $v->getId()),
            'text' => $v->getTitle()
            );
    }

    return $data;
}

function makeCronForm($item) {
    if ($item->add)
    {
        $db['cron'] = $this->data->convertAll(new CronEntity());
        return array(
            'db' => $db,
            'itype' => $this->tools
            ->makeId($item->type)
            );
    }
    else
    {
        $rep_cron = $this->em->getRepository('TotemCronBundle:Cron');
        $cron = $rep_cron->find($item->id);
        $e_values = $this->em->getRepository('TotemCronBundle:Values')->findBy(array('cron'=>$cron));
        $values = array();
        $value_titles = array();
        foreach( $e_values as $val)
        {
            $values[$val->getVariable()->getTitle()] = $val->getValue();
                // on recupere le titre de la valeur de periode_choix
            if ($val->getVariable()->getTitle() == 'periode_choix')
            {
                $value_titles['periode_choix'] = $this->_periodes[$val->getValue()];
            }
        }
        $request = $cron->getRequest();
        if ($request === null)
        {
            $request = new RequestEntity();
        }
        return  array(
            'title' => $cron->getTitle(),
            'db' => array('cron' => $this->data->convertAll($cron),
                'values' => $values,
                'value_titles' => $value_titles,
                'request' => $this->data->convertAll($request))
            );
    }
}     
public function makePeriodeList()
{
    $data = array();
    foreach($this->_periodes as $v => $d)
    {
        $data[] = array('v' => $v, 'd' => $d);
    }
    return $data;
}

function makeCronList($item, $parent, $query)
{
    $data = array();
        //     $data[] = array('v' => $folder->getId(), 'd' => $folder->getTitle());
    return $data;
}

public function saveCronData($_item, $_data)
{
    $res = array();
    $error = '';
    if ($_item->add)
    {
        $cron = new CronEntity();
        $cron->setTitle($_data['title']);
        $this->em->persist($cron);
        $this->em->flush();
    }
    else
    {
        $valide = $this->saveCronData_valid($_data);
        if($valide === true)
        {
            $rep = $this->em->getRepository('TotemCronBundle:Cron');
            $cron = $rep->find($_item->id);
            if($cron === false)
            {
                $valide = $this->transXeon->get('mod_cron.no_exist');
            }
            else
            {
                $cron->setStatus($_data['active']);
                $req = $this->em->getRepository('TotemRequestBundle:Request')->find($_data['request']);
                $repDefinition = $this->em->getRepository('TotemRequestBundle:Definition');
                $sDefintion = $this->_service->get('totem_request.service.definition');

                if($req)
                {
                    $cron->setRequest($req);
                }
                $removeValues = $cron->getValues();
                foreach($removeValues as $rv)
                {
                    $this->em->remove($rv);
                }
                $this->em->flush();

                $values = array();
                $values[] = $this->getValueByCron($cron, 'heure_lancement', $_data);
                $values[] = $this->getValueByCron($cron, 'periode_choix', $_data);

                if($_data['periode_choix'] == '0')
                {
                    $valtmp = $this->getValueByCron($cron, 'periode_hebdo', $_data);
                    $valtmp->setValue(join(',', $_data['periode_hebdo']));
                    $values[] = $valtmp;
                    $values[] = $this->getValueByCron($cron, 'periode_hebdo_semaine', $_data);
                }
                else if($_data['periode_choix'] == '1')
                {
                    $values[] = $this->getValueByCron($cron, 'periode_mensuelle_le_2', $_data);
                    $values[] = $this->getValueByCron($cron, 'periode_mensuelle_le_2_1', $_data);
                    $values[] = $this->getValueByCron($cron, 'periode_mensuelle_tous_2', $_data);
                }
                if(isset($_data['grid_var']))
                {
                    $tabVarDels = $this->em->getRepository('TotemCronBundle:VariableDefinition')->findByCron($cron);
                    foreach($tabVarDels as $varDel)
                    {
                        $this->em->remove($varDel);
                    }
                    foreach($_data['grid_var'] as $def => $def_var)
                    {
                        $e_def = $repDefinition->find($def);
                        $sDefintion->checkValue($e_def, $def_var);

                        $newDefVar = new VariableDefinitionEntity();
                        $newDefVar->setCron($cron)
                        ->setDefinition($e_def)
                        ->setValue($def_var);
                        $this->em->persist($newDefVar);                            
                    }
                }
                foreach($values as $v)
                {
                    $this->em->persist($v);    
                }
                $this->em->persist($cron);
                $this->em->flush();
            }
        }
        if ($valide !== true)
        {
            $this->error->sendError($this->transXeon->get('mod_cron.validation'), $valide);
        }
    }
}
public function deleteCronItem($_item)
{
    $rep = $this->em->getRepository('TotemCronBundle:Cron');
    $cron = $rep->find($_item->id);
    if ($cron !== false)
    {
        $this->em->remove($cron);
        $this->em->flush();
    }
}
private function saveCronData_valid($_data)
{
    if(empty($_data['title']))
    {
        return $this->errorForm('text', $this->transXeon->get('mod_cron.title'));
    }
    preg_match('/^([0-9]{2}):([0-9]{2})$/', $_data['heure_lancement'], $res);


    if(empty($_data['request']))
    {
        return $this->errorForm('text', $this->transXeon->get('mod_cron.choose_request'));   
    }

    if (empty($res) || $res[1] > 23 || $res[2] > 59)
    {
        return $this->errorForm('format', $this->transXeon->get('mod_cron.heure_lancement'));
    }

    if ($_data['periode_choix'] == '')
    {
        return $this->errorForm('text', $this->transXeon->get('mod_cron.periode'));
    }
    else if ($_data['periode_choix'] == '0' &&
        (empty($_data['periode_hebdo']) ||
            empty($_data['periode_hebdo_semaine']) ||
            !is_numeric($_data['periode_hebdo_semaine'])
            )
        )
    {
        return $this->errorForm('zone', $this->transXeon->get('mod_cron.periode_hebdo'));
    }
    else if ($_data['periode_choix'] == '1' &&
        ($_data['periode_mensuelle_le_2'] == '' ||
            $_data['periode_mensuelle_le_2_1'] == '' ||
            empty($_data['periode_mensuelle_tous_2'])
            )
        )
    {
        return $this->errorForm('zone', $this->transXeon->get('mod_cron.periode_mensuelle'));
    }

    return true;
}

private function errorForm($type, $title)
{
    $text = $this->transXeon->get('mod_cron.error_form_'.$type);
    return str_replace('{0}', $title, $text);
}
    /*
    on cherche la value correspondant s'il l'existe pas on le cree c'est comme pour variable
    */
    private function getValueByCron($_cron, $_var, $_data)
    {
        $rep = $this->em->getRepository('TotemCronBundle:Values');
        $values = $rep->findByCron($_cron);
        $value = false;

        foreach ($values as $val) {
            if($val->getVariable()->getTitle() == $_var)
            {
                $value = $val;
                break;
            }
        }
        // si la valeur existe pas on la cree
        if($value === false)
        {
            $value = new ValuesEntity();
            $repV = $this->em->getRepository('TotemCronBundle:Variable');
            $variable = $repV->findOneBy(array('title' => $_var));
            // la varibale nexist pas on la cree
            if (!$variable)
            {
                $variable = new VariableEntity();
                $variable->setTitle($_var);
            }
            
            $value->setCron($_cron);
            $value->setVariable($variable);
        }
        $value->setValue($_data[$_var]);
        return $value;
    }

    private function generateToUnix(CronEntity $_cron)
    {
        $config = array();
        $config['minute'] = '*';
        $config['heure'] = '*';
        $config['jour'] = '*';
        $config['mois'] = '*';
        $config['jourSemaine'] = '*';
        $config['user'] = '*';
        $config['commande'] = '';

        $rep = $this->em->getRepository('TotemCronBundle:Values');
        $values = $rep->findByCron($_cron);

        foreach ($values as $val) {
            $var_title = $val->getVariable()->getTitle();
            $$var_title == $val;
        }

        /*$heure_lancements = explode(':', $heure_lancement);
        $config['heure'] = $heure_lancements[0];
        $config['minute'] = $heure_lancements[1];*/

        // on a demande tous les X jour
        if($periode_choix == 0)
        {
            $config['jour'] = '*/'.$periode_quotidien_jour;
        }
        // choix jour par semaine / tous les X semaines
        if($periode_choix == 1)
        {
            $config['jourSemaine'] = $periode_hebdo.'/'.$periode_hebdo_semaine;
        }
        if($periode_choix == 2)
        {
            $config['jour'] = $periode_mensuelle_le;
            $config['mois'] = '*/'.$periode_mensuelle_tous;
        }
        if($periode_choix == 3)
        {
            $tranches = array('1-7','8-14', '15-21', '21-31');
            $config['jour'] = $tranches[$periode_mensuelle_le_2];
            $config['jourSemaine'] = $periode_mensuelle_le_2_1;
            $config['mois'] = '*/'.$periode_mensuelle_tous_2;
        }

        $cmd = join(' ', $config);
    }

    public function getValues($id)
    {
        $datas = array();
        $cron = $this->em->getRepository('TotemCronBundle:Cron')->find($id);
        
        if(!$cron)
        {
            return false;
        }
        $cronVals = $this->em->getRepository('TotemCronBundle:VariableDefinition')->findByCron($cron);
        foreach($cronVals as $cronVal)
        {
            $datas[$cronVal->getDefinition()->getId()] = $cronVal->getValue();
        }
        return $datas;
    }

    // Liste des médias
    function makeMediaList($mode, $folder, $fileId)
    {
        $this->file = $this->em->getRepository('GMMediaBundle:File');
        $rep_cron = $this->em->getRepository('TotemCronBundle:Cron');
        $item = $this->tools
        ->getId($folder);
        $cron = $rep_cron->find($item->id);
        $request = $cron->getRequest();
        if ($request === null)
        {
            $request = new RequestEntity();
        }
        $data = array();
        $parent = FALSE;
        $upload = FALSE;
        $link = FALSE;

        foreach($this->file
           ->findBy(
            array(
                'request' => $request->getId()
                )
            )  as $file)
            $data[] = array(
                'item' => 'media.fe'.$file->getFolder()
                ->getType()
                ->getId().'.'.
                $file->getId(),

                'title' => $file->getTitle(),
                'icon' => $this->url
                ->gen($file, '100x100'),
                'isFile' => true
                );
        

        
        return array(
            'data' => $data,
            'folder' => $folder,
            'parent' => $parent,
            'upload' => $upload,
            'link' => $link,
            'size_limit' => 0
            );
    }
}