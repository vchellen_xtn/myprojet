<?php

namespace Totem\XeonBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class TotemXeonBundle extends Bundle
{
	public function getParent()
	{
		return 'GMXeonBundle';
	}
}
