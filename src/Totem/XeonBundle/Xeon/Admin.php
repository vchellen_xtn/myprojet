<?php
namespace Totem\XeonBundle\Xeon;

use GM\XeonBundle\Xeon\Admin as BaseAdmin;
use GM\SiteBungle\Entity\Config as ConfigEntity;
class Admin extends BaseAdmin 
{
    protected $_sFlash;
    protected $sService;
    protected $_container;
    public function __construct($config, $locale, $translator, $module, $site, $sc, $lang, $tool, $error, $data, $domain, $country, $alias, $domainXeon, $_session, $_right, $_sFlash, $sService, $_container) {
        parent::__construct($config, $locale, $translator, $module, $site, $sc, $lang, $tool, $error, $data, $domain, $country, $alias, $domainXeon, $_session, $_right);
        $this->_sFlash = $_sFlash;
        $this->sService = $sService;
        $this->command['saveHomeInformation'] = 'html';
        $this->_container = $_container;
    }

    public function init() {
        $datas = parent::init();
        $datas['showInformationPanel'] = true;
        
        $config = $this->sService->service_get('em')->getRepository('GMSiteBundle:Config')->find(1);

        $datas['htmlHomeInformation'] = $config->getValue();

        return $datas;
    }
    protected function executeCommand($item, $action, $element, $arg, $callback = FALSE) {
        $data = parent::executeCommand($item, $action, $element, $arg, $callback);
        
        if(!in_array($element, array('Tree', 'List')))
        {
            $data['messageFlash'] = $this->_sFlash->getAllTextFlash();
        }
        return $data;
    }
    public function saveHomeInformation($html)
    {
        $em = $this->sService->service_get('em');
        $config = $em->getRepository('GMSiteBundle:Config')->find(1);
        $config->setValue($html);
        $em->persist($config);
        $em->flush();
    }
    // Formulaire d'édition des donnéees de configuration
    function makeModuleForm($item, $parent) {
        if ($item->add) {
            $data = array();
            $list = array_merge(glob(COMMON . '/module/module.*.inc.php'), glob(ADMINMVC . '/module/module.*.inc.php'));

            foreach ($list as $file) {
                preg_match('#module\.(.+)\.inc\.php#', $file, $m);
                $mod = xeon::loadModule($m[1], FALSE);

                if (isset($mod->name) && isset($mod->version) && isset($mod->author) && isset($mod->icon)) {
                    $data[] = array($m[1], $mod->name, $mod->icon, '<b>' . $mod->name . '</b> v' . $mod->version . '<br />' . $mod->author);
                }
            }

            return array('mod' => $data);
        } else {
            //$mod = xeon::loadModule($item->id);
            //var_dump($item);
            $mod = $this->module->findByName($item->id);
            if($this->bundleExists('GM'.ucfirst(strtolower($item->id)).'Bundle'))
            {
                $config = $this->config->getInArray('GM'.ucfirst(strtolower($item->id)).'Bundle:Config');
            }
            else
            {
                $config = $this->config->getInArray('Totem'.ucfirst(strtolower($item->id)).'Bundle:Config');
            }
            return array(
                'title' => $mod->getName(),
                //'cfg' => config::getData($item->id),
                'cfg' => $config,
                'module' => $item->id,
                'version' => $mod->version,
                'author' => $mod->author,
                'plugin' => false //class_exists($item->id . '_plugin', FALSE)
            );
        }
    }
    public function bundleExists($bundle){
        if (array_key_exists($bundle, $this->_container->getParameter('kernel.bundles'))) {
            return true;
        }
        return false;
    }
}