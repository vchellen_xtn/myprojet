<?php

namespace Totem\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


/**
 * @Route("/site")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $tree = $this->get('totem_request.service.request')->makeTree(false);
        $messageAccueil = 'test';

        return $this->render('TotemFrontBundle:Default:catalogue.html.twig',
            array('treeRequest' => $tree,"module" => "index",'messageAccueil' => $messageAccueil )
            );

    }

    /**
     * @Route("/catalogue",name="front_catalogue_requete")
     */
    public function catalogueAction()
    {
        $em = $this->getDoctrine()->getManager();
        $messageAccueil = $em->getRepository('GMSiteBundle:Config')->findOneByName('home Information');

        $tree = $this->get('totem_request.service.request')->makeTree();
        return $this->render('TotemFrontBundle:Default:includes/left/sidebar-menu.html.twig',
            array('treeRequest' => $tree,"module" => "catalogue",'messageAccueil' => $messageAccueil )
            );
    }


    /**
     * @Route("/cron",name="front_cron_requete")
     */
    public function cronAction()
    {
        $em = $this->getDoctrine()->getManager();
        $messageAccueil = $em->getRepository('GMSiteBundle:Config')->findOneByName('home Information');
        if ($this->getUser()->isGranted('ROLE_SUPER_ADMIN')){
            $group = false;
        }else{
            $group = $this->getUser()->getParent();
        }
        $results = $em->getRepository('TotemRequestBundle:Request')->findByCategoryWithGroup(false,$group,null,true);
        return $this->render('TotemFrontBundle:Default:includes/left/sidebar-menu.html.twig',
            array('treeRequest' => $results,"module" => "cron",'messageAccueil' => $messageAccueil )
            );
    }

    /**
     * @Route("/favoris",name="front_favoris_requete")
     */
    public function favorisAction()
    {
        $em = $this->getDoctrine()->getManager();
        $messageAccueil = $em->getRepository('GMSiteBundle:Config')->findOneByName('home Information');

        $favorites = $em->getRepository('TotemRequestBundle:Favorite')->findByUser($this->getUser());
        return $this->render('TotemFrontBundle:Default:includes/left/sidebar-menu.html.twig',
            array('treeRequest' => $favorites,"module" => "favoris",'messageAccueil' => $messageAccueil )
            );
    }

     /**
     * @Route("/recent",name="front_recent_requete")
     */
     public function recentAction()
     {
        $em = $this->getDoctrine()->getManager();
        $messageAccueil = $em->getRepository('GMSiteBundle:Config')->findOneByName('home Information');

        $recents = $em->getRepository('TotemRequestBundle:Log')->findByUser($this->getUser(),array('date'=>'desc'),10);
        return $this->render('TotemFrontBundle:Default:includes/left/sidebar-menu.html.twig',
            array('treeRequest' => $recents,"module" => "recents",'messageAccueil' => $messageAccueil )
            );
    }

    /**
     * @Route("/search",name="front_search_requete")
     */
    public function searchAction(Request $request)
    {

        $keyword= $request->request->get('keyword');
        if ($this->getUser()->isGranted('ROLE_SUPER_ADMIN')){
            $group = false;
        }else{
            $group = $this->getUser()->getParent();
        }
        $em = $this->getDoctrine()->getManager();
        $messageAccueil = $em->getRepository('GMSiteBundle:Config')->findOneByName('home Information');
        $results = $em->getRepository('TotemRequestBundle:Request')->findByCategoryWithGroup(false,$group,$keyword);
        return $this->render('TotemFrontBundle:Default:includes/left/sidebar-menu.html.twig',
            array('treeRequest' => $results,"module" => "search",'messageAccueil' => $messageAccueil )
            );
    }



/**
     * @Route("/flash",name="front_flash")
     */
public function flashAction()
{
    $em = $this->getDoctrine()->getManager();
    $flashes = $em->getRepository('TotemSiteBundle:Flash')->findByActif(true);

    return $this->render('TotemFrontBundle:Default:includes/flash.html.twig',
        array('flashes' => $flashes )
        );
}


    /**
     * @Route("/click-request-cron",name="front_click_request_cron")
     */
    public function clickRequestCronAction(Request $request){

        $em = $this->getDoctrine()->getManager();
        $id = $request->request->get('id');
        $files = $em->getRepository('GMMediaBundle:File')->findBy(array("request" => $id, "ext" => "6"));

        return $this->render('TotemFrontBundle:Default:file-list.html.twig',array(
            "files"         =>  $files
            ));
    }


    /**
     * @Route("/click-request",name="front_click_request")
     */
    public function clickRequestAction(Request $request){

        $em = $this->getDoctrine()->getManager();
        $id = $request->request->get('id');
        $requete = $em->getRepository('TotemRequestBundle:Request')->findOneById($id);


        $favorite = $request->request->get('favorite');
        $values = null;
        $module = "";
        $favoriteId = "";
        if (isset($favorite)&& !is_null($favorite)){
            $module = "favorite";
            $values = $this->get('totem_request.service.favorite')->getValues($favorite);
            $favoriteId = $favorite;
        }

        $parametres = array();
        if (count($requete->getDefinitions())>0){
            $parametres = $requete->getDefinitions();
        }

        return $this->render('TotemFrontBundle:Default:before-results.html.twig',array(
            'requete'       =>  $requete,
            'parametres'    =>  $parametres,
            'values'        =>  $values,
            'module'        =>  $module,
            'favoriteId'    =>  $favoriteId
            ));
    }


    /**
     * @Route("/get-result",name="front_get_result")
     */
    public function getResultAction(Request $request){

        $em = $this->getDoctrine()->getManager();
        $id = $request->request->get('id');
        $requete = $em->getRepository('TotemRequestBundle:Request')->findOneById($id);

        $datas = array();
        if (!is_null($request->request->get("parameters"))){
            $datas = $request->request->get("parameters");
        }

        $sql = $this->get('totem_request.service.sql')->translateSQL($requete, $datas);

        $requete->setValue($sql);
        // si pas de paramètre on affiche le résultat
        $query = $this->get('totem_request.service.sql')->execSQL($requete);

        $item = new \stdClass;
        $item->id = $requete->getId();
        $parent = new \stdClass;
        $parent->type = 'launchSQL';
        $values["values"] = $datas;
        $this->get('totem_request.service.request')->saveData($item, $values, $parent, true);
        return $this->render('TotemFrontBundle:Default:results.html.twig',array(
            'tbody'       => $query->fetchAll(),
            'thead'       => $requete->getColonnes()
            ));

    }


    /**
     * @Route("/add-favorite",name="front_add_favorite")
     */
    public function addFavoriteAction(Request $request){
        $em = $this->getDoctrine()->getManager();

        $favoriteService =   $this->get('totem_request.service.favorite');
        $id = $request->request->get("id");
        $requete = $em->getRepository('TotemRequestBundle:Request')->find($id);
        $datas = array();
        $datas['title'] = $request->request->get("title");
        $datas['values'] = array();
        if (!is_null($request->request->get("parameters"))){
            $datas['values'] = $request->request->get("parameters");
        }
        $favoriteService->saveFavorite($datas,$requete);

        return new JsonResponse('Favori enregistré avec succès');

    }


    /**
     * @Route("/remove-favorite",name="front_remove_favorite")
     */
    public function removeFavoriteAction(Request $request){
        $em = $this->getDoctrine()->getManager();

        $favoriteService =   $this->get('totem_request.service.favorite');
        $id = $request->request->get("id");
        $favoriteService->deleteFavorite($id);

        return new JsonResponse('Favori supprimé avec succès');

    }

    /**
     * @Route("/open-pdf/{name}",name="front_open_pdf")
     */
    public function openPdfAction($name)
    {
       $request = $this->get('request');
       $path = $this->container
                        ->get('gm_media.dir')
                        ->getMediaDir();
        $filename = $name.'.pdf';
        if(file_exists($path.$filename))
        {
           $content = file_get_contents($path.$filename);
           $response = new Response();
           $response->headers->set('Content-Type: application/pdf');
           $response->headers->set('Content-Disposition', 'attachment;filename="'.$filename);
           $response->setContent($content);
           return $response;  
        }
        else
        {
            return new Response('Error');
        }

   }  

}
