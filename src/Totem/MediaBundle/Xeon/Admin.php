<?php
namespace Totem\MediaBundle\Xeon;
//
use GM\MediaBundle\Xeon\Admin as BaseAdmin;

class Admin extends BaseAdmin
{
	protected $_user;
	protected $_sRight;

	public function __construct($util, $folder, $type, $file, $right, $render, $dir, $url)
	{
		parent::__construct($util, $folder, $type, $file, $right, $render, $dir, $url);
		$this->_user = $util->get('sc')->getToken()->getUser();

		$this->_sRight = $util->get('service')->get('gm_user.right');

		foreach ($this->type as $key => $type) {
			if(!$this->_sRight->isSuperAdmin())
			{
				if(isset($type['addRight'])) unset($this->type[$key]['addRight']);
				if(isset($type['deleteRight'])) unset($this->type[$key]['deleteRight']);
			}
		}
		
		
	}
	public function makeMediaTree($item, $parent = 0)
	{
		$data = array();
		$conds = array('type' => $this->srvType->find($item->cfg['type']));
		if(!$this->_sRight->isSuperAdmin())
		{
			$conds['title'] = 'Invité';
			if(is_object($this->_user) && is_object($this->_user->getParent()))
			{
				$conds['title'] = $this->_user->getParent()->getTitle();
			}
		}
		// Création de la liste des éléments
		if (!isset($tree))
		{
			$tree = array();
			foreach($this->folder
						 ->findBy($conds) as $folder)
			{

				$parentId = 0;
				if($folder->getParent())
					$parentId = $folder->getParent()->getId();

				$tree[$parentId][$folder->getId()] = $folder;
			}
		}
		
		if (!isset($tree[$parent])) {
			return ($parent == 0) ? $data : FALSE;
		}

		// Liste des enfants
		foreach ($tree[$parent] as $k => $v)
		{
			$data[] = array(
				'item' => $this->tools->makeId('fd'.$item->id, $k), //xeon::makeId('fd'.$item->id, $k),
				'text' => $v->getTitle(),
				'iconCls' => 'i-folder',
				'children' => $this->makeMediaTree($item, $k)
			);
		}
		// on ajout invite si ce ne sont pas les superadmins
		if(!$this->_sRight->isSuperAdmin() && $parent == 0)
		{
			$invites = $this->folder->findBy(array('title' => 'Invité'));
			foreach($invites as $invite)
				if(!isset($tree[$parent][$invite->getId()]))
				{
					$data[] = array(
						'item' => $this->tools->makeId('fd'.$item->id, $invite->getId()), //xeon::makeId('fd'.$item->id, $k),
						'text' => $invite->getTitle(),
						'iconCls' => 'i-folder',
						'children' => $this->makeMediaTree($item, $invite->getId())
					);		
				};
		}
		return $data;
	}

	public function findItem($search)
	{
		return array();
	}
}