<?php

namespace Totem\MediaBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class TotemMediaBundle extends Bundle
{
	public function getParent()
	{
		return 'GMMediaBundle';
	}
}
