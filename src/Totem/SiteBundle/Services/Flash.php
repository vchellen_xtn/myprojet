<?php
namespace Totem\SiteBundle\Services;

use Totem\SiteBundle\Entity\Flash as FlashEntity;

use GM\XeonBundle\Services\Service;

class Flash extends Service
{
	protected $entity = 'TotemSiteBundle:Flash';

	public function makeRoot()
	{
		return array();
	}	
	
	public function makeGrid($_item, $_parent)
	{
		$datas = array();
		$flashs = $this->_em->getRepository('TotemSiteBundle:Flash')->findBy(array(), array('actif'=> 'DESC'));
		foreach($flashs as $flash)
		{
			$datas[] = $this->_data->convertAll($flash);
		}
		return array('data' => $datas);
	}
	public function makeTree($item)
    {
        $data = array();

        $rep = $this->_em->getRepository('TotemSiteBundle:Flash');

        foreach( $rep->findAll() as $k => $v )
        {
            $data[] = array(
                            'item' => $this->_command->makeId('flash', $v->getId()),
                            'text' => $v->getTitle(),
                            'iconCls' => 'i-page-white-'. ($v->getActif() ? 'text' : 'delete')
                        );
        }

        return $data;
    }

    public function makeForm($item)
    {
        if( $item->add )
        {
            return array('title' => '',
                         'db' => array('title' => '',
                                       'text'  => ''
                                    )
                    );
        }
        else
        {
            $rep = $this->_em->getRepository('TotemSiteBundle:Flash');
            $r = $rep->find( $item->id );

            return  array(
            'title' => $r->getTitle(),
            'db' => $this->_data
                         ->convertAll($r)
            );
        }
    }

    public function saveData($item, $data)
    {
        if ($item->add)
        {
            $rep = new FlashEntity();
            $rep->setTitle($data['title'])
            ->setText($data['text'])
            ->setActif(0);
            $this->_em->persist($rep);
            $this->_em->flush();
        }
        else
        {
            $rep = $this->_em->getRepository('TotemSiteBundle:Flash');
            $r = $rep->find( $item->id );

                $this->_data
                     ->save(
                        $r,
                        $data
                        )
                     ->flush();
        }
    }

    public function deleteItem($item)
    {
        $rep = $this->_em->getRepository('TotemSiteBundle:Flash');
        $r = $rep->find($item->id);

        if( $r !== false )
        {
            $this->_em->remove($r);
            $this->_em->flush();
        }
    }

    public function getAllTextFlash()
    {
    	$strs = array();
    	$flashs = $this->_em->getRepository('TotemSiteBundle:Flash')->findBy(array('actif' => 1));
    	foreach ($flashs as $f)
    	{
    		$strs[]= $f->getText();
    	}
    	$str = join(' - ', $strs);

    	return $str;
    }

}
