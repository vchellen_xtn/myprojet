
//
// Interface media v2.1.10
// Auteur : Sylvain Durozard
//

Xeon.module.site =
{
	allowDrop: function(tree, node, target) {
		return true;
	},

	makeFlashRoot: function(ct, item, data)
	{
		var items = [];

		items.push({
			header: lang.xeon.mod_site_title.title,
			dataIndex: 'title',
			width: 150,
			itemLink: ['site.flash', 'id']
          },{
			header: lang.xeon.mod_site_title.text,
			dataIndex: 'text',
			width: 250
          },{
			header: lang.xeon.mod_site_title.actif,
			dataIndex: 'actif',
			width: 60,
			renderer: Xeon.renderStatus,
			editor: new Ext.ux.form.ToggleBox()
          });
		ct.add(new Ext.ux.grid.GridPanel({
			title: lang.xeon.mod_site_title.flash,
			iconCls: 'i-page-white-stack',
			itype: 'site.flash',
			rowId: 'id',
			paging:  false,
			columns: items,
			reloadAfterEdit : true,
			quickSearch: false,
			search: false
		}));
	},

	makeFlashForm: function(ct, item, data)
	{
		var items = [];

		items.push({
			fieldLabel: lang.xeon.title,
			allowBlank: false,
			name: 'title',
			//value: data.db.title
			value: data.db.title,
			displayValue: data.db.title
		},{
			xtype: 'textarea',
			fieldLabel: lang.xeon.mod_site.description,
			name: 'text',
			//value: data.db.text
			value: data.db.text,
			displayValue: data.db.text
		});

		if (item.add)
		{
			ct.add(new Ext.ux.FormPanel({
				items: items
			}));
		}
		else
		{
			items.push({
				xtype: 'togglebox',
				fieldLabel: lang.xeon.mod_site_title.actif,
				name: 'actif',
				value: data.db.actif
			});
			ct.add(new Ext.ux.FormPanel({
				title: lang.xeon.mod_site.flash,
				iconCls: ct.iconCls,
				items: items
			}));
		}
	},

	makeConfigForm: function(ct, item, data)
	{

    }
}