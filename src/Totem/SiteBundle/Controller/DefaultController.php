<?php

namespace Totem\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('TotemSiteBundle:Default:index.html.twig', array('name' => $name));
    }
}
