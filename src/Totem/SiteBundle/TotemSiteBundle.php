<?php

namespace Totem\SiteBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class TotemSiteBundle extends Bundle
{
	public function getParent()
	{
		return 'GMSiteBundle';
	}
}
