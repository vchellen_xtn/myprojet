<?php

namespace Totem\SiteBundle\Xeon;

//
// Module site v1.1.0

use Totem\SiteBundle\Entity\Flash as FlashEntity;

class Admin {
    protected $_service;
    protected $_sFlash;

    public function hasRight() {
        
        $right = $this->_service->get('gm_user.right');
        if($right->isSuperAdmin())
        {
            return true;
        }
        return false;
    }

    public function getName() {
        return $this->name;
    }

    public function getIcon() {
        return $this->icon;
    }

    // Initialise le module
    function __construct($util) {
        $this->transXeon = $util->get('translator_xeon');
        $this->tools = $util->get('command');
       
        $this->_service = $util->get('service');
        $this->_sFlash = $this->_service->get('totem_site.flash');

        // Paramètres du module
        $this->name = $this->transXeon->get('mod_site.mod_name');
        $this->version = '1.0';
        $this->author = 'Xavier HERRIOT';
        $this->icon = 'i-images';
        
        
        $this->right = array(
            1 => array()
            );

        $this->type = array(
            'flash' => array(
                'name' => 'flash',
                'tree' => 1,
                'itemIcon' => 'i-page-white',
                'addRight' => TRUE,
                'deleteRight' => TRUE
            )/*,
            'config' => array(
                'name' => 'config',
                'tree' => 1,
                'itemIcon' => 'i-user'
            )*/
        );


        $this->tree = array(
            1 => array(
                'name' => 'flash',
                'title' => $this->transXeon->get('mod_site.flash'),
                'rootIcon' => 'i-page-white-flash',
                'displayRight' => 1
            )/*,
            2 => array(
                'name' => 'config',
                'title' => $this->transXeon->get('mod_site.config'),
                'rootIcon' => 'i-page',
                'displayRight' => 1
            )*/
        );

       
    }

    // Initialise les données
    function initData() {
        
        // Création du fichier .htaccess
        if (!file_exists($this->dir->getMediaDir() . '/.htaccess')) {
            $data = 'Options +FollowSymlinks -Indexes' . "\n" .
                    'ErrorDocument 404 /' . "\n" .
                    'RewriteEngine on' . "\n" .
                    'RewriteRule ^.*-([0-9]+)\.([a-z0-9]+)$ ' . $this->dir->getMediaWebDir() . '$1.$2 [NC,L]' . "\n" .
                    'RewriteRule ^.*-([0-9]+)-([0-9]+)x([0-9]+)\.([a-z0-9]+)$ ' . $this->dir->getCacheWebDir() . '$2x$3/$1.$4 [NC,L]' . "\n";

            file_put_contents($this->dir->getMediaDir() . '/.htaccess', $data);
        }
    }

    /**
     * Create the root of a contact tree
     *
     * @return	array
     */
    function makeSiteRoot($item) {
        return array();
    }

    public function getService($name)
    {
        if(isset($this->{'_s'.$name}) && !empty($this->{'_s'.$name}))
        {
            return $this->{'_s'.$name};
        }
        return false;
    }

}