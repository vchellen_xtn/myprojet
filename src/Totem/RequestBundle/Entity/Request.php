<?php

namespace Totem\RequestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Request
 *
 * @ORM\Table(name="request")
 * @ORM\Entity(repositoryClass="RequestRepository")
 */
class Request
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="wording", type="text")
     */
    private $wording;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="fullTitle", type="string", length=255)
     */
    private $fullTitle;

    /**
     * @ORM\ManyToOne(targetEntity="Category")
     */
    private $parent;

    /**
     * @ORM\ManyToMany(targetEntity="Keyword", mappedBy="requests")
     */
    private $keywords;

    /**
     * @ORM\OneToMany(targetEntity="Colonne", mappedBy="request", cascade={"persist","remove"})
     */
    private $colonnes;

    /**
     * @ORM\OneToMany(targetEntity="Definition", mappedBy="request", cascade={"persist","remove"})
     */
    private $definitions;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="text")
     */
    private $value;

    /**
     * @ORM\ManyToMany(targetEntity="GM\UserBundle\Entity\Group")
     */
    private $group;

    /**
     * @ORM\ManyToOne(targetEntity="Environment")
     */
    private $environment;

    /**
     * @var boolean
     *
     * @ORM\Column(name="actif", type="boolean")
     */
    private $actif;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->keywords = new \Doctrine\Common\Collections\ArrayCollection();
        $this->colonnes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->definitions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->group = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    public function clearId()
    {
        $this->id = null;
    }

    public function clearKeywords()
    {
        $this->keywords = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Request
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set wording
     *
     * @param string $wording
     * @return Request
     */
    public function setWording($wording)
    {
        $this->wording = $wording;
    
        return $this;
    }

    /**
     * Get wording
     *
     * @return string 
     */
    public function getWording()
    {
        return $this->wording;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Request
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set fullTitle
     *
     * @param string $fullTitle
     * @return Request
     */
    public function setFullTitle($fullTitle)
    {
        $this->fullTitle = $fullTitle;
    
        return $this;
    }

    /**
     * Get fullTitle
     *
     * @return string 
     */
    public function getFullTitle()
    {
        return $this->fullTitle;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return Request
     */
    public function setValue($value)
    {
        $this->value = $value;
    
        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set actif
     *
     * @param boolean $actif
     * @return Request
     */
    public function setActif($actif)
    {
        $this->actif = $actif;
    
        return $this;
    }

    /**
     * Get actif
     *
     * @return boolean 
     */
    public function getActif()
    {
        return $this->actif;
    }

    /**
     * Set parent
     *
     * @param \Totem\RequestBundle\Entity\Category $parent
     * @return Request
     */
    public function setParent(\Totem\RequestBundle\Entity\Category $parent = null)
    {
        $this->parent = $parent;
    
        return $this;
    }

    /**
     * Get parent
     *
     * @return \Totem\RequestBundle\Entity\Category 
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add keywords
     *
     * @param \Totem\RequestBundle\Entity\Keyword $keywords
     * @return Request
     */
    public function addKeyword(\Totem\RequestBundle\Entity\Keyword $keywords)
    {
        $this->keywords[] = $keywords;
    
        return $this;
    }

    /**
     * Remove keywords
     *
     * @param \Totem\RequestBundle\Entity\Keyword $keywords
     */
    public function removeKeyword(\Totem\RequestBundle\Entity\Keyword $keywords)
    {
        $this->keywords->removeElement($keywords);
    }

    /**
     * Get keywords
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * Add colonnes
     *
     * @param \Totem\RequestBundle\Entity\Colonne $colonnes
     * @return Request
     */
    public function addColonne(\Totem\RequestBundle\Entity\Colonne $colonnes)
    {
        $this->colonnes[] = $colonnes;
    
        return $this;
    }

    /**
     * Remove colonnes
     *
     * @param \Totem\RequestBundle\Entity\Colonne $colonnes
     */
    public function removeColonne(\Totem\RequestBundle\Entity\Colonne $colonnes)
    {
        $this->colonnes->removeElement($colonnes);
    }

    /**
     * Get colonnes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getColonnes()
    {
        return $this->colonnes;
    }

    /**
     * Add definitions
     *
     * @param \Totem\RequestBundle\Entity\Definition $definitions
     * @return Request
     */
    public function addDefinition(\Totem\RequestBundle\Entity\Definition $definitions)
    {
        $this->definitions[] = $definitions;
    
        return $this;
    }

    /**
     * Remove definitions
     *
     * @param \Totem\RequestBundle\Entity\Definition $definitions
     */
    public function removeDefinition(\Totem\RequestBundle\Entity\Definition $definitions)
    {
        $this->definitions->removeElement($definitions);
    }

    /**
     * Get definitions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDefinitions()
    {
        return $this->definitions;
    }

    /**
     * Set group
     *
     * @param \GM\UserBundle\Entity\Group $group
     * @return Request
     */
    public function setGroup(\GM\UserBundle\Entity\Group $group = null)
    {
        $this->group = $group;
    
        return $this;
    }

    /**
     * Get group
     *
     * @return \GM\UserBundle\Entity\Group 
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set environment
     *
     * @param \Totem\RequestBundle\Entity\Environment $environment
     * @return Request
     */
    public function setEnvironment(\Totem\RequestBundle\Entity\Environment $environment = null)
    {
        $this->environment = $environment;
    
        return $this;
    }

    /**
     * Get environment
     *
     * @return \Totem\RequestBundle\Entity\Environment 
     */
    public function getEnvironment()
    {
        return $this->environment;
    }

    /**
     * Add group
     *
     * @param \GM\UserBundle\Entity\Group $group
     * @return Request
     */
    public function addGroup(\GM\UserBundle\Entity\Group $group)
    {
        $this->group[] = $group;
    
        return $this;
    }

    /**
     * Remove group
     *
     * @param \GM\UserBundle\Entity\Group $group
     */
    public function removeGroup(\GM\UserBundle\Entity\Group $group)
    {
        $this->group->removeElement($group);
    }
}