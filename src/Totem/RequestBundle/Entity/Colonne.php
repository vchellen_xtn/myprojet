<?php

namespace Totem\RequestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Colonne
 *
 * @ORM\Table(name="colonne")
 * @ORM\Entity
 */
class Colonne
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @ORM\ManyToOne(targetEntity="Request", inversedBy="colonnes")
     */
    private $request;

    /**
     * @ORM\Column(name="rang", type="smallint")
     */
    private $rang;

    /**
     * @ORM\Column(name="unique_title", type="string", length=255)
     */
    private $unique_title;

    /**
     * @ORM\Column(name="rubrique", type="string", length=50, nullable=true)
     */
    private $rubrique;

    /**
     * @ORM\Column(name="cadrage", type="string", length=10, options={"default":"Droite"}, nullable=true)
     */
    private $cadrage;

    /**
     * @ORM\Column(name="format_numeric", type="smallint", options={"default":2}, nullable=true)
     */
    private $format_numeric;

    /**
     * @ORM\Column(name="format_separator", type="string", length=1, options={"default":";"}, nullable=true)
     */
    private $format_separator;

    /**
     * @ORM\Column(name="format_date", type="string", length=10, options={"default":"d-m-Y"}, nullable=true)
     */
    private $format_date;

    /**
     * @ORM\Column(name="width", type="integer", options={"default":60}, nullable=true)
     */
    private $width;

    public function clearId()
    {
        $this->id = null;
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Colonne
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set rang
     *
     * @param integer $rang
     * @return Colonne
     */
    public function setRang($rang)
    {
        $this->rang = $rang;
    
        return $this;
    }

    /**
     * Get rang
     *
     * @return integer 
     */
    public function getRang()
    {
        return $this->rang;
    }

    /**
     * Set unique_title
     *
     * @param string $uniqueTitle
     * @return Colonne
     */
    public function setUniqueTitle($uniqueTitle)
    {
        $this->unique_title = $uniqueTitle;
    
        return $this;
    }

    /**
     * Get unique_title
     *
     * @return string 
     */
    public function getUniqueTitle()
    {
        return $this->unique_title;
    }

    /**
     * Set rubrique
     *
     * @param string $rubrique
     * @return Colonne
     */
    public function setRubrique($rubrique)
    {
        $this->rubrique = $rubrique;
    
        return $this;
    }

    /**
     * Get rubrique
     *
     * @return string 
     */
    public function getRubrique()
    {
        return $this->rubrique;
    }

    /**
     * Set cadrage
     *
     * @param string $cadrage
     * @return Colonne
     */
    public function setCadrage($cadrage)
    {
        $this->cadrage = $cadrage;
    
        return $this;
    }

    /**
     * Get cadrage
     *
     * @return string 
     */
    public function getCadrage()
    {
        return $this->cadrage;
    }

    /**
     * Set format_numeric
     *
     * @param integer $formatNumeric
     * @return Colonne
     */
    public function setFormatNumeric($formatNumeric)
    {
        $this->format_numeric = $formatNumeric;
    
        return $this;
    }

    /**
     * Get format_numeric
     *
     * @return integer 
     */
    public function getFormatNumeric()
    {
        return $this->format_numeric;
    }

    /**
     * Set format_separator
     *
     * @param string $formatSeparator
     * @return Colonne
     */
    public function setFormatSeparator($formatSeparator)
    {
        $this->format_separator = $formatSeparator;
    
        return $this;
    }

    /**
     * Get format_separator
     *
     * @return string 
     */
    public function getFormatSeparator()
    {
        return $this->format_separator;
    }

    /**
     * Set format_date
     *
     * @param string $formatDate
     * @return Colonne
     */
    public function setFormatDate($formatDate)
    {
        $this->format_date = $formatDate;
    
        return $this;
    }

    /**
     * Get format_date
     *
     * @return string 
     */
    public function getFormatDate()
    {
        return $this->format_date;
    }

    /**
     * Set width
     *
     * @param integer $width
     * @return Colonne
     */
    public function setWidth($width)
    {
        $this->width = $width;
    
        return $this;
    }

    /**
     * Get width
     *
     * @return integer 
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set request
     *
     * @param \Totem\RequestBundle\Entity\Request $request
     * @return Colonne
     */
    public function setRequest(\Totem\RequestBundle\Entity\Request $request = null)
    {
        $this->request = $request;
    
        return $this;
    }

    /**
     * Get request
     *
     * @return \Totem\RequestBundle\Entity\Request 
     */
    public function getRequest()
    {
        return $this->request;
    }

    public function __toString()
    {
        return strval($this->getId());
    }


}