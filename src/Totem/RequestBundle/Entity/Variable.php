<?php

namespace Totem\RequestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VariableSave
 *
 * @ORM\Table(name="variable")
 * @ORM\Entity
 */
class Variable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Definition", inversedBy="values")
     */
    private $definition;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="text")
     */
    private $value;
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return Variable
     */
    public function setValue($value)
    {
        $this->value = $value;
    
        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set definition
     *
     * @param \Totem\RequestBundle\Entity\Definition $definition
     * @return Variable
     */
    public function setDefinition(\Totem\RequestBundle\Entity\Definition $definition = null)
    {
        $this->definition = $definition;
    
        return $this;
    }

    /**
     * Get definition
     *
     * @return \Totem\RequestBundle\Entity\Definition 
     */
    public function getDefinition()
    {
        return $this->definition;
    }
}