<?php

namespace Totem\RequestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FavoriteValue
 *
 * @ORM\Table(name="favorite_value")
 * @ORM\Entity
 */
class FavoriteValue
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Favorite")
     */
    private $favorite;

    /**
     * @ORM\ManyToOne(targetEntity="Definition")
     */
    private $definition;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="text")
     */
    private $value;
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return FavoriteValue
     */
    public function setValue($value)
    {
        $this->value = $value;
    
        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set favorite
     *
     * @param \Totem\RequestBundle\Entity\Favorite $favorite
     * @return FavoriteValue
     */
    public function setFavorite(\Totem\RequestBundle\Entity\Favorite $favorite = null)
    {
        $this->favorite = $favorite;
    
        return $this;
    }

    /**
     * Get favorite
     *
     * @return \Totem\RequestBundle\Entity\Favorite 
     */
    public function getFavorite()
    {
        return $this->favorite;
    }

    /**
     * Set definition
     *
     * @param \Totem\RequestBundle\Entity\Definition $definition
     * @return FavoriteValue
     */
    public function setDefinition(\Totem\RequestBundle\Entity\Definition $definition = null)
    {
        $this->definition = $definition;
    
        return $this;
    }

    /**
     * Get definition
     *
     * @return \Totem\RequestBundle\Entity\Definition 
     */
    public function getDefinition()
    {
        return $this->definition;
    }
}