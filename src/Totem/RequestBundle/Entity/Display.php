<?php

namespace Totem\RequestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Display
 *
 * @ORM\Table(name="display")
 * @ORM\Entity
 */
class Display
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="Request", inversedBy="display")
     */
    private $request;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var boolean
     *
     * @ORM\Column(name="position", type="boolean")
     */
    private $position;

    /**
     * @var integer
     *
     * @ORM\Column(name="decimalLength", type="integer")
     */
    private $decimalLength;

    /**
     * @var string
     *
     * @ORM\Column(name="numberSeparator", type="string", length=255)
     */
    private $numberSeparator;

    /**
     * @var string
     *
     * @ORM\Column(name="dateFormat", type="string", length=255)
     */
    private $dateFormat;

    /**
     * @var string
     *
     * @ORM\Column(name="columnWidth", type="decimal")
     */
    private $columnWidth;

    /**
     * @var integer
     *
     * @ORM\Column(name="rank", type="integer")
     */
    private $rank;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Display
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set position
     *
     * @param boolean $position
     * @return Display
     */
    public function setPosition($position)
    {
        $this->position = $position;
    
        return $this;
    }

    /**
     * Get position
     *
     * @return boolean 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set decimalLength
     *
     * @param integer $decimalLength
     * @return Display
     */
    public function setDecimalLength($decimalLength)
    {
        $this->decimalLength = $decimalLength;
    
        return $this;
    }

    /**
     * Get decimalLength
     *
     * @return integer 
     */
    public function getDecimalLength()
    {
        return $this->decimalLength;
    }

    /**
     * Set numberSeparator
     *
     * @param string $numberSeparator
     * @return Display
     */
    public function setNumberSeparator($numberSeparator)
    {
        $this->numberSeparator = $numberSeparator;
    
        return $this;
    }

    /**
     * Get numberSeparator
     *
     * @return string 
     */
    public function getNumberSeparator()
    {
        return $this->numberSeparator;
    }

    /**
     * Set dateFormat
     *
     * @param string $dateFormat
     * @return Display
     */
    public function setDateFormat($dateFormat)
    {
        $this->dateFormat = $dateFormat;
    
        return $this;
    }

    /**
     * Get dateFormat
     *
     * @return string 
     */
    public function getDateFormat()
    {
        return $this->dateFormat;
    }

    /**
     * Set columnWidth
     *
     * @param string $columnWidth
     * @return Display
     */
    public function setColumnWidth($columnWidth)
    {
        $this->columnWidth = $columnWidth;
    
        return $this;
    }

    /**
     * Get columnWidth
     *
     * @return string 
     */
    public function getColumnWidth()
    {
        return $this->columnWidth;
    }

    /**
     * Set rank
     *
     * @param integer $rank
     * @return Display
     */
    public function setRank($rank)
    {
        $this->rank = $rank;
    
        return $this;
    }

    /**
     * Get rank
     *
     * @return integer 
     */
    public function getRank()
    {
        return $this->rank;
    }

    /**
     * Set request
     *
     * @param \Totem\RequestBundle\Entity\Request $request
     * @return Display
     */
    public function setRequest(\Totem\RequestBundle\Entity\Request $request = null)
    {
        $this->request = $request;
    
        return $this;
    }

    /**
     * Get request
     *
     * @return \Totem\RequestBundle\Entity\Request 
     */
    public function getRequest()
    {
        return $this->request;
    }
}