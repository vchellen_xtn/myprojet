<?php

namespace Totem\RequestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SqlResult
 *
 * @ORM\Table(name="sql_result")
 * @ORM\Entity(repositoryClass="SqlResultRepository")
 */
class SqlResult
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Log")
     */
    private $log;


    /**
     * @ORM\Column(name="value", type="text")
     */
    private $value;

    /**
     * @ORM\Column(name="ligne", type="integer")
     */
    private $ligne;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return SqlResult
     */
    public function setValue($value)
    {
        $this->value = $value;
    
        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set ligne
     *
     * @param integer $ligne
     * @return SqlResult
     */
    public function setLigne($ligne)
    {
        $this->ligne = $ligne;
    
        return $this;
    }

    /**
     * Get ligne
     *
     * @return integer 
     */
    public function getLigne()
    {
        return $this->ligne;
    }

    /**
     * Set log
     *
     * @param \Totem\RequestBundle\Entity\Log $log
     * @return SqlResult
     */
    public function setLog(\Totem\RequestBundle\Entity\Log $log = null)
    {
        $this->log = $log;
    
        return $this;
    }

    /**
     * Get log
     *
     * @return \Totem\RequestBundle\Entity\Log 
     */
    public function getLog()
    {
        return $this->log;
    }
}