<?php

namespace Totem\RequestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Variable
 *
 * @ORM\Table(name="definition")
 * @ORM\Entity
 */
class Definition
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="wording", type="text", nullable=true)
     */
    private $wording;

    /**
     * @ORM\Column(name="default_value", type="string",length=255, nullable=true)
     */
    private $defaultValue;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true, options={"default": "Aucun"})
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="list_value", type="text", nullable=true)
     */
    private $listValue;

    /**
     * @ORM\OneToMany(targetEntity="Variable", mappedBy="definition", cascade={"persist","remove"})
     */
    private $values;

    /**
     * @var boolean
     *
     * @ORM\Column(name="obligatory", type="boolean", options={"default": 0})
     */
    private $obligatory;

    /**
     * @ORM\ManyToOne(targetEntity="Request", inversedBy="definitions")
     */
    private $request;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->values = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    public function clearId()
    {
        $this->id = null;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Definition
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set wording
     *
     * @param string $wording
     * @return Definition
     */
    public function setWording($wording)
    {
        $this->wording = $wording;
    
        return $this;
    }

    /**
     * Get wording
     *
     * @return string 
     */
    public function getWording()
    {
        return $this->wording;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Definition
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set obligatory
     *
     * @param boolean $obligatory
     * @return Definition
     */
    public function setObligatory($obligatory)
    {
        $this->obligatory = $obligatory;
    
        return $this;
    }

    /**
     * Get obligatory
     *
     * @return boolean 
     */
    public function getObligatory()
    {
        return $this->obligatory;
    }

    /**
     * Set defaultValue
     *
     * @param \Totem\RequestBundle\Entity\Variable $defaultValue
     * @return Definition
     */
    public function setDefaultValue($defaultValue)
    {
        $this->defaultValue = $defaultValue;
    
        return $this;
    }

    /**
     * Get defaultValue
     *
     * @return \Totem\RequestBundle\Entity\Variable 
     */
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }

    /**
     * Add values
     *
     * @param \Totem\RequestBundle\Entity\Variable $values
     * @return Definition
     */
    public function addValue(\Totem\RequestBundle\Entity\Variable $values)
    {
        $this->values[] = $values;
    
        return $this;
    }

    /**
     * Remove values
     *
     * @param \Totem\RequestBundle\Entity\Variable $values
     */
    public function removeValue(\Totem\RequestBundle\Entity\Variable $values)
    {
        $this->values->removeElement($values);
    }

    /**
     * Get values
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * Set request
     *
     * @param \Totem\RequestBundle\Entity\Request $request
     * @return Definition
     */
    public function setRequest(\Totem\RequestBundle\Entity\Request $request = null)
    {
        $this->request = $request;
    
        return $this;
    }

    /**
     * Get request
     *
     * @return \Totem\RequestBundle\Entity\Request 
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * Set listValue
     *
     * @param string $listValue
     * @return Definition
     */
    public function setListValue($listValue)
    {
        $this->listValue = $listValue;
    
        return $this;
    }

    /**
     * Get listValue
     *
     * @return string 
     */
    public function getListValue()
    {
        return $this->listValue;
    }
}