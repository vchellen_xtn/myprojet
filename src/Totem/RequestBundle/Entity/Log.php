<?php

namespace Totem\RequestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Log
 *
 * @ORM\Table(name="log")
 * @ORM\Entity
 */
class Log
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="GM\UserBundle\Entity\User")
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=50)
     */
    private $email;

    /**
     * @ORM\Column(name="sql_text", type="text")
     */
    private $sql;

    /**
     * @ORM\Column(name="request_resume", type="text")
     */
    private $requestResume;

    /**
     * @ORM\ManyToOne(targetEntity="Request")
     */
    private $request;

    /**
     * @ORM\Column(name="variables", type="text")
     */
    private $variables;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;
    
    /**
     * @ORM\ManyToOne(targetEntity="Environment")
     */
    private $environment;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Log
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set sql
     *
     * @param string $sql
     * @return Log
     */
    public function setSql($sql)
    {
        $this->sql = $sql;
    
        return $this;
    }

    /**
     * Get sql
     *
     * @return string 
     */
    public function getSql()
    {
        return $this->sql;
    }

    /**
     * Set request
     *
     * @param string $request
     * @return Log
     */
    public function setRequest($request)
    {
        $this->request = $request;
    
        return $this;
    }

    /**
     * Get request
     *
     * @return string 
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * Set variables
     *
     * @param string $variables
     * @return Log
     */
    public function setVariables($variables)
    {
        $this->variables = $variables;
    
        return $this;
    }

    /**
     * Get variables
     *
     * @return string 
     */
    public function getVariables()
    {
        return $this->variables;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Log
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set user
     *
     * @param \GM\UserBundle\Entity\User $user
     * @return Log
     */
    public function setUser(\GM\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \GM\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set requestResume
     *
     * @param string $requestResume
     * @return Log
     */
    public function setRequestResume($requestResume)
    {
        $this->requestResume = $requestResume;
    
        return $this;
    }

    /**
     * Get requestResume
     *
     * @return string 
     */
    public function getRequestResume()
    {
        return $this->requestResume;
    }

    /**
     * Set environment
     *
     * @param string $environment
     * @return Log
     */
    public function setEnvironment($environment)
    {
        $this->environment = $environment;
    
        return $this;
    }

    /**
     * Get environment
     *
     * @return string 
     */
    public function getEnvironment()
    {
        return $this->environment;
    }
}