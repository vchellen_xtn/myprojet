<?php
namespace Totem\RequestBundle\Xeon;
//
// Module requête v1.1.0

class Admin
{
    private $_sRequest;
    private $_sCategorie;
    private $_sDefinition;
    private $_sColonne;
    private $_sEnvironment;
    private $_sEnvironmentType;
    private $_sLog;
    private $_sSql;
    private $_sFavorite;
    private $_sImport;
    
    public function hasRight($user)
    {
        // To do : right
        return true;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getIcon()
    {
        return $this->icon;
    }

	// Initialise le module
   function __construct($util)
    {
        $this->transXeon = $util->get('translator_xeon');
        $this->tools = $util->get('command');
        $this->em = $util->get('em');

        $service = $util->get('service');
        $this->_sRequest = $service->get('totem_request.service.request');
        $this->_sCategorie = $service->get('totem_request.service.categorie');
        $this->_sDefinition = $service->get('totem_request.service.definition');
        $this->_sColonne = $service->get('totem_request.service.colonne');
        $this->_sLog = $service->get('totem_request.service.log');
        $this->_sSql = $service->get('totem_request.service.sql');
        $this->_sEnvironment = $service->get('totem_request.service.environment');
        $this->_sEnvironmentType = $this->_sEnvironment->getType();
        $this->_sFavorite = $service->get('totem_request.service.favorite');
        $this->_sImport = $service->get('totem_request.service.import');

		// Paramètres du module
		$this->name = $this->transXeon->get('mod_request.mod_name');
		$this->version = '1.0';
		$this->author = 'Xavier HERRIOT';
		$this->icon = 'i-images';
                
	    // Types d'éléments
        $this->type = array();

        $this->type = array_merge($this->_sRequest->getType(),
            $this->_sCategorie->getType(),
            $this->_sDefinition->getType(),
            $this->_sColonne->getType(),
            $this->_sEnvironment->getType(),
            $this->_sLog->getType(),
            $this->_sSql->getType(),
            $this->_sFavorite->getType(),
            $this->_sImport->getType(),
            $this->type);

    	/*$this->type['env'] = array(
            'name' => 'env',
            'tree' => 2,
            'itemIcon' => 'i-server',
            'addText' => $this->transXeon->get('mod_request.add_env'),
            'addRight' => 8,
            //'copyRight' => 8,
            'deleteRight' => 10,
            'exportRight' => true
        );*/

        $this->tree = array(
            1 => array(
                'name' => 'request',
                'title' => $this->transXeon->get('mod_request.mod_name'),
                'rootIcon' => 'i-picture',
                'displayRight' => 1
            ),
            2 => array(
                'name' => $this->_sEnvironmentType['env']['name'],
                'title' => $this->transXeon->get('mod_request.mod_env'),
                'rootIcon' => $this->_sEnvironmentType['env']['itemIcon'],
                'displayRight' => 8,
            ),
            3 => array(
                'name' => 'log',
                'title' => $this->transXeon->get('mod_request.mod_log'),
                'rootIcon' => 'i-page',
                'displayRight' => 8
            ),
            4 => array(
                'name' => 'import',
                'title' => $this->transXeon->get('mod_request.mod_import'),
                'rootIcon' => 'i-page',
                'displayRight' => 8
            )
        );
        $right = $service->get('gm_user.right');
        if(!$right->isSuperAdmin())
        {
            unset($this->tree[2]);
            unset($this->tree[4]);
        }
    }

    public function findItem($search)
    {
        return $this->_sRequest->findItem($search);
    }

    public function getService($name)
    {
        if(isset($this->{'_s'.$name}) && !empty($this->{'_s'.$name}))
        {
            return $this->{'_s'.$name};
        }
        return false;
    }
}