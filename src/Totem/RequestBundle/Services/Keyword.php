<?php
namespace Totem\RequestBundle\Services;

use GM\XeonBundle\Services\Service;
use Doctrine\Common\Util\Debug as Debug;
use Totem\RequestBundle\Entity\Request as RequestEntity;
use Totem\RequestBundle\Entity\Keyword as KeywordEntity;

class Keyword extends Service
{
	public function getByRequest(RequestEntity $request)
	{
		return $request->getKeywords();
	}

	public function toString(RequestEntity $request)
	{

		$keywords = $this->getByRequest($request);
		$strs = array();
		foreach ($keywords as $key) {
			$strs[] = $key->getValue();
		}

		return join(' ', $strs);
	}

	public function saveByRequest($str, RequestEntity $request)
	{
		$str = trim($str);
		
		$str = preg_replace('/\s\s+/', ' ', $str);
		$strs = explode(' ', $str);
		$keys = $this->getByRequest($request);

		foreach ($keys as $k)
		{
			$val = $k->getValue();
			if(!in_array($val, $strs))
				$k->removeRequest($request);
			else
			{
				$keyExists = array_keys($strs, $val);
				foreach ($keyExists as $ke)
					unset($strs[$ke]);				
			}
		}
		// apres ce checking il y a que les nouveaux qui vont etre inséré
		$repK = $this->_em->getRepository('TotemRequestBundle:Keyword');
		foreach ($strs as $s)
		{
			if(!empty($s))
			{
				$keyExist = $repK->findOneByValue($s);
				if($keyExist)
				{
					$keyExist->addRequest($request);
					$this->_em->persist($keyExist);
				}
				else
				{
					$e_key = new KeywordEntity();
					$e_key->addRequest($request)
					->setValue($s);
					$this->_em->persist($e_key);
				}
			}
		}
		if(sizeof($strs))
		{
			$this->_em->flush();
		}
	}	
}
