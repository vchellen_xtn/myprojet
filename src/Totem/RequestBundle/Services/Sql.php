<?php
namespace Totem\RequestBundle\Services;

use GM\XeonBundle\Services\Service;

use Totem\RequestBundle\Entity\Request as RequestEntity;
use Totem\RequestBundle\Entity\Log as LogEntity;
use Totem\RequestBundle\Entity\SqlResult as SqlResultEntity;

use Doctrine\Common\Util\Debug as Debug;

class Sql extends Service
{
	
	public function getType()
	{
		$type = array('sql' => array(
 		        'name' => 'sql',
		 		'tree' => 1,
		 		'itemIcon' => 'i-page'
		 	)
		);
		$type['sql']['exportRight'] = $type['sql']['exportPDFRight'] = true;
		return $type;
	}
	public function makeGrid($_item, $_parent, $search, $start, $limit)
	{
		if(!isset($_item->id) || !isset($_parent->id))
		{
			return array();
		}
		$req = $this->_em->getRepository('TotemRequestBundle:Request')->find($_parent->id);
		$log = $this->_em->getRepository('TotemRequestBundle:Log')->find($_item->id);
		if(!$req || !$log)
		{
			return array();
		}
		$sColonne = $this->_sc->get('totem_request.service.colonne');

		$datas = $this->getDataFromBDDByLog($log, $search, $start, $limit);
		$total = $datas['total'];
		$datas = $datas['datas'];
		$cols = $sColonne->getByRequest($req);
		
		foreach($cols as $col)
		{
			$fields[] = array('name' => $col['dataIndex']);
		}
		return array('data' => $datas,
			'total' => $total,
			'metaData' => array(
				'totalProperty' => 'total',
				'root'=>'data',
				'columns' => $cols,
				'fields' => $fields
				));
	}
	/*
	on traduit la requete avec toutes les valeurs
	*/
	public function translateSQL(RequestEntity $_req, $_values = null)
	{
		$sDef = $this->_sc->get('totem_request.service.definition');
		$sql = $_req->getValue();

        $defs = $this->_em->getRepository('TotemRequestBundle:Definition')->findByRequest($_req);
		$vars = array();
		$values = array();
		$valuesToUse = array();
		if(is_array($_values))
		{
			$valuesToUse = $_values;
		}
		foreach ($defs as $def) {
			if(!$sDef->checkValue($def, $valuesToUse[$def->getId()]))
			{
				break;
			}
			if(!empty($valuesToUse[$def->getId()]))
			{
				$vars[]	= $def->getTitle();
				$values[] = $valuesToUse[$def->getId()];
			}
		}

		return str_replace($vars, $values, $sql);
	}
	/*
	on execute la requete avec la deuxieme connection
	*/
	public function execSQL($_value, $action = 'query')
	{
		if ($_value instanceof LogEntity)
		{
			$sql = $_value->getSql();
			$env = $_value->getEnvironment();
		}
		else if($_value instanceof RequestEntity)
		{
			$sql = $_value->getValue();
			$env = $_value->getEnvironment();
		}
		else
		{
			$this->_error->sendError($this->_transXeon->get('mod_request_error.title'),
				$this->_transXeon->get('mod_request_error.request_no_ident'));	
		}
		if(!$env)
		{
			$this->_error->sendError($this->_transXeon->get('mod_request_error.title'), $this->_transXeon->get('mod_request_error.request_no_environment'));
		}
		$driver = $env->getDriver();
		$host = $env->getHost();
		$db = $env->getDb();
		$db_user = $env->getUser();
		$db_pass = $env->getPassword();
		try
		{
			$connection_factory = $this->_sc->get('doctrine.dbal.connection_factory');
			$connection = $connection_factory->createConnection(
				array(
				    'driver' => $driver,
				    'user' => $db_user,
				    'password' => $db_pass,
				    'host' => $host,
				    'dbname' => $db
			));
			$res = $connection->$action($sql);
		}
		catch (\Exception $error)
		{
			$this->_error->sendError($this->_transXeon->get('mod_request_error.title'), nl2br($error->getMessage()));
		}

		return $res;
	}
	/*
	on passe par le SqlResultRepository pour lancer la recherche avec Doctrine/Paginator
	*/
	public function getDataFromBDDByLog(LogEntity $_log, $_search, $_start, $_limit)
	{
		$datas = array('datas' => array(), 'total' => 0);
		$pages = $this->_em->getRepository('TotemRequestBundle:SqlResult')->findResultByLog($_log, $_search, $_start, $_limit);

		$datas['total'] = count($pages);
		$iters = $pages->getIterator();
		$dataSort = array();
		$hasSort = false;
		$hasDir = false;
		
		if(!empty($_POST['dir']))
		{
			$hasDir = true;
			$dir = @constant('SORT_'.$_POST['dir']);
		}
		
		if(!empty($_POST['sort']))
		{
			$hasSort = true;
			$sort = $_POST['sort'];
		}

		while(false !== $iters->valid())
		{
			$res = $iters->current();
			$data = $this->unserialize($res->getValue());

			if($hasSort)			
				$dataSort[] = $data[$sort];

			$datas['datas'][] = $data;
			$iters->next();
		}

		if($hasSort && $hasDir)
		{
			array_multisort($dataSort, $dir, $datas['datas']);
		}
		return $datas;
	}
	/*
		on export les resultats à partir d'un log
	*/
	public function exportGrid($_item, $_parent = null, $_search = NULL, $_rows = NULL, $_cols = NULL)
	{
		$pages = $this->getResultFromBDD($_item, $_search);
		$iters = $pages->getIterator();
		
		$log = $this->_em->getRepository('TotemRequestBundle:Log')->find($_item->id);
		// pour avoir le rang
		$sColonne = $this->_sc->get('totem_request.service.colonne');

		$cols = $sColonne->getByRequest($log->getRequest());

		$handle = fopen('/tmp/resultSQL_'. $_item->id.'.csv', 'w+');
		
		// on cree la ligne des colonnes
		$colNames = array();
		foreach($cols as $col)
		{
			$colNames[$col['dataIndex']] = $col['text'];
		}

		fputcsv($handle, $colNames, ';', '"');
		
		

		while(false !== $iters->valid())
		{
			$row = array();
			$data = $this->unserialize($iters->current()->getValue());
			foreach($colNames as $cN => $tmp)
			{
				$row[] = $data[$cN];
			}
            fputcsv($handle, $row, ';', '"');
            $iters->next();
        }
        rewind($handle);
        $output = stream_get_contents($handle);
        fclose($handle);

		return array('name' => 'csv.csv',
		'data' => $output);
	}


    public function exportGridCsv($_item,$filename, $_parent = null, $_search = NULL, $_rows = NULL, $_cols = NULL)
    {
        $pages = $this->getResultFromBDD($_item, $_search);
        $iters = $pages->getIterator();

        $log = $this->_em->getRepository('TotemRequestBundle:Log')->find($_item->id);
        // pour avoir le rang
        $sColonne = $this->_sc->get('totem_request.service.colonne');

        $cols = $sColonne->getByRequest($log->getRequest());

        $handle = fopen('/tmp/resultSQL_'. $filename .'.csv', 'w+');

        // on cree la ligne des colonnes
        $colNames = array();
        foreach($cols as $col)
        {
            $colNames[$col['dataIndex']] = $col['text'];
        }

        fputcsv($handle, $colNames, ';', '"');



        while(false !== $iters->valid())
        {
            $row = array();
            $data = $this->unserialize($iters->current()->getValue());
            foreach($colNames as $cN => $tmp)
            {
                $row[] = $data[$cN];
            }
            fputcsv($handle, $row, ';', '"');
            $iters->next();
        }
        rewind($handle);
        $output = stream_get_contents($handle);
        fclose($handle);

        return array('name' => 'csv.csv',
            'data' => $output);
    }



    public function exportPDFGrid($_item, $_parent = null, $_search = NULL, $_rows = NULL, $_cols = NULL)
	{
		$pages = $this->getResultFromBDD($_item, $_search);
		$iters = $pages->getIterator();

		$log = $this->_em->getRepository('TotemRequestBundle:Log')->find($_item->id);

		// pour avoir le rang
		$sColonne = $this->_sc->get('totem_request.service.colonne');

		$cols = $sColonne->getByRequest($log->getRequest());
		
		// on cree la ligne des colonnes
		$datas = array();
		$colNames = array();
		foreach($cols as $col)
		{
			$colNames[$col['dataIndex']] = $col['text'];
		}

		$datas[] = $colNames;
		
		

		while(false !== $iters->valid())
		{
			$row = array();
			$data = $this->unserialize($iters->current()->getValue());
			foreach($colNames as $cN => $tmp)
			{
				$row[] = $data[$cN];
			}
            $datas[] = $row;
            $iters->next();
        }

        $html = $this->_sc->get('templating')->render(
	        'TotemRequestBundle:Default:export-to-pdf.html.twig',
	        array('datas' => $datas, 'log' => $log)
	    );
        return array(
		            'name'  => 'export_'.date('Y-m-d'),
		            'data' => $html
		        );
	}

	private function getResultFromBDD($_item, $_search = null)
	{
		if(empty($_item->id))
		{
			return $this->_error->sendError($this->_transXeon->get('mod_request_error.title'),
				$this->_transXeon->get('mod_request_error.request_need_launch'));
		}
		$log = $this->_em->getRepository('TotemRequestBundle:Log')->find($_item->id);
		if(!$log)
		{
			return $this->_error->sendError($this->_transXeon->get('mod_request_error.title'),
				$this->_transXeon->get('mod_request_error.log_no_found'));
		}
		$pages = $this->_em->getRepository('TotemRequestBundle:SqlResult')->findResultByLog($log, $_search);
		$iters = $pages->getIterator();

		if(!$iters->valid() || $pages === null || $pages->count() == 0)
		{
			return $this->_error->sendError($this->_transXeon->get('mod_request_error.title'),
				$this->_transXeon->get('mod_request_error.data_no_usable'));
		}

		return $pages;
	}
	/*
	on sauvegarde l'execution d'un log dans la table sql_result
	en sérialisant les résultats - cela facilite la recherche avec LIKE
	*/
	public function saveResultIntoBDDByLog(LogEntity $_log)
	{
		$traitments = $this->getAllTraitment($_log);

		$sColonne = $this->_sc->get('totem_request.service.colonne');
		$query = $this->execSQL($_log);
		$res = $query->fetchAll();
		// les resultats en associatif
		$ligne = 0;
		foreach($res as $value)
		{
			$value = $this->setTraitmentToData($value, $traitments);
			$i = 0;
			// ce while c'est pour simuler un nombre important de resultat
			//while($i++ < 50)
			{
				$data = new SqlResultEntity();
				$data->setLog($_log)
				->setLigne($ligne++)
				->setValue($this->serialize($value));
				$this->_em->persist($data);
				// le clear pour vider la memoire que peut prendre le persist
				// mais comme il vide tout, on lui reinjecte le log
				if($ligne % 300 == 0)
				{
					$this->_em->flush();
					$this->_em->clear();
					$_log = $this->_em->getRepository('TotemRequestBundle:Log')->find($_log->getId());
				}		
			}
		}
		$this->_em->flush();
	}
	/*
	pour effacer les resultats pour un log donné
	*/
	public function clearByLog(LogEntity $_log)
	{
		$ress = $this->_em->getRepository('TotemRequestBundle:SqlResult')->findByLog($_log);
		foreach ($ress as $res) {
			$this->_em->remove($res);
		}
		$this->_em->flush();
	}
	/*
	on recupere tous les traitements lié au champs
	*/
	public function getAllTraitment(LogEntity $_log)
	{
		$cols = $this->_em->getRepository('TotemRequestBundle:Colonne')->findByRequest($_log->getRequest());
		// ce tableau contient les index ou il y a un traitement à faire
		$traitments = array();
		foreach($cols as $col)
		{
			$paramNumbers = array();
			if($col->getFormatNumeric() != null)
			{
				$paramNumbers['decimal'] = $col->getFormatNumeric();
			}
			if($col->getFormatSeparator() != null)
			{
				$paramNumbers['separator'] = $col->getFormatSeparator();
			}
			if($col->getFormatDate() != null)
			{
				$traitments[$col->getUniqueTitle()] = array('action' => 'format_date', 'format_date' => $col->getFormatDate());
			}
			else if(sizeof($paramNumbers))
			{
				$traitments[$col->getUniqueTitle()] = array_merge(array('action' => 'number_format'), $paramNumbers);
			}
		}
		return $traitments;
	}
	/*
	on applique les traitements au resultat
	*/
	private function setTraitmentToData($datas, $traitments)
	{
		foreach($datas as $k => $v)
		{
			if(isset($traitments[$k]) && $traitments[$k]['action'] == 'number_format')
			{
				$decimal = 2;
				$separator = null;
				if(isset($traitments[$k]['decimal']))
				{
					$decimal = $traitments[$k]['decimal'];
				}
				if(isset($traitments[$k]['separator']))
				{
					$separator = $traitments[$k]['separator'];
				}
				$datas[$k] = number_format($v, $decimal, '.', $separator);
			}
			else if(isset($traitments[$k]) && $traitments[$k]['action'] == 'format_date')
			{
				$newDate = new \DateTime($v);
				$datas[$k] = $newDate->format($traitments[$k]['format_date']);
			}
		}
		return $datas;
	}

	/*
	pour eviter les probleme d'encodage pour la (un)serialize
	*/
	private function serialize($value)
	{
		return utf8_encode(serialize($value));
	}

	private function unserialize($value)
	{
		$data = unserialize(utf8_decode($value));
		return array_map('utf8_encode', $data);
	}
	
}