<?php
namespace Totem\RequestBundle\Services;

use GM\XeonBundle\Services\Service;
use Totem\RequestBundle\Entity\Definition as DefinitionEntity;
use Totem\RequestBundle\Entity\Request as RequestEntity;

class Definition extends Service
{
	private $_type = array();

	public function getType()
	{
		$this->_type = array('aucun' => $this->_transXeon->get('mod_request_definition.type_null'),
		'integer' => $this->_transXeon->get('mod_request_definition.type_int'),
		'float' => $this->_transXeon->get('mod_request_definition.type_float'),
		'string' => $this->_transXeon->get('mod_request_definition.type_string'),
		'date' => $this->_transXeon->get('mod_request_definition.type_date')
		);

		return array(
        'def' => array(
			'name' => 'Definition'
		));
	}
	public function saveData($_item, $_data)
	{
		if(!$_item->add)
		{
			// ca sert a mapper les champs de la grid de l'admin avec les setters de lentity Definition
			$mapping = array('default_value' => 'DefaultValue',
				'list_value' => 'ListValue');
			if($_item->id == 'affectation')
			{
				// TODO something
				return;
			}
			$rep = $this->_em->getRepository('TotemRequestBundle:Definition');
			$e = $rep->find($_item->id);
			if ($e)
			{
				foreach ($_data as $colName => $value)
				{
					$field = ucfirst(strtolower($colName));
					if(isset($mapping[$colName]))
					{
						$field = $mapping[$colName];
					}
					$e->{'set'.$field}($value);
					if ($colName == 'type' && $value == 'aucun')
					{
						$e->setListValue('');
					}
				}
				
				if($this->checkValue($e) && $this->checkListValue($e))
				{
					$this->_em->persist($e);
					$this->_em->flush();
				}
			}
		}
	}
	
	public function makeGrid($_item, $_parent, $search, $start, $limit)
	{
		if(isset($_item->id))
		{
			$infoImportants = explode('_', $_item->id);
			if($infoImportants[0] == 'selectFav')
			{
				$dataImportants = $this->_sc->get('totem_request.service.favorite')->getValues($infoImportants[1]);
			}
			else if($infoImportants[0] == 'fromCron')
			{
				$dataImportants = $this->_sc->get('totem_cron.xeon')->getValues($infoImportants[1]);
			}
		}
		$datas = array();
		$repReq = $this->_em->getRepository('TotemRequestBundle:Request');
		$req = $repReq->find($_parent->id);

		$repDef = $this->_em->getRepository('TotemRequestBundle:Definition');
		$defs = $repDef->findBy(array('request' => $req));
		$total  = sizeof($defs);
		$defs = array_slice($defs, $start, $limit);
		foreach ($defs as $def) {
			// dans la base c'est la valeur de select
			// ce setType c'est pour son nom en affichage
			$def->setType($this->_type[$def->getType()]);
			if(isset($dataImportants[$def->getId()]))
			{
				$def->setDefaultValue($dataImportants[$def->getId()]);
			}
			$datas[] = $this->_data->convertAll($def);
		}
		return array('data' => $datas, 'total' => $total);
	}

	// creation definition pat request
	public function createByRequest($_req)
	{
		$repDef = $this->_em->getRepository('TotemRequestBundle:Definition');
		$sql = $_req->getValue();
		// on recupere tous les %XXX% qui existent
		preg_match_all('/%\w*%/',$sql, $defs);
		$vars = array();
		$dontDels = array();
		// si on le trouve on mets un tag pour ne pas le supprimer - sinon on le cree
		foreach ($defs[0] as $var) {
			$e_var = $repDef->findOneBy(array('request' => $_req, 'title' => $var));
			if(!$e_var)
			{
				$e_var = new DefinitionEntity();
				$e_var->setTitle($var);
				$e_var->setRequest($_req);
				$e_var->setType('aucun');
				$e_var->setObligatory(false);
				$vars[] = $e_var;	
			}
			else
			{
				$dontDels[$e_var->getId()] = $e_var->getId();
			}
		}
		// tous les defs qui ne sont pas dans le tag on detruit
		$varDeletes = $repDef->findBy(array('request' => $_req));
		foreach($varDeletes as $vd)
		{
			if(!isset($dontDels[$vd->getId()]))
			{
				$this->_em->remove($vd);
			}
		}
		// on persist les nouveaux + existant
		foreach($vars as $var)
		{
			$this->_em->persist($var);
		}
		if(sizeof($vars))
		{
			$this->_em->flush();
		}
	}
	/*
	on verifie soit $_value / $_def
	soit la valeur par_default
	*/
	public function checkValue(DefinitionEntity $_def, $_value = null)
	{
		if(empty($this->_type))
		{
			$this->getType();
		}
		$type = $_def->getType();
		$value = $_value;
		if($value === null)
		{
			$value = $_def->getDefaultValue();
		}
		if(empty($value) || preg_match('/^\**$/', $value))
		{
			return true;
		}
		
		if(!isset($this->_type[$type]))
		{
			$this->_error->sendError($this->_transXeon->get('mod_request.checking'), $this->_transXeon->get('mod_request_definition.type_no_exist'));
		}
		$error = false;
		$plusType = '';
		if($type == 'date')
		{
			// on regarde si le format contient que de valeur autorisé
			$pattern = '/^(m{1,2}|d{1,2}|y{1,4}){1}(\-|\\/)(m{1,2}|d{1,2}|y{1,4}){1}(\-|\\/)(m{1,2}|d{1,2}|y{1,4}){1}$/';
			$pattern2 = '/^(\d{1,4}){1}(\-|\\/)(\d{1,4}){1}(\-|\\/)(\d{1,4}){1}$/';
			
			// on est en mode declaration definition
			if($_value === null && !preg_match($pattern, $value))
				$error = true;
			// on va tester ici la valeur saisie le format demande
			// on est en mode saisie valeur 
			elseif ($_value !== null && !preg_match($pattern2, $value))
				$error = true;
			else if ($value != $_def->getDefaultValue())
			{				
				preg_match_all($pattern, $_def->getDefaultValue(), $matchesD);
				array_shift($matchesD);
				preg_match_all($pattern2, $value, $matchesV);
				array_shift($matchesV);
				if(!$error)
					$error = !$this->checkValueDate($matchesD[0][0], $matchesV[0][0]);
				if(!$error)
					$error = !$this->checkValueDate($matchesD[2][0], $matchesV[2][0]);
				if(!$error)
					$error = !$this->checkValueDate($matchesD[4][0], $matchesV[4][0]);
				$plusType = '('.$_def->getDefaultValue().')';
			}
		}
		switch (true) {
			case ($type == 'integer' && (!is_numeric($value) || intval($value) != $value)):
			case ($type == 'float' && !is_float($value)):
			case ($type == 'string' && (!is_string($value) || is_numeric($value))):
			case $error:
				$strError = $this->_transXeon->get('mod_request_definition.type_error');
				$this->_error->sendError($this->_transXeon->get('mod_request.checking'),
					str_replace(
						array('{0}', '{1}', '{2}'),
						array($_def->getTitle(), $this->_type[$type].$plusType, $value),
						$strError
					));
		}
		
		return !$error;
	}
	/*
	on check la liste de valeur
	*/
	public function checkListValue(DefinitionEntity $_def, $_list = null)
	{
		$list = $_list;
		if($_list === null)
		{
			$list = $_def->getListValue();
		}
		if(empty($list))
		{
			return true;
		}
		$lists = explode(';', $list);
		foreach ($lists as $l)
		{
			if(!$this->checkValue($_def, $l))
			{
				return false;
			}
		}
		return true;
	}

	public function getListType()
	{
		return $this->_type;
	}

	private function checkValueDate($str, $integer)
	{
		$error = false;
		$count = strlen($str);
		$count2 = strlen($integer);
		
		switch (true) {
			// test le mois
			case $integer <= 0:
			case $count2 != $count:				
			case preg_match('/^m+$/', $str) && $integer > 12:
			case preg_match('/^d+$/', $str) && $integer > 31:
			case preg_match('/^y+$/', $str) && $count == 4 && $integer < 1500:
				$error = true;
		}
		return !$error;
	}
}