<?php
namespace Totem\RequestBundle\Services;

use GM\XeonBundle\Services\Service;
use Doctrine\Common\Util\Debug as Debug;
use Totem\RequestBundle\Entity\Request as RequestEntity;
use GM\UserBundle\Entity\User as UserEntity;

class Request extends Service
{
	public function getType()
	{
		$type = 
		array('req' => array(
	 		        'name' => 'request',
		 		'tree' => 1,
		 		'itemIcon' => 'i-page',
		 		'addText' => $this->_transXeon->get('mod_request.add_request'),
		 		'copyText' => 'Dupliquer',
		 	)
		);
		if ($this->checkRight('request_manager_request', false))
		{
			$type['req']['addRight'] = true;
			$type['req']['deleteRight'] = true;
			$type['req']['copyRight'] = true;
			//$type['req']['exportRight'] = true;
		}
		return $type;
	}
	
	public function copyItem($_item, $_data)
	{
		$e_reqOrigin = $this->_em->getRepository('TotemRequestBundle:Request')->find($_item->id);
		$new = clone $e_reqOrigin;
		$new->clearId();
		$new->setTitle($_data['title']);
		$new->setParent(null);
		$new->clearKeywords();

		if (!empty($_data['parent']))
		{
			$cate = $this->_em->getRepository('TotemRequestBundle:Category')->find($_data['parent']);
			$new->setParent($cate);
		}
		$new->setDescription($_data['description']);
		$new->setActif(0);
		
		$cols = $e_reqOrigin->getColonnes();
		foreach($cols as $col)
		{
			$newCol = clone $col;
			$newCol->clearId();
			$newCol->setRequest($new);
			$new->addColonne($newCol);
		}

		$defs = $e_reqOrigin->getDefinitions();
		foreach($defs as $def)
		{
			$newDef = clone $def;
			$newDef->clearId();
			$newDef->setRequest($new);
			$new->addDefinition($newDef);
		}

		$this->_em->persist($new);
		$this->_em->flush();

		$sKey = $this->_sc->get('totem_request.service.keyword');
		$keyword = $sKey->toString($e_reqOrigin);

		$sKey->saveByRequest($keyword, $new);
	}

	public function makeCopy($_item)
	{
		$dbs = array();
		$rep = $this->_em->getRepository('TotemRequestBundle:Request');
		$e_req = $rep->find($_item->id);

		if ($e_req->getParent() != null)
		{
			$dbs['parent'] = $e_req->getParent()->getId();
			$dbs['parent_title'] = $e_req->getParent()->getTitle();
		}
		$text_copy = $this->_transXeon->get('mod_request.text_copy');
		$dbs['title'] = str_replace('{0}', $e_req->getTitle(), $text_copy);
		$dbs['description'] = str_replace('{0}', $e_req->getDescription(), $text_copy);

		return array('db' => $dbs);
	}
	public function makeRoot()
	{
		return array();
	}
	public function makeList($item)
	{
		$em = $this->_em;
		$rep = $em->getRepository('TotemRequestBundle:Request');
		$data = array();
		$conds = array();

		if(!$this->isSuperAdmin())
		{
			$conds['actif'] = true;
		}

		$reqs = $rep->findBy($conds);

		foreach($reqs as $req)
		{
			$data[] = array('v' => $req->getId(), 'd' => $req->getTitle());
		}
		return $data;
	}

	public function makeGrid($_item,$_parent, $search)
	{
		$datas = array();
		$reqs = array();
		$group = false;
		if(!$this->isSuperAdmin())
		{
			$group = null;
			$user = $this->_security->getToken()->getUser();

			if($user instanceof UserEntity)
			{
				$group = $user->getParent();
			}
		}
		if($_parent->type == 'root')
		{
			$reqs = $this->_em->getRepository('TotemRequestBundle:Request')->findByCategoryWithGroup(false, $group, $search);
		}
		else if($_parent->type = 'cat')
		{
			$rep = $this->_em->getRepository('TotemRequestBundle:Request');
			$cate = $this->_em->getRepository('TotemRequestBundle:Category')->find($_parent->id);

			$reqs = $rep->findByCategoryWithGroup($cate, $group, $search);
		}
		foreach ($reqs as $req) {
			$data = $this->_data->convertAll($req);
			if ($req->getParent())
			{
				$data['category_title'] = $req->getParent()->getTitle();
			}
			$datas[] = $data;
		}
		return array('data' => $datas);
	}
	/*
	Afficher le formulaire de la requete
	*/
	public function makeForm($_item)
	{
		if($_item->add)
		{
			$req = array(
            'parent' => 0,
            'parent_title' => $this->_transXeon->get('root')
            );
            $title = $this->_transXeon->get('mod_request.new');
		}
		else
		{
			$e_req = $this->getRequestByUser($_item->id);
			if($e_req)
			{
				$req = $this->_data->convertAll($e_req);
				$req['actif'] = intval($req['actif']);
				if(!$this->isSuperAdmin())
				{
					$req['description'] = nl2br($req['description']);
				}
				$req['parent_title']=$this->_transXeon->get('root');
				if ($e_req->getParent())
				{
					$req['parent'] = $e_req->getParent()->getId();
					$req['parent_title'] = $e_req->getParent()->getTitle();
				}
				if ($e_req->getGroup())
				{
					$req['multi_grp'] = array();
					foreach($e_req->getGroup() as $e_grp)
					{
						$req['multi_grp'][] = array($e_grp->getId(), $e_grp->getTitle());	
					}
				}
				if ($e_req->getEnvironment())
				{
					$req['environment_id'] = $e_req->getEnvironment()->getId();
					$req['environment_title'] = $e_req->getEnvironment()->getTitle();	
				}
				$sKey = $this->_sc->get('totem_request.service.keyword');
				$req['keywords'] = $sKey->toString($e_req);
				$title = $e_req->getTitle();
			}
			else
			{
				$this->_error->sendError($this->_transXeon->get('mod_request_error.title'),
					$this->_transXeon->get('mod_request_error.request_no_enable'));
			}
		}

        return array(
        		'title' => $title,
                'db' => $req,
                'itype' => $this->_command->makeId($_item->type),
                'listDefinitionType' => $this->_sc->get('totem_request.service.definition')->getListType()
            );
	}
	
	public function saveData($_item, $_data, $_parent, $_cron = false)
	{
		
		if(!empty($_parent) && in_array($_parent->type, array('showSQL', 'launchSQL')))
		{
			$sLog = $this->_sc->get('totem_request.service.log');
			$sS = $this->_sc->get('totem_request.service.sql');
			if(!$_cron)
			{
				$req = $this->getRequestByUser($_item->id);
			}
			else
			{
				$req = $this->_em->getRepository('TotemRequestBundle:Request')->findOneBy(array('id'=>$_item->id, 'actif' => 1));
			}
			
			$vars = array();
			if(isset($_data['values']) && is_array($_data['values']))
			{
				$vars = $_data['values'];
			}
			// on traduit la chaine sql
			$sql = $sS->translateSQL($req, $vars);

			// a partir du bouton lancer j'ai déjà les colonnes
			if($_parent->type == 'launchSQL')
			{
				$this->_session->set('orderColumn_' . $_item->id, null);
				// on insere le log
				$environment = null;
				if(isset($_data['admin_environment']) && !empty($_data['admin_environment']))
				{
					$environment = $_data['admin_environment'];
				}
				$log = $sLog->save($req, $vars, $sql, $environment);
				// on sauvegarde dans la base les resultats
				$sS->saveResultIntoBDDByLog($log);

				return array('log_id'=> $log->getId());
			}
			else if($_parent->type == 'showSQL')
			{
				return array('sql' => $sql);
			}
		}

		

		if ($_item->add)
		{
			$req = new RequestEntity();
			if (!empty($_data['parent']))
			{
				$cate = $this->_em->getRepository('TotemRequestBundle:Category')->find($_data['parent']);
				$req->setParent($cate);
			}
			
			$req->setTitle($_data['title']);
			$req->setDescription($_data['description']);
			$req->setWording('');
			$req->setFullTitle('');
			$req->setValue('');
			$req->setActif(false);
			$this->_em->persist($req);
			$this->_em->flush();
		}
		else
		{
			$req = $this->_em->getRepository('TotemRequestBundle:Request')->find($_item->id);
			
			if(isset($_data['saveSQL'])) // TODO : a remettre !empty($_data['codeSql']))
			{
				if ($req)
				{
					$req->setValue($_data['codeSql']);
					// on analyse pour recuperer les colonnes
					$sColonne = $this->_sc->get('totem_request.service.colonne');
					$columns = $sColonne->createByRequest($req);
					// on analyser pour recupere les definitions
					$sDefinition = $this->_sc->get('totem_request.service.definition');
					$sDefinition->createByRequest($req);
				}
			}
			else if(isset($_data['group']))
			{
				$req->setGroup(null);
				if(!empty($_data['group']))
				{
					$grp_ids = explode(',', $_data['group']);
					foreach($grp_ids as $grp_id)
					{
						$req->addGroup($this->_em->getRepository('GMUserBundle:Group')->find($grp_id));
					}
				}
			}
			else
			{
				if(isset($_data['title']))
				{
					$req->setTitle($_data['title']);
				}
				if(isset($_data['full_title']))
				{
					$req->setFullTitle($_data['full_title']);
				}
				if(isset($_data['description']))
				{
					$req->setDescription($_data['description']);
				}
				
				$req->setParent(null);
				if (!empty($_data['parent']))
				{
					$cate = $this->_em->getRepository('TotemRequestBundle:Category')->find($_data['parent']);
					$req->setParent($cate);
				}
				$req->setEnvironment(null);
				if (!empty($_data['environment']))
				{
					$env = $this->_em->getRepository('TotemRequestBundle:Environment')->find($_data['environment']);
					$req->setEnvironment($env);
				}
				$req->setActif($_data['actif']);

				$sKey = $this->_sc->get('totem_request.service.keyword');
				$sKey->saveByRequest($_data['keywords'], $req);
			}
			$this->_em->persist($req);
			$this->_em->flush();
		}
		return array();
	}

	public function deleteItem($_item)
	{
		// right DELETE
		$right = $this->checkRight('request_manager_request');
		if($right !== true)
		{
			return $right;
		}
		$req = $this->_em->getRepository('TotemRequestBundle:Request')->find($_item->id);
		$this->_em->remove($req);
		$this->_em->flush();
	}
	public function makeTree($cron=false)
	{
		if(is_object($cron))
			$cron = false;
		$datas = $this->loadTree(null,$cron);
        return $datas;
	}
	/*
	charger TREE des requetes en commencant par les cate
	*/
	private function loadTree($parent = null,$cron=false)
	{
		$isSuperAdmin = $this->_sc->get('gm_user.right')->isSuperAdmin();
        
		$em = $this->_em;
		$cateRep = $em->getRepository('TotemRequestBundle:Category');
		$cates = $cateRep->findBy(array('parent'=> $parent));
		
		$datas = array();
		// on ajout d'abord les acte de la cate en cours
		foreach ($cates as $cate)
		{
			$oneCate = array(
				'item' => 'request.cat.'.$cate->getId(),
				'text' => $cate->getTitle(),
				'iconCls' => 'i-folder',
				'children' => array()
				);
			$children = $this->loadTree($cate,$cron);
			if(is_array($children))
			{
				$oneCate['children'] = array_merge($oneCate['children'], $children);
			}
			if(sizeof($oneCate['children']) || $isSuperAdmin)
			{		
				$datas[] = $oneCate;
			}
		}
		// on ajout ensuite les req pour la cate en cours
		$reqParents = $this->loadRequest($parent,$cron);
		if(is_array($reqParents))
		{
			$datas = array_merge($datas,$reqParents);
		}
		if(sizeof($datas) == 0)
		{
			return false;
		}
		return $datas;
	}
	/*
	charger les requetes de chaque categorie
	*/
	private function loadRequest($_cate,$cron)
	{
		$status = array(0 => 'i-page-delete', 1 => 'i-page');

		$isSuperAdmin = $this->_sc->get('gm_user.right')->isSuperAdmin();

		$em = $this->_em;
		$group = false;

		if(!$isSuperAdmin)
		{
			$group = null;
			$user = $this->_security->getToken()->getUser();

			if($user instanceof UserEntity)
			{
				$group = $user->getParent();
			}
		}

		$reqs = $em->getRepository('TotemRequestBundle:Request')->findByCategoryWithGroup($_cate, $group,null,$cron);
		$datas = false;
		if(sizeof($reqs))
		{
			$datas = array();
			foreach($reqs as $req)
			{
				$datas[] = array(
					'item' => 'request.req.'.$req->getId(),
					'text' => $req->getTitle(),
					'qtipTitle' => $req->getFullTitle(),
					'qtip' => nl2br($req->getDescription()),
					'iconCls' => $status[$req->getActif()]
					);
			}
		}
		return $datas;
	}

	public function exportXML($item)
	{
		$file = 'export_'.$item->id;
		$datas = array();
		$em = $this->_em;
		$req = $em->getRepository('TotemRequestBundle:Request')->find($item->id);

		$exporter = $this->_sc->get('totem_export.exporter');
		$exporter->setOptions('xml', array('fileName' => $file,'memory'));
		$exporter->setRowLabel('req');
		$exporter->setColumns(array('title','wording','description','fullTitle','value'));
		$exporter->setData(array($req));

		$keys = $req->getKeywords();
		if($keys)
		{
			$exporter->clearColumns();
			$exporter->setRowLabel('key');
			$exporter->setColumns(array('[value]'));
			$sKey = $this->_sc->get('totem_request.service.keyword');
			$keyword = $sKey->toString($req);
			$exporter->setData(array(
				array('value' => $keyword)
				));
		}

		$cols = $req->getColonnes();
		if($cols)
		{
			$exporter->clearColumns();
			$exporter->setRowLabel('col');
			$exporter->setColumns(array('title','rang','uniqueTitle','cadrage','formatNumeric','formatSeparator','formatDate','width'));
			$exporter->setData($cols);
		}
		
		$defs = $req->getDefinitions();
		if($defs)
		{
			$exporter->clearColumns();
			$exporter->setRowLabel('def');
			$exporter->setColumns(array('title','wording','defaultValue','type','listValue','obligatory'));
			$exporter->setData($defs);
		}

		$datas['data'] = $exporter->render();
		$datas['name'] = $file;
		return $datas;
	}
	
	public function getRequestByUser($_id)
	{
		$condGroup = false;
		if(!$this->isSuperAdmin())
		{
			$condGroup = null;
			if(is_object($this->_user) && is_object($this->_user->getParent()))
			{
				$condGroup = $this->_user->getParent();
			}
		}

		$e_req = $this->_em->getRepository('TotemRequestBundle:Request')->findOneByUserGroup($_id, $condGroup);
		if(!$e_req)
		{
			$this->_error->sendError($this->_transXeon->get('mod_request_error.title'),
				$this->_transXeon->get('mod_request_error.request_no_acces'));
		}
		return $e_req;
	}

	public function findItem($search)
	{
		$status = array(0 => 'i-page-delete', 1 => 'i-page');
		$datas = array();
		$group = false;
		if(!$this->isSuperAdmin())
		{
			$group = null;
			$user = $this->_security->getToken()->getUser();

			if($user instanceof UserEntity)
			{
				$group = $user->getParent();
			}
		}

		$reqs = $this->_em->getRepository('TotemRequestBundle:Request')->findByCategoryWithGroup(false, $group, $search);

		foreach($reqs as $req)
		{
			$datas[] = array(
				'v' => 'request.req.'.$req->getId(),
				'd' => $req->getTitle(),
				'i' => $status[$req->getActif()]);
		}

		return $datas;
	}
}