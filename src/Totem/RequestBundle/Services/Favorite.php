<?php
namespace Totem\RequestBundle\Services;

use GM\XeonBundle\Services\Service;

use Totem\RequestBundle\Entity\Request as RequestEntity;
use Totem\RequestBundle\Entity\Favorite as FavoriteEntity;
use Totem\RequestBundle\Entity\FavoriteValue as FavoriteValueEntity;

use Doctrine\Common\Util\Debug as Debug;

class Favorite extends Service
{
	
	public function getType()
	{
		$type = array('fav' => array(
 		        'name' => 'Favorite',
		 		'tree' => 1,
		 		'itemIcon' => 'i-page'
		 	)
		);
		return $type;
	}

	public function makeList($_item, $_parent)
	{
		$datas = array();
		$conds = array();
		if(!$this->isSuperAdmin())
		{
			$user = null;
			if(is_object($this->_user))
			{
				$user = $this->_user;
			}
			$conds['user'] = $user;
		}
		$conds['request'] = $this->_em->getRepository('TotemRequestBundle:Request')->find($_parent->id);
		if(!$conds['request'])
		{
			$this->_error->sendError($this->_transXeon->get('mod_request_error.title'), $this->_transXeon->get('mod_request_error.request_no_exist'));
		}
		$favs = $this->_em->getRepository('TotemRequestBundle:Favorite')->findBy($conds);
		foreach($favs as $fav)
		{
			$datas[] = array('v' => $fav->getId(), 'd' => $fav->getTitle());
		}
		return $datas;
	}

	public function saveData($_item, $_data, $_parent)
	{
		$req = $this->_em->getRepository('TotemRequestBundle:Request')->find($_parent->id);
		if(!$req)
		{
			$this->_error->sendError($this->_transXeon->get('mod_request_error.title'), $this->_transXeon->get('mod_request_error.request_no_exist'));
		}
		$fav = new FavoriteEntity();
		if(is_object($this->_user))
		{
			$fav->setUser($this->_user);
		}
		$fav->setRequest($req);

		$title = $_data['title'];
		$title_2 = '';
		foreach($_data['values'] as $def => $value)
		{
			$e_def = $this->_em->getRepository('TotemRequestBundle:Definition')->find($def);
			if($e_def)
			{
				$val = new FavoriteValueEntity();
				$val->setFavorite($fav)
				->setDefinition($e_def)
				->setValue($value);
				$this->_em->persist($val);
				$title_2 .='['.$e_def->getTitle().']:'.$value;
			}
		}
		if(empty($title))
		{
			$title = $title_2;
		}
		$fav->setTitle($title);
		$this->_em->persist($fav);
		$this->_em->flush();
	}

    public function saveFavorite($_data, $request)
    {
        if(!$request)
        {
            $this->_error->sendError($this->_transXeon->get('mod_request_error.title'), $this->_transXeon->get('mod_request_error.request_no_exist'));
        }
        $fav = new FavoriteEntity();
        if(is_object($this->_user))
        {
            $fav->setUser($this->_user);
        }
        $fav->setRequest($request);

        $title = $_data['title'];
        $title_2 = '';
        foreach($_data['values'] as $def => $value)
        {
            $e_def = $this->_em->getRepository('TotemRequestBundle:Definition')->find($def);
            if($e_def)
            {
                $val = new FavoriteValueEntity();
                $val->setFavorite($fav)
                    ->setDefinition($e_def)
                    ->setValue($value);
                $this->_em->persist($val);
                $title_2 .='['.$e_def->getTitle().']:'.$value;
            }
        }
        if(empty($title))
        {
            $title = $title_2;
        }
        $fav->setTitle($title);
        $this->_em->persist($fav);
        $this->_em->flush();
    }



	public function deleteItem($_item)
	{
		$fav = $this->_em->getRepository('TotemRequestBundle:Favorite')->find($_item->id);
		if(!$fav)
		{
			$this->_error->sendError($this->_transXeon->get('mod_request_error.title'), $this->_transXeon->get('mod_request_error.favori_no_exist'));
		}
		$this->_em->remove($fav);

		$favVals = $this->_em->getRepository('TotemRequestBundle:FavoriteValue')->findByFavorite($fav);
		foreach($favVals as $favVal)
		{
			$this->_em->remove($favVal);
		}

		$this->_em->flush();
	}

	public function getValues($_favId)
	{
		$datas = array();
		$fav = $this->_em->getRepository('TotemRequestBundle:Favorite')->find($_favId);
		if(!$fav)
		{
			return false;
		}
		$favVals = $this->_em->getRepository('TotemRequestBundle:FavoriteValue')->findByFavorite($fav);
		foreach($favVals as $favVal)
		{
			$datas[$favVal->getDefinition()->getId()] = $favVal->getValue();
		}
		return $datas;
	}


    public function deleteFavorite($favorite)
    {
        $fav = $this->_em->getRepository('TotemRequestBundle:Favorite')->find($favorite);
        if(!$fav)
        {
            $this->_error->sendError($this->_transXeon->get('mod_request_error.title'), $this->_transXeon->get('mod_request_error.favori_no_exist'));
        }
        $this->_em->remove($fav);

        $favVals = $this->_em->getRepository('TotemRequestBundle:FavoriteValue')->findByFavorite($fav);
        foreach($favVals as $favVal)
        {
            $this->_em->remove($favVal);
        }

        $this->_em->flush();
    }
}