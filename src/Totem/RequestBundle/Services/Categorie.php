<?php
namespace Totem\RequestBundle\Services;

use GM\XeonBundle\Services\Service;
use Totem\RequestBundle\Entity\Category as CategoryEntity;

class Categorie extends Service
{
	public function getType()
	{
		$type = array(
        'cat' => array(
		'name' => 'categorie',
		'tree' => 1,
		'itemIcon' => 'i-folder',
        'addText' => $this->_transXeon->get('mod_request.add_cat')
        ));

		if ($this->checkRight('request_manager_categorie', false))
		{
			$type['cat']['addRight'] = 5;
			$type['cat']['deleteRight'] = 6;
		}
		return $type;
	}
	public function saveData($_item, $_data)
	{
		$rep = $this->_em->getRepository('TotemRequestBundle:Category');
		if ($_item->add)
		{
			$cate = new CategoryEntity();
		}
		else
		{
			$cate = $rep->find($_item->id);
		}
		if (!empty($_data['parent']))
		{
			$cate->setParent($rep->find($_data['parent']));
		}
		$cate->setTitle($_data['title']);
		$this->_em->persist($cate);
		$this->_em->flush();
	}
	public function makeForm($_item)
	{
		$parent = 0;
		$parent_title = '';
		$title = '';
		if($_item->add)
		{
			$folder = array(
	            'parent' => 0,
	            'title' => '',
	            'parent_title' => $this->_transXeon->get('root')
            );
		}
		else
		{
			$cate = $this->_em->getRepository('TotemRequestBundle:Category')->find($_item->id);
			if($cate->getParent() !== null)
			{
				$parent = $cate->getParent()->getId();
				$parent_title = $cate->getParent()->getTitle();
			}
			$folder = array(
	            'parent' => $parent,
	            'title' => $cate->getTitle(),
	            'parent_title' => $parent_title
            );
            $title = $cate->getTitle();
		}
		return array('db' => $folder,
			'title'=>$title,
			'itype' => $this->_command->makeId($_item->type)
			);
	}
	/*
	faire la liste pour les champs listbox
	*/
	public function makeList()
	{
		$em = $this->_em;
		$rep = $em->getRepository('TotemRequestBundle:Category');
		$data = array();
		$cates = $rep->findAll();
		foreach($cates as $cate)
		{
			$data[] = array('v' => $cate->getId(), 'd' => $cate->getTitle());
		}
		return $data;
	}
}