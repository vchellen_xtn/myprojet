<?php
namespace Totem\RequestBundle\Services;

use GM\XeonBundle\Services\Service;
use Totem\RequestBundle\Entity\Request as RequestEntity;
use Totem\RequestBundle\Entity\Log as LogEntity;

class Log extends Service
{
	public function getType()
	{
		return array(
        'log' => array(
			'name' => 'Log'
		));
	}

	public function save(RequestEntity $_req, $_variables = array(), $_sql = '', $environment = null)
	{
		$reqDef = $this->_em->getRepository('TotemRequestBundle:Definition');
		$log = new LogEntity();
		$log->setRequest($_req);
		$vars = array();
		foreach ($_variables as $defId => $value) {
			$def = $reqDef->find($defId);
			$vars[] = $def->getTitle().'['.$def->getType().']:'.$value;
		}
		$log->setRequestResume($_req->getTitle());
		$log->setSql($_sql);
		$log->setVariables(join("\n", $vars));
		$log->setEmail('test@goldenmarket.fr');
		$log->setDate(new \DateTime('NOW'));
		$user = $this->_user;
		if (is_object($user))
		{
			$log->setUser($user);
		}
		$log->setEnvironment($_req->getEnvironment());
		if($this->isSuperAdmin() && !empty($environment))
		{
			$log->setEnvironment($this->_em->getRepository('TotemRequestBundle:Environment')->find($environment));
		}
		$this->_em->persist($log);
		$this->_em->flush();
		return $log;
	}

	public function makeRoot(){
		return array();
	}
	public function makeTree(){}

	public function makeGrid($_item,$_parent)
	{
		$datas = array();
		$reqLog = $this->_em->getRepository('TotemRequestBundle:Log');
		$conds = array();
		$user = $this->_security->getToken()->getUser();
		$conds['user'] = $user;
		if (!is_object($user))
		{
			$conds['user'] = null;
		}
		
        if($this->isSuperAdmin())
        {
			unset($conds['user']);
		}
		
		$logs = $reqLog->findBy($conds, array('date' => 'DESC'));
		foreach ($logs as $log) {
			$onelog = array();
			$onelog['id'] = $log->getId();
			$onelog['id_request'] = $log->getRequest()->getId();
			$onelog['date'] = $log->getDate()->format('d-m-Y H:i:s');
			$onelog['user'] = '';
			if ($log->getUser() != null)
			{
				$onelog['user'] = $log->getUser()->getFirstname().' '.$log->getUser()->getLastname();
			}
			$onelog['request_title'] = $log->getRequestResume();
			$onelog['variable'] = $log->getVariables();
			$onelog['sql_final'] = $log->getSql();
			$onelog['environment'] = '';
			if($log->getEnvironment() != null)
			{
				$onelog['environment'] = $log->getEnvironment()->getTitle();
			}
			$datas[] = $onelog;
		}
		return array('data' => $datas);
	}
}