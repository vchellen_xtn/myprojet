<?php
namespace Totem\RequestBundle\Services;

use GM\XeonBundle\Services\Service;
use Totem\RequestBundle\Entity\Colonne as ColonneEntity;
use Totem\RequestBundle\Entity\Request as RequestEntity;

class Colonne extends Service
{
	public function getType()
	{
		return array(
        'col' => array(
        	'itemIcon'=> 'i-page',
			'name' => 'Colonne',
			'addRight' => 1,
			'addText' => $this->_transXeon->get('mod_request_colonne.add'),
			'deleteRight' => 1
		));
	}
	public function makeForm($_item, $_parent)
	{
		return array('parent' => $this->_command->makeId($_parent->type, $_parent->id));
	}

	public function saveData($_item, $_data)
	{
		if($_item->id == 'saveOrderColonne' && !empty($_data['id_request']))
		{
			$this->_session->set('orderColumn_' . $_data['id_request'], $_data['columns']);
			return;
		}
		// Pour ajouter un nom sql d'une colonne avec grid
		if($_item->add)
		{
			if(!empty($_data['name_sql']))
			{
				$repCol = $this->_em->getRepository('TotemRequestBundle:Colonne');
				// parent est un champ caché generé lors dans la creation
				$idReq = $this->_command->getId($_data['parent']);
				$req = $this->_em->getRepository('TotemRequestBundle:Request')->find($idReq->id);
				$colExist = $repCol->findOneByTitle(trim($_data['name_sql']));
				if($colExist)
				{
					$this->_error->sendError($this->_transXeon->get('mod_request.validation'), $this->_transXeon->get('mod_request_colonne.text_exist'));
				}
				else
				{
					$maxCol = $repCol->findOneByRequest($req, array('rang'=> 'DESC'));
					$col = new ColonneEntity();
					$col->setTitle($_data['name_sql']);
					$col->setUniqueTitle($_data['name_sql']);
					$col->setRang($maxCol->getRang() + 1);
					$col->setRequest($req);
					$this->_em->persist($col);
					$this->_em->flush();
				}
			}
		}
		else
		{
			// ca sert a mapper les champs de la grid de l'admin avec les setters de lentity Colonne
			$mapping = array('format_numeric' => 'FormatNumeric',
				'format_separator' => 'FormatSeparator',
				'format_date' => 'FormatDate');
			$repCol = $this->_em->getRepository('TotemRequestBundle:Colonne');
			$col = $repCol->find($_item->id);
			foreach ($_data as $colName => $value)
			{
				$var = ucfirst(strtolower($colName));
				if(isset($mapping[$colName]))
				{
					$var = $mapping[$colName];
				}
				$col->{'set'.$var}($value);
			}
			$this->_em->persist($col);
			$this->_em->flush();
		}
	}
	
	public function makeList($_item)
	{
		
	}

	public function makeGrid($_item,$_parent, $search, $start, $limit)
	{
		$cadrages = array(0 => $this->_transXeon->get('mod_request_colonne.cadrage_left'),
			1 => $this->_transXeon->get('mod_request_colonne.cadrage_center'),
			2 => $this->_transXeon->get('mod_request_colonne.cadrage_right'),
			'' => ''
			);
		$repReq = $this->_em->getRepository('TotemRequestBundle:Request');
		$req = $repReq->find($_parent->id);
		$repCol = $this->_em->getRepository('TotemRequestBundle:Colonne');
		
		$cols = $repCol->findBy(array('request' => $req), array('rang'=>'ASC'));
		$total = sizeof($cols);
		$cols = array_slice($cols, $start, $limit);
		$datas = array();
		foreach ($cols as $col) {
			$data = $this->_data->convertAll($col);
			$data['cadrage'] = $cadrages[$data['cadrage']];
			$datas[] = $data;
		}
		return array('data' => $datas, 'total' => $total);
	}
	
	/*
	drag an drop pour le index des lignes des colonnes
	*/
	public function moveItem($_item, $_target, $_rank)
	{
		$repCol = $this->_em->getRepository('TotemRequestBundle:Colonne');
		$colAct = $repCol->find($_item->id);
		$oldRang = $colAct->getRang();
		$cols = $repCol->findBy(array('request' => $colAct->getRequest()), array('rang'=>'ASC'));
		// les index de la grid part 1 -> X
		// le rang va de 0 -> X-1
		// si on remonte on fait -1.5 - lordre des lignes [0.1.2.3] - 3 va aller entre 0 et 1 - mais rank = 2 donc on lui tranche -1.5 pour avoir 0.5 et se retrouver entre 0-1
		// la meme logique quand on redescend, on fait -0.5.
		// et reaffecte par rapport au $mapping
		$mapping = array();
		foreach ($cols as $col) {
			$rang = $col->getRang();
			if ($col->getId() == $colAct->getId())
			{
				$rang = $_rank - 1.5;
				if ($oldRang < $_rank)
				{
					$rang = $_rank - 0.5;	
				}
			}
			$mapping[strval($rang)] = $col;
		}
		ksort($mapping, SORT_NUMERIC);
		$i = 0;
		foreach($mapping as $k => $col)
		{
			$col->setRang($i++);
			$this->_em->persist($col);
		}
		$this->_em->flush();
	}
	
	public function deleteItem($_item)
	{
		$repCol = $this->_em->getRepository('TotemRequestBundle:Colonne');
		$colAct = $repCol->find($_item->id);
		$cols = $repCol->findBy(array('request' => $colAct->getRequest()), array('rang'=>'ASC'));
		$i = 0;
		foreach ($cols as $col) {
			if($col->getId() == $colAct->getId())
			{
				$this->_em->remove($col);
				continue;
			}
			$col->setRang($i++);
			$this->_em->persist($col);
		}
		$this->_em->flush();
	}
	/*
	on cree tous les colonnes à partir d'une requete
	*/
	public function createByRequest(RequestEntity $_req)
	{
		$repCol = $this->_em->getRepository('TotemRequestBundle:Colonne');
		$colExists = $repCol->findBy(array('request' => $_req));
		// on ne recree pas si on a au moins une colonne
		if(sizeof($colExists) > 0)
		{
			return;
		}

		$query = $this->_sc->get('totem_request.service.sql')->execSQL($_req);
		$totColonne = $query->columnCount();
		// comme on recree les col je recupere tous les cols existant ou pas
		// et on tag les existants
		$cols = array();
		$dontDels = array();
		for($i=0;$i<$totColonne;$i++)
		{
			$tmpcol = $query->getColumnMeta($i);
			$col = $this->get($tmpcol['name'], $_req, true);
			if($col)
			{
				$cols[] = $col;
				if($col->getId() != null)
				{
					$dontDels[$col->getId()] = $col->getId();
				}
			}
		}
		// remove tous les col qui ne sont pas taggué
		/*$colDeletes = $repCol->findBy(array('request' => $_req));
		foreach($colDeletes as $cd)
		{
			if(!in_array($cd->getId(), $dontDels))
			{
				$this->_em->remove($cd);
			}
		}*/
		// insert les (ancien+nouveau) reinitialisant le rang
		foreach($cols as $i => $col)
		{
			$col->setRang($i);
			$this->_em->persist($col);
		}
		if(sizeof($cols))
		{
			$this->_em->flush();
		}
	}

	public function getByRequest(RequestEntity $_req)
	{
		$cadrages = array(0 => 'left', 1 => 'center', 2 => 'right', '' => 'left'); 
		$datas = array();
		$cols = $this->getColumnsFromSession($_req->getId());
		if(!$cols)
		{
			$repCol = $this->_em->getRepository('TotemRequestBundle:Colonne');
			$cols = $repCol->findByRequest($_req, array('rang' => 'ASC'));
		}
		foreach($cols as $col)
		{
			$data = array('text' => $col->getTitle(),'header' => $col->getTitle(), 'dataIndex' => $col->getUniqueTitle());
			$width = $col->getWidth();
			$data['align'] = $cadrages[$col->getCadrage()];
			if(empty($width))
			{
				$width = 60;
			}

			$data['width'] = $width;
			$datas[] = $data;
		}
		return $datas;
	}

	/*
	on essai de cherche une colonne sinon on le cree
	*/
	public function get($_titre, RequestEntity $_req, $_create = false)
	{
		$repCol = $this->_em->getRepository('TotemRequestBundle:Colonne');
		$col = $repCol->findOneBy(array('unique_title' => $this->formatUnique($_titre), 'request' => $_req));
		if(!$col && $_create)
		{
			$col = new ColonneEntity();
			$col->setRequest($_req);
			$col->setTitle($_titre);
			$col->setUniqueTitle($this->formatUnique($_titre));
		}

		return $col;
	}

	public function formatUnique($unique)
	{
	    $unique = strtolower(strtr($unique, utf8_decode('ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ()[]\'"~$&%*@ç!?;,:/\^¨€{}<>|+.- '),  'aaaaaaaaaaaaooooooooooooeeeeeeeecciiiiiiiiuuuuuuuuynn____________c_________e_________'));
	    $unique = preg_replace('/(-| )*/', '', $unique);
	    $unique = trim($unique);
	    return $unique;
	}

	/*
	on retourne le tableau 
	*/
	private function getColumnsFromSession($_id)
	{
		if($this->_session->has('orderColumn_' . $_id) && $this->_session->get('orderColumn_' . $_id) !== null)
		{
			$cols = array();
			$colSessions = $this->_session->get('orderColumn_' . $_id);
			foreach($colSessions as $cs)
			{
				$cols[] = $this->_em->getRepository('TotemRequestBundle:Colonne')->findOneByTitle($cs);
			}
			return $cols;
		}
		else
		{
			return false;
		}
		
	}
}