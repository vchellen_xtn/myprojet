<?php
namespace Totem\RequestBundle\Services;

use GM\XeonBundle\Services\Service;
use Totem\RequestBundle\Entity\Request as RequestEntity;
use Totem\RequestBundle\Entity\Keyword as KeywordEntity;
use Totem\RequestBundle\Entity\Colonne as ColonneEntity;
use Totem\RequestBundle\Entity\Definition as DefinitionEntity;
use Symfony\Component\DomCrawler\Crawler;
use Doctrine\Common\Util\Debug;

class Import extends Service
{
	public function getType()
	{
		return $type = 
		array('import' => array(
 		        'name' => 'import'
		 	)
		);
	}
	public function makeTree()
	{
		return array();
	}
	public function makeRoot()
	{
		return array();
	}

	public function saveData($data)
	{
		$error = true;
		if(isset($_FILES['media']))
		{
			$file = $_FILES['media'];
			$path = pathinfo($file['name']);
			
			if($path['extension'] == 'xml')
			{
				$error = false;
				$handle = fopen($file['tmp_name'], 'r');
				$content = stream_get_contents($handle);

				$crawler = new Crawler($content);
				//$crawler->addXMLContent($content);
				
				$request = new RequestEntity();
				$reqs = $crawler->filter('row[name="req"] > column');
				foreach($reqs as $el)
				{
					$fields = $el->getAttribute('name');
					$action = 'set'.ucfirst($fields);
					$request->$action($el->nodeValue);
					
				}
				$title = $request->getTitle();
				$request->setTitle('[IMPORT]'.$title);
				$request->setActif(0);
				$this->_em->persist($request);
				
				$this->setEntityFromXML($crawler, 'col', new ColonneEntity(), $request);
				$this->setEntityFromXML($crawler, 'def', new DefinitionEntity(), $request);

				$this->_em->flush();

				$key = $crawler->filter('row[name="key"]');
				foreach($key as $el)
				{
					$keyword = $el->childNodes->item(0)->nodeValue;
					$sKey = $this->_sc->get('totem_request.service.keyword');
					$sKey->saveByRequest($keyword, $request);
				}
			}
		}
		
		if($error)
		 	return array('error' => array(
		 		'title' => $this->_transXeon->get('mod_request_error.title'),
		 		'text' => $this->_transXeon->get('mod_request_error.import_no_file')));
	}

	private function setEntityFromXML($crawler, $row, $entity, $request)
	{
		$res = $crawler->filter('row[name="'.$row.'"]');
		foreach($res as $el)
		{
			$vals = $el->childNodes;
			$e = clone $entity;
			foreach($vals as $val)
			{
				$field = $val->getAttribute('name');
				$action = 'set'.ucfirst($field);
				$e->$action($val->nodeValue);
			}
			$e->setRequest($request);
			$this->_em->persist($e);
		}
	}
}
