<?php
namespace Totem\RequestBundle\Services;

use GM\XeonBundle\Services\Service;
use Totem\RequestBundle\Entity\Environment as EnvironmentEntity;

class Environment extends Service
{
	public function getType()
	{
		return array('env' => array(
								'name' => 'environment',
								'tree' => 2,
								'itemIcon' => 'i-server',
								'addText' => $this->_transXeon->get('mod_request.add_env'),
								'addRight' => 8,
								'copyRight' => 8,
								'deleteRight' => 10,
								'exportRight' => true
							)
				);
	}


	/**
	 * Get list of all environments
	 * @return Collection      Collection Objet of entity
	 */
	public function getList()
	{
		$rep = $this->_em->getRepository('TotemRequestBundle:Environment');
		$r = $rep->findAll();

        return $r;
	}

	/**
	 * Get Object of an environment by id
	 * @param  integer $id id of an Environment
	 * @return object      Objet of entity
	 */
	public function getById( $id = 0 )
	{
		$id = intval($id);

		if( $id != 0 ) {
			$rep = $this->_em->getRepository('TotemRequestBundle:Environment');
			$r = $rep->find( $id );
		}
		else {
			$r = $this->getEntityFields();
		}

		return $r;
    }

	/**
	 * Add an environment
	 * @param  array $_data 	datas of environment to add
	 * @return object      		Objet of entity add
	 */
	public function add( $_data )
	{
		//$rep = $this->em->getRepository('TotemRequestBundle:Environment');
		$rep = new EnvironmentEntity();

		//$rep->setTitle($_data['title']);
		//$rep->setText($_data['text']);

		foreach( $_data as $field => $v )
		{
		    $rep->{'set'.ucfirst($field)}($v);
		}

		$this->_em->persist($rep);
		$this->_em->flush();

		//return $this->_data->getLastEntity()->getId();
		return $rep->getId();
	}

	/**
	 * Edit an environment
	 * @param  integer 	$_id 	id of environment to edit
	 * @param  array 	$_data 	new datas of this environment
	 * @return object      		Objet of entity edit
	 */
	public function edit( $id, $_data )
	{
        $rep = $this->_em->getRepository('TotemRequestBundle:Environment');
        $r = $rep->find( $id );

            $this->_data
                 ->save(
                    $r,
                    $_data
                    )
                 ->flush();

		//return $this->_data;
		return $r;
	}

	/**
	 * Get all fiels of an entity
	 * @return object 	Objet of entity empty
	 */
	public function getEntityFields( $entity = FALSE )
	{
		$db = new EnvironmentEntity();

		/*$db = array_map(function($value) {
						return $value === NULL ? '' : $value;
					}, $db);*/

		/*$db = array_map(function($v) {
						return (is_null($v)) ? '' : $v;
					},$db);*/

		/*foreach( $db as $field => $v )
		{
			if( is_null($v) ) {
				$db->$field = '';
			}
		}*/

		return $db;
	}


	/***********************************
	*	XEON
	***********************************/
    function makeRoot($item) {
        return array();
    }

    function makeTree($item)
    {
        $data = array();

        //$list = $this->_sEnvironment->getList();
        $list = $this->getList();
        $type = $this->getType();

        foreach( $list as $k => $v )
        {
            $data[] = array(
                            'item' => $this->_command->makeId('env', $v->getId()),
                            'text' => $v->getTitle(),
                            //'iconCls' => $this->_sEnvironmentType['env']['itemIcon']
                            'iconCls' => $type['env']['itemIcon']
                        );
        }

        return $data;
    }

	function makeForm($item)
    {
        if ($item->add)
        {
            $r = $this->getEntityFields();

            return array('db' =>  $this->_data->convertAll($r));
        }
        else
        {
            $r = $this->getById( $item->id );
            $data = $this->_data->convertAll($r);
            $data['password'] = '******';
            return  array('title' => $r->getTitle(),
                          'db' => $data
                        );
        }
    }

    // Grille des caractéristiques produit
    function makeGrid($item, $parent)
    {
        $data = array();

        $list = $this->getList();

        foreach( $list as $env )
        {
            $data[] = $this->_data->convertAll($env);
        }

        return array(
            'data' => $data,
            'total' => count($data)
        );
    }

    /**
     * Save an Env
     *
     * @return  array
     */
    function saveData($item, $data)
    {
        if( $item->add )
        {
            $id = $this->add( $data );

            return array('item' => $this->_command->makeId($item->type, $id));
        }
        else
        {
        	if($data['password'] == '******')
        	{
        		unset($data['password']);
        	}
            $r = $this->edit( $item->id, $data );

            return array('title' => $r->getTitle());
        }
    }

    public function makeList()
    {
    	$envs = $this->_em->getRepository('TotemRequestBundle:Environment')->findAll();
    	$datas = array();
		foreach($envs as $env)
		{
			$datas[] = array('v' => $env->getId(), 'd' => $env->getTitle());
		}
		return $datas;
    }
}