<?php

namespace Totem\RequestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('TotemRequestBundle:Default:index.html.twig', array('name' => $name));
    }
}
